;VERSION: 1FE003_SDXZ_01D; $Date: 2017-09-08 14:09:18 +0200 (Fr, 08 Sep 2017) $, $Revision: 24866 $

PROC GM_MASCH_PARAM

 def string[32] _prg_nam="GM_MASCH_PARAM"
; Maschinen mit Kettenmagazin Werkzeugsystem HSK 100
; Maschinenabhaengige Parameter CTX Gamma 1250 TC 2nd (085)
; Maschinenabhaengige Parameter CTX Gamma 2000 TC 2nd (086)
; Maschinenabhaengige Parameter CTX Gamma 3000 TC 2nd (087)
; ---------- V8 GA 0742 09.11.2017 Masch. Nr.00860000531-------------
; Daten werden in diesem Programm GM_MASCH_PARAM gesetzt
; Dieses Programm wird beim Referenzfahren aufgerufen
; Daten werden abgelegt in GUD3 Feld GM_PARAM[101,3]

;----------- POSITIONEN FUER WERKZEUGWECHSEL ------------------------
;----------- WERTE AUS MKS, MASCHINEN-KOORDINATEN-SYSTEM-------------
 GM_PARAM[0,1]=100             ;Werkzeugsystem
                               ;CompactMaster, 100=HSK100, 108=Capto_C8
 GM_PARAM[1,1]=1243.347/RG778  ;Werkzeugwechselposition X1 im Durchmesser
 GM_PARAM[2,1]=-69.835/RG778   ;Werkzeugwechselposition Y1
 GM_PARAM[3,1]=177.190/RG778   ;Werkzeugwechselposition Z1
 GM_PARAM[4,1]=0.00            ;Werkzeugwechselposition B1
 GM_PARAM[5,1]= 98.00          ;Werkzeugwechselposition C1 HSK-Aufnahme
 GM_PARAM[3,0]=0/RG778         ;Z-Position der Werkzeugklappe

 GM_PARAM[1,2]=0/RG778     ;Weg zum Freifahren vom Wechselarm
 GM_PARAM[2,2]=0.00        ;Winkel des Aushubweges zur positiven X-Achse
 GM_PARAM[3,2]=0/RG778     ;Freifahr-Komponente in Z-Richtung
 GM_PARAM[4,2]=  2         ;Winkel Q3-Achse Werkzeugspanner loesen Start
 GM_PARAM[5,2]=280         ;Winkel Q3-Achse Werkzeugspanner spannen Start

;----------- MASSE FUER TOOL-CARRIER --------------------------------
 GM_PARAM[6,0]=0/RG778          ;Y-Komponente Kopf-Versatz
 GM_PARAM[6,1]=-0.002/RG778     ;X-Komponente Kopf-Versatz
 GM_PARAM[6,2]=260.045/RG778    ;Z-Komponente (halber Durchmesser Bi-Achse)
 GM_PARAM[7,0]=0/RG778          ;Y-Komponente Kopf-Versatz
 GM_PARAM[7,1]=-0.002/RG778     ;X-Komponente Kopf-Versatz
 GM_PARAM[7,2]=260.045/RG778    ;Z-Komponente (halber Durchmesser Bi-Achse)
 GM_PARAM[8,0]=367.925          ;Y-Komponente Vektor
 GM_PARAM[8,1]=-0.002           ;X-Komponente Vektor
 GM_PARAM[8,2]=-0.004           ;Z-Komponente Vektor

;----------- PROZENTSATZ FUER EILGANGOVERRIDE -----------------------
 GM_PARAM[14,0]= 2           ;ZUSAMMENSPIEL TASTE/PROGRAMM
               ; 0 NUR L717 SETZT DATEN
               ; 1 L717 UND TASTE SETZEN DATEN
               ; 2 NUR TASTE SETZT DATEN
 GM_PARAM[14,1]=10           ;EILGANGOVERRIDE FUER ACHSEN
 GM_PARAM[14,2]=20           ;EILGANGOVERRIDE FUER SPINDELN

;----------- KRAFT BEI FAHREN AUF FESTANSCHLAG ----------------------
 GM_PARAM[15,1]=10          ;FXST[Z3] fuer L730
 GM_PARAM[15,2]=10          ;FXST[Z3] fuer L733

;----------- KRAFT BEI ABSTECHKONTROLLE -----------------------------
 GM_PARAM[16,1]=10          ;FXST[Z3] fuer L732

;----------- WINKELFEHLERAUSGLEICH ZWISCHEN SCHLITTEN 1+2 -----------
 GM_PARAM[18,1]=210.00      ;an Spindel 4
 GM_PARAM[18,2]=210.00      ;an Spindel 3

;------------------------ MASSE FUER LASER-WERKZEUGMESSEN TYP10/14 --
 GM_PARAM[21,0]=570.028/RG778       ;LASERPOSITION X
 GM_PARAM[22,0]=136.148/RG778       ;LASERPOSITION Y
 GM_PARAM[23,0]=  8.449/RG778       ;LASERPOSITION Z

;---------------- MASSE FUER WUERFEL-MITTE-WERKZEUGMESSEN TYP10/14 --
 GM_PARAM[21,1]=570.028/RG778       ;TASTERPOSITION X
 GM_PARAM[22,1]=185.122/RG778       ;TASTERPOSITION Y
 GM_PARAM[23,1]= 39.223/RG778       ;TASTERPOSITION Z

;----------- Y-MASSE FUER WERZEUG-MESS ARM --------------------------
 GM_PARAM[21,2]=-80.0/RG778         ;TASTERPOSITION Y SCHLITTEN 1
 GM_PARAM[22,2]=  0.0/RG778         ;TASTERPOSITION Y SCHLITTEN 2

; - - - - -  Kettenmagazin gamma 2000 - - - - - - - - - - - - - - - -
; - - - - -  Spindel 4: ISM 76/102 und Spindel 3: ISM 76/102  - - - -
; - - - - -  Schlitten 2 mit Luenette - - - - - - - - - - - - - - - -
; - - - - -  Teleskopblech zwischen Z2 und Z3 7-lagig - - - - - - - -
 GM_PARAM[25,0]= 230/RG778   ;min. Abstand der Achsen Z2 und Z3
 GM_PARAM[25,1]=   0/RG778   ;reserviert; Verhaubung 7
 GM_PARAM[25,2]=   0/RG778   ;reserviert; Verhaubung 7
 GM_PARAM[27,0]= -50/RG778   ;max. Grenzwert der Y1-Achse an SP4, Verhaubung 1
 GM_PARAM[27,1]= 225/RG778   ;min. Grenzwert der Z1-Achse an SP4, Verhaubung 1
 GM_PARAM[27,2]=   0/RG778   ;reserviert; Verhaubung 4
 GM_PARAM[28,0]=   0/RG778   ;reserviert; Verhaubung 3
 GM_PARAM[28,1]= 435/RG778   ;min. Grenzwert der X1-Achse an SP3, Verhaubung 3
 GM_PARAM[28,2]= 235/RG778   ;min. Abstand der Achsen Z1 und Z3 an SP3, Verhaubung 3
 GM_PARAM[29,0]= 583/RG778   ;min. Grenzwert der X1-Achse an SP3, Verhaubung 4
 GM_PARAM[29,1]=  52/RG778   ;min. Abstand der Achsen Z1 und Z3 an SP3, Verhaubung 4
 GM_PARAM[29,2]= 460/RG778   ;Schluesselweite der Saeule fuer die Y-Achse
 GM_PARAM[59,1]=   0/RG778   ;reserviert; Flugkreis 1 der B1-Achse
 GM_PARAM[59,2]=   0/RG778   ;reserviert; max. Grenzpos. in X-Richtung

; - - - - -  Kettenmagazin  - - - - - - - - - - - - - - - - - - - - -
; - - - - -  Spindel 4 mit ISM 127 und allem anderen  - - - - - - - -
; zusaetzliche Angaben, weil der Spindelkasten 100mm in -Z-Richtung geht
 GM_PARAM[3,0] = 100/RG778   ;Z-Position der Werkzeugklappe
 GM_PARAM[25,2]= 600/RG778   ;min. Grenzwert der Z2-Achse an SP4, Verhaubung 7
 GM_PARAM[27,1]= 225/RG778   ;min. Grenzwert der Z1-Achse an SP4, Verhaubung 1

;----------- POSITIONEN FUER WERKZEUGWECHSEL ZUSATZMAGAZIN 6 --------
;    Typ-Auswahl fuer spezielle Behandlung
; GM_PARAM[49,0]=  0 ; Standard Auswertung
; GM_PARAM[49,0]=  1 ; 1-4/D80x700, 5/D160x360, 6/D200x250/D165x300    ;HSK63,Capto C6
; GM_PARAM[49,0]=  2 ; 1-4/D100x700, 5/D160x280, 6/D200x250/D160x280   ;HSK100
; GM_PARAM[49,0]=  3 ; 1-3/D100x700, 4/-, 5/-, 6/-                     ;Beta 2000 TC ,HSK63,Capto C6
; GM_PARAM[49,0]=  8 ; 1-4/D80x700, 5/D160x280, 6/D200x250/D160x280    ;Capto C8
; GM_PARAM[49,0]= 99 ; 1-4/D100x700, 5/D80x500, 6/D80x500              ;HSK63,Capto C6,HSK100,Capto C8
; GM_PARAM[49,0]= 98 ; 1-4/-, 5/D250x400, 6/D250x400                   ;Sonderfall, nur 2 Plaetze
; GM_PARAM[49,0]= 97 ; 1-4/D80x700, 5/D80x500/D160x360, 6/D80x500/D160x300/D200x250 ;Sonderfall NOV
; GM_PARAM[49,0]= 2833284 ; 1-4/D80x700, 5/D80x500/D160x360, 6/D80x500/D160x300/D200x250    ;HSK63    ,01.07.2016
; GM_PARAM[49,0]= 3418191 ; 1-4/D80x700, 5/D80x500/D160x360, 6/D80x500/D160x300/D200x250    ;Capto C6 ,07.07.2016
; GM_PARAM[49,0]= 3497890 ; 1-4/D80x700, 5/D80x500/D160x450, 6/D80x500/D200x450    ;HSK63             ,16.05.2017
; GM_PARAM[49,0]= 3498204 ; 1-4/D80x700, 5/D80x500/D160x450, 6/D80x500/D200x450    ;Capto C6          ,12.05.2017
; GM_PARAM[49,0]= 3458689 ; 1-4/D100x700, 5/D80x500/D160x450, 6/D80x500/D200x450   ;HSK100            ,09.03.2017
; GM_PARAM[49,0]= 3419604 ; 1-4/D80x700, 5/D80x500/D160x450, 6/D80x500/D200x450    ;Capto C8          ,03.02.2017
; GM_PARAM[49,0]= 3500404 ; 1-2/D80x700, 3/D80x500/D160x300/D200x250               ;Sonderfall mit U-Achse, 17.03.2017
;
 GM_PARAM[49,0]= 3458689 ; 1-4/D100x700, 5/D100x500/D160x450, 6/D100x500/D200x450 ;HSK100            ,07.11.2017
;
;----------- WERTE AUS MKS, MASCHINEN-KOORDINATEN-SYSTEM ------------
;----------- WERTE FUER PLATZ 1,2,3,4,5,6 ---------------------------
 GM_PARAM[40,0]= 180.00       ;Werkzeugwechselposition B1
 GM_PARAM[40,1]= 000.00       ;Werkzeugwechselposition C1
 GM_PARAM[40,2]= 700.00/RG778 ;Maximale Werkzeuglaenge Zusatz-Magazin
;----------- WERTE FUER PLATZ 1 -------------------------------------
 GM_PARAM[41,0]= 517.880/RG778 ;Werkzeugwechselposition X1 im Radius
 GM_PARAM[41,1]=-209.050/RG778 ;Werkzeugwechselposition Y1
 GM_PARAM[41,2]=2216.420/RG778 ;Werkzeugwechselposition Z1 (1320/2120/3120)
;----------- WERTE FUER PLATZ 2 -------------------------------------
 GM_PARAM[42,0]= 518.060/RG778 ;Werkzeugwechselposition X1 im Radius
 GM_PARAM[42,1]= -69.600/RG778 ;Werkzeugwechselposition Y1
 GM_PARAM[42,2]=2194.230/RG778 ;Werkzeugwechselposition Z1 (1298/2098/3098)
;----------- WERTE FUER PLATZ 3 -------------------------------------
 GM_PARAM[43,0]= 518.260/RG778 ;Werkzeugwechselposition X1 im Radius
 GM_PARAM[43,1]=  70.150/RG778 ;Werkzeugwechselposition Y1
 GM_PARAM[43,2]=2172.320/RG778 ;Werkzeugwechselposition Z1 (1276/2076/3076)
;----------- WERTE FUER PLATZ 4 -------------------------------------
 GM_PARAM[44,0]= 518.450/RG778 ;Werkzeugwechselposition X1 im Radius
 GM_PARAM[44,1]=209.940 /RG778 ;Werkzeugwechselposition Y1
 GM_PARAM[44,2]=2150.570/RG778 ;Werkzeugwechselposition Z1 (1254/2054/3054)
;----------- WERTE FUER PLATZ 5 -------------------------------------
 GM_PARAM[45,0]= 622.610/RG778; Werkzeugwechselposition X1 im Radius
 GM_PARAM[45,1]=-170.130/RG778 ;Werkzeugwechselposition Y1
 GM_PARAM[45,2]=2194.650/RG778 ;Werkzeugwechselposition Z1 (1243.5/2043.5/3043.5)
;----------- WERTE FUER PLATZ 6 -------------------------------------
 GM_PARAM[46,0]= 612.550/RG778 ;Werkzeugwechselposition X1 im Radius
 GM_PARAM[46,1]= 176.340/RG778 ;Werkzeugwechselposition Y1
 GM_PARAM[46,2]=2150.300/RG778 ;Werkzeugwechselposition Z1 (1189.5/1989.5/2989.5)
;----------- ZUSAETZLICHE WERTE -------------------------------------
 GM_PARAM[50,0]=  55.00/RG778 ;Freifahr-Komponente in Z-Richtung
 GM_PARAM[50,1]=   0.00/RG778 ;frei
 GM_PARAM[50,2]=  32.00/RG778 ;Aushubweg Platz 1-4
;----------- WERTE FUER PLATZ 5 -------------------------------------
 GM_PARAM[55,0]= 200.00       ;Werkzeugwechselposition B1
;; GM_PARAM[55,1]=GM_PARAM[45,1]+169.75/RG778 ;Ein-/Ausfahrposition Y1 mit Werkzeug
 GM_PARAM[55,1]=GM_PARAM[45,1]+146.5/RG778 ;Ein-/Ausfahrposition Y1 mit Werkzeug
 GM_PARAM[55,2]=  65.00/RG778 ;Aushubweg Platz 5
;----------- WERTE FUER PLATZ 6 -------------------------------------
 GM_PARAM[56,0]= 200.00       ;Werkzeugwechselposition B1
;; GM_PARAM[56,1]=GM_PARAM[46,1]-169.75/RG778 ;Ein-/Ausfahrposition Y1 mit Werkzeug
 GM_PARAM[56,1]=GM_PARAM[46,1]-165.5/RG778 ;Ein-/Ausfahrposition Y1 mit Werkzeug
 GM_PARAM[56,2]=  65.00/RG778 ;Aushubweg Platz 6

;----------- ANPASSUNG DER REITSTOCKDATEN ---------------------------
; Reitstock-Typen: 3,5,30
 GM_PARAM[60,0]= 1200.0       ;F_max, Kraftgrenzen in dN
 GM_PARAM[60,1]=  250.0       ;F_min, Kraftgrenzen in dN
 GM_PARAM[60,2]=    0.4/RG778 ;Abhebeweg
 GM_PARAM[61,0]=    0.0275    ;Steigung der Momenten - Kraftkurve
 GM_PARAM[61,1]=    0.5       ;Versatz der Momenten - Kraftkurve
 GM_PARAM[61,2]= 1800 /RG778  ;Eilganggeschwingigkeit

;----------- MIT REITSTOCK B1-ACHSE SCHWENKEN MIT Y ZUR SEITE -------
 GM_PARAM[63,0]=0         ;ZUM SCHWENKEN DER B1-ACHSE Y-ACHSE ZUR SEITE
                          ;NEIN=0, JA=1
 GM_PARAM[63,1]=850/RG778 ;Z-KOORDINATE DES SCHWENKPUNKTES
 GM_PARAM[63,2]=0         ;FREI

;----------- B1-Achse:Geschwindigkeit u. Beschleunigung in Abhaengigkeit von WZ-Typ -------
 GM_PARAM[80,0]=731       ;Werkzeugtyp
 GM_PARAM[80,1]=30        ;maximale Achsgeschwindigkeit [1/min]  MD32000
 GM_PARAM[80,2]=1         ;maximale Achsbeschleunigung  [1/s^2]  MD32300[0]

;----------- SCHALTER FUER DIVERSE ANWENDUNGEN ----------------------
 GM_PARAM[92,1]=0        ;Dreh-Bearbeitung im TRAORI
;    0 - Alter Ablauf
;    1 - C1 Positionierung mit Tcarr2 und ohne Tcarr5
;    2 - Auswertung TOWMCS/TOWWCS bei Werkzeug-Korrekturen
 GM_PARAM[93,2]=0        ;Auch Gesperrte Werkzeuge mit BLUM Messen
;    1 - Gesperrt durch Stand-Zeit Ueberwachung
;    2 - Gesperrt durch Stueck-Zahl Ueberwachung
;    4 - Gesperrt durch D-Verschleiss Ueberwachung
;    8 - Gesperrt durch DL-Verschleiss Ueberwachung
;   16 - Gesperrt durch ohne Verschleiss Ueberwachung
 GM_PARAM[93,1]=0        ;Startsatzsuche fuer Reitstock/Luenette
;                        ;0=NEIN, 1=JA, 2=JA, mit Dialog
 GM_PARAM[93,0]=0        ;Maximale Werkzeuglaenge fuer BLUM Werkzeug-Messen
;                        ; 0 - maximale Werkzeuglaenge 400mm
;                        ; 1 - maximale Werkzeuglaenge 550mm
;                        ; 9 - maximale Werkzeuglaenge wie GM_PARAM[40,2]
 GM_PARAM[94,2]=0        ;Kontrolle Standard-Werkzeuggroesse im Zusatzmagazin
;                        ; 0 = Standard-Kontrolle
;                        ; 1 = Keine Kontrolle
 GM_PARAM[94,1]=0        ;Verfahrwege L710ff in Simulation korrekt
;                        ;0=NEIN, 1=JA
;GM_PARAM[94,0]=0        ;Spezielle Einstellungen fuer Werkzeugwechsel
 GM_PARAM[94,0]=0        ;alle Funktionen entsprechen dem Standard
 GM_PARAM[94,0]=(4+16)   ;empfohlene Einstellung fuer diese Funktionen
;    1 - Kuehlmittel aus/Blasluft ein wird geprueft            _no_h2O
;    2 - Wartezeit bei B-Achs-Klemmung entfaellt               _no_g04
;    4 - B-Achs-Klemmung entfaellt                             _no_Bbr
;    8 - Werkzeugluke vorab oeffnen                            _op_Luk
;   16 - Geschwindigkeit fuer Q3-Achse aus Maschinendatum      _fq3_md
;   32 - Werkzeugmasse auf Revolver fuer Gegenspindel positiv  _t2psp3
;   64 - Y-Verfahrweg fuer kleine Werkzeug weglassen           _noyhak
;  128 - Werkzeugwechsel ohne TOWSTD/TOWWCS                    _notow
;  256 - Werkzeugwechsel ohne CUTMOD/TCOABS                    _nocut
;  512 - Werkzeugwechsel keine Kontrolle/Anpassung R691        _noR691
; 1024 - Werkzeug freiliegend Spannen, Q3 hat geloest          _wzfrei
 GM_PARAM[95,2]=0        ;Schalter fuer CUST_TECHCYC
;    1 - Kein Futterleertest an der Gegenspindel
 GM_PARAM[95,1]=2        ;SCHALTER FUER C-ACHS-NULL IN BASISVERSCHIEBUNG
;                        ;0=NEIN, KEINE MANIPULATION
;                        ;1=JA, DIN- NULL SETZEN, ShopTurn-WINKEL AUS GM_PARAM
;                        ;2=JA, DIN- UND ShopTurn-WINKEL AUS GM_PARAM SETZEN
 GM_PARAM[95,0]=0        ;SCHALTER FUER VIRTUELLE MASCHINE
;                        ;0=REALE MASCHINE, 1=VIRTUELLE MASCHINE
 GM_PARAM[96,2]=0        ;SCHALTER BOHRERBRUCHKONTROLLE IM TC-ZYKLUS
;                        ;0=NEIN, 1=JA
 GM_PARAM[96,0]=0        ;SCHALTER STANDZEIT IN D1 SAMMELN
;                        ;0=NEIN, 1=JA, 2=Min-Max-Reset, 4=Werkzeugeinsatzliste
 GM_PARAM[97,2]=0        ;SCHALTER FUER SPINDELSTOP VOR WERKZEUGWECHSEL ShopTurn
;                        ;0=NEIN, 1=JA
 GM_PARAM[97,1]=0        ;SCHALTER FUER BRUCHUEBERWACHUNG
;                        ;0=KEINE, 1=ARTIS_CTM, 2=TOOLINSPECT, 3=TOOLMONITOR, 4=EASYTOOLMONITOR, 5=TOOLSCOPE, 6=ARTIS_GEM, 8=EASYTOOLMONITOR 2.0
 GM_PARAM[97,0]=0        ;SCHALTER KEINE BLASLUFT BEI WERKZEUGWECHSEL 
;                        ;0=BLASEN, 1=NICHT BLASEN IN TC UND L710/713
 GM_PARAM[98,0]=0        ;SCHALTER GESPERRTE WERKZEUGE AUSCHLEUSEN, 0=NEIN, 1=JA
                         ; R=16, Y=32, G=48, W=64, B=80, Hupe=128
                         ; +0=Dauer-, +1=0.5Hz, +2=1Hz, +3=2Hz Blink-Licht
 GM_PARAM[99,2]=0        ;SCHALTER SPEZIELLE ARTIS-ANZEIGE 
 GM_PARAM[99,1]=0        ;SCHALTER FUER L781/L782, WZ SPERREN 0=AUS, 1=EIN
;GM_PARAM[99,0]=0        ;SCHALTER ROHTEILDAUERBETRIEB PORTAL
 GM_PARAM[100,0]=17      ;SPRACHENSCHALTER FUER TEXT+PARAMETER AUSGABEN
                ; 0 - GR -  DEUTSCH
                ; 1 - UK -  ENGLISCH
                ; 2 - FR -  FRANZOESISCH
                ; 3 - IT -  ITALIENISCH
                ; 4 - SP -  SPANISCH
                ; 5 - DK -  DAENISCH
                ; 6 - NL -  NIEDERLAENDISCH
                ; 7 - PO -  PORTUGIESISCH
                ; 8 - JA -  JAPANISCH
                ; 9 - CZ -  TSCHECHISCH
                ;10 - PL -  POLNISCH
                ;11 - SW -  SCHWEDISCH
                ;12 - FI -  FINNISCH
                ;13 - LI -  LIUNESISCH
                ;14 - HU -  UNGARISCH
                ;15 - CH -  CHINESISCH
                ;16 - TW -  TAIWANISCH
                ;17 - RU -  RUSSISCH
                ;18 - SLV - SLOWENISCH
                ;19 - ROM - RUMAENISCH
                ;20 - TR  - TUERKISCH
                ;21 - SKY - SLOWAKISCH
                ;22 - KOR - KOREANISCH
                ;23 - BUL - BULGARISCH

;GM_PARAM[100,1]            ;MERKER AKTUELLE TRANSFORMATION KANAL 1 
;GM_PARAM[100,2]            ;MERKER AKTUELLE TRANSFORMATION KANAL 2

M17
____
