#uplevel #0{
# =====================================================
# out custom header
# 26-12-2018
proc USER_out_header {} {
# =====================================================
	global mom_part_name mom_toolpath_time mom_parent_group_name
	global mom_date mom_logname mom_group_name mom_path_name mom_toolpath_time
	global mom_oper_program local_mom_group_name mom_toolpath_cutting_time

	if { ![info exists mom_group_name] } { set mom_group_name $mom_path_name } 
	if { [string compare $mom_oper_program $mom_group_name] == 0 } {
		set local_mom_group_name $mom_group_name
	} else {
		set local_mom_group_name [format "%s_%s" $mom_oper_program $mom_path_name]
	}
	MOM_output_literal ";START OF PROGRAM: $local_mom_group_name"
	set out_part_info 1
	if { $out_part_info == 1 } {
		MOM_output_literal "; COMPANY    : ZAO CHP SESPEL"
		MOM_output_literal "; MACHINE    : DMG CTX gamma 2000 TC"
		MOM_output_literal "; PROGRAMMER : [string toupper $mom_logname]"
		set date_time [clock scan today]
		set date_time [clock format $date_time -format "%x %H:%M"]
		MOM_output_literal "; DATE-TIME  : $date_time"
		MOM_output_literal "; MAIN-FILE  : [string toupper $mom_part_name]"
		MOM_output_literal "; POSTNAME   : CTX GAMMA 2000 TC ver.190215" 
		MOM_output_literal ";"
		#MOM_output_literal ";PART NAME: [string toupper $mom_part_name];"
		#MOM_output_literal ";TOTAL MACHINE TIME: [format "%.3f" $mom_toolpath_time] MIN"
		#MOM_output_literal ";CUTTING MACHINE TIME: [format "%.3f" $mom_toolpath_cutting_time] MIN"
		USER_out_tool_list
	}
}
#}