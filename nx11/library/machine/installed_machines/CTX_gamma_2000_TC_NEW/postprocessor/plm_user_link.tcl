#=============================================================
proc PB_CMD_link_add_tcl { } {
#=============================================================
#<21-12-2018> Mitiouk
#impotr this file in PB as a PB_CMD procedure
#insert this proc in Program Start Sequence
#add user module with command "user_link <filename.tcl>"
#use proc user_link <you_source_file.tcl>
global mom_sys_master_post mom_event_handler_file_name
	
    proc user_link { file_source_name } {
		if { [file exists [file dirname $::mom_event_handler_file_name]/$file_source_name] } { 
		    source [file dirname $::mom_event_handler_file_name]/$file_source_name} {
			
			MOM_display_message "Файл: [file dirname $::mom_event_handler_file_name]/$file_source_name не найден!" "PB_CMD_link_add_tcl"
		}
    }

user_link plm_user_display_msg.tcl
user_link Tool_list_and_header.tcl
USER_out_header
}