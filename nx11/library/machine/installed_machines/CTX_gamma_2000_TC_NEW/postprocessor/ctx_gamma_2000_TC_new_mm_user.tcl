proc USER_rotate_cycle_coordinate { } {
# <Mitiouk> 02-03-2019
  global cycle_init_flag
  global mom_siemens_cycle_count
  global cycle_plane_change_flag
  global mom_kin_machine_type
  global mom_tool_axis mom_tool_axis_type
  global mom_siemens_5axis_mode
  global mom_pos mom_mcs_goto
  global mom_prev_out_angle_pos mom_out_angle_pos
  global mom_siemens_coord_rotation
  global save_mom_kin_machine_type
  global mom_prev_tool_axis
  global coord_ang_A coord_ang_B coord_ang_C
  global mom_cycle_rapid_to_pos mom_cycle_retract_to_pos mom_cycle_feed_to_pos
  global mom_cycle_rapid_to mom_cycle_retract_to mom_cycle_feed_to
  global coord_offset mom_current_motion
  global mom_cycle_spindle_axis
  global save_coord_ang_A save_coord_ang_B save_coord_ang_C
  global mom_cycle_clearance_pos mom_cycle_clearance_plane
  
  

  if { ![string match "*5_axis*" $mom_kin_machine_type] } { return }
  if {[info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation ==1} { return }
  
  if {![info exists save_coord_ang_A]} {
      set save_coord_ang_A 0
      set save_coord_ang_B 0
      set save_coord_ang_C 0
   }
	 
   if {[info exists coord_ang_A]} {
      set save_coord_ang_A $coord_ang_A
      set save_coord_ang_B $coord_ang_B
      set save_coord_ang_C $coord_ang_C
	  #MOM_output_literal ";foo USER_rotate_cycle_coordinate: SAVE COORD"
   }
   
   if {![info exists mom_prev_out_angle_pos]} {
		set mom_prev_out_angle_pos(0) 0.0
		set mom_prev_out_angle_pos(1) 0.0
	}
	
   if {![EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] || ![EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
		set cycle_rotate_flag 1
	} else {
		set cycle_rotate_flag 0
	}
	
   if {[EQ_is_equal $mom_tool_axis(2) 1.0]} {
		set coord_ang_A 0
		set coord_ang_B 0
		set coord_ang_C 0
# If in cycle plane change event, the tool axis changed back to Z axis,
# reload mom_prev_out_angle_pos to prevent the coordinate rotation code output twice.
		if {[info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation == 2} {
			set mom_prev_out_angle_pos(0) $mom_out_angle_pos(0)
			set mom_prev_out_angle_pos(1) $mom_out_angle_pos(1)
			MOM_reload_variable -a mom_prev_out_angle_pos
		}
	} else {
		USER_auto_3D_rotation
	}


#	MOM_output_literal ";foo  coord_ang_A= $coord_ang_A"
#	MOM_output_literal ";foo  coord_ang_B= $coord_ang_B"
#	MOM_output_literal ";foo  coord_ang_C= $coord_ang_C"	
#MOM_output_literal ";foo mom_tool_axis(2)=$mom_tool_axis(2)"
#MOM_output_literal ";foo save_coord_ang_A= $save_coord_ang_A coord_ang_A= $coord_ang_A"
#MOM_output_literal ";foo save_coord_ang_B= $save_coord_ang_B coord_ang_B= $coord_ang_B"
#MOM_output_literal ";foo save_coord_ang_C= $save_coord_ang_C coord_ang_C= $coord_ang_C"
   
   if { $mom_siemens_coord_rotation == 2 } {
      set mom_cycle_spindle_axis 2

    # Output coordinate rotation code
	
	
	 if { [string match "TRAORI" $mom_siemens_5axis_mode] } {
	   if {[info exist debug_mode] && [$debug_mode]} {
			#MOM_output_literal ";foo USER_rotate_cycle_coordinate: RUN 3D rotation" }
			MOM_output_literal "TRANS X0 Y0 Z0"
            USER_output_coord_rotation
	 }

	
      if {![EQ_is_equal $coord_ang_A $save_coord_ang_A] || ![EQ_is_equal $coord_ang_B $save_coord_ang_B] || ![EQ_is_equal $coord_ang_C $save_coord_ang_C]} {
         if { ![string match "SWIVELING" $mom_siemens_5axis_mode] } {
            MOM_output_literal "TRANS X0 Y0 Z0"
            USER_output_coord_rotation
            #MOM_output_literal "PB_CMD_output_coord_rotation"
         } else {  
		 
		 PB_call_macro CYCLE800_sl

         }
      }

      VMOV 3 mom_pos mom_cycle_rapid_to_pos
      VMOV 3 mom_pos mom_cycle_feed_to_pos
      VMOV 3 mom_pos mom_cycle_retract_to_pos
        VMOV 3 mom_pos mom_cycle_clearance_pos
      set mom_cycle_rapid_to_pos(2) [expr $mom_pos(2)+$mom_cycle_rapid_to]
      set mom_cycle_retract_to_pos(2) [expr $mom_pos(2)+$mom_cycle_retract_to]
      set mom_cycle_feed_to_pos(2) [expr $mom_pos(2)+$mom_cycle_feed_to]
        set mom_cycle_clearance_pos(2) [expr $mom_pos(2)+$mom_cycle_clearance_plane]
   }

}

# =====================================================
proc USER_auto_3D_rotation {} {
# =====================================================
# This command is used to get auto 3D coordinate rotation by tool axis vector.
	
	#MOM_output_literal ";foo USER_auto_3D_rotation"
	global mom_kin_machine_type save_mom_kin_machine_type mom_tool_axis mom_tool_axis_type
	global mom_siemens_coord_rotation mom_kin_coordinate_system_type mom_siemens_5axis_mode
	global mom_operation_type mom_kin_4th_axis_point save_mom_kin_4th_axis_point mom_sys_4th_axis_has_limits
	global mom_kin_5th_axis_point save_mom_kin_5th_axis_point mom_sys_5th_axis_has_limits
	global mom_kin_4th_axis_vector save_mom_kin_4th_axis_vector save_mom_sys_4th_axis_has_limits
	global mom_kin_5th_axis_vector save_mom_kin_5th_axis_vector save_mom_sys_5th_axis_has_limits
	global mom_mcs_goto mom_pos mom_prev_pos mom_out_angle_pos
	global coord_ang_A coord_ang_B coord_ang_C mom_tool_path_type
	global coord_ref_x coord_ref_y coord_ref_z RAD2DEG DEG2RAD
	global cycle_init_flag mom_current_motion mom_cycle_rapid_to mom_prev_out_angle_pos

	if { ![string match "*5_axis*" $mom_kin_machine_type] } { return }
	if { [info exists mom_kin_coordinate_system_type] && [string match "CSYS" $mom_kin_coordinate_system_type] } { return }
	if { [info exists mom_siemens_5axis_mode] && [string match "TRAFOOF" $mom_siemens_5axis_mode] } { return }
	if {![info exists mom_tool_axis_type]} { set mom_tool_axis_type 0 }
	if { ![info exists mom_tool_axis_type] } { set mom_tool_axis_type 0 }
	if { ![info exists mom_tool_path_type] } { set mom_tool_path_type "undefined" }
	#
	#
	if {[info exists debug_mode] && [$debub_mode]} {
	MOM_output_literal ";mom_tool_axis(2)=$mom_tool_axis(2)"
	MOM_output_literal ";mom_tool_axis_type=$mom_tool_axis_type"
	MOM_output_literal ";mom_tool_path_type=$mom_tool_path_type"
	MOM_output_literal ";mom_operation_type=$mom_operation_type"
	}
	
	
	
	if { [EQ_is_equal $mom_tool_axis(2) 1.0]  || ($mom_tool_axis_type >= 2  &&  [string match "Variable-axis *" $mom_operation_type])  || \
		[string match "Sequential Mill Main Operation" $mom_operation_type] || \
		([string match "variable_axis" $mom_tool_path_type] && [string match "Variable-axis *" $mom_operation_type])} {
		return
	} else {
		set mom_siemens_coord_rotation 2
	}
	
# Get initial/first point by rotation
	if {[string match "5_axis_dual_head" $mom_kin_machine_type]} {
		set rot0 [expr $mom_out_angle_pos(1)*$DEG2RAD]
		set rot1 [expr $mom_out_angle_pos(0)*$DEG2RAD]
		set mom_pos(3) $mom_out_angle_pos(1)
		set mom_pos(4) $mom_out_angle_pos(0)
	} else {
		set rot0 [expr $mom_out_angle_pos(0)*$DEG2RAD]
		set rot1 [expr $mom_out_angle_pos(1)*$DEG2RAD]
		set mom_pos(3) $mom_out_angle_pos(0)
		set mom_pos(4) $mom_out_angle_pos(1)
	}
# Reolad kinematics to dual-table machine
	PB_CMD_save_kinematics
	if { [string match "5_axis_dual_head" $mom_kin_machine_type] } {
		PB_CMD_swap_4th_5th_kinematics
		set angle_pos(0) $mom_out_angle_pos(0)
		set mom_out_angle_pos(0) $mom_out_angle_pos(1)
		set mom_out_angle_pos(1) $angle_pos(0)
	}
	if { ![string match "5_axis_dual_table" $mom_kin_machine_type] } { set mom_kin_machine_type "5_axis_dual_table" }
	set x 0.0; set y 0.0; set z 0.0;
	VEC3_init x y z mom_kin_4th_axis_point
	VEC3_init x y z mom_kin_5th_axis_point
	MOM_reload_kinematics

	VECTOR_ROTATE mom_kin_5th_axis_vector [expr -1*$rot1] mom_mcs_goto v
	VECTOR_ROTATE mom_kin_4th_axis_vector [expr -1*$rot0] v mom_pos
	MOM_reload_variable -a mom_pos
	if {[info exists cycle_init_flag] && $cycle_init_flag == "TRUE"} {
		if { [string match "initial_move" $mom_current_motion]} {
			set mom_pos(2) [expr $mom_pos(2) + $mom_cycle_rapid_to]
		}
	}
	set mom_prev_out_angle_pos(0) $mom_out_angle_pos(0)
	set mom_prev_out_angle_pos(1) $mom_out_angle_pos(1)
	MOM_reload_variable -a mom_out_angle_pos
	MOM_reload_variable -a mom_prev_out_angle_pos

# Initialize coordinate rotation angles
	set coord_ang_A 0.0 ; set coord_ang_B 0.0 ; set coord_ang_C 0.0
	set coord_ref_x 0.0 ; set coord_ref_y 0.0 ; set coord_ref_z 0.0
	set X(0) 1.0; set X(1) 0.0; set X(2) 0.0
	set Y(0) 0.0; set Y(1) 1.0; set Y(2) 0.0
	set Z(0) 0.0; set Z(1) 0.0; set Z(2) 1.0
# Calculate rotation matrix
	VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] X v1
	VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] Y v2
	VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] Z v3

	VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v1 X
	VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v2 Y
	VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v3 Z

	MTX3_init_x_y_z X Y Z rotation_matrix
 # Calculate euler angles , rotation order is X->Y->Z
	if { [info exists rotation_matrix] } {
		set m0 $rotation_matrix(0)
		set m1 $rotation_matrix(1)
		set m2 $rotation_matrix(2)
		set m3 $rotation_matrix(3)
		set m4 $rotation_matrix(4)
		set m5 $rotation_matrix(5)
		set m6 $rotation_matrix(6)
		set m7 $rotation_matrix(7)
		set m8 $rotation_matrix(8)

		set cos_b_sq [expr $m0*$m0 + $m3*$m3]
		if {[EQ_is_equal $cos_b_sq 0.0]} {
			set cos_b 0.0
			set cos_c 1.0
			set cos_a $m4
			set sin_c 0.0
			set sin_a [expr -1*$m5]
			if {$m6 < 0.0} { set sin_b 1.0 } else { set sin_b -1.0 }
		} else {
			set cos_b [expr sqrt($cos_b_sq)]
			set sin_b [expr -$m6]
			set cos_a [expr $m8/$cos_b]
			set sin_a [expr $m7/$cos_b]
			set cos_c [expr $m0/$cos_b]
			set sin_c [expr $m3/$cos_b]
		}
		set coord_ang_A [expr -atan2($sin_a,$cos_a)*$RAD2DEG]
		set coord_ang_B [expr -atan2($sin_b,$cos_b)*$RAD2DEG]
		set coord_ang_C [expr -atan2($sin_c,$cos_c)*$RAD2DEG]
	}
}

# =====================================================
proc USER_output_coord_rotation { } {
# =====================================================
	#MOM_output_literal ";foo USER_output_coordinate_rotation START"
	global sinumerik_version mom_operation_type mom_kin_coordinate_system_type
	global mom_out_angle_pos mom_siemens_coord_rotation mom_siemens_5axis_output_mode
	global coord_ref_x coord_ref_y coord_ref_z coord_ang_A coord_ang_B coord_ang_C
	global coord_ang_1 coord_ang_2 coord_ang_3 coord_offset cycle800_inc_retract
	global cycle800_tc cycle800_dir cycle800_st cycle800_mode mom_siemens_5axis_mode
	global mom_kin_machine_type mom_siemens_ori_def mom_siemens_ori_inter mom_tool_axis_type
	global rot_angle_pos mom_init_pos mom_rotary_direction_4th mom_rotary_direction_5th
	global mom_pos mom_alt_pos mom_out_angle_pos mom_siemens_ori_coord
	global mom_kin_machine_type mom_kin_4th_axis_type mom_kin_5th_axis_type mom_operation_name

	if { [string match "3_axis_mill" $mom_kin_machine_type] } { return }
#-----------------------------------------------------------
#Please set your swivel data record
#-----------------------------------------------------------
	set cycle800_tc "" ;# For example,please put your data here
#Please set your incremental retraction
	set cycle800_inc_retract "0"
	
#DEBAG INFO
	global debug_mode
  if {[info exists debug_mode] && $debug_mode=="ON"} {
  MOM_output_literal ";>> USER_output_coord_rotation: mom_siemens_coord_rotation=$mom_siemens_coord_rotation"
 }
#DEBUG INFO

	if { ![info exists mom_siemens_coord_rotation] } { set mom_siemens_coord_rotation 0 }
	if { $mom_siemens_coord_rotation == 0 } {
		if { [array exists coord_offset] } {
			if { ![EQ_is_zero $coord_offset(0)] || ![EQ_is_zero $coord_offset(1)] || ![EQ_is_zero $coord_offset(2)] } {
				MOM_force once X Y Z
				MOM_do_template frame_trans
				global coord_offset_flag
				set coord_offset_flag 1
			}
			MOM_output_literal "TRANS"
		}
# Local csys rotation is setting
	} else {
# Output TRANS and AROT
		if { ![string match "SWIVELING" $mom_siemens_5axis_mode] } {
			if { [array exists coord_offset] } {
				if { ![EQ_is_zero $coord_offset(0)] || ![EQ_is_zero $coord_offset(1)] || ![EQ_is_zero $coord_offset(2)] } {
					MOM_force once X Y Z
					MOM_do_template frame_trans
					
				}
			}
			#MOM_output_literal "TRANS"
			if { ![EQ_is_zero $coord_ang_A] } {
				MOM_do_template frame_arot_x
			}
			if { ![EQ_is_zero $coord_ang_B] } {
				MOM_do_template frame_arot_y
			}
			if { ![EQ_is_zero $coord_ang_C] } {
				MOM_do_template frame_arot_z
			}
# Switch rotary angles reference coordinate
			if { [string match "*ROTARY*" $mom_siemens_ori_def] && (($mom_tool_axis_type >=2 && [string match "Variable-axis *" $mom_operation_type ]) || [string match "Sequential Mill Main Operation" $mom_operation_type]) } {
				if { [string match "ORIMKS" $mom_siemens_ori_coord] } {
					if { [string match "*dual_head*" $mom_kin_machine_type] || [string match "*dual_table*" $mom_kin_machine_type] } {
						if { $mom_siemens_5axis_output_mode == 1 } {
							global save_mom_kin_machine_type
							set save_mom_kin_machine_type  $mom_kin_machine_type
							set mom_pos(3) $mom_init_pos(3)
							set mom_pos(4) $mom_init_pos(4)
							MOM_reload_variable -a mom_pos
							set mom_kin_machine_type "5_axis_head_table"
						} else {
							global mom_warning_info
							set mom_warning_info "$mom_operation_name: Wrong rotary axes with respect to Machine Coordinate.Switch to ORIWKS."
							MOM_catch_warning
							MOM_output_literal "ORIWKS"
						}
					}
				} else {
					if { [string match "*head_table*" $mom_kin_machine_type] } {
						if { $mom_siemens_5axis_output_mode == 1 } {
							global save_mom_kin_machine_type
							set save_mom_kin_machine_type  $mom_kin_machine_type
							set mom_pos(3) $mom_init_pos(3)
							set mom_pos(4) $mom_init_pos(4)
							MOM_reload_variable -a mom_pos
							set mom_kin_machine_type "5_axis_dual_table"
						} else {
							global mom_warning_info
							set mom_warning_info "$mom_operation_name:Wrong rotary axes with respect to Workpiece Coordinate.Switch to ORIMKS."
							MOM_catch_warning
							MOM_output_literal "ORIMKS"
						}
					}
				}
			}
# Output CYCLE800
		} else {
			global cycle800_tc
			USER_set_cycle800_tc
			set cycle800_dir $mom_rotary_direction_4th
			set cycle800_st 100000
			set cycle800_mode 57
			set coord_ang_1 $coord_ang_A
			set coord_ang_2 $coord_ang_B
			set coord_ang_3 $coord_ang_C
			PB_call_macro CYCLE800_sl
		}
	}
	#MOM_output_literal ";foo USER_output_coordinate_rotation END"
}

# =====================================================
proc USER_rotate_rapid_coordinate {} {
# =====================================================
# This command is used to detect rotary axis change inside operation for 3+2 milling.
# This command will output coordinate rotation code if the rotary axis change the position.
	#MOM_output_literal ";foo USER_rotate_rapid_coordinat"
	global mom_kin_machine_type mom_tool_axis mom_tool_axis_type mom_siemens_5axis_mode
	global mom_pos mom_mcs_goto mom_prev_out_angle_pos mom_out_angle_pos
	global mom_siemens_coord_rotation save_mom_kin_machine_type mom_prev_tool_axis
	global coord_ang_A coord_ang_B coord_ang_C coord_offset mom_current_motion
	global mom_out_angle_pos mom_prev_out_angle_pos mom_kin_machine_type


	global mom_coordinate_output_mode mom_machine_mode
	global mom_motion_event
	
	if { ![info exists mom_coordinate_output_mode] } { set mom_coordinate_output_mode "CARTESIAN XYZ" }
	
	if { ![string compare $mom_machine_mode "MILL"] } {
		if { $mom_motion_event == "initial_move" || $mom_motion_event == "first_move" } {
			if { $mom_coordinate_output_mode == "POLAR INTERPOLATION TRANSMIT" } { 
				MOM_do_template rapid_move_transmit
			}
		}
	}
	
	if { [info exists mom_siemens_5axis_mode] && [string match "TRAORI" $mom_siemens_5axis_mode] } { return }


	if { ![string match "*5_axis*" $mom_kin_machine_type] } { return }\
	
	if { [info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation == 1 } { return }
	
	if { ![info exists mom_prev_out_angle_pos] } {
		set mom_prev_out_angle_pos(0) 0.0
		set mom_prev_out_angle_pos(1) 0.0
	}
#	
#	MOM_output_literal ";foo  mom_prev_out_angle_pos(0)= $mom_prev_out_angle_pos(0) \
#	mom_prev_out_angle_pos(1)= $mom_prev_out_angle_pos(1)"
#	
#	MOM_output_literal ";foo  mom_out_angle_pos(0)= $mom_out_angle_pos(0) \
#	mom_out_angle_pos(1)= $mom_out_angle_pos(1)"
#	

	if { [EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] &&\
		[EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)] } {		
		return
	}
	if { [info exists mom_tool_axis] && [info exists mom_prev_tool_axis] } {
		if { [VEC3_is_equal mom_tool_axis mom_prev_tool_axis] } { return }
	}
	if { [EQ_is_equal $mom_tool_axis(2) 1.0] } {
		set coord_ang_A 0
		set coord_ang_B 0
		set coord_ang_C 0
		if { [info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation == 2 } {
			set mom_prev_out_angle_pos(0) $mom_out_angle_pos(0)
			set mom_prev_out_angle_pos(1) $mom_out_angle_pos(1)
			MOM_reload_variable -a mom_prev_out_angle_pos
		}
	} else {
		USER_auto_3D_rotation
	}
	if { $mom_siemens_coord_rotation == 2 } {
# Output coordinate rotation code
		USER_MILL_output_trans_arot
		if { [USER__check_block_CYCLE800] } { PB_call_macro CYCLE800_sl }
		MOM_force Once X Y Z
	}
}

# =====================================================
proc USER_MILL_output_trans_arot {} {
# =====================================================
# This custom command is used to output coordinate rotation codes.
# This command is used with PB_CMD_set_csys to output cycle800.
	global sinumerik_version mom_operation_type mom_kin_coordinate_system_type
	global mom_out_angle_pos mom_siemens_coord_rotation mom_siemens_5axis_output_mode
	global coord_ref_x coord_ref_y coord_ref_z coord_ang_A coord_ang_B coord_ang_C
	global coord_ang_1 coord_ang_2 coord_ang_3 coord_offset cycle800_inc_retract coord_offset_flag
	global cycle800_tc cycle800_dir cycle800_st cycle800_mode mom_siemens_5axis_mode
	global mom_kin_machine_type mom_siemens_ori_def mom_siemens_ori_inter mom_siemens_cycle_plane
	global rot_angle_pos mom_init_pos mom_rotary_direction_4th mom_rotary_direction_5th
	global mom_pos mom_alt_pos mom_out_angle_pos mom_siemens_ori_coord mom_tool_path_type
	global mom_kin_machine_type mom_kin_4th_axis_type mom_kin_5th_axis_type mom_machine_mode
	global mom_tool_axis_type mom_operation_name mom_tool_axis sinumerik_control_version
	global mom_out_angle_pos DEG2RAD mom_siemens_5axis_output_mode mom_csys_origin 
	global save_mom_kin_machine_type mom_parent_csys_matrix mom_warning_info save_mom_kin_machine_type
    if {[info exist debug_mode] && [$debug_mode]} {
	  MOM_output_literal ";foo USER_MILL_output_trans_arot"
	   MOM_output_literal ";foo mom_machine_mode=$mom_machine_mode"
	    MOM_output_literal ";foo sinumerik_control_version $sinumerik_control_version"
	     MOM_output_literal ";foo mom_kin_machine_type=$mom_kin_machine_type"
	      MOM_output_literal ";foo mom_siemens_coord_rotation $mom_siemens_coord_rotation"
	       MOM_output_literal ";foo mom_siemens_5axis_mode $mom_siemens_5axis_mode" 
		 }
	if {[string compare $mom_machine_mode "MILL"]} { return }
	if {[info exists sinumerik_control_version] && [string match $sinumerik_control_version "Solutionline"]} { set mom_siemens_cycle_plane 1 }
	if { [string match "3_axis_mill" $mom_kin_machine_type] } { return }
	if { ![info exists mom_siemens_coord_rotation] } { set mom_siemens_coord_rotation 0 }

	if { $mom_siemens_coord_rotation == 0 } {
		if { [array exists coord_offset] } {
			if { ![EQ_is_zero $coord_offset(0)] || ![EQ_is_zero $coord_offset(1)] || ![EQ_is_zero $coord_offset(2)] } {
				MOM_force once X Y Z
				MOM_do_template frame_trans
				set coord_offset_flag 1
			}
		}
	} else {
#Local csys rotation is setting
		if {$mom_siemens_coord_rotation == 1} {
			if {![EQ_is_equal $mom_tool_axis(2) 1.0]} {
				MOM_output_to_listing_device "Warning in $mom_operation_name: Wrong Local MCS, Z axis is not parallel to tool axis vector."
			}
		}
# Output TRANS and AROT
		if { ![string match "SWIVELING" $mom_siemens_5axis_mode] } {
			if { [array exists coord_offset] } {
				if {(![EQ_is_zero $coord_offset(0)] || ![EQ_is_zero $coord_offset(1)]) && $mom_siemens_5axis_output_mode == 0} {
					set x $mom_parent_csys_matrix(9) ; set y $mom_parent_csys_matrix(10)
					set rr [expr sqrt($coord_offset(0)*$coord_offset(0)+$coord_offset(1)*$coord_offset(1))]
					set coord_offset(0) [expr $rr*cos(atan2($y,$x)-$mom_out_angle_pos(1)*$DEG2RAD)]
					set coord_offset(1) [expr $rr*sin(atan2($y,$x)-$mom_out_angle_pos(1)*$DEG2RAD)]
				}
				MOM_force once X Y Z
				MOM_do_template frame_trans
			}
			if {$mom_siemens_5axis_output_mode == 0} {
				set save_coord_ang_C $coord_ang_C
				set coord_ang_C [expr -1*$mom_out_angle_pos(1)]
				MOM_do_template frame_arot_z
				set coord_ang_C $save_coord_ang_C
			}
			if { ![EQ_is_zero $coord_ang_A] } { MOM_do_template frame_arot_x }
			if { ![EQ_is_zero $coord_ang_B] } { MOM_do_template frame_arot_y }
			if { ![EQ_is_zero $coord_ang_C] } { MOM_do_template frame_arot_z }
# Switch rotary angles reference coordinate
			if { ![info exists mom_tool_axis_type] } { set mom_tool_axis_type 0 }
			if { ![info exists mom_tool_path_type] } { set mom_tool_path_type "undefined" }
			if { [string match "*ROTARY*" $mom_siemens_ori_def] &&\
				(($mom_tool_axis_type >=2 && [string match "Variable-axis *" $mom_operation_type ]) ||\
				[string match "Sequential Mill Main Operation" $mom_operation_type] || \
				([string match "variable_axis" $mom_tool_path_type] && ![string match "Variable-axis *" $mom_operation_type]))} {
				if { [string match "ORIMKS" $mom_siemens_ori_coord] } {
					if { [string match "*dual_head*" $mom_kin_machine_type] || [string match "*dual_table*" $mom_kin_machine_type] } {
						if { $mom_siemens_5axis_output_mode == 1 } {
							set save_mom_kin_machine_type  $mom_kin_machine_type
							set mom_pos(3) $mom_init_pos(3)
							set mom_pos(4) $mom_init_pos(4)
							MOM_reload_variable -a mom_pos
							set mom_kin_machine_type "5_axis_head_table"
						} else {
							set mom_warning_info "$mom_operation_name: Wrong rotary axes with respect to Machine Coordinate.Switch to ORIWKS."
							MOM_catch_warning
							MOM_output_literal "ORIWKS"
						}
					}
				} else {
					if { [string match "*head_table*" $mom_kin_machine_type] } {
						if { $mom_siemens_5axis_output_mode == 1 } {
							set save_mom_kin_machine_type  $mom_kin_machine_type
							set mom_pos(3) $mom_init_pos(3)
							set mom_pos(4) $mom_init_pos(4)
							MOM_reload_variable -a mom_pos
							set mom_kin_machine_type "5_axis_dual_table"
						} else {
							set mom_warning_info "$mom_operation_name:Wrong rotary axes with respect to Workpiece Coordinate.Switch to ORIMKS."
							MOM_catch_warning
							MOM_output_literal "ORIMKS"
						}
					}
				}
			}
		} else {USER__check_block_CYCLE800}
  	}
}
# =====================================================
proc USER__check_block_CYCLE800 {} {
# =====================================================
# This custom command should return
#   1 : Output BLOCK
#   0 : No output
	global sinumerik_control_version mom_rotary_direction_4th mom_siemens_coord_rotation
	global coord_ang_A coord_ang_B coord_ang_C cycle800_inc_retract cycle800_tc cycle800_dir
	global cycle800_st cycle800_mode coord_ang_1 coord_ang_2 coord_ang_3
	global mom_siemens_5axis_mode mom_siemens_cycle_dmode mom_machine_mode

	if { [string compare $mom_machine_mode "MILL"] } { return 0 }

#Please set your swivel data record
	set cycle800_tc "\"R_DATA\"" ;# For example,please put your data here
#-----------------------------------------------------------
#Please set your incremental retraction
#-----------------------------------------------------------
	set cycle800_inc_retract "1"
#-----------------------------------------------------------
	if {[info exists sinumerik_control_version] && [string match $sinumerik_control_version "Solutionline"]} {
		set mom_siemens_cycle_dmode 0
	} else {
		if { [info exists mom_siemens_cycle_dmode] } { unset mom_siemens_cycle_dmode }
	}

      if { ![info exists mom_siemens_coord_rotation] } { set mom_siemens_coord_rotation 0 }
      if {$mom_siemens_coord_rotation !=0 && [info exists mom_siemens_5axis_mode] && [string match "SWIVELING" $mom_siemens_5axis_mode] } {
		set cycle800_dir $mom_rotary_direction_4th
		set cycle800_st 100000
		set cycle800_mode 57
		set coord_ang_1 $coord_ang_A
		set coord_ang_2 $coord_ang_B
		set coord_ang_3 $coord_ang_C
		return 1
	}
	return 0
}

###############################################
#USER_operator_message
###############################################
proc USER_operator_message { } {

# Default handler for UDE MOM_operator_message
# - Do not attach it to any event!
#
# This procedure is executed when the Operator Message command is activated.
#
   global mom_operator_message mom_operator_message_defined
   global mom_operator_message_status
   global ptp_file_name group_output_file mom_group_name
   global mom_sys_commentary_output
   global mom_sys_control_in
   global mom_sys_control_out
   global mom_sys_ptp_output

   if { [info exists mom_operator_message_defined] } {
      if { $mom_operator_message_defined == 0 } {
return
      }
   }

   if { [string compare "ON" $mom_operator_message] && [string compare "OFF" $mom_operator_message] } {

      set brac_start [string first \( $mom_operator_message]
      set brac_end   [string last \) $mom_operator_message]

      if { $brac_start != 0 } {
         set text_string ";("
      } else {
         set text_string ""
      }

      append text_string $mom_operator_message

      if { $brac_end != [expr [string length $mom_operator_message] - 1] } {
         append text_string ")"
      }

      MOM_close_output_file   $ptp_file_name

      if { [info exists mom_group_name] } {
         if { [info exists group_output_file($mom_group_name)] } {
            MOM_close_output_file $group_output_file($mom_group_name)
         }
      }

      MOM_output_text      $text_string

      if { ![string compare "ON" $mom_sys_ptp_output] } {
         MOM_open_output_file    $ptp_file_name
      }

      if { [info exists mom_group_name] } {
         if { [info exists group_output_file($mom_group_name)] } {
            MOM_open_output_file $group_output_file($mom_group_name)
         }
      }

      set need_commentary $mom_sys_commentary_output
      set mom_sys_commentary_output OFF
      regsub -all {[)]} $text_string $mom_sys_control_in text_string
      regsub -all {[(]} $text_string $mom_sys_control_out text_string

      MOM_output_literal $text_string

      set mom_sys_commentary_output $need_commentary

   } else {
      set mom_operator_message_status $mom_operator_message
   }
 }
 
 
 #########################################################
 # Ставить в конце цикла
 #########################################################
 proc USER_retract_cycle { } {
 
 #<Mitiouk> 02-03-2019

	#MOM_output_literal ";foo retract cycle start"
	global mom_siemens_5axis_mode
	
	if { [info exists mom_siemens_5axis_mode] && [string match "TRAFOOF" $mom_siemens_5axis_mode] } { return }
	if { [info exists mom_siemens_5axis_mode] && [string match "SWIVELING" $mom_siemens_5axis_mode] } { 
		#MOM_output_literal "ROT"
		#MOM_output_literal "SUPA X750. Y-108 D0"
		#PB_CMD_GAMMA_2000_TC_TOOL_CHANGE
	}
	if { [info exists mom_siemens_5axis_mode] && [string match "TRAORI" $mom_siemens_5axis_mode] } {
		#
		#MOM_output_literal "TRAFOOF"
		#MOM_output_literal "ROT"
		#PB_CMD_reset_all_motion_variables_to_zero
		}
	#MOM_output_literal ";foo retract cycle end"
 }

 
 #=============================================================
proc USER_reload_kin_to_dual_table { config } {
#=============================================================
# config "" is default
# config C_LOCK
#global config
            uplevel #0 {
                        #definition section
						if { ![info exists config] } { set config "" }
						
						global mom_kin_machine_type mom_kin_spindle_axis
                        global mom_kin_4th_axis_ang_offset mom_kin_4th_axis_center_offset
                        global mom_kin_4th_axis_direction mom_kin_4th_axis_incr_switch
                        global mom_kin_4th_axis_leader mom_kin_4th_axis_limit_action
                        global mom_kin_4th_axis_max_limit mom_kin_4th_axis_min_incr
                        global mom_kin_4th_axis_min_limit mom_kin_4th_axis_plane
                        global mom_kin_4th_axis_point mom_kin_4th_axis_rotation
                        global mom_kin_4th_axis_type mom_kin_4th_axis_vector
                        global mom_kin_4th_axis_zero
                        global mom_kin_5th_axis_ang_offset mom_kin_5th_axis_center_offset
                        global mom_kin_5th_axis_direction mom_kin_5th_axis_incr_switch
                        global mom_kin_5th_axis_leader mom_kin_5th_axis_limit_action
                        global mom_kin_5th_axis_max_limit mom_kin_5th_axis_min_incr
                        global mom_kin_5th_axis_min_limit mom_kin_5th_axis_plane
                        global mom_kin_5th_axis_point mom_kin_5th_axis_rotation
                        global mom_kin_5th_axis_type mom_kin_5th_axis_vector
                        global mom_kin_5th_axis_zero
                        
						##############################################################
						# 4-th AXIS
						
                        set mom_kin_machine_type "5_axis_dual_table"
                        set mom_kin_spindle_axis(0) "0.0"
                        set mom_kin_spindle_axis(1) "0.0"
                        set mom_kin_spindle_axis(2) "1.0"

                        set mom_kin_4th_axis_ang_offset "0.0"
                        set mom_kin_4th_axis_center_offset(0) "0.0"
                        set mom_kin_4th_axis_center_offset(1) "0.0"
                        set mom_kin_4th_axis_center_offset(2) "0.0"
                        set mom_kin_4th_axis_direction "MAGNITUDE_DETERMINES_DIRECTION"
                        set mom_kin_4th_axis_incr_switch "OFF"
                        set mom_kin_4th_axis_leader "B1="
                        set mom_kin_4th_axis_limit_action "Warning"
                        set mom_kin_4th_axis_max_limit "210.0" ;#
                        set mom_kin_4th_axis_min_incr "0.0001"
                        set mom_kin_4th_axis_min_limit "-30.0" ;#
                        set mom_kin_4th_axis_plane "ZX"
                        set mom_kin_4th_axis_point(0) "0.0"
                        set mom_kin_4th_axis_point(1) "0.0"
                        set mom_kin_4th_axis_point(2) "0.0"
                        set mom_kin_4th_axis_rotation "standard"
                        set mom_kin_4th_axis_type "Table"
                        set mom_kin_4th_axis_vector(0) "0"
                        set mom_kin_4th_axis_vector(1) "1"
                        set mom_kin_4th_axis_vector(2) "0"
                        set mom_kin_4th_axis_zero "0.0"
						
						#5-th AXIS
						
                        set mom_kin_5th_axis_ang_offset "0.0"
                        set mom_kin_5th_axis_center_offset(0) "0.0"
                        set mom_kin_5th_axis_center_offset(1) "0.0"
                        set mom_kin_5th_axis_center_offset(2) "0.0"
                        set mom_kin_5th_axis_direction "MAGNITUDE_DETERMINES_DIRECTION"
                        set mom_kin_5th_axis_incr_switch "OFF"
                        set mom_kin_5th_axis_leader "C4"
                        set mom_kin_5th_axis_limit_action "Warning"
                        
						#lock C-axis
						
						if {[string match "C_LOCK" $config]} {	
						set mom_kin_5th_axis_max_limit "0"
                        set mom_kin_5th_axis_min_incr "0.0001"
                        set mom_kin_5th_axis_min_limit "0"
						} else {
						set mom_kin_5th_axis_max_limit "360.0"
                        set mom_kin_5th_axis_min_incr "0.0001"
                        set mom_kin_5th_axis_min_limit "0.0"
						}
						
                        set mom_kin_5th_axis_plane "XY"
                        set mom_kin_5th_axis_point(0) "0.0"
                        set mom_kin_5th_axis_point(1) "0.0"
                        set mom_kin_5th_axis_point(2) "0.0"
                        set mom_kin_5th_axis_rotation "standard"
                        set mom_kin_5th_axis_type "Table"
                        set mom_kin_5th_axis_vector(0) "0"
                        set mom_kin_5th_axis_vector(1) "0"
                        set mom_kin_5th_axis_vector(2) "1"
                        set mom_kin_5th_axis_zero "0.0"
                        MOM_reload_kinematics
                        global mom_pos mom_out_angle_pos
                        MOM_reload_variable -a mom_pos
                        MOM_reload_variable -a mom_out_angle_pos
            } ;#uplevel
}

####################################################################
# Разработать процедуру получения значения переменной конфигурации
# пользовательской кинематики
####################################################################
proc USER_get_kin_config { } {

return 0
}


#=============================================================
proc USER_CHECK_LOCK_C_AXIS { } {
#=============================================================
#<Mitiouk> 03-03-2019
global user_c_axis_mode
global debug_mode
#set user_c_axis_mode "CARTESIAN"

 # if {[info exists debug_mode] && $debug_mode=="ON"} {
 #   MOM_output_literal ";>>USER_CHECK_LOCK_C_AXIS USER_CHECK_LOCK_C_AXIS"
 # }
 #if { [info exists user_c_axis_mode] && [string match "CARTESIAN" $user_c_axis_mode] } {
 #   USER_lock_C_axis
 #   } else {
 #   USER_unlock_C_axis
 #   }
}


#=============================================================
#
proc USER_lock_C_axis { } {
#
#=============================================================
#
# Урезанная версия блокирует только ось Y
#
#=============================================================
 global mom_sys_coordinate_calculation_method mom_kin_arc_output_mode
 global mom_lock_axis mom_lock_axis_plane mom_coordinate_output_mode
			global mom_lock_axis_value mom_lock_axis_value_defined mom_sys_leader
			global saved_ude_setting saved_lock_yaxis_setting
			 
				set mom_sys_coordinate_calculation_method "CARTESIAN"
				set mom_kin_arc_output_mode "FULL_CIRCLE"
				#set saved_ude_setting $mom_coordinate_output_mode
				set mom_lock_axis "CAXIS"
				#set mom_lock_axis_plane 0
				set mom_lock_axis_value "0.0"
				set mom_lock_axis_value_defined 1
				MOM_lock_axis
				set saved_lock_yaxis_setting "TRUE"
				MOM_reload_kinematics
				MOM_reload_variable -a mom_pos
		MOM_reload_variable -a mom_out_angle_pos
}
#####################################################################
#
proc USER_unlock_C_axis { } {
#
#=============================================================
#
# Урезанная версия разблокирует ось C блокирует ось Y
#
#=============================================================
 global mom_sys_coordinate_calculation_method mom_kin_arc_output_mode
 global mom_lock_axis mom_lock_axis_plane mom_coordinate_output_mode
			global mom_lock_axis_value mom_lock_axis_value_defined mom_sys_leader
			global saved_ude_setting saved_lock_yaxis_setting
				set mom_sys_coordinate_calculation_method "CARTESIAN"
				set mom_kin_arc_output_mode "FULL_CIRCLE"
				#set saved_ude_setting $mom_coordinate_output_mode
				set mom_lock_axis "OFF"
				set mom_lock_axis_plane "NONE"
				set mom_lock_axis_value "0.0"
				set mom_lock_axis_value_defined 0
				MOM_lock_axis
				#set saved_lock_yaxis_setting "TRUE"
				MOM_reload_kinematics
				MOM_reload_variable -a mom_pos
		MOM_reload_variable -a mom_out_angle_pos
}

#######################################################################
proc USER_coolant { } {
# Used UDE CTX_GAMMA_COOLANT
#######################################################################
   global mom_ude_collant_m108
   global mom_ude_coolant_m154
   global mom_ude_coolant_m150
   global mom_ude_coolant_m871
   global mom_ude_coolant_m122
   
   
   if {[info exists mom_ude_coolant_m154]} {
	if {[string match "ON" $mom_ude_coolant_m154]} {
	 MOM_output_literal "M154;"
	}
   }
   if {[info exists mom_ude_coolant_m150]} {
	if {[string match "ON" $mom_ude_coolant_m150]} {
	 MOM_output_literal "M150;"
	}
   }
   if {[info exists mom_ude_coolant_m871]} {
	if {[string match "ON" $mom_ude_coolant_m871]} {
	 MOM_output_literal "M871;"
	} 
   }
   if {[info exists mom_ude_coolant_m871]} {
	if {[string match "ON" $mom_ude_coolant_m871]} {
	 MOM_output_literal "M122;"
	} 
   }
   # ON/OFF all coolant
   if {[info exists mom_ude_collant_m108]} {
	if {[string match "ON" $mom_ude_collant_m108]} {
	 MOM_output_literal "M108 ; Coolant ON"
	} else {
	  MOM_output_literal "M109 ; Coolant OFF"
	}
   }
 
}

#=========================================================
proc USER_M_function { } {
#=========================================================
# Special function code out
# Used CTX_GAMMA_SETTING
  
  global mom_ude_m710; # Вытяжка
  #global mom_ude_m740; # 
  #global mom_ude_m742; #
  
  if {[info exists mom_ude_collant_m108]} {
	if {[string match "ON" $mom_ude_m710]} {
	 MOM_output_literal "M710 ; Coolant ON"
	} else {
	  MOM_output_literal "M742 ; Coolant OFF"
	}
   } 
}

#=============================================================
proc USER_initial_move { } {
#=============================================================
# <Mitiouk> 04-03-2019

  global mom_feed_rate mom_feed_rate_per_rev mom_motion_type
  global mom_kin_max_fpm mom_motion_event 
  global mom_cutcom_status mom_cutcom_mode  mom_sys_cutcom_code
  global mom_current_oper_is_last_oper_in_program
  global mom_next_oper_has_tool_change
  
  USER_check_operation_parameters
  if {[info exist debug_mode] && [$debug_mode]} {
    MOM_output_literal ";Start initial move"
  }
  USER_output_diameter_mode
   # Выводится SETMS($spindle)
  
  if { [PB_CMD__check_block_getd]} {
	MOM_do_template getd
  }
  
  USER_tool_change;			#MOM_output_literal ";Tool Change" 
 
  USER_CHECK_LOCK_C_AXIS
 
   #IF TURN MOVE==============================================
   
 if { [PB_CMD__check_block_TURN] } {
   
  #PB_CMD_handle_generic_cycle_start_motion; #for cycle95
  #DIAMOF/DIAMON
  MOM_force once fourth_axis
  MOM_do_template fourth_axis
  MOM_do_template safe_turn_string
  if {[USER_check_block_stop]} { MOM_do_template user_stop }
  #PB_CMD_start_of_operation_force_addresses
  PB_CMD_TURN_spindle_sfm_start
  PB_CMD_sync_spindle_output;#код синхронизации
 } 

   #IF MILL===================================================
 if { [PB_CMD__check_block_MILL] } { 
  
  PB_CMD_define_feed_variable_value
  
  #ini spindle S4 S3
  PB_CMD_GAMMA_2000_TC_Subroutine_L705_L707
  
  if { [PB_CMD__check_block_change_master_spindle] } {
     MOM_do_template initial_move
   }
  MOM_do_template rotation_axes_B_1
  MOM_force once fifth_axis
  MOM_do_template fifth_axis
  MOM_suppress once fifth_axis
  
   if {[USER_check_block_stop]} { MOM_do_template user_stop }
  
  
  
  PB_CMD_MILL_detect_5axis_mode
  PB_CMD_MILL_auto_3D_rotation

  # check transmit_mode
  # USER_transmit
  # параметры сглаживания
  # PB_CMD_smooth
  # параметры sinumerik SMOOTH\TRANSMIT\TRAORI
  
  USER_output_Sinumerik_setting
  USER_output_coord_rotation
  USER_TRANS_5A
 
  MOM_do_template user_trans
	# смещения
   if { [PB_CMD__check_block_MILL_fixture_offset] } {
      MOM_force Once G_offset
      MOM_do_template fixture_offset
   }
	
	 # Выводим корректор
      MOM_force Once D
      MOM_do_template cutter_compensation
   
   # MACRO ORIRESET
   #if { [PB_CMD__check_block_ORIRESET] } {
   #   PB_call_macro ORIRESET
   #}
   
   PB_CMD_MILL_output_cutcom_mode
   
   PB_CMD_sync_spindle_output
  
   PB_CMD_coolant_on
   PB_CMD_set_fixture_offset
   global mom_kin_machine_type
   #MOM_output_literal ";foo mom_kin_machine_type: $mom_kin_machine_type"
   if { [PB_CMD__check_block_ori_interpolation] } {
   
   }
   #PB_CMD_move_force_addresses
   #PB_CMD_start_of_operation_force_addresses
   #USER_output_coord_rotation
   #PB_CMD_recal_cycle_rapid_pos
   #MOM_do_template rapid_move_2
 }
 #all=========================================================
   
   PB_CMD_start_of_operation_force_addresses
   #MOM_output_literal ";END OF  INITIAL MOVE"

 # below in standart post
 # global mom_programmed_feed_rate
 #  if { [EQ_is_equal $mom_programmed_feed_rate 0] } {
 #     MOM_rapid_move
 #  } else {
 #     MOM_linear_move
 #  }

 # Configure turbo output settings 
 #  if { [CMD_EXIST CONFIG_TURBO_OUTPUT] } {
 #     CONFIG_TURBO_OUTPUT
 #  }
 
} 

#=============================================================
proc USER_first_move { } {
#=============================================================
# <Mitiouk> 04-03-2019

  global mom_feed_rate mom_feed_rate_per_rev mom_motion_type
  global mom_kin_max_fpm mom_motion_event
  
  USER_output_diameter_mode
  USER_check_operation_parameters
  
  if {[info exist debug_mode] && [$debug_mode]} {
    MOM_output_literal ";Start first move"  
  }
 
 if { [PB_CMD__check_block_change_master_spindle] } {
      MOM_do_template initial_move
   }
  
  USER_tool_change; #MOM_output_literal ";Tool Change"
  
  USER_CHECK_LOCK_C_AXIS
   
   #IF TURN MOVE==============================================
 if { [PB_CMD__check_block_TURN] } {
  MOM_force once fourth_axis
  MOM_do_template fourth_axis
  #PB_CMD_handle_generic_cycle_start_motion; #for cycle95
  #DIAMOF/DIAMON
  #PB_CMD_output_diameter_mode
  MOM_do_template safe_turn_string
  
  if {[USER_check_block_stop]} { MOM_do_template user_stop }
  
  #PB_CMD_start_of_operation_force_addresses
  PB_CMD_TURN_spindle_sfm_start
 }
  
   #IF MILL===================================================
 if { [PB_CMD__check_block_MILL] } { 
  
  PB_CMD_define_feed_variable_value
 
  #ini spindle S4 S3
  PB_CMD_GAMMA_2000_TC_Subroutine_L705_L707
  MOM_do_template rotation_axes_B_1
  MOM_force once fifth_axis
  MOM_do_template fifth_axis
  MOM_suppress once fifth_axis
  
  if {[USER_check_block_stop]} { MOM_do_template user_stop }
  
  
  
  PB_CMD_MILL_detect_5axis_mode
  PB_CMD_MILL_auto_3D_rotation
  
   
   # check transmit_mode
  # USER_transmit
  # параметры сглаживания
  # PB_CMD_smooth
  # параметры sinumerik SMOOTH\TRANSMIT\TRAORI
  USER_output_Sinumerik_setting
 
  #USER_MILL_output_trans_arot 
  USER_output_coord_rotation
  USER_TRANS_5A
  #PB_CMD_start_of_operation_force_addresses
  MOM_do_template user_trans
	# смещения
   if { [PB_CMD__check_block_MILL_fixture_offset] } {
      MOM_force Once G_offset
      MOM_do_template fixture_offset
   }
	
	 # Выводим корректор
      MOM_force Once D
	  MOM_do_template cutter_compensation
	  
   # MACRO ORIRESET
   #if { [PB_CMD__check_block_ORIRESET] } {
   #   PB_call_macro ORIRESET
   #}
   
   PB_CMD_MILL_output_cutcom_mode

   PB_CMD_sync_spindle_output
   #PB_CMD_move_force_addresses
   PB_CMD_coolant_on
   PB_CMD_set_fixture_offset
   PB_CMD_recal_cycle_rapid_pos
 
  #USER_output_coord_rotation
  #DEBUG INFO
  global mom_kin_machine_type
  global debug_mode
  if { [info exists debug_mode] && $debug_mode=="ON" } {
   MOM_output_literal ";foo mom_kin_machine_type: $mom_kin_machine_type"
   }
   #DEBUG INFO
   if { [PB_CMD__check_block_ori_interpolation] } {
   
   }
 }
  
  
  #all
   PB_CMD_start_of_operation_force_addresses
   #MOM_output_literal ";End of first move"

 # below in standart post
 # global mom_programmed_feed_rate
 #  if { [EQ_is_equal $mom_programmed_feed_rate 0] } {
 #     MOM_rapid_move
 #  } else {
 #     MOM_linear_move
 #  }

 # Configure turbo output settings 
 #  if { [CMD_EXIST CONFIG_TURBO_OUTPUT] } {
 #     CONFIG_TURBO_OUTPUT
 #  }
 
} 

#=============================================================
proc USER_end_of_path { } {
#=============================================================
#<Mitiouk>  04-03-2019
 
 global sinumerik_version
 global mom_operation_type
 global mom_next_oper_has_tool_change
 global mom_current_oper_is_last_oper_in_program
 global mom_operation_type
 global flag_generic_motion_device
 
 if {[info exists flag_generic_motion_device] && $flag_generic_motion_device==1} {
    set flag_generic_motion_device 0
    #unset flag_generic_motion_device
    return
  }

 #PB_CMD_reset_all_motion_variables_to_zero

#  global mom_sys_add_cutting_time mom_sys_add_non_cutting_time
#  global mom_cutting_time mom_machine_time
 
  #PB_CMD_MILL_reset_control_mode
  #PB_CMD_MILL_end_of_path
  USER_TURN_end_path
  USER_MILL_end_of_path
  # MOM_do_template user_trans_0
  MOM_do_template off_coolant
  if { [info exist debug_mode]&& $debug_mode=="ON" } {
    MOM_output_literal ";foo end of path" }
}

#=============================================================
proc USER_TURN_end_path { } {
#=============================================================
#<Mitiouk> 04-03-2019
 global mom_machine_mode user_CRH
 
 if {[info exists mom_machine_mode] && [string compare $mom_machine_mode "TURN"]} { return }
 
 global thread_cycle_flag
 set thread_cycle_flag 0#

 global spindle_is_out
 catch {unset spindle_is_out}

 global sinumerik_version
 global mom_operation_type
 global mom_next_oper_has_tool_change
 global mom_current_oper_is_last_oper_in_program
 global mom_operation_type
 global home_pos_mode
  
 global mom_current_oper_is_last_oper_in_program
 global mom_next_oper_has_tool_change
 if { ([info exists mom_current_oper_is_last_oper_in_program] && $mom_current_oper_is_last_oper_in_program=="YES" ) || \
 		([info exists mom_next_oper_has_tool_change] && $mom_next_oper_has_tool_change=="YES") } {
	
	USER_return_to_home
 }

}

#=============================================================
proc USER_MILL_end_of_path { } {
#=============================================================
#<Mitiouk> 04-03-219
 global mom_machine_mode
 if {[info exists mom_machine_mode] && [string compare $mom_machine_mode "MILL"]} { return }

 global sinumerik_version
 global mom_operation_type
 global mom_next_oper_has_tool_change
 global mom_current_oper_is_last_oper_in_program 
 #
 global mom_machine_mode SP
 global home_pos_mode
 global mom_siemens_coord_rotation
 global mom_kin_machine_type
 global mom_siemens_5axis_mode
 global mom_siemens_5axis_output_mode
 global mom_machine_mode
 global coord_ang_A coord_ang_B coord_ang_C
 global coord_offset_flag  
 #
 global mom_coordinate_output_mode
 #
 global mom_lock_axis_value mom_lock_axis_plane
 global mom_lock_axis mom_lock_axis_value_defined
 
  
 
 USER_return_to_home
 
 #перенести с mill_end_of_path  
   if { [PB_CMD__check_block_reset_trans] } {
      MOM_do_template user_trafoof
	  PB_CMD_reset_all_motion_variables_to_zero
   }
 
 if { $mom_siemens_coord_rotation != 0 } {
     if { ![string match "SWIVELING" $mom_siemens_5axis_mode] } {
        #MOM_output_literal "TRANS X0 Y0 Z0"
     } else {
        MOM_output_literal "CYCLE800()"
     }
     if { $mom_siemens_coord_rotation == 2 } {
        set mom_siemens_coord_rotation 0
        set coord_ang_A 0; set coord_ang_B 0; set coord_ang_C 0
     }
  }
 
  
  if { [info exists coord_offset_flag] && $coord_offset_flag == 1 } {
     MOM_output_literal "TRANS X0 Y0 Z0"
  }
  
  #TRANSMIT_OFF
  if {[info exists mom_coordinate_output_mode] && $mom_coordinate_output_mode == "TRANSMIT"} {
     #MOM_output_literal "TRANS_OFF; TRANSMIT MODE OFF"
     MOM_enable_address fifth_axis
     #MOM_do_template trafoof
	 USER_unlock_C_axis
     #set mom_coordinate_output_mode "XY-Z OUTPUT"
	 
  } elseif { $mom_siemens_5axis_output_mode == 1 } {
     set mom_siemens_5axis_output_mode 0
     #MOM_output_literal "TRANS_OFF; TRAORI MODE OFF"
     #MOM_do_template trafoof
  }
  
  
  #if {[info exists mom_coordinate_output_mode] && [string match $mom_coordinate_output_mode "CX-Z OUTPUT"]} {
  #    set mom_lock_axis "OFF"
  #    set mom_lock_axis_plane "NONE"
  #    set mom_lock_axis_value_defined 0
  #    set mom_lock_axis_value 0.0
  #    set mom_coordinate_output_mode "XY-Z OUTPUT"
  #    MOM_lock_axis
  #}

  # Unset 3Dcutcom mode
  global mom_siemens_3Dcutcom_mode mom_cutter_data_output_indicator
  global mom_cutcom_status
  set mom_cutter_data_output_indicator 0
  set mom_siemens_3Dcutcom_mode "OFF"

  # Motion message flag
  global mom_siemens_pre_motion
  set mom_siemens_pre_motion "end"

  PB_CMD_restore_kinematics
  
  
#if { [info exists sinumerik_version] && [string match "V5" $sinumerik_version] } {
 #   MOM_output_literal ";"
 #   MOM_output_literal "FFWOF"
 #   MOM_output_literal "UPATH"
 ##   MOM_output_literal "SOFT"
 #   MOM_output_literal "COMPOF"
 #   MOM_output_literal "G64"
 #} else {
   if { ![string match "Point to Point" $mom_operation_type] && ![string match "Hole Making" $mom_operation_type] && \
         ![string match "Drilling" $mom_operation_type] && ![string match "Generic Motion" $mom_operation_type]} {
       MOM_output_literal "CYCLE832()"
    }
# }
}
#=============================================================
proc USER_check_operation_parameters { } {
#=============================================================
#<Mitiouk> 04-03-2019
	global mom_spindle_speed mom_tool_number mom_path_name
	global mom_tool_name mom_fixture_offset_value debug_mode
	global mom_clearance_plane_status
	global mom_coordinate_output_mode
	global mom_operation_name

	#можно отключить в режиме debug_mode
	#проверка на нулевую скорость резания
	if {$mom_spindle_speed == "0"} {
		 set res [MOM_display_message "WARNING! Spindle Speed $mom_operation_name: is Nul" "Alarm Message"\
			 "W" "Ok"];# "Ignorе"]
			 #if {$res == 1} { MOM_abort_program "WARNING!"}
		}
	#проверка на величину подачи для постоянной скорости резания
}

#=============================================================
proc USER_check_feedrate { } {
  global mom_spindle_mode feed_mode mom_operation_name
  
  if { [info exist mom_spindle_mode] && ($mom_spindle_mode=="SFM" || $mom_spindle_mode=="SMM") } {
			
		if { [info exists feed_mode] && $feed_mode=="MMPM" } {
			 set res [MOM_display_message "WARNING! Feed Rate in MMPM: operation: $mom_operation_name" "Alarm Message"\
			 "W" "Ok"];# "Ignorе"]
			 #if {$res == 1} { MOM_abort "WARNING!"}
		}
		
	}

}


#=============================================================
proc USER_tool_change { } {
#=============================================================
#<Mitiouk> 04-03-2019
global mom_tool_number
global mom_tool_name
global mom_sys_lathe_x_double
global mom_out_angle_pos
global tool_holder_spindle_axis
global mom_post_in_simulation
global old_tool
global mom_tool_adjust_register
global user_debug_mode
global mom_angl_pos
#
global prev_start_tool_name;    # Предыдущий угол держателя
global prev_start_tool_angle;      # Предыдущий угол оси инструмента
global prev_start_tool_holder_spindle_axis


  if { ![info exists prev_start_tool_name] } { set prev_start_tool_name "" }
  if { ![info exists prev_start_tool_angle] } { set prev_start_tool_angle "" }
  if { ![info exists prev_start_tool_holder_spindle_axis] } { set prev_start_tool_holder_spindle_axis "" }


# PREDEFINITION PROC
PB_CMD_check_flip_a_axis
PB_CMD_GAMMA_2000_TC_START_ANGLE_DEFINITION
#

#if {![info exists old_tool]} {set old_tool $mom_tool_name} else {
#    if {$old_tool==$mom_tool_name} {return 0} else {set old_tool $mom_tool_name}
#    }

 #if { [info exists user_debug_mode] && $user_debug_mode==1} {
 #   MOM_output_literal ";foo mom_out_angle_pos(0) $mom_out_angle_pos(0)"
 #   MOM_output_literal ";foo tool_holder_spindle_axis $tool_holder_spindle_axis"
 #}
 # used $mom_tool_adjust_register $mom_out_angle_pos(0) $tool_holder_spindle_axis
  if { $prev_start_tool_name != $mom_tool_name || \
       $prev_start_tool_angle != $mom_out_angle_pos(0) || \
	   $prev_start_tool_holder_spindle_axis != $tool_holder_spindle_axis } {
  
  #USER_return_to_home
  MOM_do_template tool_select
  PB_call_macro TC

  set prev_start_tool_name $mom_tool_name
  set prev_start_tool_angle $mom_out_angle_pos(0)
  set prev_start_tool_holder_spindle_axis $tool_holder_spindle_axis
  }
  
  
  
#MOM_output_literal ";mom_angl_pos=$mom_angl_pos(0)"
 if {![info exists mom_tool_adjust_register]|| $mom_tool_adjust_register==0} {

    set ansv 0
        MOM_display_message "Tool ${mom_tool_name}: ajustment register not defined! Set default value: D1!" " " "Ok"
     if {$ansv==2} { MOM_abort }
         set $mom_tool_adjust_register 1
 }
  

}
#=============================================================
proc USER_return_to_home { } {
#=============================================================
#<Mitiouk> 04-03-2019
#
#
  global mom_current_oper_is_last_oper_in_program
  global mom_current_oper_is_last_oper_in_program
  global mom_next_oper_has_tool_change
  global home_pos_mode
  global user_CRH
  global user_DRH
  global user_home_pos
  
  
   #OPTIONAL OUT M0
   if {[USER_check_block_stop]} { MOM_do_template user_stop }
   
   MOM_do_template user_trafoof
   
  #MOM_output_literal "; !!!!!!!!!!!$home_pos_mode"
  
 #if { ([info exists mom_current_oper_is_last_oper_in_program] && $mom_current_oper_is_last_oper_in_program=="YES" ) || \
 #		([info exists mom_next_oper_has_tool_change] && $mom_next_oper_has_tool_change=="YES") }
#
#		{
		
		if { [info exist home_pos_mode]} {
            switch $home_pos_mode {
            "USER" {				
				MOM_output_literal "; Return to User Home Position!"
				
				MOM_do_template return_home_x
				if {[info exists user_home_pos(1)]} {
					MOM_do_template return_home_y
				 }
				if {[info exists user_home_pos(2)]} {
					MOM_do_template return_home_z
				 }
				}
            "CYCLE" {
                if {![info exists user_CRH] } { set user_CRH 710}
				if {![info exists user_DRH] } { set user_DRH 0}
				MOM_do_template cycle_ret_to_home
				#MOM_output_literal "$user_CRH"
				}
			"NONE" { 
				#MOM_do_template stop
			}
			
			"AUTO" { }
            }
        }  else {
		 #Сообщаем, что возврат затруднён, будет применена стандартная стратегия
		  MOM_display_message "WARNING! SET UDE HOME POSITION. Output  Home to Position code will be default!"
		  global mom_sys_home_pos
		  global user_home_pos
		  if { ![info exists user_home_pos] } {
		    set user_home_pos(0) 750.
			set user_home_pos(1) -108.2
			set user_home_pos(2) 1300.0
		  
		  }
		 
		  MOM_output_literal "DIAMOF"
		  MOM_output_literal ";Warning! Move to Default Home Position"
		  MOM_do_template return_home_x
		  MOM_do_template return_home_y
		}
        MOM_do_template end_of_path
#     }

}

#=============================================================
proc USER_transmit { } {
#=============================================================

  global mom_siemens_transmit_mode
  global mom_coordinate_output_mode
  global mom_machine_mode
  global spindle_name mom_channel_id mom_fixture_offset_value
  global mom_sys_lathe_x_factor mom_sys_lathe_i_factor mom_pos

  #if {[string compare $mom_machine_mode "MILL"]} { return 0 }

  #if {[info exists mom_siemens_transmit_mode] && $mom_siemens_transmit_mode == "ON"} {
     if {$mom_fixture_offset_value == 2} {
        set spindle_name 3
     } else {
        set spindle_name 4 }
	 
	 PB_CMD_USER_LOCK_C_AXIS
	 MOM_force once forth_axis
     set mom_coordinate_output_mode "TRANSMIT"
     MOM_enable_address Y
     
	 #MOM_force once fifth_axis
	 
	 MOM_disable_address forth_axis
     MOM_disable_address fifth_axis
	 
     #MOM_output_literal "; $mom_pos(4)"
     #MOM_do_template transmit_xyz
	 #MOM_do_template transmit
 #  } else { } 
}

#=============================================================
proc USER_output_Sinumerik_setting { } {
#=============================================================
# This command is used to output Sinumerik 840D high speed machining and 5 axis codes.
  global mom_siemens_5axis_output_mode
  global mom_siemens_coord_rotation
  global mom_5axis_control_mode
  global mom_oper_method
  global mom_kin_machine_type
  global sinumerik_version
  global mom_operation_type
  global mom_siemens_method
  global mom_siemens_tol_status
  global mom_siemens_tol
  global mom_siemens_smoothing
  global mom_siemens_compressor
  global mom_siemens_feedforward
  global mom_siemens_5axis_mode
  global mom_siemens_ori_coord
  global mom_siemens_ori_inter
  global mom_siemens_ori_def
  global mom_siemens_milling_setting
  global mom_kin_4th_axis_leader
  global mom_kin_5th_axis_leader
  global mom_tool_axis_type
  global mom_warning_info mom_operation_name
  global mom_siemens_5axis_output
  global mom_machine_mode
  global user_traori_mode
  global tol
  global debug_mode
  

  if {[info exists debug_mode] && $debug_mode=="ON"} {
   MOM_output_literal ";foo USER_output_Sinumerik_setting START:"
   if {[info exists sinumerik_version]} {MOM_output_literal ";foo High Speed Machining for: $sinumerik_version"} }
  
  if {[string compare $mom_machine_mode "MILL"]} { return  }
# ARC OUTPUT MODE
  
  global mom_kin_arc_output_mode save_mom_kin_arc_output_mode
  global mom_kin_helical_arc_output_mode save_mom_kin_helical_arc_output_mode
  global mom_siemens_compressor
  
  if {[info exists mom_siemens_compressor] && $mom_siemens_compressor != "COMPOF"} {
     if {![info exists save_mom_kin_arc_output_mode]} {
        set save_mom_kin_arc_output_mode $mom_kin_arc_output_mode
        set save_mom_kin_helical_arc_output_mode $mom_kin_helical_arc_output_mode
     }
     set mom_kin_arc_output_mode "LINEAR"
     set mom_kin_helical_arc_output_mode "LINEAR"
     MOM_reload_kinematics
	 
  } else {
		 #MOM_output_literal ";>>>>I here"
		set mom_kin_arc_output_mode "FULL_CIRCLE"
		set mom_kin_helical_arc_output_mode "FULL_CIRCLE"
		MOM_reload_kinematics
		}
#OUT_TOLERANCE
# If tolerance is redefined in UDE, output again as User Defined

 global cycle832_tolm
 global cycle832_v832
 global cycle832_tol
 global cycle832_otol
 
   if {![string match "Point to Point" $mom_operation_type] && ![string match "Hole Making" $mom_operation_type] && \
         ![string match "Drilling" $mom_operation_type] && ![string match "Generic Motion" $mom_operation_type]} {
        # Output High Speed Machining codes
		if { ![info exists mom_siemens_tol_status] } { set mom_siemens_tol_status "System" }
		#if { ![info exists mom_siemens_tol] } { set mom_siemens_tol 0.01 }
		
		#set tol [format "%.3f" $mom_siemens_tol]
		#set cycle832_tol $tol   
		#set cycle832_otol 1
		USER_check_block_CYCLE832
		#PB_call_macro CYCLE832_v47
		
		
		
		if { [info exists mom_siemens_compressor] && $mom_siemens_compressor=="ON" } {
			MOM_output_literal "COMPON; Compressor_ON"} else {
			MOM_output_literal "COMPOF; Compressor OF"
			}
		if { [info exists mom_siemens_smoothing] && $mom_siemens_smoothing=="ON" } { 
		  MOM_output_literal "G642; Smoothing ON" 
		  } else { 
		  MOM_output_literal "G60; Fine Position ON" }
 
 # Output 5 axis orientation coordinate and interpolation mode
  if {![string match "3_axis_mill*" $mom_kin_machine_type] && [string match "TRAORI*" $mom_siemens_5axis_mode]} {
     MOM_output_literal ";"
     if {[info exists mom_siemens_ori_coord]} {
        MOM_output_literal "$mom_siemens_ori_coord"
     }
     if {[info exists mom_siemens_ori_inter]} {
        #MOM_output_literal "$mom_siemens_ori_inter"
     }
  }

   if {[info exists user_debug_mode]} {
     MOM_output_literal ";foo USER_output_Sinumerik_setting END:"
     if {[info exists sinumerik_version]} {MOM_output_literal ";foo High Speed Machining for: $sinumerik_version" } }
}
}
#=============================================================
proc USER_TRANS_5A { } { 
#=============================================================
# TRANSMIT TRAORI
# Вставить строку для вывода шаблона user_trans
#<Mitiouk> 16-02-219
global mom_siemens_transmit_mode
#global mom_coordinate_output_mode
#global mom_machine_mode
#global mom_out_angle_pos
#global mom_operation_type
#global mom_post_in_simulation
global mom_siemens_5axis_output
global mom_siemens_5axis_mode
global mom_siemens_5axis_output_mode
global mom_fixture_offset_value
global trans_5a_on
global user_traori_mode
global SP
global mom_ude_tracyl_diameter
global mom_ude_tracyl_mode

  if {[info exist debug_mode]&&[$debug_mode]} {
    MOM_output_literal "; USER_TRANS_5A"
    MOM_output_literal "; mom_siemens_5axis_output_mode=$mom_siemens_5axis_output_mode"
    MOM_output_literal "; mom_siemens_5axis_output=$mom_siemens_5axis_output"	
  }
  
  set user_traori_mode "TRANS_OFF"

  if { [info exists mom_siemens_5axis_output_mode] && $mom_siemens_5axis_output_mode==1 } {
     if { [info exists mom_siemens_5axis_output] && $mom_siemens_5axis_output=="TRAORI" } {
        if { $mom_fixture_offset_value == 1 } { set SP 4 } else { set SP 3 }
		set user_traori_mode "TRANS_5A($SP,\"BC\",0)"
	 }
  }
  if { [info exists mom_siemens_5axis_output_mode] && $mom_siemens_5axis_output_mode==0} {
	  if { [info exists mom_siemens_transmit_mode] && $mom_siemens_transmit_mode=="ON"} {
	  #присвоить TRANSMIT
	  if { $mom_fixture_offset_value == 1 } { set SP 4 } else { set SP 3 }
      set user_traori_mode "TRANSMIT_S$SP"               
      } 
	}
    # Вставить TRACYL_S3 TRACYL_S4
	# Базовый диаметр
	if { [info exists mom_siemens_5axis_output_mode] && $mom_siemens_5axis_output_mode==0} {
	  if { [info exists mom_ude_tracyl_mode] && $mom_ude_tracyl_mode=="ON"} {
	#  #присвоить TRANSMIT
	  if { $mom_fixture_offset_value == 1 } { set SP 4 } else { set SP 3 }
      set user_traori_mode "TRACYL_S$SP\($mom_ude_tracyl_diameter\,512)"               
   #  } 
	}
} 

#=============================================================
proc USER_check_block_stop { } {
#=============================================================
# <Mitiouk> 08-03-2019
# This custom command should return
#   1 : Output
#   0 : No output
#  check M0 mode
   
   global user_STOP
   global mom_user_stop_mode
   global user_stop_mode
   global user_program_stop_code
   
    if { ![info exists user_program_stop_code] }  {set user_program_stop_code 0}
    if { ![info exists user_STOP] } { set user_program_stop_code 0; return 1}
	if {$user_STOP!=0} { return 1 }
	return 0
}

#==========================================================
proc USER_sync_spindle_output {} {
#==========================================================
# output L for GAMMA 2000

  global mom_ude_following_spindle_name
  global dpp_spindle_sync_is_output

  if {[info exists mom_ude_following_spindle_name]} {
     if { ![info exists dpp_spindle_sync_is_output] } {
        #MOM_output_literal "$mom_ude_following_spindle_name=0"
        #PB_call_macro COUPDEF
        #PB_call_macro COUPON
        MOM_output_literal "L726(0.0); Synchronous Mode Start"
		set dpp_spindle_sync_is_output 1
     } else {
        MOM_display_message "One spindle synchronize coupling has been defined, please delete it before specify the other one.\
                             \nPost Processing will be aborted." "Postprocessor error message" "E"
        MOM_abort_program
     }
  }
}

#=============================================================
proc USER_cancel_spindle_synchronous { } {
#=============================================================
# Cancle spindle synchronous. Take note this UDE is coupled with "Set Spindle Sync" UDE.
#
# 2014-02-27 levi - Initial version.
#<Mitiouk> 09-03-2019 for Gamma 2000 TC

  global mom_ude_following_spindle_name
  global mom_ude_master_spindle_name
  global mom_ude_speed_ratio_numerator
  global mom_ude_speed_ratio_denominator
  global mom_ude_block_behavior
  global mom_ude_coupl_type
  global dpp_spindle_sync_is_output

  if {![info exists mom_ude_following_spindle_name]} {
     MOM_display_message "You haven't specify a spindle synchronous UDE yet! Please specify it before.\
                          \nPost Processing will be aborted." "Postprocessor error message" "E"
     MOM_abort_program
  } else {
     #PB_call_macro COUPOF
     #PB_call_macro COUPDEL
     MOM_output_literal "L727; Synchronous Mode Stop"
	 catch {unset mom_ude_following_spindle_name}
     catch {unset mom_ude_master_spindle_name}
     catch {unset mom_ude_speed_ratio_numerator}
     catch {unset mom_ude_speed_ratio_denominator}
     catch {unset mom_ude_block_behavior}
     catch {unset mom_ude_coupl_type}
     catch {unset dpp_spindle_sync_is_output}
  }
}

#=============================================================
proc USER_handle_subspindle_motion { } {
#=============================================================
  global mom_move_type_name
  global mom_ssm_active_travel_to_fixed_stop
  global mom_ssm_clearance_plane_status
  global mom_ssm_clearance_plane
  global mom_ssm_custom_feed_rate
  global mom_ssm_custom_feed_rate_status
  global mom_ssm_fixed_stop_torque
  global mom_ssm_fixed_stop_torque_status
  global mom_ssm_fixed_stop_window
  global mom_ssm_fixed_stop_window_status
  global mom_ssm_move_to_home
  global mom_smm_move_direct
  global mom_ssm_move
  global mom_ssm_from_point
  global mom_ssm_from_point_pos
  global mom_ssm_to_point
  global mom_ssm_to_point_pos
  global mom_ssm_clearance_plane_mode

  global mom_cm_main_sub
  global mom_cm_jaw_position
  global mom_cm_jaw_mode

  global mom_wp_transfer_dir

  global flag_generic_motion_device; # Флаг использования под операций 
  
  

  if {![info exists mom_move_type_name]} {return}
  
  
  #MOM_output_literal ";foo $mom_move_type_name"; return
  
  if {![string compare $mom_move_type_name "Workpiece_Transfer"]} {
     if {[string match "*M140*" $mom_wp_transfer_dir]} {
        MOM_output_literal "M140"
     } else {
        MOM_output_literal "M141"
     }
     set flag_generic_motion_device 1
  }

  if {![string compare $mom_move_type_name "Chuck_Motion"]} {
     
	 # M433 - Open chuck on main spindle
     # M333 - Open chuck on sub spindle
     # M437 - Close chuck on main spindle
     # M337 - Close chuck on sub spindle
	 
	 #MOM_output_literal "; Spindle $mom_cm_main_sub"
     
	 if {[string match "Main*" $mom_cm_main_sub]} {
        set side "MAIN"
        set side_num "4"
     } else {
        set side "SUB"
        set side_num "3"
     }
	 
     if {$mom_cm_jaw_mode == "Position"} {
        set jaw_pos [format "%.3f" $mom_cm_jaw_position]
        MOM_output_literal "G53 G0 CHUCK_$side=$jaw_pos"
     } else {
        if {$mom_cm_jaw_mode == "Open"} {
           set status "33"
        } else {
           set status "37"
        }
        MOM_output_literal "M$side_num$status ; $mom_cm_jaw_mode Chuck on $side Spindle"
		#MOM_output_literal "M1$side_num$status ; $mom_cm_jaw_mode Chuck on $side Spindle"
     }
     set flag_generic_motion_device 1
  }

  if {![string compare $mom_move_type_name "Subspindle_Motion"]} {
     # Move to Home
     if {[info exists mom_ssm_move_to_home] && $mom_ssm_move_to_home == 1} {
        MOM_output_literal "G90 G53 G0 Z3=0" ;#можно использовать SUPA?
     } else {
     # Send Sub Spindle over to Main Spindle
        if {[info exists mom_smm_move_direct] && $mom_smm_move_direct == 1} {
           set z_pos $mom_ssm_move
           set g_mode "G90"
        } elseif {[info exists mom_ssm_to_point_pos] && [info exists mom_ssm_from_point_pos]} {
           set z_pos [expr $mom_ssm_to_point_pos(2) - $mom_ssm_from_point_pos(2)]
           set g_mode "G91"
        }

        # Get feed rate value
        if {[info exists mom_ssm_custom_feed_rate_status] && $mom_ssm_custom_feed_rate_status == 1} {
           set subspindle_feed [format "%.2f" $mom_ssm_custom_feed_rate]
        } else {
           set subspindle_feed 250
        }

        # Fixed stop, fixed stop torque and window
        if {[info exists mom_ssm_active_travel_to_fixed_stop] && $mom_ssm_active_travel_to_fixed_stop == 1} {
           set fixed_stop "FXS\[Z3\]=1"
           if {[info exists mom_ssm_fixed_stop_torque_status] && $mom_ssm_fixed_stop_torque_status == 1} {
              append fixed_stop " FXST\[Z3\]=[format "%.3f" $mom_ssm_fixed_stop_torque]"
           }
           if {[info exists mom_ssm_fixed_stop_window_status] && $mom_ssm_fixed_stop_window_status == 1} {
              append fixed_stop " FXSW\[Z3\]=[format "%.3f" $mom_ssm_fixed_stop_window]"
           }
        } else {
           set fixed_stop ""
        }

        if {[info exists z_pos]} {
           set z_pos [format "%.3f" $z_pos]
           if {[string match "Before*" $mom_ssm_clearance_plane_mode]} {
              set mom_ssm_clearance_plane [format "%.3f" $mom_ssm_clearance_plane]
              MOM_output_literal "$g_mode G53 G0 Z3=$z_pos + $mom_ssm_clearance_plane"
              MOM_output_literal "$g_mode G53 G1 Z3=$z_pos F$subspindle_feed $fixed_stop"
           } elseif {[string match "After*" $mom_ssm_clearance_plane_mode]} {
              set mom_ssm_clearance_plane [format "%.3f" $mom_ssm_clearance_plane]
              MOM_output_literal "$g_mode G53 G1 Z3=$z_pos - $mom_ssm_clearance_plane F$subspindle_feed $fixed_stop"
              MOM_output_literal "$g_mode G53 G0 Z3=$z_pos"
           } else {
              MOM_output_literal "$g_mode G53 G0 Z3=$z_pos $fixed_stop"
           }
           if {$g_mode == "G91"} { MOM_output_literal "G90"}
           if {[info exists mom_ssm_active_travel_to_fixed_stop] && $mom_ssm_active_travel_to_fixed_stop == 1} {
              MOM_output_literal "ERR1: IF \$AA_FXS\[Z3\] <> 2 GOTOF CONT1"
              MOM_output_literal "MSG(\"Travel to fixed stop not reached\")"
              MOM_output_literal "M0"
              MOM_output_literal "STOPRE"
              MOM_output_literal "GOTOB ERR1"
              MOM_output_literal "CONT1:"
           }
        }
     }
     set flag_generic_motion_device 1
  }
}

#=============================================================
proc USER_output_diameter_mode { } {
#=============================================================
  global mom_channel_id
  global mom_machine_mode
  global mom_sys_lathe_x_double
  global mom_operation_type
  global mom_diamof_fl

 
 if { ![info exists mom_diamof_fl] } { set mom_diamof_fl 1 }
 if { ![string compare $mom_operation_type "Generic Motion"] } { return }
 if { ![string compare $mom_operation_type "MILL"] } {
   MOM_output_literal "DIAMOF TOWSTD"
   return }

  if { $mom_sys_lathe_x_double == 1 } { MOM_output_literal "DIAMOF TOWSTD" } elseif {
  $mom_sys_lathe_x_double == 2 } { MOM_output_literal "DIAMON TOWSTD" }

}

#=============================================================
proc USER_check_SPEED { } {
#=============================================================
#<Mitiouk> 04-03-2019
}

#=============================================================
proc USER_set_cycle800_tc { } {
#=============================================================
  global cycle800_tc
  global mom_fixture_offset_value SP
  if { $mom_fixture_offset_value == 1 } { set SP 4 } else { set SP 3 }
  if {$SP==4} { set cycle800_tc "HSP"} else {set cycle800_tc "GSP"}
}


#=============================================================
proc USER_check_block_CYCLE832 { } {
#=============================================================
# This custom command should return
#   1 : Output BLOCK
#   0 : No output
#
# 22-Dec-2016 shuai - CYCLE832 enhancement.
#

   global mom_logname
   global mom_operation_type
   global mom_operation_name
   global mom_cutmthd_libref
   global mom_machine_mode
   global mom_siemens_method
   global mom_siemens_smoothing
   global mom_siemens_compressor
   global mom_siemens_feedforward
   global mom_siemens_5axis_output
   global mom_siemens_tol_status
   global mom_siemens_tol
   global mom_siemens_rotary_tol
   global mom_siemens_top_surface_smooth
   global mom_siemens_5axis_output_mode
   global mom_siemens_transmit_mode
   global sinumerik_version
   global cycle832_tolm
   global cycle832_v832
   global cycle832_tol
   global cycle832_otol

   if {[string compare $mom_machine_mode "MILL"]} { return 0 }

   if {[info exists mom_siemens_transmit_mode] && $mom_siemens_transmit_mode == "ON"} { return 0 }
   set cycle832_tol 0
   #если настроена технология
   if {[info exists mom_cutmthd_libref]} {

      switch $mom_cutmthd_libref {

         OPD0_00021 {set mom_siemens_method "ROUGHING"}
         OPD0_00022 {set mom_siemens_method "ROUGH-FINISHING"}
         OPD0_00023 {set mom_siemens_method "FINISHING"}
         default    {set mom_siemens_method "DESELECTION"}
      }

   } else {

      set mom_siemens_method "DESELECTION"
   }
   
   # If tolerance is redefined in UDE, output again as User Defined.
   if {![info exists mom_siemens_tol_status] } {
      set mom_siemens_tol_status "System"
	  
   }
   global siemens_top_surface_smooth
   
   if { [info exists mom_siemens_top_surface_smooth] } {
     switch $mom_siemens_top_surface_smooth {
		"TOP SURFACE SMOOTH OFF" {set mom_siemens_method "ROUGHING"}
		"TOP SURFACE SMOOTH ON"  {set mom_siemens_method "ROUGH-FINISHING"}
		"ADVANCED SURFACE" {set mom_siemens_method "FINISHING"}
		default  {set mom_siemens_method "DESELECTION"}
	 }
   }

   if {[info exists mom_siemens_tol]} {
      set tol [format "%.6f" $mom_siemens_tol]
	  set cycle832_tol $tol
      #MOM_output_literal "_camtolerance=$tol  ;  User Defined"
   }

   
   # <lili 2013-06-20> Enhancement for new drilling cycle operation type.
   # Output cycle832 for milling operation.
   if {[string compare "Point to Point" $mom_operation_type] && \
       [string compare "Hole Making" $mom_operation_type] && \
       [string compare "Drilling" $mom_operation_type]} {

      # If compressor is on, set circular and helical motion to linear.
      PB_CMD_arc_compressor_mode

      # <22-Dec-2016 shuai> - Add the output conditions when NCK version is 4.5 and 4.7.
      #                       Adjust the TCL codes format to make the logic more clear.

			
            if {[PB_CMD_detect_5axis_tool_path]} {

               set rot "_ORI"
               set cycle832_otol 1; #$mom_siemens_rotary_tol
            }

            #if {![string compare "V47" $sinumerik_version] } {
#
#               if {[info exists mom_siemens_top_surface_smooth] && [string compare "" $mom_siemens_top_surface_smooth]} {
#                  set top "${mom_siemens_top_surface_smooth}+"
#               }
#            }
			
            if { [info exists mom_siemens_method] } {
			switch -- $mom_siemens_method {

               "ROUGHING"        {set cycle832_tolm "_ROUGH"}
               "ROUGH-FINISHING" {set cycle832_tolm "_SEMIFIN"}
               "FINISHING"       {set cycle832_tolm "_FINISH"}

               default           {

                  set cycle832_tolm "_OFF"
                  set top ""
                  set rot ""
                  set cycle832_otol 1
               }
            }
			
			} else {
			  set rot ""
              set top ""

              set cycle832_tol 0; #"_camtolerance"
              set cycle832_otol 1
			}

            #set cycle832_tolm $top$rot$cycle832_tolm

            PB_call_macro CYCLE832_v47

            return 0
      
      
   }

   return 0

}

#=========================================================
proc USER_TRAORI_INIT { } {
#=========================================================
# выводить углы насильно

}