#uplevel #0 {
#=============================================================
proc USER_SET_KINEMATIC { } {
#=============================================================
uplevel #0 {
                        global mom_ude_a_axis_max mom_ude_a_axis_min
						global mom_current_motion
						global mom_kin_4th_axis_max_limit mom_kin_4th_axis_min_incr
						global mom_kin_4th_axis_min_limit mom_kin_4th_axis_plane
						#global mom_kin_5th_axis_leader mom_kin_5th_axis_limit_action
						#global mom_kin_4th_axis_leader mom_kin_4th_axis_limit_action
						global mom_kin_5th_axis_max_limit mom_kin_5th_axis_min_incr
						global mom_kin_5th_axis_min_limit mom_kin_5th_axis_plane

                        #set mom_kin_4th_axis_limit_action "Warning"
						#set mom_kin_5th_axis_limit_action "Warning"
                        #if {[info exists mom_ude_b_axis_max]} {
						#	set mom_kin_4th_axis_max_limit $mom_ude_b_axis_max
						#}
						
                        #set mom_kin_4th_axis_min_incr "0.0001"
						#if {[info exists mom_ude_b_axis_min]} {
						#	set mom_kin_4th_axis_min_limit $mom_ude_b_axis_min
						#}
						
                        #set mom_kin_5th_axis_max_limit "360"
                        #set mom_kin_5th_axis_min_incr "0.0001"
                        #set mom_kin_5th_axis_min_limit "-360"
						
						#if {[info exists mom_current_motion]} {
						#MOM_output_literal ";FOO mom_current_motion=$mom_current_motion" }
						
						#if {![string compare $mom_current_motion "initial_move"] || ![string compare $mom_current_motion "first_move"]} {
						#}
						
						if {[info exists mom_ude_a_axis_max] && [info exists mom_ude_a_axis_min]} {
						  set  mom_kin_4th_axis_max_limit $mom_ude_a_axis_max
						  set  mom_kin_4th_axis_min_limit $mom_ude_a_axis_min
						} else {
						  set  mom_kin_4th_axis_max_limit 15.0
						  set  mom_kin_4th_axis_min_limit -115.0
						}
						#MOM_output_to_listing_device ">>> mom_kin_4th_axis_max_limit: $mom_kin_4th_axis_max_limit"
						#MOM_output_to_listing_device ">>> mom_kin_4th_axis_min_limit: $mom_kin_4th_axis_min_limit"
						
                        MOM_reload_kinematics
                        global mom_pos mom_out_angle_pos
                        MOM_reload_variable -a mom_pos
                        MOM_reload_variable -a mom_out_angle_pos
            } ;#uplevel

}
#=============================================================
proc USER_CHECK_LIMITS { } {
#=============================================================
	#Проверка на превышение ограничений перемещения
	#Входные данные:
	#Конечная точка перемещения: mom_pos
	#Угол поворота координат trans_c
	#Возвращаемое значение:
	#		0-нет превышения
	#		1-превышение
	
	global mom_pos machine_pos mom_msys_origin trans_c trans_ic
	global RAD2DEG
	global LIM_MAX_X LIM_MIN_X LIM_MAX_Y LIM_MIN_Y LIM_MAX_A LIM_MIN_A LIM_MAX_C LIM_MIN_C
	
	#установки пользователя
	
	set LIM_MAX_X 	 690.0
	set LIM_MIN_X 	-690.0
	set LIM_MAX_Y   1010.0
	set LIM_MIN_Y 	-140.0
	
	#array set c_pos [list 0 0 1 0 2 0]
	array set machine_pos [list 0 0 1 0 2 0]	
	array set _matrix [list 11 0 21 0 31 0 12 0 22 0 32 0]
	VMOV 3 mom_pos machine_pos
	
	#MOM_output_to_listing_device ">>>WCS:0 WX=$mom_pos(0) WY=$mom_pos(1) WZ=$mom_pos(2)"
	
	set machine_pos(0) [expr $machine_pos(0) + $mom_msys_origin(0)] 
	set machine_pos(1) [expr $machine_pos(1) + $mom_msys_origin(1)]
	set machine_pos(2) [expr $machine_pos(2) + $mom_msys_origin(2)]
	
	#MOM_output_to_listing_device ">>>MCS:0 add Angle:$trans_c MX=$machine_pos(0) MY=$machine_pos(1) MZ=$machine_pos(2)"	
	#матрица поворота
	set _matrix(11) [expr cos($trans_c / $RAD2DEG)]; set _matrix(21) [expr sin($trans_c / $RAD2DEG)* -1 ]	; set _matrix(31) $mom_msys_origin(0)
	set _matrix(12) [expr sin($trans_c / $RAD2DEG)]; set _matrix(22) [expr cos($trans_c / $RAD2DEG)]		; set _matrix(32) $mom_msys_origin(1)
	
	set mx [expr $machine_pos(0) * $_matrix(11) - $machine_pos(1) * $_matrix(21) ]
	set my [expr $machine_pos(0) * $_matrix(21) + $machine_pos(1) * $_matrix(22) ]
	
	set  machine_pos(0) $mx
	set  machine_pos(1) $my
	
	#проверка возвращает: координату и квадрант
	set over_axis "NORM"
	if { $machine_pos(0)>$LIM_MAX_X || $machine_pos(0)<$LIM_MIN_X} { set over_axis "X-OVER" }
	if { $machine_pos(1)>$LIM_MAX_Y || $machine_pos(1)<$LIM_MIN_Y} { set over_axis "Y-OVER" }
	
	global user_cvadrant
	#catch {unset user_cvadrant}
	set user_cvadrant 0.0
	if {$machine_pos(0) > 0} {
		set user_cvadrant 1.0
	} else {
		set user_cvadrant -1.0
	}	
	#MOM_output_to_listing_device ">>>trans_c:$trans_c trans_ic:$trans_ic user_cvadrant:$user_cvadrant"
	#MOM_output_to_listing_device ">>>MCS1:MX=$machine_pos(0) MY=$machine_pos(1) MZ=$machine_pos(2)"
	#MOM_output_to_listing_device ">>>MCS0:MX=$machine_pos(0) MY=$machine_pos(1) MZ=$machine_pos(2)"
	#MOM_output_to_listing_device ">>>MCS1:Solve $over_axis MX=$machine_pos(0) MY=$machine_pos(1) MZ=$machine_pos(2)"
	
	if {$over_axis!="NORM"} { 
		
		return 1 
	}
	
	return 0	
}

#=============================================================
proc USER_UDE_TRANSMIT { } {
#=============================================================
	global user_transmit_mode mom_ude_transmit mom_arc_mode 
	global restore_arc_mode mom_linearization_flag mom_kin_arc_output_mode
	
	set restore_arc_mode $mom_arc_mode	
	if {[info exists mom_ude_transmit] && $mom_ude_transmit=="ON"} {
		set user_transmit_mode ON
		set mom_kin_arc_output_mode "LINEAR"
		set mom_linearization_flag TRUE
		MOM_reload_kinematics
		MOM_reload_variable -a mom_arc_mode
		 
		#MOM_output_to_listing_device ">>> USER_UDE_TRANSMIT user_transmit_mode: $user_transmit_mode"
	} else {
	catch {unset user_transmit_mode }
	}
	
}

#=============================================================
proc USER_POS_ANALIZER { } {
#=============================================================
# Анализ траектории
# Вычисление направления обработки
# _angl Угол направления обработки относительно СК детали
	global mom_pos mom_nxt_alt_pos mom_msys_origin mom_contact_normal machine_pos
	global mom_prev_pos
	global counter
	global uvect _tg _angl
	global RAD2DEG
	global over_axis
	global trans_c
	global trans_ic
	global mom_current_motion
	global _matrix
	global user_transmit_mode
	global user_cvadrant
	global _angl
	
	if {![info exists user_transmit_mode]} {return}
	
	if {![info exists _angl]} {set _angl 0}
	if {![info exists trans_c]} {set trans_c 0}
	if {![info exists trans_c]} {set trans_c $mom_pos(4)}
	
	set user_transmit_mode "OFF"
	#calculate vector mom_pos перемещение линейное
	if {[string match "linear*" $mom_current_motion]} {
		array set v1 [list 0 [expr $mom_pos(0) - $mom_prev_pos(0)] 1 [expr $mom_pos(1) - $mom_prev_pos(1)]]
		set _angl [expr atan2($v1(1),$v1(0))*$RAD2DEG]
		set r [expr sqrt($v1(0)*$v1(0) + $v1(1)*$v1(1))]
		#MOM_output_to_listing_device ">>> Move: Linear L=$r Angle=$_angl "
	}
	
	#calculate vector mom_pos перемещение по дуге находим угол между векторами
	if { [string match "circular*" $mom_current_motion] } {
		global mom_pos_arc_center
		array set v1 [list 0 [expr $mom_prev_pos(0) - $mom_pos_arc_center(0)] 1 [expr $mom_prev_pos(1) - $mom_pos_arc_center(1)]]
		array set v2 [list 0 [expr $mom_pos(0) - $mom_pos_arc_center(0)] 1 [expr $mom_pos(1) - $mom_pos_arc_center(1)]]
		set r [expr sqrt($v1(0)*$v1(0) + $v1(1)*$v1(1))]
		set a1 [expr atan2($v1(1),$v1(0)) * $RAD2DEG]
		set a2 [expr atan2($v2(1),$v2(0)) * $RAD2DEG]
		set _angl [expr $a2 - $a1]
		#MOM_output_to_listing_device ">>>Move: Radius: r=$r a1=$a1 a2=$a2 Angle=$_angl"
	}
	global user_cvadrant

	#if {[string match "linear*" $mom_current_motion]} {		
	if {[USER_CHECK_LIMITS]} {		
		set rel_angle [expr $trans_c - $_angl]
		set trans_ic [expr (0 - $rel_angle)*$user_cvadrant]
		if {abs($trans_ic) > 180} {set trans_ic [expr 180.-$trans_ic]}
		set trans_c [expr $trans_c + $trans_ic]
		if {$trans_c > 360.0} {set trans_c [expr $trans_c - 360]} elseif {$trans_c < -360} {set trans_c [expr $trans_c + 360]}
		#MOM_output_to_listing_device ">>>trans_ic:$trans_ic"
		
		set user_transmit_mode "ON"
	} else {
		set user_transmit_mode "OFF"
	}
	#}
	
	
	#MOM_output_to_listing_device ">>> $trans_c"	
		
	if { ![info exists counter]} {set counter 0} else {incr counter}
	
	#MOM_output_to_listing_device ">>> Analizer |$counter| motion:$mom_current_motion mode: $user_transmit_mode"
	#MOM_output_to_listing_device ">>> 
	#MOM_output_to_listing_device ">>> X=$mom_pos(0) Y=$mom_pos(1) Z=$mom_pos(2)"
	#MOM_output_to_listing_device ">>> $_matrix(11)\t$_matrix(21)\t$_matrix(31)"
	#MOM_output_to_listing_device ">>> $_matrix(12)\t$_matrix(22)\t$_matrix(32)"
	
	
	#MOM_output_to_listing_device ">>> uvect: $uvect(0)\t $uvect(1)\t $uvect(2)"
	#MOM_output_to_listing_device ">>> Angle: $_angl"
	#MOM_output_to_listing_device ">>> MACHINE $over_axis"
	#MOM_output_to_listing_device ">>> Analizer |end|\n"
}

#=============================================================
proc USER_CHIP_CONVEYOR { } {
#=============================================================
	global mom_ude_chip_conveyor_mode mom_ude_chip_conveyor_interval mom_ude_chip_conveyor_time
	
	if { [info exists mom_ude_chip_conveyor_mode] } {
		#MOM_output_to_listing_device ">>> UDE CHIP CONVEYOR $mom_ude_chip_conveyor_mode Interval=$mom_ude_chip_conveyor_interval \[minute\ mom_ude_chip_conveyor_time=$mom_ude_chip_conveyor_time \[second\]]"
		switch $mom_ude_chip_conveyor_mode {
			"ON" {
				#MOM_output_to_listing_device ">>> UDE CHIP CONVEYOR $mom_ude_chip_conveyor_mode Interval=$mom_ude_chip_conveyor_interval \[minute\ mom_ude_chip_conveyor_time=$mom_ude_chip_conveyor_time \[second\]]"
				MOM_output_literal "CYCLE DEF 312"
				set mom_ude_chip_conveyor_interval [format "%0.0f" $mom_ude_chip_conveyor_interval]
				set mom_ude_chip_conveyor_time [format "%0f" $mom_ude_chip_conveyor_time]
				MOM_output_literal "Q1577=$mom_ude_chip_conveyor_interval ; Interval \[minute\]"
				MOM_output_literal "Q1578=$mom_ude_chip_conveyor_time ; Time \[second\]"
				MOM_output_literal "M36 ; Conveyor ON"
				MOM_output_literal ";================================"
				}
			"OFF" {
				MOM_output_literal "M35 ; Conveyor OFF"
			}
			default {}
		}
	}
}

#=============================================================
proc USER_PRESSURE_COOLANT { } {
#=============================================================
	global mom_ude_pressure_coolant 
	
	if { [info exists mom_ude_pressure_coolant] } {
		#MOM_output_to_listing_device ">>> UDE PRESSURE COOLANT is ENABLE Pressure=$mom_ude_pressure_coolant \[bar\]"
		
		set mom_ude_pressure_coolant [format "%0.1f" $mom_ude_pressure_coolant]
		MOM_output_literal "CYCL DEF 304 Q1565=$mom_ude_pressure_coolant"
		#MOM_output_literal "Q1565=$mom_ude_pressure_coolant ; coolant pressure \[bar\]"
		MOM_output_literal ";================================"
	
	}

}

#=============================================================
proc USER_TABLE_AUTO_ROTATE { } {
#=============================================================
	#global mom_pos mom_nxt_alt_pos mom_msys_origin mom_contact_normal
	
	#if { $mom_pos(1) > 1100.0 } {
		#set x [expr $mom_pos(0)+$mom_msys_origin(0)]
		#set y [expr $mom_pos(1)+$mom_msys_origin(1)]
		#MOM_output_to_listing_device ">>>USER_TABLE_AUTO_ROTATE/n"
		#MOM_output_to_listing_device ">>> UTAR X=$mom_pos(0) Y=$mom_pos(1)"
		#MOM_output_to_listing_device ">>> UTAR X=$x Y=$y"
		#MOM_output_to_listing_device ">>> UTAR I=$mom_contact_normal(0) J=$mom_contact_normal(1) K=$mom_contact_normal(2)"
		#MOM_output_to_listing_device "\n"
		#MOM_output_literal ";FOO USER_TABLE_AUTO_ROTATE"
		
	#}
}

#=============================================================
proc USER_CREATE_INI { } {
#=============================================================
#Вывод файла данных для симулятора

	#MOM_output_to_listing_device ">>> USER_CREATE_INI"
	
}

#=============================================================
proc USER_TOLERANCE_OUPUT { } {
#=============================================================
global user_ude_tolerance mom_ude_tolerance_value

if { [info exists user_ude_tolerance] } {
	if { [info exists mom_ude_tolerance_value] } {
		set mom_ude_tolerance_value [format "%+0.3f" $mom_ude_tolerance_value]
		#MOM_output_to_listing_device ">>> mom_ude_tolerance_value $mom_ude_tolerance_value" 
		MOM_output_literal ";================================"
		MOM_output_literal "CYCL DEF 32.0 TOLERANCE"
		MOM_output_literal "CYCL DEF 32.1 T$mom_ude_tolerance_value"
		MOM_output_literal "CYCL DEF 32.2 HSC-MODE:0 TA0.1"
		MOM_output_literal ";================================"
		} else {
		set mom_ude_tolerance_value "0.1"
		}
 }
 
 
}

#=============================================================
proc USER_SEQUENCE_NUMBER { } { 
#=============================================================
   global mom_sequence_mode
   global mom_sequence_number
   global mom_sequence_increment
   global mom_sequence_frequency
   global mom_sequence_text
   set mom_sequence_mode ON
   #MOM_output_to_listing_device ">>> USER_SEQUENCE_NUMBER"
   #if { [info exists mom_sequence_mode] && $mom_sequence_mode != Off } {
    #MOM_output_to_listing_device ">>> USER_SEQUENCE_NUMBER mom_sequence_mode: $mom_sequence_mode "
	MOM_set_seq_on
   #} else { 
   # set  mom_sequence_mode OFF 
	#}

}

#=============================================================
proc USER_PROGRAM_START { } {
#=============================================================
  global user_Q1 dpp_TNC_m128_feed_value
  
  set user_Q1 10
  MOM_output_literal "FN 0: QL1=$user_Q1 ; OPER_NUMBER"
   MOM_output_literal "FN 0: QL2=$dpp_TNC_m128_feed_value ;RAPID FEED mm/min"
  #MOM_output_literal "CALL LBL 998 ; RESET"
  MOM_do_template return_home_z
  MOM_force once M
  MOM_do_template return_home_xy
  #MOM_output_literal "L A0.0 C0.0 FQL2"; #"L A0.0 C0.0 FQL2 M91"
  MOM_do_template move_start_axis
  MOM_output_literal ";================================"
  MOM_output_literal "CYCL DEF 300 Q1597=+1 Q1598=+3 Q1599=+3; PRODUVKA SOJ"
  
}

#=============================================================
proc USER_START_OPERATION_OUTPUT { } { 
#=============================================================
  global user_Q1
  global user_initial_output
  
	if { ![info exists user_Q1] } { set user_Q1 0}
	if  { ![info exists user_initial_output] } { 
		set user_initial_output 0 
		PB_CMD_detect_local_offset
		PB_CMD_define_fixture_csys
		#tolenace setting output
		USER_TOLERANCE_OUPUT
		MOM_output_literal "FN 11: IF +QL1 GT +9 GOTO LBL QL1"
		
		#MOM_output_literal ";================================"
		USER_out_LBL
		} else {
			incr user_initial_output 1
			incr user_Q1 10
			#MOM_output_literal ";"
			#MOM_output_literal ";===== start operation <$user_Q1> ====="
			USER_out_LBL
		} 
	USER_COOLANT
}

#=============================================================
proc USER_out_LBL998 {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
	MOM_output_literal ";"
	MOM_output_literal "LBL 998;\tSUBPROG"
	#MOM_output_literal "M27"
	#MOM_output_literal "CYCL DEF 9.0 WYDERSHKA WREMENI"
	#MOM_output_literal "CYCL DEF 9.1 WYD.WR 3.0"
	MOM_output_literal "M9"
		
	global mom_post_in_simulation mom_output_mode_define
	  if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
	   MOM_output_literal "M129"
	  } else {
	   MOM_output_literal "M129"
	   #MOM_output_literal "FUNCTION RESET TCPM"
	  }
	MOM_do_template return_home_z
	MOM_force once M
	MOM_do_template return_home_xy

	MOM_output_literal "LBL 0;\t\tRETURN FROM 998 SUBPROG"
	MOM_force_block once coolant_text
	MOM_force once M_coolant_txt M_coolant
}

#=============================================================
proc USER_out_LBL997 {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
#	MOM_output_literal ";"
#	MOM_output_literal ";======== end of program ========"
#	MOM_output_literal ";"
#	MOM_output_literal "LBL 997;\tSUBPROG"
#	MOM_output_literal "M9"
#	MOM_output_literal "LBL 0;\t\tRETURN"
}

#=============================================================
proc USER_out_LBL999 {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
	MOM_output_literal ";"
	MOM_output_literal "LBL 999;\tSUBPROG"
	MOM_output_literal "M0"
	MOM_output_literal "LBL 0;\t\tRETURN"
}

#=============================================================
proc USER_out_LBL {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================

	global user_Q1 mom_operation_name
	MOM_output_literal ";"
	#MOM_output_literal ";=<$user_Q1> start of operation ===="
	MOM_output_literal "* - LBL $user_Q1\; $mom_operation_name"
	MOM_output_literal "LBL $user_Q1"
}

#=============================================================
proc USER_out_end_of_operation {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
	#MOM_output_literal "TOOL CALL Z"
	MOM_do_template m140
	#MOM_output_literal "CALL LBL 998;RESET"
	MOM_output_literal "M9"
	MOM_output_literal "M1; STOP"	
	#global mom_post_in_simulation mom_output_mode_define
	# if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
	#   MOM_output_literal "M129"
	#  } else {
	#   MOM_output_literal "M129"
	#   #MOM_output_literal "FUNCTION RESET TCPM"
	#  }
	MOM_output_literal "PLANE RESET STAY"
	MOM_do_template return_home_z
	MOM_force once M
	MOM_do_template return_home_xy
	
	global mom_tool_change_prohibit
	global mom_next_tool_number mom_tool_number
	global mom_sys_hand_tc_pos mom_prev_tool_number
	global mom_next_oper_has_tool_change
	#MOM_output_literal "Foo: mom_next_oper_has_tool_change:$mom_next_oper_has_tool_change"
	if { [info exists mom_tool_change_prohibit] && $mom_tool_change_prohibit == 1 } {
		if {[info exists mom_next_oper_has_tool_change] && $mom_next_oper_has_tool_change=="YES" } {
		MOM_output_literal "L A0.0 FQL2"
		#Обороты не включать
		MOM_do_template spindle_off
		#сдвинуть стол в крайнюю позицию по X$
		MOM_output_literal "L X$mom_sys_hand_tc_pos(0) FQL2 M91;"
		MOM_output_literal "L Y$mom_sys_hand_tc_pos(1) FQL2 M91;"
		MOM_output_literal "M0; SMENI INSTRUMENT!"
		#Установить запрет автоматической смены инструмента
		MOM_output_literal "L Y1010.0 FQL2 M91;"
		#MOM_output_literal "L X0.0 FQL2 M91;"
		#MOM_force once T S M
		#MOM_do_template spindle_on
		catch {unset mom_tool_change_prohibit}
		MOM_output_literal ";==== HAND TOOL CHANGE END ===="
		} 
	}
	
	#MOM_output_literal "CALL LBL 999;STOP"
	MOM_output_literal ";"
	MOM_output_literal ";=== KONEC OPERACII ===="
	MOM_output_literal ";"
	
	global mom_end_operation_pos mom_pos
	
	set mom_end_operation_pos(0) $mom_pos(0)
	set mom_end_operation_pos(1) $mom_pos(1)
	set mom_end_operation_pos(2) $mom_pos(2)
	set mom_end_operation_pos(3) $mom_pos(3)
	set mom_end_operation_pos(4) $mom_pos(4)
	
	global user_transmit_mode mom_ude_transmit
	if { [info exists user_transmit_mode]} {
		catch {unset user_transmit_mode}
	}
	if { [info exists mom_ude_transmit] } {
		catch {unset mom_ude_transmit}
	}
	USER_RESET_KINEMATIC
	#DISABLE TRANSMIT_MODE
	global mom_prev_cycle_feed_to
	catch { unset user_transmit_mode}

}

proc USER_end_of_program { } {
	global  mom_sys_leader home_x_var
    global  mom_sys_leader home_y_var 
    global  mom_sys_leader home_z_var
	global  mom_tool_change_prohibit
	global  mom_sys_hand_tc_pos
	
	
	if {[info exists mom_tool_change_prohibit] && $mom_tool_change_prohibit==1 } {
	MOM_output_literal ";VNIMANIE! SMENA INSTRUMENTA WRUCHNUYU!"
	MOM_output_to_listing_device ";VNIMANIE! SMENA INSTRUMENTA WRUCHNUYU!"
	MOM_output_literal "PLANE RESET STAY"
	MOM_output_literal "L Z$mom_sys_leader(home_z_var) FQL2 M91"
	MOM_output_literal "L A0.0 FQL2;"
	MOM_output_literal "L X$mom_sys_hand_tc_pos(0) FQL2 M91;"
	MOM_output_literal "L Y$mom_sys_hand_tc_pos(1) FQL2 M91;"
	MOM_output_literal "M0 ;SMENI INSTRUMENT!"
	MOM_output_literal "L Z$mom_sys_leader(home_z_var) FMAX M91"
	MOM_output_literal "L Y$mom_sys_leader(home_y_var) FMAX M91"
	MOM_output_literal "L X$mom_sys_leader(home_x_var) FMAX M91"
	} else {
	#MOM_output_literal "M140 MB MAX"
	#MOM_output_literal "L A+0.0 C+0.0  FMAX"
	MOM_do_template opstop
	MOM_output_literal "TOOL CALL 0"
	}
	
	MOM_output_literal "L C0.0 FMAX;"
	MOM_output_literal "M30"
	
	#USER_CLEAR_FOLDER
}

#=V1.0========================================================
proc USER_CHECK_MODE { } {
#=============================================================
# Контроль режимов резания
# Контроль частоты вращения
# для Heidenhain только частота вращения

	global feed mom_feed_rate mom_user_feed mom_spindle_speed mom_user_feed_value mom_feed_cut_value
	global warning_msg warning_msg_list user_error_list
	global error_operation mom_operation_name
	global mom_ude_min_feed_rate mom_ude_max_feed_rate mom_ude_min_speed mom_ude_max_speed
	global mom_ude_set_limits
	set uf_pm [ASK_FEEDRATE_FPM]
	
	if {[info exists mom_ude_set_limits] && $mom_ude_set_limits == ON} {
		#
		if {[info exists mom_ude_max_feed_rate]} {
			set MAX_FEED_RATE $mom_ude_max_feed_rate
		}
		if {[info exists mom_ude_min_feed_rate]} {
			set MIN_FEED_RATE $mom_ude_min_feed_rate
		}
		if {[info exists mom_ude_min_speed]} {
			set MIN_SPINDLE_SPEED $mom_ude_min_speed
		}
		if {[info exists mom_ude_max_speed]} {
			set MAX_SPINDLE_SPEED $mom_ude_max_speed
		}
		MOM_output_to_listing_device ">>>MAX_FEED_RATE:$MAX_FEED_RATE MIN_FEED_RATE:$MIN_FEED_RATE MAX_SPINDLE_SPEED:$MAX_SPINDLE_SPEED MIN_SPINDLE_SPEED:$MIN_SPINDLE_SPEED"
		#MOM_output_to_listing_device ">>>uf_pm:$uf_pm                                              mom_spindle_speed:$mom_spindle_speed"
		#MOM_output_to_listing_device ">>>feed:$feed"
		#MOM_output_to_listing_device ">>>mom_feed_cut_value:$mom_feed_cut_value"
		
		set control_feed $mom_feed_cut_value
		
		catch {unset mom_ude_set_limits}
	} {
		set MAX_FEED_RATE 30000.
		set MIN_FEED_RATE 0.001
		set MAX_SPINDLE_SPEED 15000.
		set MIN_SPINDLE_SPEED 0.01
	}
	
	#set error_operation 
	#set warning_msg_list 0

	if { [info exists control_feed] } {
		if { $control_feed < 0 || $control_feed == 0 || $control_feed < $MIN_FEED_RATE } {
		
			set warning_msg "OPERATION:<$mom_operation_name> FEEDRATE TOO SLOW Feed=$control_feed FMIN=$MIN_FEED_RATE"
			lappend warning_msg_list $warning_msg
			lappend user_error_list 1
		}
		if { $control_feed > $MAX_FEED_RATE } {
			set warning_msg "OPERATION:<$mom_operation_name> FEEDRATE TOO FAST Feed=$control_feed FMAX=$MAX_FEED_RATE"
			lappend warning_msg_list $warning_msg
			lappend user_error_list 1
			
		}
	}
	
	if { [info exists mom_spindle_speed] } {
		if {$mom_spindle_speed == 0 || $mom_spindle_speed < $MIN_SPINDLE_SPEED} {
			
			set warning_msg "OPERATION:<$mom_operation_name> SPINDLE SPEED TOO SLOW"
			lappend warning_msg_list $warning_msg
			lappend user_error_list 1
		}
		if { $mom_spindle_speed > $MAX_SPINDLE_SPEED} {
			set warning_msg "OPERATION:<$mom_operation_name> SPINDLE SPEED TOO FAST"
			lappend warning_msg_list $warning_msg
			append user_error_list 2			
		}
	}
	
	if { [info exists warning_msg_list] } {
		set i 0
		MOM_output_to_listing_device "\n==============================================================\n"
		foreach a $warning_msg_list {
			
			MOM_output_to_listing_device ">>>WARNING!<$i>  $a\n"
			incr i
		}
		MOM_output_to_listing_device "\n==============================================================\n"
	
		set res [MOM_display_message "Invalid values ​​detected:" "WARNING LIST" "" "Continue" "Abort"]
		if { $res == 2 } {
			MOM_abort "\
			\n\n==========================================\
			\n\n Вывод программы прерван! \
			\n\n==========================================\n\n"
	}
	}
}

#=============================================================
proc USER_init_cycle { } {
#=============================================================
# This command initializes some drilling cycle variables.
#
# 06-27-2013 Jason - Initial version
# 03-20-2014 Jason - Refine dpp_TNC_cycle_thread_pitch dpp_TNC_cycle_tap_feed and dpp_TNC_cycle_step_clearance
# 08-21-2015 szl - Enhance the warning message when users set wrong pitch and wrong spindle speed,fix PR7463004

   global cycle_init_flag
   global user_cycle_counter
   global mom_operation_name
   global mom_cycle_delay mom_cycle_top_delay
   global mom_motion_event
   global mom_cycle_step1
   global mom_cycle_step_clearance
   global mom_cycle_retract_mode
   global mom_cycle_feed_to mom_cycle_rapid_to mom_cycle_retract_to
   global mom_tool_pitch
   global mom_cycle_thread_pitch
   global mom_cycle_thread_right_handed
   global mom_cycle_orient
   global mom_spindle_direction mom_spindle_speed
   global js_prev_pos               ;# diy previous Z height
   global js_return_pos             ;# returnZ incremental from top of hole
   global mom_pos
   global mom_prev_pos
   global mom_output_unit
   global feed
   global dpp_ge
   global dpp_TNC_Q203_pos
   global dpp_TNC_cycle_thread_pitch
   global dpp_TNC_cycle_tap_feed
   global dpp_TNC_cycle_step_clearance
   global dpp_TNC_cycle_feed
   global user_TNC_cycle_step_quantity user_TNC_cycle_step; #
   global mom_cycle_feed_rate_mode
   global mom_cycle_feed_rate
   global mom_tool_name
   global mom_feed_cut_unit
   global mom_spindle_rpm
   
   #DEBUG OUTPUT
  #MOM_output_literal ";>>>Foo mom_motion_event:$mom_motion_event"
  #MOM_output_literal ";>>>Foo tool_type:$mom_motion_event"
   
   #<Mitiouk>15-05-2019
 global user_coord_rotation_flag
 if {![info exists user_coord_rotation_flag]} {
	MOM_output_to_listing_device ">>> ERROR! No clearance distance"
	MOM_display_message "No clearance for Drill Operation!" "User Abort Post processing" \
	"E" "OK"
	MOM_abort "*** User Abort Post Processing *** "
	
	
	}
	
	#<Mitiouk>29-07-219 change depth holle
	global mom_prev_cycle_feed_to
	if {![info exists mom_prev_cycle_feed_to] && [info exists mom_cycle_feed_to]} {
		set mom_prev_cycle_feed_to $mom_cycle_feed_to
	}
	
  if {![info exists user_cycle_counter]} {
        global mom_pos
        #MOM_output_literal ";>>> foo cycle_coord_rotation first X:$mom_pos(0) Y:$mom_pos(1)"
        #PB_CMD_cycle_coord_rotation
        #PB_CMD_coord_rotation_in_operation
        #MOM_output_literal ";>>> foo cycle_coord_rotation first X:$mom_pos(0) Y:$mom_pos(1)"
        set user_cycle_counter 0
    } else {
		#<Mitiouk>29-07-219 change depth holle
		if {[info exists mom_cycle_feed_to]} {
			if {[EQ_is_equal $mom_cycle_feed_to $mom_prev_cycle_feed_to]} {
				incr user_cycle_counter
			} else {
				set user_cycle_counter 0
				set mom_prev_cycle_feed_to $mom_cycle_feed_to
		  } 		
		}
	}
	

  #--------------------Set default cycle orient------------------------------------------------
   if { ![info exists mom_cycle_orient] } {
      set mom_cycle_orient 0
   }
  #--------------------Set default cycle delay-------------------------------------------------
   if { ![info exists mom_cycle_delay] } {
      set mom_cycle_delay 0.0
   }
  #-------------------Set default cycle top delay----------------------------------------------
   if { ![info exists mom_cycle_top_delay]} {
	  set mom_cycle_top_delay 0.0
   }
  #--------------------Set retract distance----------------------------------------------------
   if { [string match "AUTO" $mom_cycle_retract_mode] } {
      set js_return_pos [expr $js_prev_pos - $mom_pos(2)] ;# calc incr retract
      if { [EQ_is_lt $js_return_pos $mom_cycle_retract_to] } {
           set js_return_pos $mom_cycle_retract_to
      }
   } else {
      set js_return_pos $mom_cycle_retract_to
   }
  #--------------------Calculate thread pitch---------------------------------------------------
   if { ![string compare "tap_move" $mom_motion_event] ||\
        ![string compare "tap_deep_move" $mom_motion_event] ||\
        ![string compare "tap_float_move" $mom_motion_event] ||\
        ![string compare "tap_break_chip_move" $mom_motion_event] } {
      if { [info exists mom_tool_pitch] } {
         if { [info exists mom_cycle_thread_pitch] } {
             set dpp_TNC_cycle_thread_pitch $mom_cycle_thread_pitch

      } else {
            set dpp_TNC_cycle_thread_pitch $mom_tool_pitch
         }
         } else {
            #---------Warning---------
            MOM_display_message "$mom_operation_name: No pitch defined on the tool. Please use Tap tool. \n Post Processing will be aborted." "Postprocessor error message" "E"
            MOM_abort_program
         }
      if {![info exists mom_spindle_speed] || [EQ_is_zero $mom_spindle_speed] } {
         MOM_display_message "$mom_operation_name : spindle speed is 0. Post Processing will be aborted." "Postprocessor error message" "E"
         MOM_abort_program
      }
   }
  #--------------------Calculate thread pitch sign-----------------------------------------------
   if {[info exists mom_cycle_thread_right_handed]} {
      if { $mom_cycle_thread_right_handed == "FALSE" } {
          set dpp_TNC_cycle_thread_pitch  [expr $dpp_TNC_cycle_thread_pitch * (-1)]
      }
   } elseif { $mom_spindle_direction == "CCLW" } {
      set dpp_TNC_cycle_thread_pitch  [expr $dpp_TNC_cycle_thread_pitch * (-1)]
   }
  #--------------------Cycle feed rate multiply by 10 in INCH unit-----------------------------
   switch $mom_output_unit {
      IN {
          set dpp_TNC_cycle_feed [expr $feed*10]
      }
      MM {
          set dpp_TNC_cycle_feed $feed
      }
   }
  #--------------------Calculate tap feed------------------------------------------------------
   if { ![string compare "tap_float_move" $mom_motion_event] } {
      set dpp_TNC_cycle_tap_feed [expr abs($dpp_TNC_cycle_thread_pitch) * $mom_spindle_rpm]
   }
  #--------------------Calculate step clearance------------------------------------------------
   if { ![string compare "tap_deep_move" $mom_motion_event] } {
      set dpp_TNC_cycle_step_clearance 0
   }
   if { ![string compare "tap_break_chip_move" $mom_motion_event] } {
      if {[info exists mom_cycle_step_clearance]} {
         if { ![EQ_is_zero $mom_cycle_step_clearance] } {
            set dpp_TNC_cycle_step_clearance $mom_cycle_step_clearance
         } else {
            MOM_output_to_listing_device "$mom_operation_name: Step clearance is 0, please set a value!"
         }
      } else {
         MOM_output_to_listing_device "$mom_operation_name: No step clearance defined."
      }
   }
  #--------------------Calculate peck sizes-----------------------------------------------------
   global mom_tool_diameter
   global cycle_peck_size cycle_type_number

   set cycle_peck_size [expr ($mom_cycle_feed_to*(-1.0))]     ;# single peck size most cycles

   if { ![string compare "drill_deep_move" $mom_motion_event] ||\
        ![string compare "drill_break_chip_move" $mom_motion_event] } {

      if { ![ info exists mom_cycle_step1 ] || $mom_cycle_step1 == 0 } {
         set cycle_peck_size  $mom_tool_diameter  ;# default peck  if not set
      } else {
         set cycle_peck_size  $mom_cycle_step1    ;# real peck
      }
	  
	  set user_TNC_cycle_step_quantity [expr int(abs($mom_cycle_feed_to / $cycle_peck_size))]
	  #set user_TNC_cycle_step_quantity [expr abs($user_TNC_cycle_step_quantity)]
	  if { [expr abs(abs($mom_cycle_feed_to) - abs(($user_TNC_cycle_step_quantity * $mom_cycle_step1)))] > 0.0001} {
		incr user_TNC_cycle_step_quantity
		set user_TNC_cycle_step [expr abs($mom_cycle_feed_to / ($user_TNC_cycle_step_quantity)) ]
	  }
	  #MOM_output_to_listing_device ">>>2 mom_cycle_feed_to:$mom_cycle_feed_to//user_TNC_cycle_step_quantity:$user_TNC_cycle_step_quantity user_TNC_cycle_step:$user_TNC_cycle_step mom_cycle_step1:$mom_cycle_step1"
   }

  # Normally cycle_init_flag is only set if this is a new cycle
  # it is specifically unset in cycle_plane_change event, which
  # happens when a drilling operation goes uphill,
  # (drills a hole at a higher Z than the previous hole)
  # it is _not_ set  when drilling downhill.
  # this next bit of code sets the variable for up or downhill
  # so that the new hole is defined - this is absolutely required
  # to ensure the hole Z height Q203 is set correctly.

   if { $mom_pos(2) != $mom_prev_pos(2) } {
      set cycle_init_flag  "TRUE"
   }
   if { $dpp_ge(cycle_clearance_plane) == "FALSE" } {
      set mom_pos(2) $dpp_TNC_Q203_pos
   }
   set dpp_ge(cycle_clearance_plane) "TRUE"
 
  
 }

#=============================================================
proc USER_END_CYCLE {} {
#=============================================================
   global user_cycle_counter
   global mom_prev_cycle_feed_to
   catch {unset user_cycle_counter; unset mom_prev_cycle_feed_to; MOM_output_cycle "-reset"}
   
}

proc USER_cycle_200_output { } {
#=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_motion_event
 set ude_alt_cycle_output ON
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
 
 set ude_alt_cycle_output ON
  if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	global dpp_TNC_cycle_feed
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	 	
	set q200 [format "%.3f" $mom_cycle_rapid_to]
	set q201 [format "%.3f" $mom_cycle_feed_to]
	set q206 [format "%.3f" $dpp_TNC_cycle_feed]
	set q202 [format "%.3f" $cycle_peck_size]
	set q210 [format "%.2f" $mom_cycle_top_delay]
	set q203 [format "%.3f" $mom_pos(2)]
	set q204 [format "%.3f" $js_return_pos]
	set q211 [format "%.2f" $mom_cycle_delay]
	
	MOM_output_cycle "CYCL DEF 200 SWERLENIJE ~"
	#прекратить нумеровать
	#set seq_status $mom_sequence_mode

	#MOM_set_seq_off
	MOM_output_cycle "	Q200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "	Q201=$q201\t;GLUBINA SVERLENIYA ~"
	MOM_output_cycle "	Q206=$q206\t;PODACHA VREZANIA ~"
	MOM_output_cycle "	Q202=$q202\t;GLUBINA VREZANIA ~"
	MOM_output_cycle "	Q210=$q210\t;VREMYA VIDERJKI VVERHU ~"
	MOM_output_cycle "	Q203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "	Q204=$q204\t;BEZOP.RASSTOIANIE ~"
	MOM_output_cycle "	Q211=$q211\t;VREMIA VIDERGKI VNIZU"
	MOM_output_cycle
	#вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} {
	#	MOM_set_seq_on}
   
  } else {
  PB_call_macro CYCL_200
  }
}

#=============================================================
proc USER_cycle_201_output { } {
#=============================================================
 #output CYCL DEF 201 alt format
 global ude_alt_cycle_output
 global mom_motion_event mom_ude_return_feed
 global mom_TNC_cycle_return_feed
 global dpp_TNC_cycle_feed mom_feed_cut_value
 global mom_cycle_retract_to
 set ude_alt_cycle_output ON;#ON/OFF
 
 # ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

 #Return_feed
	global mom_TNC_cycle_return_feed dpp_TNC_cycle_feed
	if {![info exists dpp_TNC_cycle_feed]} {
	  set dpp_TNC_cycle_feed $mom_feed_cut_value
	}
   if {![info exists mom_ude_return_feed] || $mom_ude_return_feed==0} {
     set mom_TNC_cycle_return_feed $dpp_TNC_cycle_feed
   } else {
	set mom_TNC_cycle_return_feed $mom_ude_return_feed
   }
  
 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	
	#Назначение
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q211 $mom_cycle_delay
	set q208 $dpp_TNC_cycle_feed
	set q203 $mom_pos(2)
	if {![info exists js_return_pos]} {set js_return_pos $mom_cycle_retract_to}
	set q204 $js_return_pos
	
	if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
	#Форматирование	
	set q200 [format "%.3f" $q200]
	set q201 [format "%.3f" $q201]
	set q206 [format "%.3f" $q206]
	set q211 [format "%.2f" $q211]
	set q208 [format "%.3f" $q208]
	set q203 [format "%.3f" $q203]
	set q204 [format "%.3f" $q204]
	
	#MOM_output_literal "CYCL DEF 201 REAMING ~ "
	MOM_output_cycle "CYCL DEF 201 RAZWIORTYWANIE ~ "
	#прекратить нумеровать
	#set seq_status $mom_sequence_mode
	MOM_set_seq_off
	MOM_output_cycle "\tQ200= $q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA SVERLENIYA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA VREZANIA ~"
	MOM_output_cycle "\tQ211=$q211\t;VREMIA VIDERGKI VNIZU ~"
	MOM_output_cycle "\tQ208=$q208\t;PODACHA OTHODA ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;BEZOP.RASSTOIANIE"
	MOM_output_cycle
	#вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} { MOM_set_seq_on }
  } else { PB_call_macro CYCL_201 }
}

#=============================================================
proc USER_cycle_202_output { } {
#=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_motion_event
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
 set ude_alt_cycle_output ON

 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	global dpp_TNC_cycle_feed
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	global mom_itnc_bore_q214_mode
	global mom_itnc_bore_q214
	global dpp_TNC_m128_feed_value
	
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q211 $mom_cycle_delay
	set q208 $dpp_TNC_m128_feed_value ;#"FQL2"
	set q203 $mom_pos(2)
	set q204 $js_return_pos
	set mom_itnc_bore_q214 1
	
   
    if { [info exists mom_itnc_bore_q214_mode] } {
		
        if { [string match "0*" $mom_itnc_bore_q214_mode] } {

            set mom_itnc_bore_q214 0
         }
        if { [string match "1*" $mom_itnc_bore_q214_mode] } {

            set mom_itnc_bore_q214 1
         }
        if { [string match "2*" $mom_itnc_bore_q214_mode] } {
            
            set mom_itnc_bore_q214 2
         }
        if { [string match "3*" $mom_itnc_bore_q214_mode] } {
            
            set mom_itnc_bore_q214 3
         }
        if { [string match "4*" $mom_itnc_bore_q214_mode] } {
            set mom_itnc_bore_q214 4
         }
    }
	set q214 $mom_itnc_bore_q214
	if {[info exists mom_cycle_orient]} {
		set q236 $mom_cycle_orient
	} else {set q236 0}
	#format

	set q200 [format "%.3f" $q200]
	set q201 [format "%.3f" $q201]
	set q206 [format "%.3f" $q206]
	set q211 [format "%.2f" $q211]
	set q208 [format "%.2f"	$q208]
	set q203 [format "%.3f" $q203]
	set q204 [format "%.3f" $q204]
	set q214 [format "%.3f" $q214]
	set q236 [format "%.3f" $q214]
	
	if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
	MOM_output_cycle "CYCL DEF 202 RASTOCHKA ~ "
	#прекратить нумеровать
	#set seq_status $mom_sequence_mode
	#MOM_set_seq_off
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA SVERLENIYA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA VREZANIA ~"
	MOM_output_cycle "\tQ211=$q211\t;VREMIA VIDERGKI VNIZU ~"
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;BEZOP.RASSTOIANIE ~"
	MOM_output_cycle "\tQ214=$q214\t;NAPR.WYCHODA IZ MAT ~"
	MOM_output_cycle "\tQ236=$q236\t;UGOL SCHPINDEL"
	MOM_output_cycle
	##вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} {
	#	MOM_set_seq_on}
  } else {
  PB_call_macro CYCL_202
  }
}

#=============================================================
proc USER_cycle_203_output { } {
#=============================================================
# Универсальное сверление
#Mitiouk <04-07-2019>
 #output CYCL DEF 203 alt format
  global ude_alt_cycle_output
  global mom_motion_event mom_ude_return_feed
  global mom_TNC_cycle_return_feed
  global dpp_TNC_cycle_feed mom_feed_cut_value
  global mom_cycle_retract_to
  global mom_cycle_rapid_to
  global mom_cycle_feed_to
  global cycle_peck_size
  global mom_cycle_top_delay
  global mom_pos
  global js_return_pos
  global mom_cycle_delay
  global seq_status mom_sequence_mode
  global mom_feed_departure_value mom_retract_feed
  global mom_TNC_cycle_return_feed dpp_TNC_cycle_feed
  global user_TNC_cycle_step_quantity user_TNC_cycle_step
  global mom_cycle_retract_mode
  
  set ude_alt_cycle_output ON;#ON/OFF
  #
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

# ----Return_feed
	if {![info exists dpp_TNC_cycle_feed]} {
	  set dpp_TNC_cycle_feed $mom_feed_cut_value
	}
   if {![info exists mom_ude_return_feed] || $mom_ude_return_feed==0} {
     set mom_TNC_cycle_return_feed $dpp_TNC_cycle_feed
   } else {
	set mom_TNC_cycle_return_feed $mom_ude_return_feed
   }
   
	if {![info exists js_return_pos]} {set js_return_pos $mom_cycle_retract_to}
 
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	#
	set q202 $cycle_peck_size
	set q210 $mom_cycle_top_delay
	set q203 $mom_pos(2)
	set q204 $js_return_pos

# ---- Декремент приращения шагов по глубине --------
	
	global mom_cycl_203_q212
	set q212 0.0;# default value
	
	if {[info exists mom_cycl_203_q212]} {
		set q212 [format "%+.3f" $mom_cycl_203_q212]
	}
		
# ---- Ломка стружки ----
global mom_cycl_203_q213 mom_cycle_203_option mom_cycle_option

#MOM_output_literal ";Foo mom_cycle_option: $mom_cycle_option"
	set q213 0
	if {([info exists mom_cycle_203_option] && $mom_cycle_203_option == "BREAK") || $mom_cycle_option == "OPTION"} {
			set q213 $user_TNC_cycle_step_quantity
			#set q202 $user_TNC_cycle_step
		}
	
	
# ---- Величина отхода при ломке стружки  ----
global cycl_203_q256
	if {[info exists mom_cycl_203_q256]} {
		set q256 $mom_cycl_203_q256
	} else { 
		set q256 1.0
	}
# ---- Минимальная глубина врезания ----
  global mom_cycl_203_q205
	set q205 0.2
	if {[info exists mom_cycl_203_q205]} {
		set q205 $mom_cycl_203_q205
	}
	set q211 $mom_cycle_delay
	
	if { [info exists mom_retract_feed] && $mom_retract_feed > 0} {
			set q208 $mom_retract_feed
		} else {
			set q208 10000.0
		}

	set q395 0;#Oпорная глубина вершина/цилиндр
	
	#режим вывода
	
# ---- форматирование ---
	set q200 [format "%+.3f" [expr abs($q200)]]
	set q201 [format "%+.3f" $q201]
	set q206 [format "%+.3f" $q206] 
	set q202 [format "%+.3f" $q202]
	set q210 [format "%+.2f" $q210]
	set q203 [format "%+.3f" $q203]
	set q204 [format "%+.3f" $q204]
	set q212 [format "%+.3f" $q212]
	set q213 [format "%+i" $q213]
	set q205 [format "%+.3f" $q205]
	set q211 [format "%+.2f" $q211]
	set q208 [format "%+.3f" $q208]
	set q256 [format "%+.3f" $q256]
	set q395 [format "%+.3f" $q395]
# ---- вывод -----
		set local_cycle_mode 0
		if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} {	
			set local_cycle_mode 1
		}
		
		#global mom_post_in_simulation mom_output_mode_define
		#if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
		#	set local_cycle_mode 1 }
			
	if {[info exists local_cycle_mode] && $local_cycle_mode==1} {
		MOM_output_cycle "CYCL DEF 203 UNIVERS. SWERLENIE ~"
		#set seq_status $mom_sequence_mode
		#MOM_set_seq_off
	
		MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
		MOM_output_cycle "\tQ201=$q201\t;GLUBINA SVERLENIYA ~"
		MOM_output_cycle "\tQ206=$q206\t;PODACHA VREZANIA ~"
		MOM_output_cycle "\tQ202=$q202\t;GLUBINA VREZANIA ~"
		MOM_output_cycle "\tQ210=$q210\t;VREMYA VIDERJKI VVERHU ~"
		MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
		MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
		MOM_output_cycle "\tQ212=$q212\t;SJOM MATERIALA ~"
		MOM_output_cycle "\tQ213=$q213\t;KOL.OPER.LOMKI STRU ~"
		MOM_output_cycle "\tQ205=$q205\t;MIN.GLUBINA WREZANJA ~"
		MOM_output_cycle "\tQ211=$q211\t;VREMIA VIDERGKI VNIZU ~"
		MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"
		MOM_output_cycle "\tQ256=$q256\t;WYCHOD PRI LOMANII ~"
		MOM_output_cycle "\tQ395=$q395\t;KOORD. OTSCHETA GLUB"
		MOM_output_cycle
	} else {
		MOM_output_literal "CYCL DEF 203 \
									Q200=$q200 \
									Q201=$q201 \
									Q206=$q206 \
									Q202=$q202 \infodba
									
									Q210=$q210 \
									Q203=$q203 \
									Q204=$q204 \
									Q212=$q212 \
									Q213=$q213 \
									Q205=$q205 \
									Q211=$q211 \
									Q208=$q208 \
									Q256=$q256 \
									Q395=$q395"		
		}
	#вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} {
	#	MOM_set_seq_on
	#}
}

#=============================================================
proc USER_cycle_205_output {} {
#=============================================================
# Универсальное сверление
#Mitiouk <04-07-2019>
#output CYCL DEF 205 alt format
 
 global ude_alt_cycle_output
 global mom_motion_event mom_ude_return_feed
 global mom_TNC_cycle_return_feed
 global dpp_TNC_cycle_feed mom_feed_cut_value
 global mom_cycle_retract_to
 global mom_cycle_rapid_to
 global mom_cycle_feed_to
 global cycle_peck_size
 global mom_cycle_top_delay
 global mom_pos
 global js_return_pos
 global mom_cycle_delay
 global seq_status mom_sequence_mode
 global mom_feed_departure_value mom_retract_feed
 global mom_TNC_cycle_return_feed dpp_TNC_cycle_feed	
 global mom_cycl_205_q257 mom_cycl_205_q256
 
 set ude_alt_cycle_output ON;#ON/OFF
 
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

 # ---- Return_feed -----
  if {![info exists dpp_TNC_cycle_feed]} {
	  set dpp_TNC_cycle_feed $mom_feed_cut_value
	}
   if {![info exists mom_ude_return_feed] || $mom_ude_return_feed==0} {
     set mom_TNC_cycle_return_feed $dpp_TNC_cycle_feed
   } else {
	set mom_TNC_cycle_return_feed $mom_ude_return_feed
   }
	if {![info exists js_return_pos]} {set js_return_pos $mom_cycle_retract_to}
# ---- Общие параметры ---	
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q202 $cycle_peck_size
	set q210 $mom_cycle_top_delay
	set q203 $mom_pos(2)
	set q204 $js_return_pos
	
# ---- Специальные параметры --------
# ---- Декремент приращения шагов по глубине --------	
global mom_sjom_materala
	set q212 0.0
	if {[info exists mom_sjom_materala]} {
		set q212 $mom_sjom_materala
	}
	set q205 0.2
	set q258 0.2 ; #RASST.BEZ. WWERCHU
	set q259 0.2 ; #RASST.BEZ. W NIZU
	
# ----WYCHOD PRI LOMANII--------------------------
	set q256 0.2
	if {[info exists mom_cycl_205_q256]} {
		set q256 $mom_cycl_205_q256
	}
	
# ----GL.SWERL.PRI LOMANII------------------------
	set q257 0
	if {[info exists mom_cycl_205_q257]} {
		set q257 $mom_cycl_205_q257
	}
		
	set q211 [format "%+.2f" $mom_cycle_delay]
# -----	TOCHKA STARTA -------------
  global mom_cycl_205_tochka_starta
  set q379 0
  if { [ info exists mom_cycl_205_tochka_starta ] } {
	   set q379 $mom_cycl_205_tochka_starta
    }
# ----- PODACHA PRED.POZIC.
	set q253 750.0

# ----- PODACHA WYCHODA 
	set q208 10000.0
	if { [ info exists mom_retract_feed ] && $mom_retract_feed > 0 } {
			set q208 $mom_retract_feed
		}
		
# ------ q395 опорная глубина /вершина/цилиндр
	set q395 0

# Вывод в программу
  set local_cycle_mode 0
  if { [ info exists ude_alt_cycle_output ] && $ude_alt_cycle_output == ON } {	
			set local_cycle_mode 1
		}
		
		#global mom_post_in_simulation mom_output_mode_define
		#if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
		#	set local_cycle_mode 1 }
			
# Форматирование
	set q200 [format "%+.3f" $q200]
	set q201 [format "%+.3f" $q201]
	set q206 [format "%+.3f" $q206] 
	set q202 [format "%+.3f" $q202]
	set q210 [format "%+.2f" $q210]
	set q203 [format "%+.3f" $q203]
	set q204 [format "%+.3f" $q204]
	set q212 [format "%+.3f" $q212]
	set q205 [format "%+.3f" $q205]
	set q258 [format "%+.3f" $q258]
	set q259 [format "%+.3f" $q259]
	set q257 [format "%+.3f" $q257]
	set q256 [format "%+.3f" $q256]
	set q211 [format "%+.2f" $q211]
	set q253 [format "%+.3f" $q253]
	set q208 [format "%+0.3f" $q208]
	set q379 [format "%+.3f" $q379]
	set q395 [format "%+.3f" $q395]
	
#
			
  if {[info exists local_cycle_mode] && $local_cycle_mode==1} {
      
	MOM_output_cycle "CYCL DEF 205 UNIW. GL. SWERLENIE ~"
#	#прекратить нумеровать
	set seq_status $mom_sequence_mode
	MOM_set_seq_off
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA NA WREZANJE ~"
	MOM_output_cycle "\tQ202=$q202\t;GLUBINA WREZANJA ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
	MOM_output_cycle "\tQ212=$q212\t;SJOM MATERIALA ~"
	MOM_output_cycle "\tQ205=$q205\t;MIN.GLUBINA WREZANJA ~"
	MOM_output_cycle "\tQ258=$q258\t;RASST.BEZ. WWERCHU ~"
	MOM_output_cycle "\tQ259=$q259\t;RASST.BEZ. W NIZU ~"
	MOM_output_cycle "\tQ257=$q257\t;GL.SWERL.PRI LOMANII ~"
	MOM_output_cycle "\tQ256=$q256\t;WYCHOD PRI LOMANII ~"
	MOM_output_cycle "\tQ211=$q211\t;WYDER.WREMENI WNIZU ~"
	MOM_output_cycle "\tQ379=$q379\t;TOCHKA STARTA ~"
	MOM_output_cycle "\tQ253=$q253\t;PODACHA PRED.POZIC. ~"
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"	
	MOM_output_cycle "\tQ395=$q395\t;KOORD. OTSCHETA GLUB"
	MOM_output_cycle
	#if {[info exists mom_sequence_mode] && $mom_sequence_mode=="ON"} {
	#  MOM_set_seq_on
	#  }
		} else {
		
	MOM_output_cycle "CYCL DEF 205 \
		Q200=$q200 \
		Q201=$q201 \
		Q206=$q206 \
		Q202=$q202 \
		Q203=$q203 \
		Q204=$q204 \
		Q212=$q212 \
		Q205=$q205 \
		Q258=$q258 \
		Q259=$q259 \
		Q257=$q257 \
		Q256=$q256 \
		Q211=$q211 \
		Q379=$q379 \
		Q253=$q253 \
		Q208=$q208 \
		Q395=$q395"
	MOM_output_cycle
	}
}

#=============================================================
proc USER_cycle_209_output { } {
 #=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_cycle_rapid_to
 global mom_cycle_feed_to
 global dpp_TNC_cycle_feed
 global cycle_peck_size
 global dpp_TNC_cycle_thread_pitch
 global mom_cycle_top_delay
 global mom_pos
 global js_return_pos
 global mom_cycle_delay
 global seq_status mom_sequence_mode
 global mom_cycle_step1
 global mom_cycle_orient
 global mom_spindle_retract_nominal_value
 set ude_alt_cycle_output ON;#ON/OFF
 
 # ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  
	set q200 [format "%+.3f" $mom_cycle_rapid_to]
	set q201 [format "%+.3f" $mom_cycle_feed_to]
	set q239 [format "%+.3f" $dpp_TNC_cycle_thread_pitch]
	set q203 [format "%+.3f" $mom_pos(2)]
	set q204 [format "%+.3f" $js_return_pos]
	set q257 [format "%+.3f" $mom_cycle_step1]
	set q256 [format "%+.3f" 1];# Коэфициент выхода при ломании
	set q336 [format "%+.3f" $mom_cycle_orient]
	 #Q403=1.    ;RPM FACTOR
	if { ![info exists mom_spindle_retract_nominal_value] } {
		set mom_spindle_retract_nominal_value 100; #this is %
	}
	set q403 [format "%.2f" [expr $mom_spindle_retract_nominal_value* 0.01]]
	set local_cycle_mode 0

	# ---- вывод -----
		if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} {	
			set local_cycle_mode 1}
		
		global mom_post_in_simulation mom_output_mode_define
		if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
			set local_cycle_mode 1 }
			
  if {$local_cycle_mode == 1} { 
  MOM_output_cycle "CYCL DEF 209 NAR.WN.REZBY/LOM.ST. ~ "
	#прекратить нумеровать
	#set seq_status $mom_sequence_mode
	MOM_set_seq_off
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA REZBY ~"
	MOM_output_cycle "\tQ239=$q239\t;SCHAG REZBY ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
	MOM_output_cycle "\tQ257=$q257\t;GL.SWERL.PRI LOMANII ~"
	MOM_output_cycle "\tQ256=$q256\t;WYCHOD PRI LOMANII ~"
	MOM_output_cycle "\tQ336=$q336\t;UGOL SCHPINDEL ~"
	MOM_output_cycle "\tQ403=$q403\t;RPM FACTOR"
	MOM_output_cycle
	#вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} {
	#	MOM_set_seq_on
	#}
  } else {    
	PB_call_macro CYCL_209
  }
}

#=============================================================
proc USER_cycle_207_output { } {
#=============================================================
 #output CYCL DEF 207 alt format
 global ude_alt_cycle_output
 global mom_motion_event
 global mom_cycle_rapid_to
 global mom_cycle_feed_to
 global dpp_TNC_cycle_feed
 global cycle_peck_size
 global mom_cycle_top_delay
 global mom_pos
 global js_return_pos
 global mom_cycle_delay
 global seq_status mom_sequence_mode
 global dpp_TNC_cycle_thread_pitch
 
 set ude_alt_cycle_output ON;# ON
 
 # ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

  set q200 [format "%.3f" $mom_cycle_rapid_to]
  set q201 [format "%.3f" $mom_cycle_feed_to]
  set q239 [format "%.3f" $dpp_TNC_cycle_thread_pitch]
  set q203 [format "%.3f" $mom_pos(2)]
  set q204 [format "%.3f" $js_return_pos]
  
  
  # ---- вывод -----
  set local_cycle_mode 0
  if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} {	
	set local_cycle_mode 1
		}
  #global mom_post_in_simulation mom_output_mode_define
  #if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
  # set local_cycle_mode 0 }
			
  if {$local_cycle_mode==1} { 
  	MOM_output_cycle "CYCL DEF 207 NAREZANIE REZBI GS ~ "
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA REZBY ~"
	MOM_output_cycle "\tQ239=$q239\t;SCHAG REZBY ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ."
	MOM_output_cycle
	
  } else {
    PB_call_macro CYCL_207
  }
}

#=============================================================
proc USER_cycle_241_output { } {
#=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_motion_event
 global mom_cycle_rapid_to mom_cycle_feed_to dpp_TNC_cycle_feed mom_cycle_delay mom_pos js_return_pos
 global mom_spindle_speed mom_sys_coolant_code mom_coolant_status
 global mom_cycle_241_coolant_on mom_cycle_241_coolant_off
 global mom_cycle_241_dwell_depth
 global mom_retract_spindle_speed mom_direct_spindle_turn mom_spindle_direction
 global mom_retract_feed mom_deeper_feed
 global mom_deeper_starting_point
 global mom_cycle_retract_to
 
 set ude_alt_cycle_output ON;  # ON/OFF Жесткое указание  на режим альтернативного вывода
 
 ### SET PARAMETER
 set q200 [format "%+.3f" $mom_cycle_rapid_to]
 set q201 [format "%+.3f" $mom_cycle_feed_to]
 set q206 [format "%+.3f" $dpp_TNC_cycle_feed]
 set q211 [format "%+.2f" $mom_cycle_delay]
 set q203 [format "%+.3f" $mom_pos(2)]
 set q204 [format "%+.3f" $js_return_pos]
 
 if { [info exists mom_deeper_starting_point] && $mom_deeper_starting_point > 0 } {
   if { [expr abs($mom_deeper_starting_point)] > [expr abs($mom_cycle_feed_to)] } {
     set q379 [format "%+.3f" 0]
	 #set q379 [format "%.3f" [ expr abs ($mom_cycle_feed_to)]]
	 MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q379 in non correct. Value has been changed! New value is $q379!"
	 } else { set q379 [format "%+.3f" $mom_deeper_starting_point] }
 } else {
   set q379 [format "%+.3f" 0]
   MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q379 in non correct. Value has been changed! New value is $q379!"
 }
 
 if { [info exists mom_deeper_feed] && $mom_deeper_feed > 0 } {  
   set q253 [format "%+.3f" $mom_deeper_feed ] } else {
   set q253 [format "%+.3f" 0 ] }
   
 if { $q379 > 0 && $q253 == 0} {
   MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q253 in non correct. Value is too slow! Value is $q253!"
 }
  
 
 if { [info exists mom_retract_feed] && $mom_retract_feed > 0} {
  set q208 [format "%+.3f" $mom_retract_feed]
 } else {
  set q208 [format "%+.3f" 0]
 }
 
 if { [info exists mom_direct_spindle_turn] } {
   switch $mom_direct_spindle_turn {
     "M3" {set q426 3}
	 "M4" {set q426 4}
	 "M5" {set q426 5}
     default { 
				if {[info exists mom_spindle_direction] } {
					switch $mom_spindle_direction {
						"CLW" {set q426 3}
						"CCLW" {set q426 4}
						default {set q426 5}
						} 
				} else {set q426 5}
			}
   }
   
   } else { 
   if {[info exists mom_spindle_direction] } {
     switch $mom_spindle_direction {
	  "CLW" {set q426 3}
	  "CCLW" {set q426 4}
	  default {set q426 5}
      } 
    } else {set q426 5}
 }
  set q426 [format "%+i" $q426]
  
  if { [info exists mom_retract_spindle_speed] } {
    set q427 $mom_retract_spindle_speed
  } else {
    set q427 $mom_spindle_speed
  }
  set q427 [format "%+.3f" $q427]
  
  set q428 [format "%+.3f" $mom_spindle_speed]
 
  if { [info exists mom_cycle_241_coolant_on] } {
   if { [info exists mom_coolant_status] && $mom_cycle_241_coolant_on == "ON" } {
     switch $mom_coolant_status {
			THRU1 { set q429 7}
			THRU  { set q429 26}
			AIR   { set q429 27}
			MIST  { set q429 28}
			FLOOD { set q429 8}
			ON    { set q429 8}
			OFF   { set q429 9}
		default   { set q429 8}
	    }
    } else {
	  if { $mom_cycle_241_coolant_on == "OFF"} {
	    set q429 9
	  } else { set q429 8 }
    }
  }
  #format
  set q429 [ format "%+i" $q429 ]
  
  if { [info exists mom_cycle_241_coolant_off] } {
    switch $mom_cycle_241_coolant_off {
	  "ON" 	{
				set q430 $q429
			}
	  "OFF" {set q430 9}
	  default {set q430 9}
	}
   } else { set q430 9 }
   
 
 if {[info exists mom_cycle_241_dwell_depth]} {
  if { $mom_cycle_241_dwell_depth < [expr abs($mom_cycle_feed_to)] && $mom_cycle_241_dwell_depth > 0 } {
   set q435 $mom_cycle_241_dwell_depth
  } else {
    if {$mom_cycle_241_dwell_depth > 0} {
    set q435 [expr abs($mom_cycle_feed_to)] } else {
	set q435 0.0
	}
	MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q435 in non correct. Value has been changed! New value is $q435!"
  }
   
 } else { set q435 0 }; #parameter is disabled
 
#format
	#200
	#201
	#206
	#211
	#203
	#204
	#279
	#273
	#208
	#426
	#427
	#426
	#427
	#428
	#429
	set q430 [format "%+i" $q430]
	set q435 [format "%+.3f" $q435]
 
 ### OUTPUT
  if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	global dpp_TNC_cycle_feed
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	
	global mom_post_in_simulation mom_output_mode_define
	
	if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
	 MOM_output_cycle "CYCL DEF 241 \
								Q200=$q200 \
								Q201=$q201 \
								Q206=$q206 \
								Q211=$q211 \
								Q203=$q203 \
								Q204=$q204 \
								Q379=$q379 \
								Q273=$q253 \
								Q208=$q208 \
								Q426=$q426 \
								Q427=$q427 \
								Q428=$q428 \
								Q429=$q429 \
								Q430=$q430 \
								Q435=$q435"
	 MOM_output_cycle
	
	} else { 
	   
	MOM_output_cycle "CYCL DEF 241 SINGLE-LIP D.H.DRLNG ~"

	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA NA WREZANJE ~"
	MOM_output_cycle "\tQ211=$q211\t;WYDER.WREMENI WNIZU ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
	MOM_output_cycle "\tQ379=$q379\t;TOCHKA STARTA ~"; #!!!
	MOM_output_cycle "\tQ253=$q253\t;PODACHA PRED.POZIC. ~"; #!!!
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"; #!!!
	MOM_output_cycle "\tQ426=$q426\t\t;DIR. OF SPINDLE ROT. ~"; #!!!
	MOM_output_cycle "\tQ427=$q427\t;ROT.SPEED INFEED/OUT ~"; #!!!
	MOM_output_cycle "\tQ428=$q428\t;ROT. SPEED DRILLING ~"; #!!!
	MOM_output_cycle "\tQ429=$q429\t\t;COOLANT ON ~";
	MOM_output_cycle "\tQ430=$q430\t\t;COOLANT OFF ~"; 
	MOM_output_cycle "\tQ435=$q435\t;DWELL DEPTH";
	MOM_output_cycle
	}
  } else {
  #PB_call_macro CYCL_200
  MOM_output_cycle "CYCL DEF 241 \
								Q200=$q200 \
								Q201=$q201 \
								Q206=$q206 \
								Q211=$q211 \
								Q203=$q203 \
								Q204=$q204 \
								Q379=$q379 \
								Q273=$q253 \
								Q208=$q208 \
								Q426=$q426 \
								Q427=$q427 \
								Q428=$q428 \
								Q429=$q429 \
								Q430=$q430 \
								Q435=$q435"
 MOM_output_cycle
  
  }
  
}

#=============================================================
proc USER_out_mashine_time { } {
 #=============================================================
	global mom_machine_time
	
	if {[info exists mom_machine_time]} {
		MOM_output_literal ";==============================================="
		MOM_output_literal "; TOTAL MACHINE TIME: [format "%.2f" $mom_machine_time] MINUTES"
		MOM_output_literal ";==============================================="
	}
}

#=============================================================
proc USER_COOLANT { } {
#=============================================================
	return
}

#=============================================================
proc USER_coolant_set { } {
#=============================================================

global mom_coolant_mode mom_coolant_status mom_sys_coolant_code
	global mom_ude_coolant_mode
	#<Mitiouk> 14-05-2019
	set mom_sys_coolant_code(AIR)                  "27"
	set mom_sys_coolant_code(THRU1)                 "7"
#продувка воздухом
	#ON/FLOOD/OFF/MIST/THRU/TAP  
	
	if { [info exists mom_ude_coolant_mode] } {
		#set mom_coolant_mode ON
		MOM_force_block Once coolant_text
		switch $mom_ude_coolant_mode {
			"OFF (M9)" 	{ set mom_coolant_status OFF 
					      set mom_coolant_mode OFF
					    }
			"THRU_SPINDLE_1(M7)" 	{ set mom_coolant_status THRU1}
			"THRU (M26)" 			{ set mom_coolant_status THRU}
			"AIR (M27)" 			{ set mom_coolant_status AIR}
			"MIST (M28)"			{ set mom_coolant_status MIST}
			"FLOOD (M8)" 			{ set mom_coolant_status FLOOD}			
			default 				{ set mom_coolant_status ON}
			}
		}
		catch {unset mom_ude_coolant_mode}
	#MOM_output_to_listing_device ">>> mom_coolant_status: $mom_coolant_status $mom_sys_coolant_code($mom_coolant_status)"
	#MOM_output_literal ";>>> M$mom_sys_coolant_code($mom_coolant_status)"
}

#=============================================================
proc USER_5_AXIS_MODE { } {
#=============================================================
  global mom_output_mode_define mom_ude_5_axis_output_mode
  #ROTATE/VECTOR
  global mom_ude_spatial_mode
  #STAY/MOVE/TURN

#MOM_output_to_listing_device ";>>>USER_5_AXIS_MODE-> ON"
  if { [info exists mom_ude_5_axis_output_mode] } {
    switch $mom_ude_5_axis_output_mode {
      VECTOR {set mom_output_mode_define "VECTOR"}
      default {set mom_output_mode_define "ROTARY AXES"}
     }
  }
  }

#=============================================================
proc USER_check_plane_change_in_rapid_move { } {
#=============================================================
#=============================================================
# Check for the tool axis change during rapid move using AUTO_3D function
#
# 06-18-2012 Jason - Initial version
# 10-18-2013 Jason - Behavior change: table axis will not return to zero if the tool vector turn to be along Z axis in rapid move.
#MOM_output_to_listing_device 
  #MOM_output_literal ";>>>USER_check_plane_change_in_rapid_move"
  global mom_kin_machine_type
  global save_mom_kin_machine_type
  global mom_pos mom_mcs_goto
  global mom_cycle_spindle_axis
  global dpp_ge
  global seq
  global mom_out_angle_pos mom_prev_out_angle_pos
  global coord_rot_angle
  global dpp_TNC_plane_change_in_rapid ;#This variable is used to record the plane change status in rapid move.
  global mom_result
  global mom_tool_axis
   #MOM_output_literal ";FOO USER_check_plane_change_in_rapid_move"
   #MOM_output_literal ";Foo $dpp_ge(toolpath_axis_num)"
   #MOM_output_literal ";Foo $mom_kin_machine_type"
  if { ![string match "*5_axis*" $mom_kin_machine_type] } {
return
  }

  if { $dpp_ge(toolpath_axis_num)=="5" } {
return
  }

  if {[info exists dpp_ge(ncm_work_plane_change_mode)] && \
      ($dpp_ge(ncm_work_plane_change_mode) != "None" && $dpp_ge(ncm_work_plane_change_mode) != "direct_change")} {
return
  }

  set dpp_ge(coord_rot) [DPP_GE_COOR_ROT "ZYX" coord_rot_angle coord_offset pos]

  if { $dpp_ge(coord_rot) == "NONE" } {
     if {![info exists mom_prev_out_angle_pos(0)] || ![info exists mom_prev_out_angle_pos(1)]} {
return
     }
#Mitiouk <28-06-2019>
	
	#if ([info exists dpp_ge(coord_rot)] && $dpp_ge(coord_rot)=="AUTO3D") {
	#	return
	#}
#

     if {![EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] || ![EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
        MOM_output_literal "PLANE RESET STAY"
		#MOM_output_literal ";1"
        # if it's not auto3d condition, restore the kinematics and recalculate mom_pos
        DPP_GE_RESTORE_KINEMATICS

        if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
           set i 0
           foreach value $mom_result {
              set mom_pos($i) $value
              incr i
           }
        }

        MOM_reload_variable -a mom_pos
        MOM_force Once fourth_axis fifth_axis
        MOM_enable_address fourth_axis fifth_axis
        MOM_force Once fourth_axis fifth_axis
        MOM_do_template rapid_rotary
     }
return
  }
	#debug
     #MOM_output_literal ";dpp_ge(coord_rot): $dpp_ge(coord_rot)"
   
	 #global mom_pos
	 #MOM_output_literal ";>>>; x=$mom_pos(0) y=$mom_pos(1) z=$mom_pos(2)"
	 #MOM_output_literal ";>>> $dpp_ge(prev_coord_rot_angle,0) *** $coord_rot_angle(2)"
	 #MOM_output_literal ";>>> $dpp_ge(prev_coord_rot_angle,1) *** $coord_rot_angle(1)"
	 #MOM_output_literal ";>>> $dpp_ge(prev_coord_rot_angle,2) *** $coord_rot_angle(0)"
	#
  if {![EQ_is_equal $dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)] || \
      ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)] || \
	  ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)]} {
     
	 set dpp_ge(coord_rot_angle,0) $coord_rot_angle(2)
     set dpp_ge(coord_rot_angle,1) $coord_rot_angle(1)
     set dpp_ge(coord_rot_angle,2) $coord_rot_angle(0)
     set dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)
     set dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)
     set dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)
     VMOV 3  pos mom_pos
	 #debug
	#MOM_output_literal ";>>>; x=$mom_pos(0) y=$mom_pos(1) z=$mom_pos(2)"
	#debug
     if {[EQ_is_lt $mom_out_angle_pos(0) 0]} {
           set seq "SEQ-"
     } else {
           set seq "SEQ+"
     }

    #<Mitiouk> 04-05-2019
   global plane_positioning prev_plane_positioning
   if { ![info exists mom_first_motion] } {
       set prev_plane_potition $plane_positioning
       set plane_positioning "MOVE"
       set mom_first_motion 1
	   #debug
       #MOM_output_to_listing_device ">>> Set mom_first_motion"
       #MOM_output_literal ";>>> Set mom_first_motion"
       #
	   MOM_do_template plane_spatial
       MOM_disable_address fourth_axis fifth_axis

    } else {
        if { [info exists prev_plane_positioning] } {
            set plane_positioning $prev_plane_positioning
				MOM_do_template plane_spatial_move
				MOM_disable_address fourth_axis fifth_axis
            }
    }

    #MOM_do_template plane_spatial_move
     MOM_disable_address fourth_axis fifth_axis

     set dpp_TNC_plane_change_in_rapid 1
	 
	 USER_CUSTOM_RAPID_MOVE AUTO
  }

}

#=============================================================
proc USER_cycle_coord_rotation { } {
#=============================================================
#=============================================================
# Handle variable-axis drilling cycles using AUTO_3D function
#
#06-18-2012 Jason - Initial version
#10-18-2013 Jason - Behavior change: table axis will not return to zero if the tool vector turn to be along Z axis in cycle plane change.
  #MOM_output_to_listing_device ":>>> Foo USER_cycle_coord_rotation"
  global mom_kin_machine_type
  global save_mom_kin_machine_type
  global mom_tool_axis mom_tool_axis_type
  global mom_5axis_control_pos
  global mom_pos mom_mcs_goto
  global mom_prev_out_angle_pos mom_out_angle_pos
  global mom_prev_tool_axis
  global mom_cycle_rapid_to_pos mom_cycle_retract_to_pos mom_cycle_feed_to_pos
  global mom_cycle_rapid_to mom_cycle_retract_to mom_cycle_feed_to
  global mom_cycle_spindle_axis
  global mom_kin_machine_type
  global mom_cycle_retract_mode
  global js_return_pos js_prev_pos
  global dpp_ge
  global dpp_cycle_plane_real_change
  global seq
  global mom_out_angle_pos
  global dpp_TNC_plane_change_in_rapid
  global coord_rot_angle
  global mom_result
  global mom_kin_machine_type
  global user_coord_rotation_flag; #Mitiouk
  
  if { ![string match "*5_axis*" $mom_kin_machine_type] } { return }

  if {[info exists dpp_ge(ncm_work_plane_change_mode)] && \
      ($dpp_ge(ncm_work_plane_change_mode) != "None" && $dpp_ge(ncm_work_plane_change_mode) != "direct_change")} {
return
  }

  set user_coord_rotation_flag 1; #Mitiouk
  set dpp_ge(coord_rot) [DPP_GE_COOR_ROT "ZYX" coord_rot_angle coord_offset pos]

  if { $dpp_ge(coord_rot) == "NONE" } {
     if {![EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] || ![EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
        MOM_output_literal "PLANE RESET STAY"
		#MOM_output_literal ";2"

        # if it's not auto3d condition, restore the kinematics and recalculate mom_pos
        DPP_GE_RESTORE_KINEMATICS
        if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
           set i 0
           foreach value $mom_result {
                 set mom_pos($i) $value
                 incr i
           }
        }

        VMOV 3 mom_pos mom_cycle_feed_to_pos
        MOM_reload_variable -a mom_cycle_feed_to_pos
        MOM_reload_variable -a mom_pos

        MOM_enable_address fourth_axis fifth_axis
        MOM_force Once fourth_axis fifth_axis
        MOM_do_template rapid_rotary
     }
return
  }

  if { $dpp_ge(coord_rot) == "LOCAL_CSYS" } {
     if { ![EQ_is_equal $mom_tool_axis(2) 1.0] } {
        MOM_output_to_listing_device "Warning in $mom_operation_name: Wrong Local MCS, Z axis is not parallel to tool axis vector."
       }
return
  }

  if {![EQ_is_equal $dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)] || ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)] || ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)]} {
     set dpp_cycle_plane_real_change 1
     set dpp_ge(coord_rot_angle,0) $coord_rot_angle(2)
     set dpp_ge(coord_rot_angle,1) $coord_rot_angle(1)
     set dpp_ge(coord_rot_angle,2) $coord_rot_angle(0)
     set dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)
     set dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)
     set dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)
     VMOV 3  pos mom_pos
  } else {
     set dpp_cycle_plane_real_change 0
  }

  if {$dpp_cycle_plane_real_change ==1} {

     set mom_cycle_spindle_axis 2

     if {[EQ_is_lt $mom_out_angle_pos(0) 0]} {
        set seq "SEQ-"
     } else {
        set seq "SEQ+"
     }
	
     MOM_do_template plane_spatial;#Mitiouk
     MOM_disable_address fourth_axis fifth_axis
  }

  if { $dpp_cycle_plane_real_change ==1 || $dpp_TNC_plane_change_in_rapid ==1 } {
     if { [string match "AUTO" $mom_cycle_retract_mode] } {
        set js_return_pos [expr $js_prev_pos - $mom_pos(2)] ;# calc incr retract
        if { [EQ_is_lt $js_return_pos $mom_cycle_retract_to] } {
           set js_return_pos $mom_cycle_retract_to
        }
     } else {
       set js_return_pos $mom_cycle_retract_to
     }

     VMOV 3 mom_pos mom_cycle_rapid_to_pos
     VMOV 3 mom_pos mom_cycle_feed_to_pos
     VMOV 3 mom_pos mom_cycle_retract_to_pos
     set mom_cycle_rapid_to_pos(2) [expr $mom_pos(2)+$js_return_pos]
     set mom_cycle_retract_to_pos(2) [expr $mom_pos(2)+$mom_cycle_retract_to]
     set mom_cycle_feed_to_pos(2) [expr $mom_pos(2)+$mom_cycle_feed_to]

     set mom_pos(0) [format %.4f "$mom_pos(0)"]
     set mom_pos(1) [format %.4f "$mom_pos(1)"]
     set mom_cycle_rapid_to_pos(2) [format %.4f "$mom_cycle_rapid_to_pos(2)"]

     MOM_force Once X Y Z
     MOM_output_literal "L X$mom_pos(0) Y$mom_pos(1) Z$mom_cycle_rapid_to_pos(2) R0 FMAX"
	 #MOM_output_literal "L X$mom_pos(0) Y$mom_pos(1) Z$mom_cycle_rapid_to_pos(2) R0 FMAX"
  }
}

#=============================================================
proc USER_coord_rotation_in_operation { } {
#=============================================================
# This command is used to detect rotary axis change inside operation for 3+2 milling.
# This command will output coordinate rotation code if the rotary axis change the
# position.
#MOM_output_to_listing_device ">>>USER_coord_rotation_in_operation"
  global mom_kin_machine_type
  global mom_tool_axis mom_prev_tool_axis mom_tool_axis_type
  global mom_tnc_5axis_control_mode mom_5axis_control_pos
  global mom_pos mom_prev_pos mom_mcs_goto
  global mom_prev_out_angle_pos mom_out_angle_pos
  global coord_offset
  global seq plane_positioning
  global dpp_ge mom_result
  global dpp_TNC_plane_change_in_rapid

  if { ![string match "*5_axis*" $mom_kin_machine_type] } {
return
  }

  if {[info exists mom_5axis_control_pos] && $mom_5axis_control_pos ==1} {
return
  }

  if {[info exists mom_tnc_5axis_control_mode] && $mom_tnc_5axis_control_mode == "M128"} {
return
  }

  DPP_GE_GET_NCM_WORK_PLANE_CHANGE_MODE
  #MOM_output_literal ";Foo dpp_ge(ncm_work_plane_change_mode):$dpp_ge(ncm_work_plane_change_mode)"
  #MOM_output_literal ";Foo dpp_ge(coord_rot)=$dpp_ge(coord_rot)"
  if {$dpp_ge(ncm_work_plane_change_mode) == "direct_change"} {
      # work plane changed , reset work plane
     # PB_CMD_auto_3D_rotation
	 
     if {$dpp_ge(coord_rot) != "NONE"} {
		MOM_do_template plane_reset
        #MOM_output_literal "PLANE RESET STAY"
		#MOM_output_literal "M140 MB MAX"
		#MOM_output_literal ";foo 3"
		set plane_positioning "MOVE ABST0"
		return
     }

      set dpp_ge(coord_rot) [DPP_GE_COOR_ROT "ZYX" coord_rot_angle coord_offset pos]
      set dpp_ge(coord_rot_angle,0) $coord_rot_angle(2)
      set dpp_ge(coord_rot_angle,1) $coord_rot_angle(1)
      set dpp_ge(coord_rot_angle,2) $coord_rot_angle(0)
      set dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)
      set dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)
      set dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)

      set plane_positioning "MOVE ABST0"

      if {$dpp_ge(coord_rot) == "NONE"} {
          MOM_enable_address fourth_axis fifth_axis
          MOM_force Once fourth_axis fifth_axis
          MOM_do_template rapid_rotary
          DPP_GE_RESTORE_KINEMATICS
          if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
             set i 0
             foreach value $mom_result {
                 set mom_pos($i) $value
                 incr i
             }
          }
      } else {
          if {[EQ_is_lt $mom_out_angle_pos(0) 0]} {
              set seq "SEQ-"
          } else {
              set seq "SEQ+"
          }
          set mom_5axis_control_pos 2
          MOM_do_template plane_spatial_move; #org MOM_do_template plane_spatial
          MOM_disable_address fourth_axis fifth_axis
          VMOV 3 pos mom_pos
          MOM_reload_variable -a mom_pos
          set dpp_TNC_plane_change_in_rapid 1
      }
  } elseif {$dpp_ge(ncm_work_plane_change_mode) != "None"} {
      if {$dpp_ge(ncm_work_plane_change_mode) == "start_change"} {
          # 5x non-cutting motion start
          # cancle tilt work plane
          if {$dpp_ge(coord_rot) != "NONE"} {
			
             MOM_output_literal "PLANE RESET STAY"
			 #MOM_output_literal ";4"
          }
          set dpp_ge(prev_coord_rot_angle,0)  0
          set dpp_ge(prev_coord_rot_angle,1)  0
          set dpp_ge(prev_coord_rot_angle,2)  0

          # output M128
		  #MOM_output_literal ";Foo USER_coord_rotation_in_operation"
		  MOM_force once feed_variable
          MOM_do_template output_m128_rapid
          set dpp_ge(itnc_ncm_m128) 1
		  #MOM_output_literal "M140 MB MAX"
          #MOM_disable_address fourth_axis fifth_axis;#Mitiouk
		  MOM_enable_address fourth_axis fifth_axis
          MOM_force once X Y Z fourth_axis fifth_axis
          DPP_GE_RESTORE_KINEMATICS
     }
      # set mom_pos to mom_mcs_goto
      global mom_mcs_goto mom_pos
      global mom_prev_mcs_goto mom_prev_pos
      global mom_arc_center mom_pos_arc_center
      set mom_pos(0)    $mom_mcs_goto(0)
      set mom_pos(1)    $mom_mcs_goto(1)
      set mom_pos(2)    $mom_mcs_goto(2)
      set mom_prev_pos(0)    $mom_prev_mcs_goto(0)
      set mom_prev_pos(1)    $mom_prev_mcs_goto(1)
      set mom_prev_pos(2)    $mom_prev_mcs_goto(2)
  } else {
      if {[info exists dpp_ge(itnc_ncm_m128)] && $dpp_ge(itnc_ncm_m128) == 1} {
      #check if current output mode is M128, if yes, then cancel it
          MOM_output_literal "M129"
          set dpp_ge(itnc_ncm_m128) 0

      #check if current work plane is tilted
           #PB_CMD_auto_3D_rotation
          set dpp_ge(coord_rot) [DPP_GE_COOR_ROT "ZYX" coord_rot_angle coord_offset pos]
          set dpp_ge(coord_rot_angle,0) $coord_rot_angle(2)
          set dpp_ge(coord_rot_angle,1) $coord_rot_angle(1)
          set dpp_ge(coord_rot_angle,2) $coord_rot_angle(0)
          set dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)
          set dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)
          set dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)

          set plane_positioning "MOVE ABST0"
          set seq ""

          if { $dpp_ge(coord_rot) == "NONE"} {
              MOM_enable_address fourth_axis fifth_axis
              MOM_force Once fourth_axis fifth_axis
              MOM_do_template rapid_rotary
          } else {
              set mom_5axis_control_pos 2
              MOM_do_template plane_spatial
              MOM_disable_address fourth_axis fifth_axis
              VMOV 3 pos mom_pos
              MOM_reload_variable -a mom_pos
              set dpp_TNC_plane_change_in_rapid 1
          }
      }
  }
}

#=============================================================
proc USER_CLEAR_FOLDER { } {
#=============================================================
  global ptp_file_name

   #MOM_output_to_listing_device ">>>File: $ptp_file_name"
  set dir [file dirname $ptp_file_name]
   
  set out_list [glob -directory $dir *.out]
    if {[info exists out_list] && [llength $out_list] > 6 && [lindex $out_list 0] != 0} {
	MOM_output_to_listing_device ">>>USER_CLEAR_FOLDER IS RUN"
	MOM_output_to_listing_device ">>>Dir:$dir\n==========List========================\n"
	foreach a $out_list {
	  #file delete $a
	  set f_owned [file owned $a] 
	  #set f_writable [file writable $a] 
	  #set file_descr [open $a w]
	  #close $file_descr
	  catch {file delete $a} opt
	  
	  if {$opt!=""} {
	    MOM_output_to_listing_device ">>> $opt\n"
	  } else {
		MOM_output_to_listing_device "Delete file: [file tail $a]"
	  }
	  
    }
	} else {
	MOM_output_to_listing_device ">>> No files .out search"
	}

}

#=============================================================
proc USER_OUT_TOOL { } {
#=============================================================
  global mom_motion_event
  global mom_tool_number mom_tool_name
  global mom_tool_change_prohibit
  
  MOM_output_literal ";========= TOOL CHANGE =========="
  MOM_output_literal "PLANE RESET STAY"
  
  if {[info exists mom_tool_change_prohibit] && $mom_tool_change_prohibit==1 } {
	return
	MOM_output_literal ";VNIMANIE! SMENA INSTRUMENTA WRUCHNUYU!"
	MOM_output_to_listing_device ";VNIMANIE! SMENA INSTRUMENTA WRUCHNUYU!"
	MOM_output_literal "L Y1010.0 FQL2 M91;"
	MOM_output_literal "L X0.0 FQL2 M91;"
	MOM_output_literal "L A0.0 FQL2 M91;"
	MOM_output_literal "M0"
  }
  if { [info exists mom_motion_event] && $mom_motion_event != "initial_move" } {
    MOM_output_literal "TOOL CALL Z"
	MOM_do_template m140
  }
 
 MOM_do_template return_home_z
 MOM_do_template return_home_xy
 #MOM_do_template return_home_ac
 MOM_force once T S M
 MOM_do_template tool_change
 MOM_force once M_spindle
 MOM_do_template spindle_on
 
 #MOM_output_literal ";FOO $mom_tool_number"
 
 #  if {[PB_CMD__check_block_spindle_on]} {
 #    MOM_do_template spindle_on
 #  } else {MOM_output_literal ";SPINDLE OFF"}
   if {[PB_CMD__check_block_tool_preselect]} {
     MOM_do_template tool_preselect_1
   }
  #MOM_do_template return_home_z
  MOM_force once A C
  MOM_output_literal ";======= END OF TOOL CHANGE ========"
}

#=============================================================
proc USER_AXIS_CLAMP { } {
	global mom_ude_clamp_a_axis mom_ude_clamp_c_axis
	if {[info exists mom_ude_clamp_a_axis]} {
		switch $mom_ude_clamp_a_axis {
			"CLAMP" 	{ MOM_do_template four_axis_clamp }
			"NON CLAMP"	{ MOM_do_template four_axis_unclamp}
			default {} 
		}
	}
	if {[info exists mom_ude_clamp_c_axis]} {
		switch $mom_ude_clamp_c_axis {
		"CLAMP" 	{ MOM_do_template fifth_axis_clamp }
		"NON CLAMP" { MOM_do_template fifth_axis_unclamp }
		default {}	
		}
	
	}
	catch {
		unset mom_ude_clamp_a_axis
		unset mom_ude_clamp_c_axis
	}
}

#=============================================================
proc USER_preposition_m128 {} {
# <Mitiouk> 17-06-2019 
# вывод блока предпозиционирования перед обработкой 5-осей
# необходим блок plane_spatial initial_move_6 initial_move_6_1 plane_reset output_m128
# =====================================================
  global mom_out_angle_pos seq
  global dpp_ge
  global mom_kin_machine_type
  global save_mom_kin_machine_type
  global mom_pos mom_mcs_goto
  global mom_cycle_spindle_axis
  global dpp_ge
  global seq
  global mom_out_angle_pos mom_prev_out_angle_pos
  global coord_rot_angle
  global dpp_TNC_plane_change_in_rapid ;#This variable is used to record the plane change status in rapid move.
  global mom_result
  global mom_tool_axis
  PB_CMD_FEEDRATE_SET
  #MOM_output_literal ";>>> Foo  USER_preposition_m128"
	if { [PB_CMD__check_block_output_m128] } {

	 if {[EQ_is_lt $mom_out_angle_pos(0) 0]} {
      set seq "SEQ-"
     } else {
      set seq "SEQ+"
     }

# Для проверки использовать 	
	if {[info exists dpp_ge(ncm_work_plane_change_mode)] } {
	 #MOM_output_literal "; FOO $dpp_ge(ncm_work_plane_change_mode)"
	 }
	 if {[info exists dpp_ge(ncm_work_plane_change_mode)] && \
      ($dpp_ge(ncm_work_plane_change_mode) != "None" && $dpp_ge(ncm_work_plane_change_mode) != "direct_change")} {
	  	   
		  return
    }
  
  set dpp_ge(coord_rot) [DPP_GE_COOR_ROT "ZYX" coord_rot_angle coord_offset pos] 
  #MOM_output_literal ";dpp_ge/(coord_rot/) = $dpp_ge(coord_rot)"
  if { $dpp_ge(coord_rot) == "NONE" } {
	if { ![info exists mom_prev_out_angle_pos] } {
		set mom_prev_out_angle_pos(0) 0
		set mom_prev_out_angle_pos(1) 0
		}
	  USER_CUSTOM_RAPID_MOVE AUTO;#XYZ
	  
	  MOM_do_template plane_reset
	  global mom_post_in_simulation mom_output_mode_define
	  if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
	    # posible value $mom_post_in_simulation 0/CSE/SYN/MTD
		#MOM_output_literal ";mom_post_in_simulation = $mom_post_in_simulation"
	    MOM_do_template output_m128
	  } else {
	      MOM_do_template output_m128
		  #MOM_output_literal "FUNCTION TCPM F TCP AXIS POS"
	      #MOM_output_literal "PATHCTRL VECTOR"
		  #MOM_force once feed_variable X Y Z A C ;#F
	    }
	  
     if {![EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] || ![EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
        MOM_output_literal "PLANE RESET STAY"
		#MOM_output_literal ";2"

        # if it's not auto3d condition, restore the kinematics and recalculate mom_pos
        DPP_GE_RESTORE_KINEMATICS
		
        if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
           set i 0
           foreach value $mom_result {
                 set mom_pos($i) $value
                 incr i
           }
        }		
        VMOV 3 mom_pos mom_cycle_feed_to_pos
        MOM_reload_variable -a mom_cycle_feed_to_pos
        MOM_reload_variable -a mom_pos

        MOM_enable_address fourth_axis fifth_axis
        MOM_force Once fourth_axis fifth_axis
        MOM_do_template rapid_rotary
     }
    return
  }
 
  if {![EQ_is_equal $dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)] || ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)] || ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)]} {
     set dpp_cycle_plane_real_change 1
     set dpp_ge(coord_rot_angle,0) $coord_rot_angle(2)
     set dpp_ge(coord_rot_angle,1) $coord_rot_angle(1)
     set dpp_ge(coord_rot_angle,2) $coord_rot_angle(0)
     set dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)
     set dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)
     set dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)
     VMOV 3  pos mom_pos
  } else {
     set dpp_cycle_plane_real_change 0
  }
	   #MOM_output_literal ";>>> Foo  USER_preposition_m128"
	  
	  #Конвертирует координаты точки
	  global mom_mcs_goto mom_tool_axis mom_pos
	  global mom_result  mom_result1 mom_tool_path_pos
	  global mom_post_result mom_post_result1
	  
	 #set tmp [MOM_convert_point mom_mcs_goto mom_tool_axis]
	 #MOM_output_literal ";Foo>>> TMP = $tmp"
	 #MOM_output_literal ";Foo     pos(x): $pos(0)\tpos(y): $pos(1) pos(z): $pos(2)"
	 #MOM_output_literal ";Foo mom_pos(x): $mom_pos(0)\t mom_pos(y): $mom_pos(1) mom_pos(z): $mom_pos(2)"
	  if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
	 
		#
		# if {[info exists mom_result]} {
		#	MOM_output_literal ";mom_result(i):$mom_result"
		# } else {MOM_output_literal ";mom_result is nothing"}
		# if {[info exists mom_result]} {
		#	MOM_output_literal ";mom;result1(i):$mom_result1"
		# } else {MOM_output_literal ";mom_result1 is nothing"}
		#
		#MOM_output_literal "; dpp_ge(coord_rot_angle,2)=$dpp_ge(coord_rot_angle,2)"
           set i 0
        if {[lindex $mom_result 4] == $mom_tool_path_pos} {  
		   foreach value $mom_result {
              set mom_pos($i) $value
              incr i }
           } elseif {[lindex $mom_result1 4] == $mom_tool_path_pos} {
			 foreach value $mom_result1 {
              set mom_pos($i) $value
              incr i}
		   } else { exit }
        }
		
	 #MOM_output_literal ";Foo     pos(x): $pos(0)\tpos(y): $pos(1) pos(z): $pos(2)"
	 #MOM_output_literal ";Foo mom_pos(x): $mom_pos(0)\t mom_pos(y): $mom_pos(1) mom_pos(z): $mom_pos(2)"

	 
	#поворот ск
	  MOM_do_template plane_spatial
	  USER_CUSTOM_RAPID_MOVE XYZ
	  MOM_do_template plane_reset
	  
	  global mom_post_in_simulation mom_output_mode_define
	  if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
	    # posible value $mom_post_in_simulation 0/CSE/SYN/MTD
		#MOM_output_literal ";mom_post_in_simulation = $mom_post_in_simulation"
	    MOM_do_template output_m128
	  } else {
		   MOM_do_template output_m128
	      #MOM_output_literal "FUNCTION TCPM F TCP AXIS POS"
	      #MOM_output_literal "PATHCTRL VECTOR"
	    }
	  #PB_CMD__check_block_output_m128
	  global dpp_ge
	  global mom_prev_mcs_goto mom_prev_pos mom_arc_center mom_pos_arc_center
	  global mom_kin_arc_output_mode mom_kin_helical_arc_output_mode
	  if { $dpp_ge(toolpath_axis_num)=="5" } {
      #MOM_force Once fourth_axis fifth_axis
       VMOV 3 mom_mcs_goto mom_pos
       VMOV 3 mom_prev_mcs_goto mom_prev_pos
       VMOV 3 mom_arc_center mom_pos_arc_center
       set mom_kin_arc_output_mode "LINEAR"
       set mom_kin_helical_arc_output_mode "LINEAR"
      MOM_reload_kinematics
	  }
	  if { [PB_CMD__check_block_rotary_axes] && [PB_CMD__check_block_rapid_rotary] } {
		#MOM_do_template rapid_rotary
	  }	  
	}
  return
}

#=============================================================
proc USER_CUSTOM_RAPID_MOVE { args } {
#=============================================================
# Настраиваемый первый подход
global mom_tool_axis mom_prev_tool_axis
global dpp_ge
	MOM_force once X Y Z feed_variable
	MOM_suppress once forth_axis fifth_axis
	if {[info exists args]} {
	  set l [llength $args]; #количество аргументов
	  #MOM_output_literal ";>>>USER_CUSTOM_RAPID_MOVE: $args"
							#MOM_output_to_listing_device ">>>AX [EQ_is_equal $mom_tool_axis(0) 1]"
							#MOM_output_to_listing_device ">>>AY [EQ_is_equal $mom_tool_axis(1) 1]"
							#MOM_output_to_listing_device ">>>AZ [EQ_is_equal $mom_tool_axis(2) 1]"
							#MOM_output_to_listing_device ">>>AX [EQ_is_equal $mom_tool_axis(0) -1]"
							#MOM_output_to_listing_device ">>>AY [EQ_is_equal $mom_tool_axis(1) -1]"
							#MOM_output_to_listing_device ">>>AZ [EQ_is_equal $mom_tool_axis(2) -1]"
	  foreach a $args {
			;#тело обработчика
			switch $a {
				"X" 	{}
				"Y" 	{}
				"Z" 	{ MOM_do_template rapid_z }
				"XY" 	{ MOM_do_template rapid_x_y }
				"XYZ"	{ MOM_do_template rapid_x_y_z}
				"AUTO" 	{ if {
							  [EQ_is_equal $mom_tool_axis(0) 1] || \
				              [EQ_is_equal $mom_tool_axis(0) -1.] || \
							  [EQ_is_equal $mom_tool_axis(1) 1.] || \
							  [EQ_is_equal $mom_tool_axis(1) -1.] || \
				              [EQ_is_equal $mom_tool_axis(2) 1.] || \
							  [EQ_is_equal $mom_tool_axis(2) -1.] } {
									MOM_force once X Y feed_variable
									MOM_do_template rapid_x_y								
									#MOM_do_template rapid_z
						
						} else {
									#если пятиосёвка то вывод с осями
									if {[info exists dpp_ge(toolpath_axis_num)] && $dpp_ge(toolpath_axis_num)=="5" } {
										MOM_force Once X Y Z
										global user_rotate
					                    if { ![info exists user_rotate] } {
										  MOM_force Once fifth_axis fourth_axis
										 }
										MOM_do_template rapid_x_y_z_a_c
									} else {
										MOM_force once X Y Z ;#feed_variable
										#MOM_do_template rapid_x_y_z
										#MOM_disable_address once X Y Z 
									}
								}
				
				}
				default {
					global user_rotate
					if { ![info exists user_rotate] } {
					MOM_force once forth_axis fifth_axis 
					}
					MOM_do_template rapid_x_y_z_a_c
				}		
			}
	  }	
	} else {
			MOM_do_template rapid_x_y
			MOM_do_template rapid_z 
			}
	#MOM_suppress once forth_axis fifth_axis
	MOM_force Once rapid1 rapid2 rapid3
}

#=============================================================
proc USER_OUT_WORKPIECE { } {
#=============================================================
uplevel #0 {
  global mom_ude_workpiece_set
  global mom_ude_workpiece_fp mom_ude_workpiece_sp
  global mom_blk_check_out
  if { [info exists mom_blk_check_out] && $mom_blk_check_out != 0 } {
	return 0
  }
  
  if {[info exists mom_ude_workpiece_set] && $mom_ude_workpiece_set=="ACTIVE"} {
    set x_min [format "%+0.3f" $mom_ude_workpiece_fp(0)]
	set y_min [format "%+0.3f" $mom_ude_workpiece_fp(1)]
	set z_min [format "%+0.3f" $mom_ude_workpiece_fp(2)]
	set x_max [format "%+0.3f" $mom_ude_workpiece_sp(0)]
	set y_max [format "%+0.3f" $mom_ude_workpiece_sp(1)]
	set z_max [format "%+0.3f" $mom_ude_workpiece_sp(2)]
	
	MOM_output_literal "BLK FORM 0.1 Z X$x_min Y$y_min Z$z_min"; #MIN POINT
    MOM_output_literal "BLK FORM 0.2 X$x_max Y$y_max Z$z_max"; #MAX POINT
	
	set mom_blk_check_out 1
  }
 }
}

#=============================================================
proc USER_RESET_KINEMATIC { } {
#=============================================================
#<Mitiou> 05-04-2019
# Used in PB_CMD__check_block_reset_cycle800
#=============================================================
 global mom_tool_axis mom_out_angle mom_pos
 global mom_prev_pos mom_out_angle_pos mom_prev_out_angle_pos

  set mom_pos(3) 0.0
  set mom_pos(4) 0.0
  set mom_prev_pos(3) 0.0
  set mom_prev_pos(4) 0.0

 MOM_reload_variable -a mom_prev_pos
 MOM_reload_variable -a mom_pos
}

#=============================================================
proc USER_HAND_TOOL_CHANGE { } {
# Смена инструмента в ручную
	
  global mom_motion_event
  global mom_tool_number mom_tool_name
  global mom_tool_change_prohibit
  global mom_sys_hand_tc_pos
  global mom_next_tool_number
  global mom_tool_number
  MOM_output_to_listing_device ";====== HAND TOOL CHANGE ======="
  MOM_output_literal ";====== HAND TOOL CHANGE ======="
  MOM_output_literal "PLANE RESET STAY"
  if { [info exists mom_motion_event] && $mom_motion_event != "initial_move" } {
    MOM_output_literal "TOOL CALL Z"
	MOM_do_template m140
  } 
 MOM_do_template return_home_z
 MOM_do_template return_home_xy
 MOM_force once T S M
 #Сменить выбранный инструмент (загрузить пустую оправку)
 MOM_do_template tool_change
 #Обороты не включать
 MOM_do_template spindle_off
 #сдвинуть стол в крайнюю позицию по X$
 MOM_output_literal "L A0.0 FQL2"
 MOM_output_literal "L X$mom_sys_hand_tc_pos(0) FQL2 M91;"
 MOM_output_literal "L Y$mom_sys_hand_tc_pos(1) FQL2 M91;"
 #Переместить шпиндель В позицию Y
 #Остановить выполнение программы
 MOM_output_literal "M0; SMENI INSTRUMENT!"
 #Установить запрет автоматической смены инструмента
 MOM_output_literal "L Y1010.0 FQL2 M91;"
 MOM_output_literal "L X0.0 FQL2 M91;"
 MOM_force once T S M
 MOM_do_template spindle_on
 MOM_output_literal ";==== HAND TOOL CHANGE END ====" 
 set mom_tool_change_prohibit 1
 #  if {[PB_CMD__check_block_spindle_on]} {
 #    MOM_do_template spindle_on
 #  } else {MOM_output_literal ";SPINDLE OFF"}
   if {[PB_CMD__check_block_tool_preselect]} {
     MOM_do_template tool_preselect_1
   }
  #MOM_do_template return_home_z
  #MOM_force once A C
  #MOM_output_literal ";======= END OF TOOL CHANGE ========"

#=============================================================
}

#=============================================================
proc USER_liner_divide { } {
	global mom_machine_mode mom_tool_axis
	global mom_pos mom_prev_pos
	global RAD2DEG DEG2RAD
	global mom_kin_linearization_tol 
	global mom_trans_flag
	if {![info exists mom_trans_flag]} {set mom_trans_flag F; return}
	array set mom_incr_pos {0 0.0 1 0.0 2 0.0}
	if { [string compare $mom_machine_mode "MILL"] } { return }
	if { [EQ_is_equal [expr abs($mom_tool_axis(2))] 1.0] } {
	
	#делит линейные перемещения на участки для режима трансформации
	MOM_output_literal ";Foo Linear_divide"
	set mom_incr_pos(0) [ expr $mom_pos(0)-$mom_prev_pos(0) ]
	set mom_incr_pos(1) [ expr $mom_pos(1)-$mom_prev_pos(1) ]
	set mom_incr_pos(2) [ expr $mom_pos(2) - $mom_prev_pos(2) ]
	set mom_line_lenth [ expr hypot($mom_incr_pos(0), $mom_incr_pos(1)) ]
	set mom_line_angl [ expr atan2($mom_incr_pos(1), $mom_incr_pos(0)) ]
	MOM_output_literal ";Foo Angle: [expr $RAD2DEG *$mom_line_angl]"
	set mom_incr_len 10.;#$mom_kin_linearization_tol
	set mom_line_step [ expr round ($mom_line_lenth / $mom_incr_len)] 
	set mom_remainder [expr abs($mom_line_lenth) - $mom_line_step * abs($mom_incr_len) ]
	if {$mom_remainder < 0 && $mom_line_step!=0} {
		incr mom_line_step 
		set mom_incr_len [expr $mom_line_lenth / $mom_line_step]
	}
	
	set mom_inc_pos(1) [expr sin($mom_line_angl)*$mom_incr_len]
	set mom_inc_pos(0) [expr cos($mom_line_angl)*$mom_incr_len]
	set trans_prev_pos(0) $mom_prev_pos(0)
	set trans_prev_pos(1) $mom_prev_pos(1)
	
	for {set ii 1} {$ii < $mom_line_step} {incr ii} {
		set trans_pos(0) [expr $trans_prev_pos(0) + $mom_inc_pos(0)]
		set trans_pos(1) [expr $trans_prev_pos(1) + $mom_inc_pos(1)]
		#PB_CMD_linear_move
		set org_angle [expr atan2($trans_pos(1),$trans_pos(0))]
		set hyp_trans [expr hypot($trans_pos(1),$trans_pos(0))]
		MOM_output_literal ";Foo org_angle: [expr $RAD2DEG * $org_angle] LIN:$hyp_trans"
		set out_x [format "%+.4f" $trans_pos(0)]
		set out_y [format "%+.4f" $trans_pos(1)]
		USER_CYCL_10 [expr $RAD2DEG * $org_angle * -1]
		MOM_output_literal "L C[expr $RAD2DEG * $org_angle * -1]"
		MOM_output_literal "L X$out_x Y$out_y"
		set trans_prev_pos(0) $trans_pos(0)
		set trans_prev_pos(1) $trans_pos(1)
	} }
	
	set mom_r [ expr sqrt($mom_pos(0) * $mom_pos(0) + $mom_pos(1) * $mom_pos(1)) ]
		
	MOM_output_literal ";Foo mom_pos: $mom_pos(0),$mom_pos(1),$mom_pos(2)"
	MOM_output_literal ";Foo mom_prev_pos: $mom_prev_pos(0),$mom_prev_pos(1),$mom_prev_pos(2)"
	MOM_output_literal ";Foo mom_incr_pos: $mom_incr_pos(0),$mom_incr_pos(1),$mom_incr_pos(2)"
	MOM_output_literal ";Foo mom_line_lenth: $mom_line_lenth mom_line_angl:$mom_line_angl"
	MOM_output_literal ";Foo mom_line_step: $mom_line_step | mom_incr_len:$mom_incr_len"
	MOM_output_literal ";Foo mom_inc_pos: $mom_inc_pos(0) | mom_inc_pos:$mom_inc_pos(1)"
}

#=============================================================
proc ARCTAN { y x } {
#=============================================================
	global PI

	if { [EQ_is_zero $y] } { set y 0 }
	if { [EQ_is_zero $x] } { set x 0 }
	if { [expr $y == 0] && [expr $x == 0] } { return 0 }
	set ang [expr atan2($y,$x)]
	if { $ang < 0 } {
      		return [expr $ang + $PI*2]
	} else {
		return $ang
	}
}

#=============================================================
proc USER_set_c_axis_mode { } {
#=============================================================
	global mom_mcs_goto mom_kin_5th_axis_rotation mom_tool_axis mom_pos mom_kin_5th_axis_ang_offset
	global RAD2DEG mom_sys_lathe_x_factor mom_sys_spindle_axis mom_out_angle_pos mom_tool_axis mom_pos
	global mom_machine_mode mom_coordinate_output_mode

	if { [string compare $mom_machine_mode "MILL"] } { return }
	if { [EQ_is_equal [expr abs($mom_tool_axis(2))] 1.0] } {
		set transmit_pos(0) [expr hypot($mom_mcs_goto(0),$mom_mcs_goto(1))]
		set transmit_pos(1) 0.0
		set transmit_pos(4) [expr ([ARCTAN $mom_mcs_goto(1) $mom_mcs_goto(0)])*$RAD2DEG*$mom_sys_spindle_axis(2)]
		if { ![string compare "reverse" $mom_kin_5th_axis_rotation] } { set transmit_pos(4) [expr -1*$transmit_pos(4)] }
		set transmit_pos(4) [expr $transmit_pos(4) + $mom_kin_5th_axis_ang_offset]
		set transmit_pos(4) [LIMIT_ANGLE $transmit_pos(4)]
		set mom_pos(0) $transmit_pos(0)
		set mom_pos(1) $transmit_pos(1)		
		set mom_out_angle_pos(1) $transmit_pos(4)
		MOM_reload_variable -a mom_pos
		MOM_reload_variable -a mom_out_angle_pos
	}
}

#=============================================================
proc USER_CYCL_10 {arg} {
#=============================================================
	global cycl_10_prev_angle
	set rot_angle [format "%+.4f" $arg]
	if {![info exists cycl_10_prev_angle]} {
	  set cycl_10_prev_angle 0.0
	} else {
	    if {$rot_angle!=$cycl_10_prev_angle} {
	       MOM_output_literal "CYCL DEF 10.0 POWOROT"
           MOM_output_literal "CYCL DEF 10.1  ROT$rot_angle"
		   set cycl_10_prev_angle $rot_angle
		} 
	}
}

#=============================================================
proc MOM_output_cycle { {a ""} } {
#=============================================================
	# Функция обеспечивает аккумулирование текста и последующий его модальный модальный вывод
	# Вызывается со значимым аргументом для добавления информации
	# Вызывается без аргумента или с пустым аргументом для вывода текста в программу
	# Вызывается с аргументом "-reset" для сброса
	# MOM_output_cycle "текст" /добавление текста в буфер
	# MOM_output_cycle /вывод цикла
	# MOM_output_cycle "-reset" /сброс
	# MOM_output_cycle "-foreach" выводит параметры цикла для каждого вызова
	# Применение:
	# Весь текст цикла выводим в функцию MOM_output_cycle "TEXT"
	# когда всё отправлено вызываем MOM_output_cycle без аргументов
	# в событии конец_цикла вызываем MOM_output_cycle -reset для сброса содержимого предварительного вывода 
	
	global cycle_buffer buffer_list
	global prev_cycle_buffer
	#<Mitiouk> 12-09-2019 Выводим определение цикла для каждого вызова
	if {[string length $a] == 0} {set a "-foreach"}
	#<Mitiouk> 12-09-2019
	if { [string equal $a "-foreach"] } {
	       catch {unset prev_cycle_buffer}
		   set a ""
	}
	#
	if {![info exists cycle_buffer]} 		{ set cycle_buffer "" }
	if {![info exists prev_cycle_buffer]} 	{ set prev_cycle_buffer "" }
	
	if { [string length $a] == 0 } {
		if { [info exists cycle_buffer] && ![string equal $prev_cycle_buffer $cycle_buffer]} {
			#MOM_output_literal $cycle_buffer
			#процедура MOM_output_literal не выводит нормально строки вида "текст \n текст" в режиме работы симуляции станка
			#выводит только до первого символа \n 
			foreach a $buffer_list {
				set out_line ""
				foreach aa $a { set out_line $out_line$aa}
				if {![info exists number_flag] } { MOM_output_literal $out_line; set number_flag 1} else { MOM_output_text $out_line }
			}
			catch { unset number_flag }
			#MOM_output_to_listing_device $cycle_buffer
			set prev_cycle_buffer $cycle_buffer	
			set buffer_list ""
		}
		catch {
		    unset cycle_buffer
		    unset buffer_list ;#<Mitiouk> 11-09-2019 Add this line Fix
		}
		return 
	}
	
	
	if {[string equal $a "-reset"]} {
		catch {unset prev_cycle_byffer}
		catch {unset cycle_buffer}
		return
		}
	
	set cycle_buffer "$cycle_buffer$a"
	lappend buffer_list [list $a]
}

#=============================================================
proc USER_kin__MOM_rotate { } {
#=============================================================
# This command handles a Rotate UDE.
#
# Key parameters set in UDE -
#   mom_rotate_axis_type        :  [ AAXIS | BAXIS   | CAXIS    | HEAD | TABLE | FOURTH_AXIS | FIFTH_AXIS ]
#   mom_rotation_mode           :  [ NONE  | ATANGLE | ABSOLUTE | INCREMENTAL ]
#   mom_rotation_direction      :  [ NONE  | CLW     | CCLW ]
#   mom_rotation_angle          :  Specified angle
#   mom_rotation_reference_mode :  [ ON    | OFF ]
#
#
## <rws 04-11-2008>
## If in TURN mode and user invokes "Flip tool around Holder" a MOM_rotate event is generated
## When this happens ABORT this event via return
##
## 09-12-2013 gsl - Made code & functionality of MOM_rotate sharable among all multi-axis posts.
##
## <11-08-2109> Mitiouk

   global mom_machine_mode


   if { [info exists mom_machine_mode] && [string match "TURN" $mom_machine_mode] } {
      if { [CMD_EXIST PB_CMD_handle_lathe_flash_tool] } {
         PB_CMD_handle_lathe_flash_tool
      }
return
   }


   global mom_rotate_axis_type mom_rotation_mode mom_rotation_direction
   global mom_rotation_angle mom_rotation_reference_mode
   global mom_kin_machine_type mom_kin_4th_axis_direction mom_kin_5th_axis_direction
   global mom_kin_4th_axis_leader mom_kin_5th_axis_leader
   global mom_kin_4th_axis_leader mom_kin_5th_axis_leader mom_pos
   global mom_out_angle_pos
   global unlocked_prev_pos mom_sys_leader
   global mom_kin_4th_axis_min_limit mom_kin_4th_axis_max_limit
   global mom_kin_5th_axis_min_limit mom_kin_5th_axis_max_limit
   global mom_prev_pos
   global mom_prev_rot_ang_4th mom_prev_rot_ang_5th
   global mom_tool_axis
   global user_rotate
   global user_rotate_angle

   if { ![info exists mom_rotation_angle] } {
     # Should the event be aborted here???
return
   }
   
   # Check option
   #MOM_output_to_listing_device  ">>>> Foo mom_rotate_axis_type: $mom_rotate_axis_type";#:  [ AAXIS | BAXIS   | CAXIS    | HEAD | TABLE | FOURTH_AXIS | FIFTH_AXIS ]
   if { !([string match CAXIS $mom_rotate_axis_type] ||\
        [string match TABLE $mom_rotate_axis_type] ||\
        [string match FIFTH_AXIS $mom_rotate_axis_type]) } {
		MOM_output_to_listing_device "#####################################################"
        MOM_output_to_listing_device ">>>> Внимание! ... Эта ось вращения не поддерживается"
		MOM_output_to_listing_device "#####################################################"
return
   }
   #Проверяем параллельность оси
   if { ![EQ_is_equal $mom_tool_axis(2) 1.0] } {
       
return		
   } 

   if { ![info exists mom_kin_5th_axis_direction] } {
      set mom_kin_5th_axis_direction "0"
   }

    #MOM_output_literal "PLANE SPATIAL SPA SPB SPC TURN TABLE ROT"
   
	set user_rotate ON
	set user_rotate_angle $mom_rotation_angle
	
return
  #
  #  Determine which rotary axis the UDE has specifid - fourth(3), fifth(4) or invalid(0)
  #
  #
   if { [string match "*3_axis_mill_turn*" $mom_kin_machine_type] } {

      switch $mom_rotate_axis_type {
         CAXIS -
         FOURTH_AXIS -
         TABLE {
            set axis 3
         }
         default {
            set axis 0
         }
      }

   } else {

      switch $mom_rotate_axis_type {
         AAXIS -
         BAXIS -
         CAXIS {
            set axis [AXIS_SET $mom_rotate_axis_type]
         }
         HEAD {
            if { ![string compare "5_axis_head_table" $mom_kin_machine_type] ||\
                 ![string compare "5_AXIS_HEAD_TABLE" $mom_kin_machine_type] } {
               set axis 4
            } else {
               set axis 3
            }
         }
         FIFTH_AXIS {
            set axis 4
         }
         FOURTH_AXIS -
         TABLE -
         default {
            set axis 3
         }
      }
   }

   if { $axis == 0 } {
      CATCH_WARNING "Invalid rotary axis ($mom_rotate_axis_type) has been specified."
      MOM_abort_event
   }

   switch $mom_rotation_mode {
      NONE -
      ATANGLE {
         set angle $mom_rotation_angle
         set mode 0
      }
      ABSOLUTE {
         set angle $mom_rotation_angle
         set mode 1
      }
      INCREMENTAL {
         set angle [expr $mom_pos($axis) + $mom_rotation_angle]
         set mode 0
      }
   }

   switch $mom_rotation_direction {
      NONE {
         set dir 0
      }
      CLW {
         set dir 1
      }
      CCLW {
         set dir -1
      }
   }
#Здесь меняется положение оси

   set ang [LIMIT_ANGLE $angle]
   set mom_pos($axis) $ang

   if { $axis == "3" } { ;# Rotate 4th axis

      if { ![info exists mom_prev_rot_ang_4th] } {
         set mom_prev_rot_ang_4th [MOM_ask_address_value fourth_axis]
      }
      if { [string length [string trim $mom_prev_rot_ang_4th]] == 0 } {
         set mom_prev_rot_ang_4th 0.0
      }

      set prev_angles(0) $mom_prev_rot_ang_4th

   } elseif { $axis == "4" } { ;# Rotate 5th axis

      if { ![info exists mom_prev_rot_ang_5th] } {
         set mom_prev_rot_ang_5th [MOM_ask_address_value fifth_axis]
      }
      if { [string length [string trim $mom_prev_rot_ang_5th]] == 0 } {
         set mom_prev_rot_ang_5th 0.0
      }

      set prev_angles(1) $mom_prev_rot_ang_5th
   }

   set p [expr $axis + 1]
   set a [expr $axis - 3]

   if { $axis == 3  &&  [string match "MAGNITUDE_DETERMINES_DIRECTION" $mom_kin_4th_axis_direction] } {

      set dirtype "MAGNITUDE_DETERMINES_DIRECTION"

      global mom_sys_4th_axis_dir_mode

      if { [info exists mom_sys_4th_axis_dir_mode] && ![string compare "ON" $mom_sys_4th_axis_dir_mode] } {

         set del $dir
         if { $del == 0 } {
            set del [expr $ang - $mom_prev_pos(3)]
            if { $del >  180.0 } { set del [expr $del - 360.0] }
            if { $del < -180.0 } { set del [expr $del + 360.0] }
         }

         global mom_sys_4th_axis_cur_dir
         global mom_sys_4th_axis_clw_code mom_sys_4th_axis_cclw_code

         if { $del > 0.0 } {
            set mom_sys_4th_axis_cur_dir $mom_sys_4th_axis_clw_code
         } elseif { $del < 0.0 } {
            set mom_sys_4th_axis_cur_dir $mom_sys_4th_axis_cclw_code
         }
      }

   } elseif { $axis == 4  &&  [string match "MAGNITUDE_DETERMINES_DIRECTION" $mom_kin_5th_axis_direction] } {

      set dirtype "MAGNITUDE_DETERMINES_DIRECTION"

      global mom_sys_5th_axis_dir_mode

      if { [info exists mom_sys_5th_axis_dir_mode] && ![string compare "ON" $mom_sys_5th_axis_dir_mode] } {

         set del $dir
         if { $del == 0 } {
            set del [expr $ang - $mom_prev_pos(4)]
            if { $del >  180.0 } { set del [expr $del - 360.0] }
            if { $del < -180.0 } { set del [expr $del + 360.0] }
         }

         global mom_sys_5th_axis_cur_dir
         global mom_sys_5th_axis_clw_code mom_sys_5th_axis_cclw_code

         if { $del > 0.0 } {
            set mom_sys_5th_axis_cur_dir $mom_sys_5th_axis_clw_code
         } elseif { $del < 0.0 } {
            set mom_sys_5th_axis_cur_dir $mom_sys_5th_axis_cclw_code
         }
      }

   } else {

      set dirtype "SIGN_DETERMINES_DIRECTION"
   }

   if { $mode == 1 } {

      set mom_out_angle_pos($a) $angle

   } elseif { [string match "MAGNITUDE_DETERMINES_DIRECTION" $dirtype] } {

      if { $axis == 3 } {
         set mom_out_angle_pos($a) [ROTSET $ang $prev_angles(0) $mom_kin_4th_axis_direction\
                                                $mom_kin_4th_axis_leader mom_sys_leader(fourth_axis)\
                                                $mom_kin_4th_axis_min_limit $mom_kin_4th_axis_max_limit]
      } else {
         set mom_out_angle_pos($a) [ROTSET $ang $prev_angles(1) $mom_kin_5th_axis_direction\
                                                $mom_kin_5th_axis_leader mom_sys_leader(fifth_axis)\
                                                $mom_kin_5th_axis_min_limit $mom_kin_5th_axis_max_limit]
      }

   } elseif { [string match "SIGN_DETERMINES_DIRECTION" $dirtype] } {

      if { $dir == -1 } {
         if { $axis == 3 } {
            set mom_sys_leader(fourth_axis) $mom_kin_4th_axis_leader-
         } else {
            set mom_sys_leader(fifth_axis) $mom_kin_5th_axis_leader-
         }
      } elseif { $dir == 0 } {
         if { $axis == 3 } {
            set mom_out_angle_pos($a) [ROTSET $ang $prev_angles(0) $mom_kin_4th_axis_direction\
                                                   $mom_kin_4th_axis_leader mom_sys_leader(fourth_axis)\
                                                   $mom_kin_4th_axis_min_limit $mom_kin_4th_axis_max_limit]
         } else {
            set mom_out_angle_pos($a) [ROTSET $ang $prev_angles(1) $mom_kin_5th_axis_direction\
                                                   $mom_kin_5th_axis_leader mom_sys_leader(fifth_axis)\
                                                   $mom_kin_5th_axis_min_limit $mom_kin_5th_axis_max_limit]
         }
      } elseif { $dir == 1 } {
         set mom_out_angle_pos($a) $ang
      }
   }


  #<04-25-2013 gsl> No clamp code output when rotation is ref only.
   if { ![string compare "OFF" $mom_rotation_reference_mode] } {
      global mom_sys_auto_clamp

      if { [info exists mom_sys_auto_clamp] && [string match "ON" $mom_sys_auto_clamp] } {

         set out1 "1"
         set out2 "0"

         if { $axis == 3 } { ;# Rotate 4th axis
            AUTO_CLAMP_2 $out1
            AUTO_CLAMP_1 $out2
         } else {
            AUTO_CLAMP_1 $out1
            AUTO_CLAMP_2 $out2
         }
      }
   }


   if { $axis == 3 } {

      ####  <rws>
      ####  Use ROTREF switch ON to not output the actual 4th axis move

      if { ![string compare "OFF" $mom_rotation_reference_mode] } {
         PB_CMD_fourth_axis_rotate_move
      }

      if { ![string compare "SIGN_DETERMINES_DIRECTION" $mom_kin_4th_axis_direction] } {
         set mom_prev_rot_ang_4th [expr abs($mom_out_angle_pos(0))]
      } else {
         set mom_prev_rot_ang_4th $mom_out_angle_pos(0)
      }

      MOM_reload_variable mom_prev_rot_ang_4th

   } else {

      if { [info exists mom_kin_5th_axis_direction] } {

         ####  <rws>
         ####  Use ROTREF switch ON to not output the actual 5th axis move

         if { ![string compare "OFF" $mom_rotation_reference_mode] } {
            PB_CMD_fifth_axis_rotate_move
         }

         if { ![string compare "SIGN_DETERMINES_DIRECTION" $mom_kin_5th_axis_direction] } {
            set mom_prev_rot_ang_5th [expr abs($mom_out_angle_pos(1))]
         } else {
            set mom_prev_rot_ang_5th $mom_out_angle_pos(1)
         }

         MOM_reload_variable mom_prev_rot_ang_5th
      }
   }

  #<05-10-06 sws> pb351 - Uncommented next 3 lines
  #<01-07-10 wbh> Reset mom_prev_pos using the variable mom_out_angle_pos
  # set mom_prev_pos($axis) $ang
   set mom_prev_pos($axis) $mom_out_angle_pos([expr $axis-3])
   MOM_reload_variable -a mom_prev_pos
   MOM_reload_variable -a mom_out_angle_pos
}
#=============================================================
proc USER_rotate_out { } {
#=============================================================
# Выводит кадр поворота стола с помощью функции PLANE PATIAL
# используется совместно с USER_kin__MOM_rotate
# Добавьте процедуру USER_rotate_reset в конце операции
  global user_rotate user_rotate_angle
  if { ![info exists user_rotate] } { return }
  if { $user_rotate != ON } { return }
  set contr_angle [expr 0 - $user_rotate_angle]
  set user_rotate_angle [format "%+.3f" $user_rotate_angle]
  set contr_angle [format "%+0.3f" $contr_angle]
  
	MOM_output_literal "PLANE SPATIAL SPA+0 SPB+0 SPC${user_rotate_angle} TURN FQL2 TABLE ROT"
	MOM_output_literal "CYCL DEF 10.0 ROTATION"
    MOM_output_literal "CYCL DEF 10.1 ROT${contr_angle}"
    MOM_suppress Always fifth_axis fourth_axis
	
}

#=============================================================
proc USER_rotate_reset { } {
#=============================================================
	# Используется совместно с USER_rotate_out
	global user_rotate
	if { [info exists user_rotate] } {
		MOM_suppress Off fifth_axis fourth_axis
	    catch {unset user_rotate}
		MOM_output_literal "CYCL DEF 10.0 ROTATION"
		MOM_output_literal "CYCL DEF 10.1 ROT+0.0"
		MOM_force Once fifth_axis forth_axis
		MOM_force_block Once move_start_axis
		MOM_do_template move_start_axis
	}
}