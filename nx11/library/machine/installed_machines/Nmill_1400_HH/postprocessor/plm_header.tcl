# =====================================================
# out custom header
# 26-12-2018
# 25-05-2019 Add calculate machine_time
# =====================================================
global user_post_version
set user_post_version "1.2.5 12-09-2019"

	uplevel #0 {
	proc MOM_OPER_BODY {} {
		global mom_toolpath_time mom_accum_toolpath_time
		global mom_operation_name_list mom_operation_name
		global mom_operation_time_list
		global mom_toolpath_cutting_length mom_toolpath_length
		global mom_feed_cut_value mom_feed_rapid_value
		
		if {![info exists mom_operation_time_list]} { 
				foreach a $mom_operation_name_list {set mom_operation_time_list($a) 0}
		}
		
			if { ![info exists mom_accum_toolpath_time] } { set mom_accum_toolpath_time 0 }
			if {[lsearch $mom_operation_name_list $mom_operation_name] >= 0} {		
				if {![info exists mom_feed_rapid_value] || $mom_feed_rapid_value == 0} {set mom_feed_rapid_value 1500}
				set mom_accum_toolpath_time [expr $mom_accum_toolpath_time + $mom_toolpath_time]
				set mom_operation_time_list($mom_operation_name) [expr ($mom_toolpath_cutting_length) / $mom_feed_cut_value + ($mom_toolpath_length - $mom_toolpath_cutting_length) /$mom_feed_rapid_value]
			}	
	}
# =====================================================
proc PB_CMD_StoreMomVars {} {
# =====================================================
  global BNL_mom_list
  set BNL_mom_list [lsort [info globals "mom_*"]]
  
  foreach var $BNL_mom_list {
    global $var BNL_STORED_$var
    if {[array exist $var]} {
      array set BNL_STORED_$var [array get $var]
    } else {
      set BNL_STORED_$var [set $var]
    }
  }
}
# =====================================================
proc PB_CMD_RestoreMomVars {} {
# =====================================================
  global BNL_mom_list

  if {![info exists BNL_mom_list]} return
  
  foreach var $BNL_mom_list {
    global $var BNL_STORED_$var
    if {[array exist BNL_STORED_$var]} {
      array set $var [array get BNL_STORED_$var]
      array unset BNL_STORED_$var
    } else {
      set $var [set BNL_STORED_$var]
      unset BNL_STORED_$var
    }
  }
  unset BNL_mom_list
}

}; #uplevel

	PB_CMD_StoreMomVars
	MOM_cycle_objects {SETUP {PROGRAMVIEW{MEMBERS}}}
	PB_CMD_RestoreMomVars
	
#=====================================================
proc USER_out_header {} {
#=====================================================
	global mom_oper_program mom_accum_toolpath_time 
	global mom_part_name
	global mom_logname mom_group_name mom_path_name 
	global mom_oper_program local_mom_group_name mom_toolpath_cutting_time mom_accum_total_time
	global mom_tool_use mom_accum_toolpath_time mom_parent_group  mom_operation_time_list
	global mom_calculate_time
	global user_post_version
	
	set mom_calculate_time 0
	if {[array exists mom_operation_time_list]} {
		foreach a [array names mom_operation_time_list] {
			#MOM_output_to_listing_device "OPER: $a Time: $mom_operation_time_list($a)"
			set mom_calculate_time [expr $mom_calculate_time + $mom_operation_time_list($a)]
		}
	}
	if { ![info exists mom_parent_group ] } {set mom_parent_group 0}
	#MOM_output_to_listing_device ">>> mom_parent_group $mom_parent_group "
	
	if { ![info exists mom_group_name] } { set mom_group_name $mom_path_name } 
	if { [string compare $mom_oper_program $mom_group_name] == 0 } {
		set local_mom_group_name $mom_group_name
	} else {
		set local_mom_group_name [format "%s_%s" $mom_oper_program $mom_path_name]
	}
	
	if {![info exists mom_accum_toolpath_time]} {set mom_accum_toolpath_time 0}
	set out_part_info 1
	if { $out_part_info == 1 } {
		set date_time [clock scan today]
		set date_time [clock format $date_time -format "%x %H:%M"]
		MOM_output_literal ";================================"
		MOM_output_literal ";Postprocessor: Nmill 1400 V$user_post_version"
		MOM_output_literal ";PROGRAM: $local_mom_group_name"
		MOM_output_literal ";DATE TIME: $date_time"
		MOM_output_literal ";MACHINE TIME: [format "%2.3f" $mom_accum_toolpath_time] MIN"
		MOM_output_literal ";MACHINE TOTAL TIME: [format "%2.3f" $mom_calculate_time] MIN"
		
		#MOM_output_literal ";PART NAME: [string toupper $mom_part_name]"
		#MOM_output_literal ";MACHINE: Nmill 1400"
		#MOM_output_literal ";PROGRAMMER: [string toupper $mom_logname]"
		#MOM_output_literal ";CUTTING MACHINE TIME: [format "%.3f" $mom_toolpath_cutting_time] MIN"	
		MOM_output_literal ";================================"		
		#USER_out_tool_list
	}
}