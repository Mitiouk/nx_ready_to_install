uplevel #0 {
# =====================================================
# output custom tool list
# 26-12-2018 Mitiouk
proc USER_out_tool_list {} {
# =====================================================
	global mom_isv_tool_count mom_tool_use 
	global mom_isv_tool_name mom_isv_tool_description mom_isv_tool_number 
	global mom_isv_tool_diameter mom_isv_tool_flute_length mom_isv_tool_type
	global mom_isv_tool_corner1_radius mom_isv_tool_adjust_register mom_isv_tool_cutcom_register
	global mom_isv_tool_x_correction mom_isv_tool_type mom_isv_tool_point_angle
	
	MOM_output_literal ";========== TOOL LIST =========="	
	for { set i 0 } { $i < $mom_isv_tool_count } { incr i } {
		if { [string match "Milling" $mom_isv_tool_type($i)] } {
			set mom_isv_tool_description1($i) "MILLING TOOL"
			MOM_output_literal ";TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i) D$mom_isv_tool_cutcom_register($i)"
			MOM_output_literal ";D=$mom_isv_tool_diameter($i) L=$mom_isv_tool_x_correction($i,0) FL=$mom_isv_tool_flute_length($i) R=$mom_isv_tool_corner1_radius($i)"
		} elseif { [string match "Drilling" $mom_isv_tool_type($i)] } {
			set mom_isv_tool_description1($i) "DRILLING TOOL"
			MOM_output_literal ";TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i)"
			MOM_output_literal ";D=$mom_isv_tool_diameter($i) L=$mom_isv_tool_x_correction($i,0) FL=$mom_isv_tool_flute_length($i) PA=$mom_isv_tool_point_angle($i)"
		} elseif { [string match "Solid" $mom_isv_tool_type($i)] } {
			set mom_isv_tool_description1($i) "PROBING TOOL"
			MOM_output_literal ";TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i)"
		}
	}

	MOM_output_literal ";==============================="	
	
}
}