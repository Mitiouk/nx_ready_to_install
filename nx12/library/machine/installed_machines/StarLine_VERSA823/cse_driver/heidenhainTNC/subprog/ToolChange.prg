0 BEGIN PGM TOOLCHANGE MM
1 M5
2 FN 18: SYSREAD Q1000 = ID20 NR2
3 FN 18: SYSREAD Q1001 = ID20 NR1
4 ; If actual tool identicall wiht preselected one skip AC block
5 FN 9: IF +Q1000 EQU +Q1001 GOTO LBL "NO_TOOLCHANGE"

0 L Z-52.5582 Y -328.6392 FMAX M91
; 0 Y -328.6392 FMAX M91

##LANGUAGE AC
  INT nToolID;
  nToolID = getVariable ("Q1000");
  STRING strToolName;
  strToolName = nToolID;

  IF (nToolID > 0);
    generateTool (getToolNameByNumber(nToolID), "S");
  ENDIF;

  IF (exist(getCurrentTool("S")));
    collision  (OFF, getCurrentTool("S"));
    visibility (     getCurrentTool("S"), OFF, TRUE);
    release    (     getCurrentTool("S"));
  ENDIF;

  IF (exist(getNextTool("S")));
    grasp      (     getNextTool("S"), getJunction("SPINDLE", "S"));
    position   (     getNextTool("S"), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    visibility (     getNextTool("S"), ON,  TRUE);
    collision  (ON,  getNextTool("S"), 2, -0.01);
    activateNextTool ("S");
  ENDIF;
##LANGUAGE NATIVE

10 FN 17: SYSWRITE ID20 NR1 = +Q1000
11 LBL "NO_TOOLCHANGE"
12 M67
13 END PGM TOOLCHANGE MM