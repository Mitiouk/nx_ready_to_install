#=============================================================
proc USER_cycle200 { } {
#=============================================================
global ude_alt_cycle_output
global mom_cycle_rapid_to
global mom_cycle_feed_to
global dpp_TNC_cycle_feed
global cycle_peck_size
global mom_dwell_at_top
global mom_pos
global js_return_pos
global mom_cycle_delay
global seq_status mom_sequence_mode
global mom_motion_event


set BUF "           "

MOM_set_seq_off


append Q200 "    " "Q200=" [string trimright [format %5.3f $mom_cycle_rapid_to]           "0"] $BUF"
MOM_output_literal "[string range $Q200 0 13] ;BEZOPASN.RASSTOYANIE ~"

append Q201 "    " "Q201=" [string trimright [format %5.3f $mom_cycle_feed_to]            "0"] $BUF"
MOM_output_literal "[string range $Q201 0 13] ;GLUBINA ~"

append Q206 "    " "Q206=" [string trimright [format %5.3f $dpp_TNC_cycle_feed]           "0"] $BUF"
MOM_output_literal "[string range $Q206 0 13] ;PODACHA VREZANIA ~"

append Q202 "    " "Q202=" [string trimright [format %5.3f $cycle_peck_size]              "0"] $BUF"
MOM_output_literal "[string range $Q202 0 13] ;GLUBINA VREZANIA~"

append Q210 "    " "Q210=" [string trimright [format %5.3f $mom_dwell_at_top]             "0"] $BUF"
MOM_output_literal "[string range $Q210 0 13] ;VREMIA VIDERGKI VVERHU ~"

append Q203 "    " "Q203=" [string trimright [format %5.3f $mom_pos(2)]                   "0"] $BUF"
MOM_output_literal "[string range $Q203 0 13] ;KOORD. POVERHNOSTI ~"

append Q204 "    " "Q204=" [string trimright [format %5.3f $js_return_pos]                "0"] $BUF"
MOM_output_literal "[string range $Q204 0 13] ;BEZOP.RASSTOIANIE  ~"

append Q211 "    " "Q211=" [string trimright [format %5.3f $mom_cycle_delay]              "0"] $BUF"
MOM_output_literal "[string range $Q211 0 13] ;VREMIA VIDERGKI VNIZU"

MOM_set_seq_on
}





#=============================================================
proc USER_cycle201 { } {
#=============================================================

#output CYCL DEF 201 alt format

global ude_alt_cycle_output
global mom_cycle_rapid_to
global mom_cycle_feed_to
global dpp_TNC_cycle_feed
global cycle_peck_size
global mom_dwell_at_top
global mom_pos
global js_return_pos
global mom_cycle_delay
global mom_retraction_feed
global seq_status mom_sequence_mode
global mom_motion_event



#прекратить нумеровать

set seq_status $mom_sequence_mode


set BUF "           "

MOM_set_seq_off

append Q200 "    " "Q200=" [string trimright [format %5.3f $mom_cycle_rapid_to]           "0"] $BUF"
MOM_output_literal "[string range $Q200 0 13] ;BEZOPASN.RASSTOYANIE ~"

append Q201 "    " "Q201=" [string trimright [format %5.3f $mom_cycle_feed_to]            "0"] $BUF"
MOM_output_literal "[string range $Q201 0 13] ;GLUBINA REZBY ~"

append Q206 "    " "Q206=" [string trimright [format %5.3f $dpp_TNC_cycle_feed]           "0"] $BUF"
MOM_output_literal "[string range $Q206 0 13] ;PODACHA VREZANIA ~"

append Q211 "    " "Q211=" [string trimright [format %5.3f $mom_cycle_delay]              "0"] $BUF"
MOM_output_literal "[string range $Q211 0 13] ;VREMIA VIDERGKI VNIZU ~"

append Q208 "    " "Q208=" [string trimright [format %5.3f $mom_retraction_feed]          "0"] $BUF"
MOM_output_literal "[string range $Q208 0 13] ;PODACHA OBR.HODA ~"

append Q203 "    " "Q203=" [string trimright [format %5.3f $mom_pos(2)]                   "0"] $BUF"
MOM_output_literal "[string range $Q203 0 13] ;KOORD. POVERHNOSTI ~"

append Q204 "    " "Q204=" [string trimright [format %5.3f $js_return_pos]                "0"] $BUF"
MOM_output_literal "[string range $Q204 0 13] ;BEZOP.RASSTOIANIE"





#вернуть нумерацию, если надо

MOM_set_seq_on

#if {$mom_sequence_mode == ON} {

	#MOM_set_seq_on

	#}


}

#=============================================================
proc USER_cycle202 { } {
#=============================================================

#output CYCL DEF 202 alt format

global ude_alt_cycle_output
global mom_cycle_rapid_to
global mom_cycle_feed_to
global dpp_TNC_cycle_feed
global cycle_peck_size
global mom_dwell_at_top
global mom_pos
global js_return_pos
global mom_cycle_delay
global mom_retraction_feed
global seq_status mom_sequence_mode
global mom_cycle_step1
global dpp_TNC_cycle_step_clearance
global mom_cycle_orient
global mom_rpm_factor
global mom_itnc_bore_q214
global mom_cycle_orient
global mom_retraction_feed_202
global mom_itnc_bore_q214
global mom_motion_event
global mom_heidenhain_disengaging_direction

global mom_logname
global mom_itnc_bore_q214
global mom_heidenhain_disengaging_direction


   if {![info exists mom_itnc_bore_q214]} {
      set mom_itnc_bore_q214 1
   }

   if {![info exists mom_heidenhain_disengaging_direction]} {
      set mom_heidenhain_disengaging_direction 1
   }




#прекратить нумеровать

set seq_status $mom_sequence_mode

set BUF "           "
set mom_cycle_orient 0.0



MOM_set_seq_off

append Q200 "    " "Q200=" [string trimright [format %5.3f $mom_cycle_rapid_to]                     "0"] $BUF"
MOM_output_literal "[string range $Q200 0 13] ;BEZOPASN.RASSTOYANIE ~"

append Q201 "    " "Q201=" [string trimright [format %5.3f $mom_cycle_feed_to]                      "0"] $BUF"
MOM_output_literal "[string range $Q201 0 13] ;GLUBINA REZBY ~"

append Q206 "    " "Q206=" [string trimright [format %5.3f $dpp_TNC_cycle_feed]                     "0"] $BUF"
MOM_output_literal "[string range $Q206 0 13] ;PODACHA VREZANIA ~"

append Q211 "    " "Q211=" [string trimright [format %5.3f $mom_cycle_delay]                        "0"] $BUF"
MOM_output_literal "[string range $Q211 0 13] ;VREMIA VIDERGKI VNIZU ~"

append Q208 "    " "Q208=" [string trimright [format %5.3f $mom_retraction_feed_202]                "0"] $BUF"
MOM_output_literal "[string range $Q208 0 13] ;PODACHA OBR.HODA ~"

append Q203 "    " "Q203=" [string trimright [format %5.3f $mom_pos(2)]                             "0"] $BUF"
MOM_output_literal "[string range $Q203 0 13] ;KOORD. POVERHNOSTI ~"

append Q204 "    " "Q204=" [string trimright [format %5.3f $js_return_pos]                          "0"] $BUF"
MOM_output_literal "[string range $Q204 0 13] ;BEZOP.RASSTOIANIE ~"

append Q214 "    " "Q214=" [string trimright [format %5.3f $mom_heidenhain_disengaging_direction]   "0"] $BUF"
MOM_output_literal "[string range $Q214 0 13] ;NAPRAVLENIE VIHODA ~"

append Q336 "    " "Q336=" [string trimright [format %5.3f $mom_cycle_orient]                        "0"] $BUF"
MOM_output_literal "[string range $Q336 0 13] ;UGOL SCHPINDELIA"

MOM_set_seq_on


}



#=============================================================
proc USER_cycle203 { } {
#=============================================================
global mom_cycle_rapid_to
global mom_cycle_feed_to
global feed
global cycle_peck_size
global mom_pos
global js_return_pos
global mom_cycle_delay
global dpp_TNC_cycle_feed
global mom_dwell_at_top
global mom_retraction_feed
global dpp_TNC_cycle_step_clearance
global user_q213


set BUF "           "

MOM_set_seq_off

append Q200 "    " "Q200=" [string trimright [format %5.3f $mom_cycle_rapid_to]           "0"] $BUF"
MOM_output_literal "[string range $Q200 0 13] ;BEZOPASN.RASSTOYANIE ~"

append Q201 "    " "Q201=" [string trimright [format %5.3f $mom_cycle_feed_to]            "0"] $BUF"
MOM_output_literal "[string range $Q201 0 13] ;GLUBINA ~"

append Q206 "    " "Q206=" [string trimright [format %5.3f $dpp_TNC_cycle_feed]           "0"] $BUF"
MOM_output_literal "[string range $Q206 0 13] ;PODACHA VREZANIA ~"

append Q202 "    " "Q202=" [string trimright [format %5.3f $cycle_peck_size]              "0"] $BUF"
MOM_output_literal "[string range $Q202 0 13] ;GLUBINA VREZANIA ~"

append Q210 "    " "Q210=" [string trimright [format %5.3f $mom_dwell_at_top]             "0"] $BUF"
MOM_output_literal "[string range $Q210 0 13] ;VREMIA VIDERGKI VVERHU ~"

append Q203 "    " "Q203=" [string trimright [format %5.3f $mom_pos(2)]                   "0"] $BUF"
MOM_output_literal "[string range $Q203 0 13] ;KOORD. POVERHNOSTI ~"

append Q204 "    " "Q204=" [string trimright [format %5.3f $js_return_pos]                "0"] $BUF"
MOM_output_literal "[string range $Q204 0 13] ;2-E BEZOP.RASSTOIANIE ~"

append Q212 "    " "Q212=" [string trimright [format %5.3f 0.0]                           "0"] $BUF"
MOM_output_literal "[string range $Q212 0 13] ;SNIATIE MATERIALA ~"

append Q213 "    " "Q213=" [string trimright [format %5.3f $user_q213]                    "0"] $BUF"
MOM_output_literal "[string range $Q213 0 13] ;LOMKA STRUGKI ~"

append Q205 "    " "Q205=" [string trimright [format %5.3f 0.0]                           "0"] $BUF"
MOM_output_literal "[string range $Q205 0 13] ;MIN.GLUBINA VREZANIA ~"

append Q211 "    " "Q211=" [string trimright [format %5.3f $mom_cycle_delay]              "0"] $BUF"
MOM_output_literal "[string range $Q211 0 13] ;VREMIA VIDERGKI VNIZU ~"

append Q208 "    " "Q208=" [string trimright [format %5.3f $mom_retraction_feed]           "0"] $BUF"
MOM_output_literal "[string range $Q208 0 13] ;PODACHA OBR.HODA ~"

append Q256 "    " "Q256=" [string trimright [format %5.3f $dpp_TNC_cycle_step_clearance] "0"] $BUF"
MOM_output_literal "[string range $Q256 0 13] ;OBRATN.HOD PRI LOMKE STRUGKI"

append Q359 "    " "Q395=" [string trimright [format %5.3f 0.0]                           "0"] $BUF"
MOM_output_literal "[string range $Q359 0 13] ;KOORD. OTSCHETA GLUB"

MOM_set_seq_on
}


#=============================================================
proc USER_cycle207 { } {
#=============================================================

#output CYCL DEF 207 alt format

global ude_alt_cycle_output
global mom_cycle_rapid_to
global mom_cycle_feed_to
global dpp_TNC_cycle_feed
global cycle_peck_size
global mom_dwell_at_top
global mom_pos
global js_return_pos
global mom_cycle_delay
global mom_retraction_feed
global seq_status mom_sequence_mode
global dpp_TNC_cycle_thread_pitch
global mom_motion_event

MOM_output_literal ";$mom_motion_event"


MOM_output_literal "CYCL DEF 207 RIGID TAPPING NEW ~"

#прекратить нумеровать

set seq_status $mom_sequence_mode

MOM_set_seq_off


set q200 [format "%.3f" $mom_cycle_rapid_to]
MOM_output_literal "    Q200= $q200\t;BEZOPASN.RASSTOYANIE ~"


set q201 [format "%.3f" $mom_cycle_feed_to]
MOM_output_literal "    Q201=$q201\t;GLUBINA REZBY ~"

set q239 [format "%.2f" $dpp_TNC_cycle_thread_pitch]
MOM_output_literal "    Q239=$q239\t;SCHAG REZBY ~"

set q203 [format "%.3f" $mom_pos(2)]
MOM_output_literal "    Q203=$q203\t;KOORD. POVERHNOSTI ~"

set q204 [format "%.3f" $js_return_pos]
MOM_output_literal "    Q204=$q204\t;2-YE BEZOP.RASSTOJ."



#вернуть нумерацию, если надо

if {$mom_sequence_mode == ON} {

	MOM_set_seq_on

	}


}



#=============================================================
proc USER_cycle209 { } {
#=============================================================

#output CYCL DEF 209 alt format

global ude_alt_cycle_output
global mom_cycle_rapid_to
global mom_cycle_feed_to
global dpp_TNC_cycle_feed
global cycle_peck_size
global mom_dwell_at_top
global mom_pos
global js_return_pos
global mom_cycle_delay
global mom_retraction_feed
global seq_status mom_sequence_mode
global mom_cycle_step1
global dpp_TNC_cycle_step_clearance
global mom_cycle_orient
global mom_rpm_factor_deep
global dpp_TNC_cycle_thread_pitch
global mom_motion_event



set BUF "           "

MOM_set_seq_off

append Q200 "    " "Q200=" [string trimright [format %5.3f $mom_cycle_rapid_to]               "0"] $BUF"
MOM_output_literal "[string range $Q200 0 13] ;BEZOPASN.RASSTOYANIE ~"

append Q201 "    " "Q201=" [string trimright [format %5.3f $mom_cycle_feed_to]                "0"] $BUF"
MOM_output_literal "[string range $Q201 0 13] ;GLUBINA REZBY ~"

append Q239 "    " "Q239=" [string trimright [format %5.3f $dpp_TNC_cycle_thread_pitch]       "0"] $BUF"
MOM_output_literal "[string range $Q239 0 13] ;SCHAG REZBY ~"

append Q203 "    " "Q203=" [string trimright [format %5.3f $mom_pos(2)]                       "0"] $BUF"
MOM_output_literal "[string range $Q203 0 13] ;KOORD. POVERHNOSTI ~"

append Q204 "    " "Q204=" [string trimright [format %5.3f $js_return_pos]                    "0"] $BUF"
MOM_output_literal "[string range $Q204 0 13] ;2-YE BEZOP.RASSTOJ. ~"

append Q257 "    " "Q257=" [string trimright [format %5.3f $mom_cycle_step1]                  "0"] $BUF"
MOM_output_literal "[string range $Q257 0 13] ;GL.SWERL.PRI LOMANII ~"

append Q256 "    " "Q256=" [string trimright [format %5.3f $dpp_TNC_cycle_step_clearance]     "0"] $BUF"
MOM_output_literal "[string range $Q256 0 13] ;WYCHOD PRI LOMANII ~"

append Q336 "    " "Q336=" [string trimright [format %5.3f $mom_cycle_orient]                 "0"] $BUF"
MOM_output_literal "[string range $Q336 0 13] ;UGOL SCHPINDEL ~"

append Q403 "    " "Q403=" [string trimright [format %5.3f $mom_rpm_factor_deep]              "0"] $BUF"
MOM_output_literal "[string range $Q403 0 13] ;RPM FACTOR"

MOM_set_seq_on

}




#=============================================================
proc USER_cycle262 { } {
#=============================================================

#output CYCL DEF 262 alt format

global ude_alt_cycle_output
global mom_cycle_rapid_to
global mom_cycle_feed_to
global dpp_TNC_cycle_feed
global cycle_peck_size
global mom_dwell_at_top
global mom_pos
global js_return_pos
global mom_cycle_delay
global mom_retraction_feed
global seq_status mom_sequence_mode
global mom_cycle_step1
global dpp_TNC_cycle_step_clearance
global mom_cycle_orient
global mom_rpm_factor
global mom_heidenhain_nominal_diameter
global mom_heidenhain_threads_per_step
global mom_heidenhain_prepos_feedrate
global mom_heidenhain_milling_type
global mom_heidenhain_milling_feedrate
global mom_podacha_podvoda
global dpp_TNC_cycle_thread_pitch
global mom_motion_event


MOM_output_literal ";$dpp_TNC_cycle_thread_pitch"

MOM_output_literal ";$mom_motion_event"
MOM_output_literal "CYCL DEF 262 THREAD MILLING ~ "

#прекратить нумеровать

set seq_status $mom_sequence_mode

MOM_set_seq_off


set q335 [format "%.3f" $mom_heidenhain_nominal_diameter]
MOM_output_literal "    Q335= $q335\t;NOMINALNYJ DIAMETR ~ "


set q239 [format "%.2f" $dpp_TNC_cycle_thread_pitch]
MOM_output_literal "    Q239=$q239\t;SCHAG REZBY ~ "


set q201 [format "%.3f" $mom_cycle_feed_to]
MOM_output_literal "    Q201=$q201\t;GLUBINA REZBY ~ "


set q355 [format "%.3f" $mom_heidenhain_threads_per_step]
MOM_output_literal "    Q355=$q355\t;DOBOWLENJE ~ "


set q253 [format "%.3f" $mom_heidenhain_prepos_feedrate]
MOM_output_literal "    Q253=$q253\t;PODACHA PRED.POZIC. ~ "


set q351 [format "%.3f" $mom_heidenhain_milling_type]
MOM_output_literal "    Q351=$q351\t;TIP FREZEROWANIA ~ "


set q200 [format "%.3f" $mom_cycle_rapid_to]
MOM_output_literal "    Q200= $q200\t;BEZOPASN.RASSTOYANIE ~ "


set q203 [format "%.3f" $mom_pos(2)]
MOM_output_literal "    Q203=$q203\t;KOORD. POVERHNOSTI ~ "

set q204 [format "%.3f" $js_return_pos]
MOM_output_literal "    Q204=$q204\t;2-YE BEZOP.RASSTOJ. ~ "


set q207 [format "%.3f" $mom_heidenhain_milling_feedrate]
MOM_output_literal "    Q207=$q207\t;PODACHA FREZER. ~ "


set q512 [format "%.3f" $mom_podacha_podvoda]
MOM_output_literal "    Q512=$q512\t;PODACHA PODVODA "


#вернуть нумерацию, если надо

if {$mom_sequence_mode == ON} {

	MOM_set_seq_on

	}


}




