#===============================================================================
# Exported Custom Commands created by a.shirkov
# on Wednesday, April 03 2019, 19:23:30 +0300
#===============================================================================



#=============================================================
proc PB_CMD_plm_header { } {
#=============================================================
global mom_part_name mom_toolpath_time mom_parent_group_name
global mom_date mom_logname mom_group_name mom_path_name mom_toolpath_time
global mom_oper_program local_mom_group_name mom_toolpath_cutting_time out_part_info


if { ![info exists mom_group_name] } {

set mom_group_name $mom_path_name
}
if { [string compare $mom_oper_program $mom_group_name] == 0 } {
    set local_mom_group_name $mom_group_name
} else {

    set local_mom_group_name [format "%s_%s" $mom_oper_program $mom_path_name]
}


set out_part_info 1
if { $out_part_info == 1 } {
    MOM_output_literal ";================================"
    MOM_output_literal ";Postprocessor : FEHLMANN VERSA 823"
    MOM_output_literal ";PROGRAM : $local_mom_group_name"
    MOM_output_literal ";DATE TIME : $mom_date"
    #MOM_output_literal ";TOTAL MACHINE TIME111111 : $mom_cutting_time"
    #MOM_output_literal ";TOTAL MACHINE TIME : [format "%.3f" $mom_toolpath_time] MIN"
    #MOM_output_literal ";CUTTING MACHINE TIME : [format "%.3f" $mom_toolpath_cutting_time] MIN"
    MOM_output_literal ";================================"
}
}





