#=============================================================
proc PB_CMD_UDE_datum_setting { } {
#=============================================================
  global mom_sys_in_operation
  global dpp_TNC_datum_ude_defined

  if { [info exists mom_sys_in_operation] && $mom_sys_in_operation == 1} {
     MOM_output_to_listing_device "Datum output UDE should be used on the operation group level!"
     MOM_abort
  }
  if { [info exists dpp_TNC_datum_ude_defined] && $dpp_TNC_datum_ude_defined == 1} {
     MOM_output_to_listing_device "Datum output UDE could only be used for once!"
     MOM_abort
  } else {
     set dpp_TNC_datum_ude_defined 1
  }
}


#=============================================================
proc PB_CMD_header_versa823 { } {
#=============================================================

global name_of_post
global mom_group_name
global mom_date
global mom_machine_time



if { ![info exists mom_group_name] } {
   set mom_group_name "NONE"
}

set name_of_post "StarLine VERSA823"


MOM_output_to_listing_device ";=============================="
MOM_output_to_listing_device ";Postprocessor: $name_of_post"
MOM_output_to_listing_device ";Program: $mom_group_name"
MOM_output_to_listing_device ";Date: $mom_date"
MOM_output_to_listing_device ";Machine time: $mom_machine_time"
MOM_output_to_listing_device ";=============================="


}


#=============================================================
proc PB_CMD_plm_header { } {
#=============================================================
global mom_part_name mom_toolpath_time mom_parent_group_name
global mom_date mom_logname mom_group_name mom_path_name mom_toolpath_time
global mom_oper_program local_mom_group_name mom_toolpath_cutting_time out_part_info
global mom_toolpath_time accum_mom_toolpath_time

global mom_point_min mom_point_max

if { ![info exists mom_group_name] } {

set mom_group_name $mom_path_name
}
if { [string compare $mom_oper_program $mom_group_name] == 0 } {
    set local_mom_group_name $mom_group_name
} else {

    set local_mom_group_name [format "%s_%s" $mom_oper_program $mom_path_name]
}


set out_part_info 1
if { $out_part_info == 1 } {


	
	#MOM_output_literal "$mom_point_min(0)"

    MOM_output_literal ";================================"
    MOM_output_literal ";Postprocessor : FEHLMANN VERSA 823"
    MOM_output_literal ";PROGRAM : $local_mom_group_name"
    MOM_output_literal ";DATE TIME : $mom_date"
    #MOM_output_literal ";TOTAL MACHINE TIME : [format "%.3f" $accum_mom_toolpath_time] MIN"
    #MOM_output_literal ";CUTTING MACHINE TIME : [format "%.3f" $mom_toolpath_cutting_time] MIN"
    MOM_output_literal ";================================"
}
}



#=============================================================
proc PB_CMD_plm_tool_list { } {
#=============================================================

    global mom_isv_tool_count mom_tool_use
    global mom_isv_tool_name mom_isv_tool_description mom_isv_tool_number
    global mom_isv_tool_diameter mom_isv_tool_flute_length mom_isv_tool_type
    global mom_isv_tool_corner1_radius mom_isv_tool_adjust_register mom_isv_tool_cutcom_register
    global mom_isv_tool_x_correction mom_isv_tool_type mom_isv_tool_point_angle

    MOM_output_literal ";========== TOOL LIST =========="
    for { set i 0 } { $i < $mom_isv_tool_count } { incr i } {
        if { [string match "Milling" $mom_isv_tool_type($i)] } {
            set mom_isv_tool_description1($i) "MILLING TOOL"
            MOM_output_literal ";TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i) D$mom_isv_tool_cutcom_register($i)"
            MOM_output_literal ";D=$mom_isv_tool_diameter($i) L=$mom_isv_tool_x_correction($i,0) FL=$mom_isv_tool_flute_length($i) R=$mom_isv_tool_corner1_radius($i)"
        } elseif { [string match "Drilling" $mom_isv_tool_type($i)] } {
            set mom_isv_tool_description1($i) "DRILLING TOOL"
            MOM_output_literal ";TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i)"
            MOM_output_literal ";D=$mom_isv_tool_diameter($i) L=$mom_isv_tool_x_correction($i,0) FL=$mom_isv_tool_flute_length($i) PA=$mom_isv_tool_point_angle($i)"
        } elseif { [string match "Solid" $mom_isv_tool_type($i)] } {
            set mom_isv_tool_description1($i) "PROBING TOOL"
            MOM_output_literal ";TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i)"
        }
    }

    MOM_output_literal ";==============================="
}


#=============================================================
proc PB_CMD_schetchik_operaci { } {
#=============================================================
global number_of_operation
global prev_number



if { ![info exists number_of_operation] } {

    set number_of_operation 10

} else {

  set number_of_operation [expr $number_of_operation + 10]

}


MOM_output_literal ";==============================="
MOM_output_literal " QL1 = $number_of_operation ; OPER NUMBER"
#MOM_output_literal "CALL LBL 998 ; RESET"
MOM_output_literal ";==============================="



}


#=============================================================
proc PB_CMD_lbl_oper_number_versa823 { } {
#=============================================================
global number_of_operation
global mom_operation_name
global mom_path_name
global mom_tool_number
global mom_tool_name



MOM_output_literal "* - LBL $number_of_operation: $mom_path_name T$mom_tool_number $mom_tool_name"
MOM_output_literal "LBL $number_of_operation" 





}


#=============================================================
proc PB_CMD_lbl_997 { } {
#=============================================================

MOM_output_literal ";"
MOM_output_literal "* - M25, M8"
MOM_output_literal "LBL 997"
MOM_output_literal "M25"
MOM_output_literal "LBL 0"
MOM_output_literal ";"


}


#=============================================================
proc PB_CMD_lbl_998 { } {
#=============================================================

MOM_output_literal "LBL 998"
MOM_output_literal "M9"
MOM_output_literal "M129"
MOM_output_literal "PLANE RESET STAY"
MOM_output_literal "LBL 0"
MOM_output_literal ";"

}


#=============================================================
proc PB_CMD_lbl_999 { } {
#=============================================================

MOM_output_literal "* - M0"
MOM_output_literal "LBL 999"
MOM_output_literal "M1"
MOM_output_literal "M25"
MOM_output_literal "LBL 0"
MOM_output_literal ";"

}


#=============================================================
proc USER_coolant_off { } {
#=============================================================
global mom_next_oper_has_tool_change

#if { ![string match "NO" $mom_next_oper_has_tool_change] } {

MOM_do_template coolant_off
MOM_do_template coolant_off_50

    #set ::mom_coolant_status "ON"
    #set ::mom_coolant_mode "ON"


   #}

 set ::mom_coolant_status "ON"
 set ::mom_coolant_mode "ON"
   
}



#=============================================================
proc USER_spindle_off { } {
#=============================================================
global mom_next_oper_has_tool_change

if { ![string match "NO" $mom_next_oper_has_tool_change] } {

    MOM_do_template spindle_off

   }

}

#=============================================================
proc USER_fn11_output { } {
#=============================================================

MOM_output_literal "FN 11: IF +QL1 GT +9 GOTO LBL Q1"

}



#=============================================================
proc USER_tool_preselect { } {
#=============================================================

global mom_next_tool_status
global mom_next_tool_name
global mom_next_tool_number

#if { ![info exists mom_next_tool_name]} { set mom_next_tool_name 1}

   if {[info exists mom_next_tool_status] && $mom_next_tool_status == "NEXT"} {
   MOM_output_literal "TOOL DEF $mom_next_tool_number ; (NEXT TOOL : $mom_next_tool_name)"
   }


}




#=============================================================
proc USER_cycle_327 { } {
#=============================================================

global mom_hsc_setup_mode



if { ![info exists mom_hsc_setup_mode] } {
   set mom_hsc_setup_mode 0
}



}


#=============================================================
proc USER_blk_form { } {
#=============================================================

global mom_point_min mom_point_max
global flag_blk

set point_min_x [format "%.2f" "$mom_point_min(0)"]
set point_min_y [format "%.2f" "$mom_point_min(1)"]
set point_min_z [format "%.2f" "$mom_point_min(2)"]

set point_max_x [format "%.2f" "$mom_point_max(0)"]
set point_max_y [format "%.2f" "$mom_point_max(1)"]
set point_max_z [format "%.2f" "$mom_point_max(2)"]




if { ![info exists flag_blk]} {

   MOM_output_literal "BLK FORM 0.1 Z X$point_min_x Y$point_min_y Z$point_min_z"
   MOM_output_literal "BLK FORM 0.2 X$point_max_x Y$point_max_y Z$point_max_z"
   
   }
   
set flag_blk 1



}



#=============================================================
proc USER_time_output { } {
#=============================================================
#uplevel #0 {
#proc MOM_OPER_BODY {} {
#global mom_toolpath_time accum_mom_toolpath_time

#if { ![info exists accum_mom_toolpath_time] } { set accum_mom_toolpath_time 0 }
#set accum_mom_toolpath_time [expr $accum_mom_toolpath_time + $mom_toolpath_time]
#MOM_output_literal "MOM_OPER_BODY = $accum_mom_toolpath_time"

}
}

#MOM_cycle_objects {SETUP {PROGRAMVIEW {MEMBERS {OPER}}}} result
#MOM_cycle_objects {SETUP {PROGRAMVIEW {MEMBERS}}}


#}



