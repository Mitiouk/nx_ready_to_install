#<Mitiouk> 29-09-2019
#=============================================================
proc USER_set_variables {} {
#=============================================================
# Не изменяйте эти строки:
global user_post_version
global user_direct_safe_line_start
global user_direct_safe_line_end
global mom_safe_user_position_x_var
global mom_safe_user_position_y_var
global mom_safe_user_position_z_var
set user_post_version "02 29-09-2019"
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Для изменения параметров вывода строки перемещения безопасности,
# измените значение переменных здесь ->
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
set user_direct_safe_line_start     1      ;#1/0
set user_direct_safe_line_end       1      ;#1/0
set mom_safe_user_position_z_var    500.0  ;#позиция безопасного перемещения по оси Z
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
#end