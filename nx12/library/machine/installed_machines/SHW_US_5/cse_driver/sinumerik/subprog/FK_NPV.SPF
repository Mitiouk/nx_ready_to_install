;*************************************************************
;* Kompensation der Restfehler FK / FKA aus den Tabellen     *
;* FK_NPKT_TAB.SPF (Stellungen FK/FKA)                       *
;* FK_NPKT_NPV.SPF (zugeh�rige Fehler)                       *
;*************************************************************
N1000 PROC FK_NPV (INT I_FK, INT I_FKA)
; 
;**********************************************************
;*   Unterprogramme                                       *
;**********************************************************
; FK_NPKT_TAB (INT, VAR INT, VAR INT, VAR INT, VAR INT)
;    I_Nummer
;    O_Anz_Mess, O_Messen, O_FKA, O_FK
; FK_NPKT_NPV (INT, VAR REAL, VAR REAL, VAR REAL)
;    I_Nummer
;    O_X, O_Y, O_Z
; 
;**********************************************************
;*   Interne Varis                                        *
;**********************************************************
N1001 DEF INT  Nummer       ; Zeilennummer in Tabelle (UP) 
N1002 DEF INT  Messen       ; 1=Ein / 0=Aus 
; 
; Indizees:
; 0 : aktueller / gemittelter Wert
; 1 : Wert f�r (FKA_1,FK_1) -> TNC : 11
; 2 : Wert f�r (FKA_1,FK_2) -> TNC : 12
; 3 : Wert f�r (FKA_2,FK_1) -> TNC : 21
; 4 : Wert f�r (FKA_2,FK_2) -> TNC : 22
N1003 DEF INT  FKA[5]       ; Position FKA (Spalten) 
N1004 DEF INT  FrK[5]       ; Position FK  (Zeilen) 
N1005 DEF REAL X_Fehl[5]    ; Fehler X-Achse  
N1006 DEF REAL Y_Fehl[5]    ; Fehler Y-Achse  
N1007 DEF REAL Z_Fehl[5]    ; Fehler Z-Achse  
N1008 DEF REAL Flaeche[5]   ; Flaeche
N1009 DEF REAL Wicht[5]     ; Wichtung
; 
N1010 DEF INT  FKA_Tab      ; FKA aus Tabelle (Pruef. Zeilennr.) 
N1011 DEF INT  FK_Tab       ; FK  aus Tabelle (Pruef. Zeilennr.)
N1012 DEF INT  Anz_Mess_Tab ; Anz. Messungen aus Tabelle
N1013 DEF INT  FKA_Mess     ; Messraster FKA 
N1014 DEF INT  FK_Mess      ; Messraster FK 
N1015 DEF INT  FKA_0   ; 1. Messpunkt FKA 
N1016 DEF INT  FK_0    ; 1. Messpunkt FK 
N1017 DEF INT  FKA_Max ; Letzter Messpunkt FKA 
N1018 DEF INT  FK_Max  ; Letzter Messpunkt FK
N1019 DEF INT  Zaehl_Mp     ; Z�hler Me�punkte (1..4)  
; 
;**********************************************************
;*   Varis vorbelegen                                     *
;**********************************************************
N1020 FKA_Mess     =   90   ; Messraster FKA 
N1021 FK_Mess      =   90   ; Messraster FK 
N1022 FKA_0   =  -270  ; 1. Messpunkt FKA 
N1023 FK_0    =  -90   ; 1. Messpunkt FK 
N1024 FKA_Max =   90  ; Letzter Messpunkt FKA 
N1025 FK_Max  =   90   ; Letzter Messpunkt FK 
; 
;**********************************************************
;*   Positionen FK/FKA lesen , umliegende Me�punkte best. *
;**********************************************************
N1026 FKA[0] = I_FKA        ; progr. Position FKA
N1027 FrK[0] = I_FK         ; progr. Position FK
; 
N1028 FKA[1] = ROUND(FKA[0] / FKA_Mess - 0.5) * FKA_Mess
N1029 FKA[2] = FKA[1]
N1030 FKA[3] = FKA[1] + FKA_Mess
N1031 FKA[4] = FKA[3]
; 
N1032 FrK[1] = ROUND(FrK[0] / FK_Mess - 0.5) * FK_Mess
N1033 FrK[3] = FrK[1]
N1034 FrK[2] = FrK[1] + FK_Mess
N1035 FrK[4] = FrK[2]
; 
;**********************************************************
;*    Messwerte lesen und Flaechen berechnen              *
;**********************************************************
N1036 FOR Zaehl_Mp = 1 TO 4
;  ********************************************************
;  * Stellung FK/FKA au�erhalb Messraster ?               *
;  ********************************************************
N1037   IF (FKA[Zaehl_Mp] < FKA_0) OR (FKA[Zaehl_Mp] > FKA_Max) OR (FrK[Zaehl_Mp] < FK_0) OR (FrK[Zaehl_Mp] > FK_Max)
N1038     Flaeche[Zaehl_Mp] = 0
N1039     X_Fehl[Zaehl_Mp]  = 0
N1040     Y_Fehl[Zaehl_Mp]  = 0
N1041     Y_Fehl[Zaehl_Mp]  = 0
N1042   ELSE
;    ******************************************************
;    * Nummer = Zeilennr. * Zeilenbr. + Spaltennr.        *
;    ******************************************************
N1043     Nummer = (FrK[Zaehl_Mp]-FK_0)/FK_Mess * ((FKA_Max-FKA_0)/FKA_Mess+1) + (FKA[Zaehl_Mp]-FKA_0)/FKA_Mess
;    ******************************************************
;    * Zeilenummer pr�fen                                 *
;    ******************************************************
N1044     FK_NPKT_TAB (Nummer, Anz_Mess_Tab, Messen, FKA_Tab, FK_Tab)
N1045     IF (FKA_Tab <> FKA[Zaehl_Mp]) OR (FK_Tab  <> FrK[Zaehl_Mp])
N1046       MSG("Fehler beim Lesen aus NPV-Tabelle")
N1047       ENDL_1:
;-!->N1048       GOTOB ENDL_1
N1049     ENDIF
;    ******************************************************
;    * Stellung nicht gemessen ?                          *
;    ******************************************************
N1050     IF (Messen == 0)
N1051       Flaeche[Zaehl_Mp] = 0
N1052       X_Fehl[Zaehl_Mp]  = 0
N1053       Y_Fehl[Zaehl_Mp]  = 0
N1054       Y_Fehl[Zaehl_Mp]  = 0
N1055     ELSE
;      ******************************************************
;      * gegen�berliegende Fl�che berechnen                 *
;      ******************************************************
N1056       Flaeche[Zaehl_Mp] = (FKA_Mess - ABS(FKA[0] - FKA[Zaehl_Mp])) * (FK_Mess - ABS(FrK[0] - FrK[Zaehl_Mp]))
;      ******************************************************
;      * Messwerte lesen                                    *
;      ******************************************************
N1057       FK_NPKT_NPV(Nummer, X_Fehl[Zaehl_Mp], Y_Fehl[Zaehl_Mp], Z_Fehl[Zaehl_Mp])
N1058     ENDIF
N1059   ENDIF
N1060   Flaeche[0] = Flaeche[0] + Flaeche[Zaehl_Mp] ; Gesamtfl�che
N1061 ENDFOR
; 
;**********************************************************
;*   Wichtungen der Messpunkte                            *
;**********************************************************
N1062 IF Flaeche[0] <> 0
N1063   Wicht[1] = Flaeche[1] / Flaeche[0]
N1064   Wicht[2] = Flaeche[2] / Flaeche[0]
N1065   Wicht[3] = Flaeche[3] / Flaeche[0]
N1066   Wicht[4] = Flaeche[4] / Flaeche[0]
N1067 ELSE
N1068   Wicht[1] = 0
N1069   Wicht[2] = 0
N1070   Wicht[3] = 0
N1071   Wicht[4] = 0
N1072 ENDIF
; 
;**********************************************************
;*   gemittelte Fehler X,Y,Z berechnen                    *
;**********************************************************
N1073 X_Fehl[0] = X_Fehl[1]*Wicht[1] + X_Fehl[2]*Wicht[2] + X_Fehl[3]*Wicht[3] + X_Fehl[4]*Wicht[4]
N1074 Y_Fehl[0] = Y_Fehl[1]*Wicht[1] + Y_Fehl[2]*Wicht[2] + Y_Fehl[3]*Wicht[3] + Y_Fehl[4]*Wicht[4]
N1075 Z_Fehl[0] = Z_Fehl[1]*Wicht[1] + Z_Fehl[2]*Wicht[2] + Z_Fehl[3]*Wicht[3] + Z_Fehl[4]*Wicht[4]
;
;**********************************************************
;*   gemittelte Fehler X,Y,Z -> DPR                       *
;**********************************************************
N1076 STOPRE
;N1077 $A_DBW[18] = -X_Fehl[0]*1000 ; neue NPV schreiben
;N1078 $A_DBW[20] = -Y_Fehl[0]*1000
;N1079 $A_DBW[22] = -Z_Fehl[0]*1000
;N1080 STOPRE
;N1081 M9999  ; NPV aktivieren
;-!->N1000 $SA_TEMP_COMP_ABS_VALUE[X1] = -X_Fehl[0]
;-!->N1000 $SA_TEMP_COMP_ABS_VALUE[Y1] = -Y_Fehl[0]
;-!->N1000 $SA_TEMP_COMP_ABS_VALUE[Z1] = -Z_Fehl[0]
;N1000 $SA_TEMP_COMP_ABS_VALUE[Y_G] = -Y_Fehl[0]
N1000 R288 = -X_Fehl[0]
N1000 R287 = -Y_Fehl[0]
N1000 R286 = -Z_Fehl[0]
N1082 M17
