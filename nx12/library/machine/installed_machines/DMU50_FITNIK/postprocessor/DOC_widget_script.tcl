#########################################################
#
# DOC_widget_script.tcl
#
#########################################################

#========================================================
proc WIDGET_YES { } {
#========================================================
puts "YES"
exit

}

#========================================================
proc WIDGET_NO { } {
 puts "NO"
 exit
}

set argv_string "My String"
 
frame .from
frame .to
label .label1 -text " Tool Sheet $argv_string exists - overwrite? "
button .yes -text "YES" -command WIDGET_YES
button .no -text "NO" -command WIDGET_NO

pack .label1
pack .yes .no -side left -padx 20