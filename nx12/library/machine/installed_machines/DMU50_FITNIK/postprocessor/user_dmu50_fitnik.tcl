#=============================================================
#SETTING
#=============================================================
proc USER_SET_SINUMERIK_SETTING {} {
#=============================================================
# This command is used to set the sinumerik_control_version and sinumerik_version.
#
# --------------------------------------------------
# "Sinumerik control version" is used to set the output format for drilling CYCLEs (such as CYCLE81).
# "Sinumerik version" is used to set the output format for CYCLE832.
# --------------------------------------------------
#    Sinumerik control version "Powerline"
# --------------------------------------------------
#       Sinumerik version 5.x - "V5"
#       Sinumerik version 6.x - "V6"
#       Sinumerik version 7.x - "V7"
# --------------------------------------------------
#
# --------------------------------------------------
#    Sinumerik control version "Solutionline"
# --------------------------------------------------
#       Sinumerik version 4.4 - "from V26 to V44"
#       Sinumerik version 4.5 - "V45"
#       Sinumerik version 4.7 - "V47"
# --------------------------------------------------
#
# 22-Dec-2016 shuai - CYCLE832 enhancement.
#                     Update the description for this command.
#

  global sinumerik_control_version
  global sinumerik_version

# set sinumerik_version "V5"
# set sinumerik_version "V6"
#  set sinumerik_version "V7"
# set sinumerik_version "from V26 to V44"
# set sinumerik_version "V45"
 set sinumerik_version "V47"

  if {![string compare "V5" $sinumerik_version] || \
      ![string compare "V6" $sinumerik_version] || \
      ![string compare "V7" $sinumerik_version] } {

     set sinumerik_control_version "Powerline"

  } elseif {![string compare "from V26 to V44" $sinumerik_version] || \
            ![string compare "V45" $sinumerik_version] || \
            ![string compare "V47" $sinumerik_version] } {

     set sinumerik_control_version "Solutionline"
  }


# This command is the default Sinumerik milling settings for Mold&Die and Aerospace production.
#
# 22-Dec-2016 shuai - CYCLE832 enhancement.
#                     Update the description for this command.
#

  global sinumerik_version

  if {![info exists sinumerik_version] } {

     return
  }


  # Set the default settings for compressor and smoothing.
  global mom_siemens_compressor
  global mom_siemens_smoothing

  switch $sinumerik_version {

     "V5" {

        set mom_siemens_compressor "COMPCURV" ; # COMPCURV / COMPOF
        set mom_siemens_smoothing  "G642" ;     # G642 / G60
     }

     "V6" {
        set mom_siemens_compressor "COMPCAD" ; # COMPCAD / COMPOF
        set mom_siemens_smoothing  "G642" ;    # G642 / G64
     }

     "V7" -

     "from V26 to V44" -

     "V45" -

     "V47" {
        set mom_siemens_compressor "COMPCAD" ; # COMPCAD / COMPOF
        set mom_siemens_smoothing  "G645" ;    # G645 / G60
     }
  }

  # Set the default settings for some variables.
  global mom_siemens_tol_status
  global mom_siemens_feedforward
  global mom_siemens_top_surface_smooth
  global mom_siemens_5axis_mode
  global mom_siemens_ori_coord
  global mom_siemens_ori_inter
  global mom_siemens_ori_def
  global mom_siemens_rotary_tol
  global mom_siemens_rotary_tol_status

  set mom_siemens_tol_status         "System" ;      # System / User tolerance
  set mom_siemens_feedforward        "FFWON" ;       # FFWON / FFWOF
  set mom_siemens_top_surface_smooth "" ;            # _TOP_SURFACE_SMOOTH_ON / _TOP_SURFACE_SMOOTH_OFF
  set mom_siemens_5axis_mode         "SWIVELING" ;      # TRAORI / SWIVELING / TRAFOOF
  set mom_siemens_ori_coord          "ORIWKS" ;      # ORIWKS / ORIMKS
  set mom_siemens_ori_inter          "ORIAXES" ;     # ORIAXES / ORIVECT
  set mom_siemens_ori_def            "ROTARY AXES" ; # ROTARY AXES / VECTOR

  # Set the rotary tolerance.
  set mom_siemens_rotary_tol        0.8
  set mom_siemens_rotary_tol_status "System" ; # System / User

  # Set the method type and settings status for milling.
  global mom_siemens_method
  global mom_siemens_milling_setting

  set mom_siemens_method          "DESELECTION"
  set mom_siemens_milling_setting "Default"

  # Disable tool axis vector in 3axis machine.
  global mom_kin_machine_type

  if {[string match "3_axis_mill" $mom_kin_machine_type]} {

     set mom_siemens_5axis_mode "TRAFOOF"

     MOM_disable_address A3 B3 C3
     MOM_disable_address fourth_axis fifth_axis

  } elseif {[string match "4*" $mom_kin_machine_type] || [string match "3_axis_mill_turn" $mom_kin_machine_type]} {

     MOM_disable_address fifth_axis
  }
  
  #Set feedrate mode
  global mom_siemens_feed_definition
  set mom_siemens_feed_definition OFF

}
#=============================================================
proc USER_INIT_INI_FILES {} {
#=============================================================
# This procedure is used with simulation_ini.tcl file which should be sourced
# in postprocessor as a user tcl file for generating ini files.
# This procedure will be excuted when simulation_ini.tcl is sourcing.
# Once you source the simulation_ini.tcl file in a postprocessor, remove this
# procedure from Start of Program will NOT affect the result.
#
# Following mom variables are used to set the options for ini files.
# mom_sinumerik_ini_create
#    "Yes"          ini files will be created
#    "No "          ini files will not be created
#
# mom_sinumerik_ini_location
#    "Part"         ini files will be output to a subfolder \cse_files\subprog
#                   of cam part file location. If \cse_files\subprog is not exists
#                   post will create this folder.
#    "CSE"          ini files will be output to sub folder \subprog of \cse_driver
#                   folder which located in same directory of \postprocessor as installed
#                   machine examples.
#    "ENV"          ini files will be output to a sub folder \subprog of enviornment
#                   variable defined directory.
#                   If it is not exists, ini files will be output to "Part".
#
# mom_sinumerik_ini_existing
#    "Rename"       Rename existing ini files to .bck in place where the one is created.
#    "Keep"         Keep ini files in place where the one is created.
#    "Delete"       Delete ini files in place where the one is created.
#
# mom_sinumerik_ini_end_status
#    "Rename"       Rename created ini file to .bck file after post run.
#    "Keep"         Keep created ini files after post run.
#    "Delete"       Delete created ini files after post run.


  global mom_sinumerik_ini_create
  global mom_sinumerik_ini_location
  global mom_sinumerik_ini_keep_existing
  global mom_sinumerik_ini_end_status

  set mom_sinumerik_ini_create     "Yes"
  set mom_sinumerik_ini_location   "Part"
  set mom_sinumerik_ini_existing   "Delete"
  set mom_sinumerik_ini_end_status "Keep"
}
#=============================================================
#debug setting
proc USER_INIT_VARIABLES {} {
#=============================================================
#This command is used to initialize the variables used in
#Sinumerik 840D postprocessors.
#
global mom_siemens_5axis_mode
set mom_siemens_5axis_mode "TRAFOOF"

#Initial 5axis mode.
  global mom_siemens_5axis_output_mode
  set mom_siemens_5axis_output_mode 0

#Coordinate rotation
  global mom_siemens_coord_rotation
  set mom_siemens_coord_rotation 0

#Cutcom type
  global mom_siemens_3Dcutcom_mode
  set mom_siemens_3Dcutcom_mode "OFF"

#Motion message
  #global mom_siemens_pre_motion
  #set mom_siemens_pre_motion "start"

#Drilling cycle count
  global mom_siemens_cycle_count
  set mom_siemens_cycle_count 0

  if {[llength [info commands MOM_ask_mcs_info ]]} {
     MOM_ask_mcs_info
  }
 #CYCLE800
 global cycle800_fr cycle800_dir1
 set cycle800_fr 0  ;#retract type 0/1/2/4/5
 set cycle800_dir1 1;#Направление поворота 1/-1/0
}
#=============================================================
proc USER_RESET_VARIABLES {} {
#=============================================================
  #CYCLE800
  global cycle800_dir1
  
  set cycle800_dir1 1
}
#=============================================================
proc USER_SET_ARC_LINEARIZATION {} {
#=============================================================
  global mom_kin_arc_output_mode mom_kin_helical_arc_output_mode
  global mom_siemens_5axis_mode  
    set lmode "FULL_CIRCLE"
	if {[info exists mom_siemens_5axis_mode] && [string match "TRAORI*" $mom_siemens_5axis_mode]} {
	      set lmode "LINEAR"
	   } 
	   
    set mom_kin_arc_output_mode $lmode
    set mom_kin_helical_arc_output_mode $lmode
    MOM_reload_kinematics 
}
#=============================================================
# EVENT
#=============================================================
proc USER_AUTO_CHANGE_TOOL {} {
#=============================================================

global mom_tool_number mom_next_tool_number
   if { ![info exists mom_next_tool_number] } {
      set mom_next_tool_number $mom_tool_number
   }
 
 #MOM_output_literal ";Tool Change"
 MOM_force Once T
 MOM_do_template tool_change
 MOM_force Once M
 MOM_do_template tool_change_1
 #MOM_do_template msg_method
 #MOM_do_template trafoof
 #MOM_force Once Text G_motion Text D
 #MOM_do_template tool_change_return_home_Z
 #MOM_force Once Text G_motion Text D
 #MOM_do_template tool_change_return_home
}
#=============================================================
#=============================================================
proc USER_START_OF_PROGRAM {} {
#=============================================================
	USER_out_header
	USER_out_tool_list
	MOM_output_literal ";START PROGRAM"
	USER_UPLEVEL
	MOM_output_literal "CYCLE800(1,\"R_DATA\",0,57,0.,0.,0.,0.,-0.,0.,0.,0.,0.,1,1.,0)"
	MOM_output_literal "CYCLE800()"
	MOM_output_literal "G17 G40"
	MOM_output_literal "TRANS TRAFOOF"
}
#=============================================================
proc USER_START_OF_OPERATION {} {
#=============================================================
   PB_CMD_start_of_extcall_operation
   #PB_CMD_output_start_program
   PB_CMD_reset_sinumerik_setting_in_group
   PB_CMD_set_resolution
   PB_CMD_set_fixture_offset
  
   #MOM_output_literal ";Start of Path"
   #PB_CMD_output_start_path

   #if { [PB_CMD__check_block_no_output] } {
   #   MOM_do_template start_of_path_2
   #}
   #PB_CMD_output_V5_compressor_tol

   #MOM_do_template home_position

   #MOM_do_template home_position_rotary

   #if { [PB_CMD__check_block_FGREF] } {
   #   MOM_do_template start_of_path
   #}
   #MOM_output_literal ""
   #MOM_output_literal "; "
   global mom_operation_name
   global user_tool_description
   USER_tool_description
   MOM_output_literal ";OPERATION: $mom_operation_name $user_tool_description"
   #MOM_output_literal "; "
   PB_CMD_start_of_operation_force_addresses
}
#=============================================================
proc USER_INITIAL_MOVE {} {
#=============================================================
global mom_motion_event
global mom_kin_arc_output_mode
global user_pattern_init_flag
  #MOM_output_literal ";>>>Foo mom_motion_event:$mom_motion_event"
   if {[info exist user_pattern_init_flag]} {
   } else {
   #USER_PATTERN_DEF
   #USER_PATTERN_SET
   set user_pattern_init_flag TRUE
   }
   
   USER_PATTERN_START
   
   PB_CMD_define_feed_variable_value
   PB_CMD_detect_5axis_mode
   PB_CMD_reset_output_digits
   PB_CMD_auto_3D_rotation
   
   USER_SET_MODE 
   #MOM_do_template g17

   #if {$mom_motion_event=="initial_move"} {
   #MOM_output_literal ";Initial Move"
   #}
   #PB_CMD_output_V5_Sinumerik_setting

   #if { [PB_CMD__check_block_no_output] } {
   #   PB_call_macro CYCLE832_v7
   #}

   #MOM_force Once transf
   MOM_do_template traori_trafoof
  
   
  
   MOM_force Once G_offset
   MOM_do_template fixture_offset
   
   if { [PB_CMD__check_block_ori_coordinate] } {
      MOM_do_template oriwks_orimks
   }

   if { [PB_CMD__check_block_ori_interpolation] } {
      MOM_do_template oriaxes_orivector
   }

   if { [PB_CMD__check_block_rotation_axes] } {
  #    MOM_force Once G_motion fourth_axis fifth_axis
  #    MOM_do_template rotation_axes
   }

   if { [PB_CMD__check_block_ORIRESET] } {
   #   PB_call_macro ORIRESET
   }
  # PB_CMD_output_trans_arot

   if { [PB_CMD__check_block_CYCLE800] } {
      PB_call_macro CYCLE800_sl
      #MOM_suppress Once Z
      #MOM_do_template rapid_traverse
   }
   #Вставить проверку на 5-ти осевой вывод
   #if { [USER_check_block_TRAORI] } { 
   #   
   #   set mom_kin_arc_output_mode "LINEAR"
   #   set mom_kin_helical_arc_output_mode "LINEAR"
   #   MOM_reload_kinematics 
  # 
  # } else {;#если 5-ти осевая выводим TRAORI
   #
   #   set mom_kin_arc_output_mode "FULL_CIRCLE"
   #   set mom_kin_helical_arc_output_mode "FULL_CIRCLE"
   #   MOM_reload_kinematics 
   #}
   PB_CMD_output_cutcom_mode
   #PB_CMD_move_force_addresses
   
  USER_SET_ARC_LINEARIZATION
  #global mom_programmed_feed_rate
  # if { [EQ_is_equal $mom_programmed_feed_rate 0] } {
      MOM_rapid_move
  # } else {
  #    MOM_linear_move
  # }
}
#=============================================================
proc USER_FIRST_MOVE {} {
#=============================================================
   global mom_motion_event
  #MOM_output_literal ";>>>Foo mom_motion_event:$mom_motion_event"
   
   PB_CMD_define_feed_variable_value
   PB_CMD_detect_5axis_mode
   PB_CMD_reset_output_digits
   PB_CMD_auto_3D_rotation

   #MOM_do_template msg_method

   MOM_do_template g17

   MOM_output_literal ";First Move"
   #PB_CMD_output_V5_Sinumerik_setting

   #if { [PB_CMD__check_block_CYCLE832] } {
   #   PB_call_macro CYCLE832_v7
   #}

   MOM_do_template traori_trafoof

   MOM_force Once G_offset
   MOM_do_template fixture_offset

   if { [PB_CMD__check_block_ori_coordinate] } {
      MOM_do_template oriwks_orimks
   }

   if { [PB_CMD__check_block_ori_interpolation] } {
      MOM_do_template oriaxes_orivector
   }

   if { [PB_CMD__check_block_rotation_axes] } {
      MOM_force Once G_motion fourth_axis fifth_axis
      MOM_do_template rotation_axes
   }

   if { [PB_CMD__check_block_ORIRESET] } {
      PB_call_macro ORIRESET
   }
   PB_CMD_output_trans_arot

   if { [PB_CMD__check_block_CYCLE800] } {
      PB_call_macro CYCLE800_sl
   }
   PB_CMD_output_cutcom_mode
   PB_CMD_move_force_addresses
   catch { MOM_$mom_motion_event }
}
#=============================================================
proc USER_END_OF_OPERATION {} {
#=============================================================
#  MOM_output_literal ";End of Path"
#
#   if { [PB_CMD__check_block_reset_trans] } {
#      MOM_do_template reset_trans
#   }
#   if { [PB_CMD__check_block_reset_cycle800] } {
      MOM_do_template reset_cycle800
#   }
#
   if { [PB_CMD__check_block_reset_traori] } {
      MOM_do_template trafoof
   }
   PB_CMD_restore_kinematics
   MOM_output_literal "G0 SUPA Z650 D0"
   MOM_force Once D
   
#
#   MOM_force Once Text G_motion Text D
#   MOM_do_template tool_change_return_home_Z
#
#   MOM_force Once Text G_motion Text D
#   MOM_do_template tool_change_return_home
#   PB_CMD_output_V5_sinumerik_reset
#
#   if { [PB_CMD__check_block_reset_cycle832] } {
#      MOM_do_template reset_cycle832
#   }
#
   #MOM_do_template spindle_off
   PB_CMD_reset_control_mode
   PB_CMD_reset_all_motion_variables_to_zero
   PB_CMD_end_of_extcall_operation
   PB_CMD_reset_Sinumerik_setting
   
   USER_PATTERN_END
   USER_RESET_VARIABLES
}
#=============================================================
proc USER_END_OF_PROGRAM {} {
#=============================================================
   PB_CMD_end_of_extcall_program

   MOM_output_literal ";End of Program"
   MOM_output_literal "CYCLE800(1,\"R_DATA\",0,57,0.,0.,0.,0.,-0.,0.,0.,0.,0.,1,1.,0)"
   MOM_output_literal "CYCLE800()"
   MOM_do_template end_of_program
   PB_CMD_end_of_program
   MOM_set_seq_off
   
   USER_RUN_EXT_EDITOR
}
#=============================================================
#
#=============================================================
proc USER_check_block_TRAORI {} {
#=============================================================
  global mom_siemens_coord_rotation mom_siemens_5axis_mode
  global mom_pos mom_mcs_goto mom_prev_mcs_goto mom_prev_pos mom_arc_center mom_pos_arc_center
  global coord_rot_angle coord_ang_A coord_ang_B coord_ang_C
  global mom_result mom_result1
  global mom_tool_axis mom_nxt_tool_axis
  global RAD2DEG DEG2RAD
  global mom_kin_4th_axis_vector mom_kin_5th_axis_vector
  global mom_out_angle_pos
  global sinumerik_control_version
  global mom_rotary_direction_4th
  global cycle800_inc_retract
  global cycle800_tc cycle800_dir
  global cycle800_st cycle800_mode
  global coord_ang_1 coord_ang_2 coord_ang_3
  global mom_siemens_cycle_dmode
  
  if {[info exists mom_siemens_5axis_mode] && [string match "TRAORI*" $mom_siemens_5axis_mode]} {
  #MOM_output_literal ";Foo Режим ТРАОРИ"
  
  #Инициализация векторов vpt X Y Z
  set x $mom_mcs_goto(0); set y $mom_mcs_goto(1); set z $mom_mcs_goto(2);
  VEC3_init x y z vpt
  
  set x 1.0; set y 0.0; set z 0.0;
  VEC3_init x y z X
  set x 0.0; set y 1.0; set z 0.0;
  VEC3_init x y z Y
  set x 0.0; set y 0.0; set z 1.0;
  VEC3_init x y z Z
  
  #Поворот векторов
  set rot0 [expr $mom_out_angle_pos(0)*$DEG2RAD]
  set rot1 [expr $mom_out_angle_pos(1)*$DEG2RAD]
  
  VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] X v1
  VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] Y v2
  VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] Z v3

  VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v1 X
  VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v2 Y
  VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v3 Z
  
  #Инициализация матрицы вращения r_matrix
  set res [MTX3_init_x_y_z X Y Z r_matrix]
    
  #Вычисление координаты точки
  MTX3_vec_multiply vpt r_matrix mom_pos
  
  #set mom_pos(0) $vptn(0)
  #set mom_pos(1) $vptn(1)
  #set mom_pos(2) $vptn(2)
       
  # Calculate euler angles , rotation order is X->Y->Z
  if { [info exists r_matrix] } {
    set m0 $r_matrix(0)
    set m1 $r_matrix(1)
    set m2 $r_matrix(2)
    set m3 $r_matrix(3)
    set m4 $r_matrix(4)
    set m5 $r_matrix(5)
    set m6 $r_matrix(6)
    set m7 $r_matrix(7)
    set m8 $r_matrix(8)

    set cos_b_sq [expr $m0*$m0 + $m3*$m3]

    if { [EQ_is_equal $cos_b_sq 0.0] } {

      set cos_b 0.0
      set cos_c 1.0
      set cos_a $m4
      set sin_c 0.0
      set sin_a [expr -1*$m5]

      if { $m6 < 0.0 } {
        set sin_b 1.0
      } else {
        set sin_b -1.0
      }

    } else {

      set cos_b [expr sqrt($cos_b_sq)]
      set sin_b [expr -$m6]

      set cos_a [expr $m8/$cos_b]
      set sin_a [expr $m7/$cos_b]

      set cos_c [expr $m0/$cos_b]
      set sin_c [expr $m3/$cos_b]

    }

    set coord_ang_A [expr -atan2($sin_a,$cos_a)*$RAD2DEG]
    set coord_ang_B [expr -atan2($sin_b,$cos_b)*$RAD2DEG]
    set coord_ang_C [expr -atan2($sin_c,$cos_c)*$RAD2DEG]
  }
   
   #Установка параметров цикла вращения
   set cycle800_tc "\"R_DATA\"" ;# For example,please put your data here

 #-----------------------------------------------------------
 #Please set your incremental retraction
 #-----------------------------------------------------------
   set cycle800_inc_retract "1"
   set mom_siemens_cycle_dmode 0
   set cycle800_dir $mom_rotary_direction_4th
   set cycle800_st 0
   set cycle800_mode 57
   set coord_ang_1 $coord_ang_A
   set coord_ang_2 $coord_ang_B
   set coord_ang_3 $coord_ang_C
   MOM_do_template rotation_axes CREATE
   MOM_output_debug " USER_check_block_TRAORI"
   PB_call_macro CYCLE800_sl
   MOM_suppress Once Z
   MOM_do_template rapid_traverse
   MOM_do_template rapid_traverse
   MOM_output_literal "CYCLE800()"
   #MOM_force Once G_offset
   #MOM_do_template fixture_offset
   MOM_force Once transf
   MOM_do_template traori_trafoof 
   MOM_force Once X Y Z fourth_axis fifth_axis
   MOM_force Once G_motion
   return 1
  }

return 0
}
#=============================================================
proc USER_detect_AUTO_3D_DRILL {} {
#=============================================================
# Детектирование режима автоматического 3D сверления
# MOM_output_literal ";Foo Detect AUTO3D DRILL\n"
 global mom_operation_type mom_siemens_5axis_mode
 global mom_nxt_event mom_nxt_motion_event
 global mom_prev_out_angle_pos mom_out_angle_pos
 
 
  if {[info exists mom_siemens_5axis_mode]} {
# MOM_output_literal ";Foo mom_siemens_5axis_mode:$mom_siemens_5axis_mode"
  }
  if { [string match "Hole Making" $mom_operation_type] || [string match "Point to Point" $mom_operation_type] || \
       [string match "Cylinder Milling" $mom_operation_type] || [string match "Thread Milling" $mom_operation_type] || \
       [string match "Drilling" $mom_operation_type] } { 
  if {[info exists mom_prev_out_angle_pos] && [info exists mom_out_angle_pos]} {
    if {[EQ_is_equal $mom_prev_out_angle_pos(0) $mom_out_angle_pos(0)] &&\
	        [EQ_is_equal $mom_prev_out_angle_pos(1) $mom_out_angle_pos(1)]} {
return 0
	}
  } else {
return 0
    }	
return 1
  }
return 0
}
#=============================================================
proc USER_rotate_rapid_coordinate {} {
#=============================================================
# This command is used to detect rotary axis change inside operation for 3+2 milling.
# This command will output coordinate rotation code if the rotary axis change the
# position.

  global mom_kin_machine_type
  global mom_tool_axis mom_tool_axis_type
  global mom_siemens_5axis_mode
  global mom_pos mom_mcs_goto
  global mom_prev_out_angle_pos mom_out_angle_pos
  global mom_siemens_coord_rotation
  global save_mom_kin_machine_type
  global mom_prev_tool_axis
  global coord_ang_A coord_ang_B coord_ang_C
  global coord_offset mom_current_motion
  global mom_out_angle_pos mom_prev_out_angle_pos
  global user_first_motion_flag
  global cycle800_dir1


  USER_SET_MODE
  #Обработчик для первого перемещения:
  if { [string match "initial_move" $mom_current_motion] || [string match "first_move"   $mom_current_motion] } {
      if {![info exists user_first_motion_flag]} {set user_first_motion_flag 1}
	  if {$user_first_motion_flag==1} {
	  incr user_first_motion_flag
	  #MOM_output_literal ";USER ROTATE RAPID COORDINATE mom_current_motion: $mom_current_motion"
# MOM_output_literal ";Foo USER_rotate_rapid_coordinate: First Motion"
	  #MOM_force Once X Y
	  #Вставить проверку на 5-ти осевой вывод
   if { [USER_check_block_TRAORI] } { 
      #set mom_kin_arc_output_mode "LINEAR"
      #set mom_kin_helical_arc_output_mode "LINEAR"
      #MOM_reload_kinematics 
   } else {;#если 5-ти осевая выводим TRAORI
      #set mom_kin_arc_output_mode "FULL_CIRCLE"
      #set mom_kin_helical_arc_output_mode "FULL_CIRCLE"
      #MOM_reload_kinematics
	  MOM_suppress Once Z
      MOM_force Once G_motion
      MOM_do_template rapid_traverse
	  MOM_force Once G_motion
	  if {[CMD_EXIST USER_detect_AUTO_3D_DRILL] && [USER_detect_AUTO_3D_DRILL] } {
		MOM_do_template rapid_traverse
        
	  }
	  return
      }
     }
	} else { catch {unset user_first_motion_flag} }
	
#если это не пятиосевой станок
  if { ![string match "*5_axis*" $mom_kin_machine_type] } {
return
  }
#сверление проверяем 3D_AUTO
#Вставить код здесь 
#Установим режим DRILL_AUTO_CHANGE
  if {[CMD_EXIST USER_detect_AUTO_3D_DRILL] && [USER_detect_AUTO_3D_DRILL] } {
    global user_3d_auto_drill
	global mom_siemens_5axis_output
    if {![info exists user_3d_auto_drill] || ([info exists user_3d_auto_drill] && $user_3d_auto_drill!=1)} {
		MOM_output_literal "CYCLE800()"
	}
	set user_3d_auto_drill 1
	set mom_siemens_coord_rotation 1
	
    set mom_siemens_5axis_output "TRAORI"
	#MOM_do_template traori_trafoof
    MOM_output_debug "AUTO_3D_DRILL TRUE"
	
	#CYCLE80 запрещаем поворот 
	set cycle800_dir1 0
	
	
	#сбросить координаты
	
	global mom_pos mom_mcs_goto
	set mom_pos(0) $mom_mcs_goto(0)
	set mom_pos(1) $mom_mcs_goto(1)
	set mom_pos(2) $mom_mcs_goto(2)
	
	#set mom_pos(3) 0
	#set mom_pos(4) 0
	
	MOM_reload_variable -a mom_pos
  }
  
#
#
  if {[info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation ==1} {
  #MOM_output_literal "; Foo Нихрена не поворачивается"
return
  }
  if {![info exists mom_prev_out_angle_pos]} {
     set mom_prev_out_angle_pos(0) 0.0
     set mom_prev_out_angle_pos(1) 0.0
  }


  if {[EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] && [EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
    #MOM_output_literal "; Foo Углы нулевые"
return
  }

  if { [info exists mom_tool_axis] && [info exists mom_prev_tool_axis] } {
     	  if { [VEC3_is_equal mom_tool_axis mom_prev_tool_axis] } {
 
return
      }
  }
  
  if {[EQ_is_equal $mom_tool_axis(2) 1.0]} {
     set coord_ang_A 0
     set coord_ang_B 0
     set coord_ang_C 0
     if {[info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation == 2} {
        set mom_prev_out_angle_pos(0) $mom_out_angle_pos(0)
        set mom_prev_out_angle_pos(1) $mom_out_angle_pos(1)
        MOM_reload_variable -a mom_prev_out_angle_pos
     }
  } else {
     PB_CMD_auto_3D_rotation
  }
 
  if {$mom_siemens_coord_rotation ==2} {
     # Output coordinate rotation code
     
	 #PB_CMD_output_trans_arot
     if { [PB_CMD__check_block_CYCLE800] } {
MOM_output_debug "USER_check_block_TRAORI"
		  PB_call_macro CYCLE800_sl
     }
     #MOM_force Once X Y
     MOM_force Once X Y Z
  }
  
  
 }
#=============================================================
proc USER_rotate_cycle_coordinate {} {
#=============================================================
# This command is used to handle variable-axis drilling cycles.
# If coordinate isn't set as csys rotation and tool axis is not along Zaxis. Frame rotation codes
# TRANS/AROT or cycle800 will be output acoording to tool axis direction.
# For dual table machine without tool tip control, this command will be ignored.

  MOM_output_debug "USER_rotate_cycle_coordinate"
  global cycle_init_flag
  global mom_siemens_cycle_count
  global cycle_plane_change_flag
  global mom_kin_machine_type
  global mom_tool_axis mom_tool_axis_type
  global mom_siemens_5axis_mode
  global mom_pos mom_mcs_goto
  global mom_prev_out_angle_pos mom_out_angle_pos
  global mom_siemens_coord_rotation
  global save_mom_kin_machine_type
  global mom_prev_tool_axis
  global coord_ang_A coord_ang_B coord_ang_C
  global mom_cycle_rapid_to_pos mom_cycle_retract_to_pos mom_cycle_feed_to_pos
  global mom_cycle_rapid_to mom_cycle_retract_to mom_cycle_feed_to
  global coord_offset mom_current_motion
  global mom_cycle_spindle_axis
  global mom_current_motion
  global mom_kin_machine_type
  global mom_cycle_clearance_pos mom_cycle_clearance_plane
  
  global mom_siemens_5axis_output
  
  if { ![string match "*5_axis*" $mom_kin_machine_type] } {
return
  }
  
  #USER 3D_AUTO
  global user_3d_auto_drill cycle800_dir
  if {[info exists user_3d_auto_drill] && $user_3d_auto_drill==1 } {
     set mom_siemens_coord_rotation 2
	 set user_3d_auto_drill 0
	 set mom_cycle_retract_mode "MANUAL"
	 #Временно здесь есть ошибка
	 catch {unset mom_prev_out_angle_pos}
	 #set mom_prev_out_angle_pos(0) $mom_out_angle_pos(0);#0.0
     #set mom_prev_out_angle_pos(1) $mom_out_angle_pos(1);#0.0
	 #
	 set mom_siemens_5axis_output "TRAFOOF"
	 MOM_do_template traori_trafoof	
	 #MOM_output_literal "G0 C0"
 
  }
    

 if {[info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation ==1} {
   return
  }
  if {![info exists mom_prev_out_angle_pos]} {
     set mom_prev_out_angle_pos(0) 0.0
     set mom_prev_out_angle_pos(1) 0.0
  }
  # <lili 04-20-2011>The command PB_CMD_auto_3D_rotation should be call again
  # at first hole to calculate mom_pos if there is no start point or clearance
  # plane because mom_pos cannot be reloaded at inital move or first move.
  # cycle_rotate_flag is used to detect if there is coordinate rotation after
  # first hole.
  
  if {![EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] || ![EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
     set cycle_rotate_flag 1
	
  } else {
     set cycle_rotate_flag 0
  }

  if {[EQ_is_equal $mom_tool_axis(2) 1.0]} {
     set coord_ang_A 0
     set coord_ang_B 0
     set coord_ang_C 0
     # If in cycle plane change event, the tool axis changed back to Z axis,
     # reload mom_prev_out_angle_pos to prevent the coordinate rotation code output twice.
	global mom_out_angle_pos
	 if {[info exists mom_siemens_coord_rotation] && $mom_siemens_coord_rotation == 2} {
        set mom_prev_out_angle_pos(0) $mom_out_angle_pos(0)
        set mom_prev_out_angle_pos(1) $mom_out_angle_pos(1)		
        set mom_out_angle_pos(0)  $mom_pos(3);
		set mom_out_angle_pos(1)  $mom_pos(4);
		
		#set mom_pos(0) $mom_mcs_goto(0)
		#set mom_pos(1) $mom_mcs_goto(1)
		#set mom_pos(2) $mom_mcs_goto(2)
		#set mom_pos(3) 0
		#set mom_pos(4) 0
		set coord_ang_C $mom_out_angle_pos(1)
		#MOM_force Once X Y Z fourth_axis fifth_axis
		MOM_output_debug "!!!!! 0000"
		#MOM_do_template rapid_traverse
		
		MOM_reload_variable -a mom_prev_out_angle_pos
		MOM_reload_variable -a mom_prev_pos
        MOM_reload_variable -a mom_pos
        MOM_reload_variable -a mom_prev_out_angle_pos
        MOM_reload_variable -a mom_out_angle_pos
        MOM_reload_variable mom_prev_rot_ang_4th
        MOM_reload_variable mom_prev_rot_ang_5th
        MOM_reload_variable mom_rotation_angle
	    MOM_reload_kinematics
	
     }
  } else {

     PB_CMD_auto_3D_rotation
  }

  if {$mom_siemens_coord_rotation ==2} {
     set mom_cycle_spindle_axis 2
     if {$cycle_rotate_flag == 1} {
       #MOM_do_template traori_trafoof
	   # Output coordinate rotation code
        PB_CMD_output_trans_arot
        if { [PB_CMD__check_block_CYCLE800] } {
         MOM_output_debug "mom_pos(4):$mom_pos(4)"
           PB_call_macro CYCLE800_sl
        }
        MOM_force Once X Y Z
     }
	 
     VMOV 3 mom_pos mom_cycle_rapid_to_pos
     VMOV 3 mom_pos mom_cycle_feed_to_pos
     VMOV 3 mom_pos mom_cycle_retract_to_pos
     VMOV 3 mom_pos mom_cycle_clearance_pos
	 
     set mom_cycle_rapid_to_pos(2) [expr $mom_pos(2)+$mom_cycle_rapid_to]
     set mom_cycle_retract_to_pos(2) [expr $mom_pos(2)+$mom_cycle_retract_to]
     set mom_cycle_feed_to_pos(2) [expr $mom_pos(2)+$mom_cycle_feed_to]
     set mom_cycle_clearance_pos(2) [expr $mom_pos(2)+$mom_cycle_clearance_plane]
  }
}
#=============================================================
# PATTERN
#=============================================================
proc USER_UPLEVEL { } {
#=============================================================
  uplevel #0 {
    proc MOM_dmu50_pattern {} {
      global mom_ude50_pattern_number_of_blades mom_dmu50_pattern_start_angle
	  global mom_dmu50_pattern_mode
	  
	  set mom_dmu50_pattern_mode 1
	  if {[info exists mom_dmu50_pattern_mode]} {
		#MOM_output_literal ";mom_dmu50_pattern_mode:$mom_dmu50_pattern_mode"
	  }
	  
	  if {[info exists mom_ude50_pattern_number_of_blades]} {
			#MOM_output_literal ";>>>> mom_ude50_pattern_number_of_blades:$mom_ude50_pattern_number_of_blades"
      } else {
	        #MOM_output_literal ";>>>> Can't read: mom_ude50_pattern_number_of_blades"
	    }
    }
  };#uplevel
}
#=============================================================
proc USER_PATTERN_DEF { } {
#=============================================================
  global mom_ude50_pattern_number_of_blades
  global mom_dmu50_pattern_mode
  global mom_dmu50_start_anlge
  global mom_dmu50_add_angle
  global mom_dmu50_total
  global mom_dmu50_pattern_start_angle
  
  if {![info exists mom_dmu50_pattern_mode]} {
    return
  } 
  
  if {![info exists mom_ude50_pattern_number_of_blades] || $mom_ude50_pattern_number_of_blades < 1} {
      
	  #MOM_display_message "Пользовательское событие: Количество повторений равно 0. Режим применён не будет" "Пользовательское событие: PATTERN" "W"
return
  }
  
  if {![info exists mom_dmu50_pattern_start_angle]} {
    set mom_dmu50_pattern_start_angle 0.0
  }
  
  set mom_dmu50_add_angle [expr 360.0/$mom_ude50_pattern_number_of_blades]
  set mom_dmu50_total [expr abs($mom_ude50_pattern_number_of_blades) - 1]
  
  set mom_dmu50_add_angle [rd [format "%0.5f" $mom_dmu50_add_angle] 4]  
  set mom_dmu50_pattern_start_angle [rd [format "%0.5f" $mom_dmu50_pattern_start_angle] 4]
  
  
  MOM_output_literal ";--------------------"
  #MOM_output_literal "DEF DEF INT COUNTER; Счётчик (R1)
  MOM_output_literal "DEF REAL START_ANGL ; Начальный угол"
  MOM_output_literal "DEF REAL ADD_ANGL   ; Угол приращения"
  MOM_output_literal "DEF INT TOTAL       ; Количество повторов"
  MOM_output_literal "DEF INT OFFSET_ANGL ; Величина углового смещения"
  #MOM_output_literal "START_ANGL=$mom_dmu50_pattern_start_angle;"
  #MOM_output_literal "ADD_ANGL=$mom_dmu50_add_angle;"
  #MOM_output_literal "TOTAL=$mom_dmu50_total;"
  #MOM_output_literal ";--------------------"
}
#=============================================================
proc USER_PATTERN_SET { } {
#=============================================================
  global mom_dmu50_pattern_mode
  
  if {![info exists mom_dmu50_pattern_mode]} {
 return
  }
  global mom_dmu50_pattern_start_angle
  global mom_dmu50_add_angle
  global mom_dmu50_total
  
  #Output 
  MOM_output_literal "START_ANGL=$mom_dmu50_pattern_start_angle;"
  MOM_output_literal "ADD_ANGL=$mom_dmu50_add_angle;"
  MOM_output_literal "TOTAL=$mom_dmu50_total;"
  MOM_output_literal ";--------------------"
}
#=============================================================
proc USER_PATTERN_START { } {
#=============================================================
  global mom_dmu50_pattern_mode
  if {![info exists mom_dmu50_pattern_mode]} {
 return
  }
  MOM_output_literal "; mom_dmu50_pattern_mode:$mom_dmu50_pattern_mode"
  global global mom_dmu50_start_anlge
  global mom_dmu50_add_angle
  global mom_dmu50_total
  MOM_output_literal ";--------------------"
  MOM_output_literal "FOR R0=0 TO TOTAL; START to CYCLE"
  MOM_output_literal "OFFSET_ANGL=START_ANGL + ADD_ANGL*R0"
  MOM_output_literal "ROT Z=OFFSET_ANGL"
  #MOM_output_literal ";--------------------"
}
#=============================================================
proc USER_PATTERN_END { } {
#=============================================================
   global mom_dmu50_pattern_mode
  if {![info exists mom_dmu50_pattern_mode]} {
 return
  }
  global global mom_dmu50_start_anlge
  global mom_dmu50_add_angle
  global mom_dmu50_total
 #MOM_output_literal ";--------------------" 
 MOM_output_literal "ENDFOR; END of CYCLE "
 MOM_output_literal ";--------------------"
}
# SPESIAL
#=============================================================
#=============================================================
proc USER_RUN_EXT_EDITOR {} {
#=============================================================
#Процедура для запуска внешнего редактора
#Добавить в процедуру ...end_of_program
  global mom_dmu50_start_external_editor
  global ptp_file_name
  global path_editor "C:\\Program Files (x86)\\Windows NT\\Accessories\\wordpad.exe"
  
  if {![info exists mom_dmu50_start_external_editor] || $mom_dmu50_start_external_editor==FALSE } {
  #MOM_display_message "Опция не активирована" "Editor Error:" Q OK
return 
 }
  set  path_editor "C:\\Program Files\\Notepad++\\notepad++.exe"
  #set  path_editor "C:\\Program Files (x86)\\Windows NT\\Accessories\\wordpad.exe"
   
  MOM_close_output_file $ptp_file_name
    
  catch {exec ${path_editor} $ptp_file_name} resp
  if { $resp != "" } {
   set resp [MOM_display_message "$resp" "Editor Error:" Q OK]
  }
}
#=============================================================
proc USER_tool_description {} {
#=============================================================
# Формирует строку описания инструмента
# Примерный формат: FREZA_10 D10. L30.0 LF6.0 Z3.
# Сохраняет строку в переменной user_tool_description
# Вставьте процедуру USER_tool_description и
# объявите переменную:user_tool_description перед выводом информации
# Используйте переменную $user_tool_description для вывода строки с параметрами инструмента


  global mom_path_name mom_tool_number mom_tool_name
  global mom_tool_type mom_tool_diameter mom_tool_length
  global mom_tool_flutes_number mom_tool_flute_length
  global mom_tool_extension_length
  global user_tool_description
  
  set user_tool_description ""
  
  set user_tool_extension ""
  if {[info exists mom_tool_extension_length]} {
	set user_tool_extension [format "%0.1f" $mom_tool_extension_length]
	set user_tool_extension "L${user_tool_extension}"
  }
  
  set user_tool_flutes_number ""
  if {[info exists mom_tool_flutes_number]} {
    set user_tool_flutes_number [format "%i" $mom_tool_flutes_number] 
	set user_tool_flutes_number "Z$user_tool_flutes_number"
  }
  
  set user_tool_length ""
  if {[info exists mom_tool_flute_length]} {
    set user_tool_length [format "%0.1f" $mom_tool_flute_length]
	set user_tool_length "FL${user_tool_length}"
  }
  
  set user_tool_diameter ""
  if {[info exists mom_tool_diameter]} {
	set user_tool_diameter [format "%0.1f" $mom_tool_diameter]
	set user_tool_diameter "D${user_tool_diameter}"
  }
  
  if {[info exists mom_tool_type]} {
	switch $mom_tool_type {
	 "Milling" 	{set user_tool_type FREZA}
	 "Turning" 		{set user_tool_type REZEC}
	 "Grooving"		{set user_tool_type GRAVER}
	 "Drilling"		{set user_tool_type SVERLO}
	 "Tap"	{set user_tool_type METCHIK}
	 default {
	   set user_tool_type "$mom_tool_type"
	   if {[string match "Milling*" $user_tool_type]} {
	     set user_tool_type FREZA
	   }
	 }
	} 
  }
	
 set user_tool_description "$user_tool_description\
							$user_tool_type\
							$user_tool_diameter\
							$user_tool_extension\
							$user_tool_length\
							$user_tool_flutes_number"
  
}
#=============================================================
proc USER_SET_MODE {} {
 global mom_siemens_5axis_mode
 global mom_siemens_5axis_output
 global mom_operation_type; #Variable-axis Surface Contouring
 
 #MOM_output_to_listing_device ";>>>> mom_operation_type:$mom_operation_type"
 if {[info exists mom_siemens_5axis_mode] && [string match "TRAORI*" $mom_siemens_5axis_mode]} {
  
    if {[PB_CMD_detect_5axis_tool_path]} {
	  set mom_siemens_5axis_output "TRAORI"
	}
  } else {
    set mom_siemens_5axis_output "TRAFOOF"
  }
}
#=============================================================
proc MOM_output_debug {{value ""}} {
#=============================================================
  global user_debug_foo
	if {[info exists user_debug_foo] && $user_debug_foo=="TRUE"} {
	  MOM_output_literal ";Foo $value"
	}
}
#=============================================================
proc MOM_debug_start { {value ON} } {
	global user_debug_foo
	if { $value==ON } {
	  set user_debug_foo TRUE;# TRUE/FALSE
	} else  {
	  set user_debug_foo FALSE
	}
}
#=============================================================
#TEMP
#=============================================================
#=============================================================
proc !USER_SET_CYCLE800 {} {
#=============================================================
  global coord_ang_A coord_ang_B coord_ang_C
  global mom_rotary_direction_4th
  global cycle800_inc_retract
  global cycle800_tc cycle800_dir
  global cycle800_st cycle800_mode
  global coord_ang_1 coord_ang_2 coord_ang_3
  global mom_siemens_cycle_dmode
  
#Установка параметров цикла вращения
   set cycle800_tc "\"R_DATA\"" ;# For example,please put your data here
 #-----------------------------------------------------------
 #Please set your incremental retraction
 #-----------------------------------------------------------
   set cycle800_inc_retract "1"
   set mom_siemens_cycle_dmode 0
   set cycle800_dir $mom_rotary_direction_4th
   set cycle800_st 0
   set cycle800_mode 57
   set coord_ang_1 $coord_ang_A
   set coord_ang_2 $coord_ang_B
   set coord_ang_3 $coord_ang_C
}
#=============================================================
proc !USER_PRE_TRAORI {} {
#=============================================================
# Вычисляет координаты точки в переменной mom_pos после поворота СК
# Требуется наличие процедуры VECTOR_ROTATE
# 
# Используются данные:
# mom_pos mom_mcs_goto mom_kin_4th_axis_vector
  global mom_pos mom_mcs_goto
  global coord_ang_A coord_ang_B coord_ang_C
  #global mom_tool_axis mom_nxt_tool_axis
  global RAD2DEG DEG2RAD
  global mom_kin_4th_axis_vector mom_kin_5th_axis_vector mom_out_angle_pos
  #global mom_rotary_direction_4th
   
  if {![info exists mom_out_angle_pos]} {
    MOM_output_to_listing_device ">>>> ERROR! Not variable mom_out_angle_pos"
	MOM_abort ""
  }
  if {![CMD_EXIST VECTOR_ROTATE]} {
    MOM_output_to_listing_device ">>>> ERROR! Not function VECTOR_ROTATE"
	MOM_abort ""
  }  
 
  #Инициализация векторов vpt X Y Z
  set x $mom_mcs_goto(0); set y $mom_mcs_goto(1); set z $mom_mcs_goto(2);
  VEC3_init x y z vpt
  
  set x 1.0; set y 0.0; set z 0.0;
  VEC3_init x y z X
  set x 0.0; set y 1.0; set z 0.0;
  VEC3_init x y z Y
  set x 0.0; set y 0.0; set z 1.0;
  VEC3_init x y z Z
  
  #Поворот векторов
  set rot0 [expr $mom_out_angle_pos(0)*$DEG2RAD]
  set rot1 [expr $mom_out_angle_pos(1)*$DEG2RAD]
  
  VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] X v1
  VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] Y v2
  VECTOR_ROTATE mom_kin_4th_axis_vector [expr $rot0] Z v3

  VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v1 X
  VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v2 Y
  VECTOR_ROTATE mom_kin_5th_axis_vector [expr $rot1] v3 Z
  
  #Инициализация матрицы вращения r_matrix
  set res [MTX3_init_x_y_z X Y Z r_matrix]
    
  #Вычисление координаты точки
  MTX3_vec_multiply vpt r_matrix mom_pos
 
  # Вычисление углов Эйлера, направление вращения X->Y->Z  
  if { [info exists r_matrix] } {
    set m0 $r_matrix(0)
    set m1 $r_matrix(1)
    set m2 $r_matrix(2)
    set m3 $r_matrix(3)
    set m4 $r_matrix(4)
    set m5 $r_matrix(5)
    set m6 $r_matrix(6)
    set m7 $r_matrix(7)
    set m8 $r_matrix(8)

    set cos_b_sq [expr $m0*$m0 + $m3*$m3]

    if { [EQ_is_equal $cos_b_sq 0.0] } {

      set cos_b 0.0
      set cos_c 1.0
      set cos_a $m4
      set sin_c 0.0
      set sin_a [expr -1*$m5]

      if { $m6 < 0.0 } {
        set sin_b 1.0
      } else {
        set sin_b -1.0
      }

    } else {

      set cos_b [expr sqrt($cos_b_sq)]
      set sin_b [expr -$m6]

      set cos_a [expr $m8/$cos_b]
      set sin_a [expr $m7/$cos_b]

      set cos_c [expr $m0/$cos_b]
      set sin_c [expr $m3/$cos_b]

    }
    set coord_ang_A [expr -atan2($sin_a,$cos_a)*$RAD2DEG]
    set coord_ang_B [expr -atan2($sin_b,$cos_b)*$RAD2DEG]
    set coord_ang_C [expr -atan2($sin_c,$cos_c)*$RAD2DEG]
  }  
}

