0 BEGIN PGM TOOLCHANGE MM

0 ; read the tool number from system data
1 M5
2 FN 18: SYSREAD Q1000 = ID20 NR2
3 FN 18: SYSREAD Q1001 = ID20 NR1
4 ; If actual tool identical with preselected one skip AC block
5 FN 9: IF +Q1000 EQU +Q1001 GOTO LBL "NO_TOOLCHANGE"

0 ; Set the tool change position values in metric X,Y,Z)
0 Q1001=0.0
0 Q1002=0.0
0 Q1003=0.0
0 ; Position for the V axis to mount the tool)
0 ;Q1004=-381.0
0 ; Z Position over the tool changer)
0 ;Q1005=711.2


0 M1001 ; deactivate tool length correction
0 L Z+Q1003 FMAX M91
0 L X+Q1001 Y+Q1002 FMAX M91

0 ; deactivate offset correction -> work like G53
0 ; NOT NEEDED use M91 as G53 !!!
0 ;CYCL DEF 7.0 
0 ;CYCL DEF 7.1 X0.0
0 ;CYCL DEF 7.2 Y0.0
0 ;CYCL DEF 7.3 Z0.0


##LANGUAGE AC
  INT nToolID;
  nToolID = getVariable("Q1000");
  STRING strToolName;
  strToolName = nToolID;
  
  IF (nToolID > 0);
    generateTool (getToolNameByNumber(nToolID), "S");
  ENDIF;
  
  IF (exist(getCurrentTool("S")));
    collision  (OFF, getCurrentTool("S"));
    visibility (     getCurrentTool("S"), OFF, TRUE);
    release    (     getCurrentTool("S"));
  ENDIF;

  IF (exist(getNextTool("S")));
    grasp      (     getNextTool("S"), getJunction("SPINDLE", "S"));
    position   (     getNextTool("S"), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    visibility (     getNextTool("S"), ON,  TRUE);
    collision  (ON,  getNextTool("S"), 2, -0.01);
    activateNextTool ("S");
  ENDIF;
  
##LANGUAGE NATIVE

10 FN 17: SYSWRITE ID20 NR1 = +Q1000
11 LBL "NO_TOOLCHANGE"
12 M67 ; activate tool length correction
13 END PGM TOOLCHANGE MM
