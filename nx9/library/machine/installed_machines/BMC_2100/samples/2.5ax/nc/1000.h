1 BEGIN PGM 1000 MM
2 ; PART NAME    : 2.5AX_TEST_SETUP_1 MM
3 ; COMPANY      : SIGNAL
4 ; MACHINE NAME : FORT BMC-2100
5 ; AUTHOR       : ADMINISTRATOR
6 ; DATE-TIME    : 25/02/19 14h.52min.
7 ;------------ TOOL LIST -------------
8 ;TOOL: SPOT_DRILL_D10 [DRILLING TOOL] T7
9 ;D=10 L=146.9 FL=0 PA=90
10 ;TOOL: DRILL_D8.8 [DRILLING TOOL] T8
11 ;D=8.8 L=120 FL=27 PA=118
12 ;TOOL: DRILL_D11 [DRILLING TOOL] T9
13 ;D=11 L=100 FL=80 PA=118
14 ;TOOL: DRILL_D7.5 [DRILLING TOOL] T10
15 ;D=7.5 L=80 FL=35 PA=118
16 ;TOOL: DRILL_D5 [DRILLING TOOL] T11
17 ;D=5 L=90 FL=25 PA=118
18 ;TOOL: TAP_M5 [DRILLING TOOL] T12
19 ;D=5 L=93.9 FL=10 PA=0
20 ;TOOL: EMS_D10_Z2_R1 [MILLING TOOL] T13
21 ;D=10 L=87 FL=22 R=1
22 ;TOOL: THREAD_MILL_M20X2.5 [DRILLING TOOL] T14
23 ;D=14.4 L=84.9 FL=10 PA=180
24 ;TOOL: REAMER_D12 [DRILLING TOOL] T16
25 ;D=12 L=111.9 FL=20 PA=0
26 ;TOOL: BORE_D12 [DRILLING TOOL] T15
27 ;D=9 L=78.65 FL=10 PA=180
28 ;-----------------------------------
29 BLK FORM 0.1 Z X-100. Y-100. Z-50.
30 BLK FORM 0.2 Z X100. Y100. Z0.
31 ; -----------------------------------------
32 ; START OF OPERATION <1> (SPOT_DRILLING)
33 ; -----------------------------------------
34 CYCL DEF 247 Q339=+1
35 * - OPERATION: SPOT_DRILLING
36 * - TOOL : SPOT_DRILL_D10
37 TOOL CALL 7 Z S477
38 M3 M8
39 L X-157.5 Y-32.5 A0.0 M3 FMAX
40 L Z+235. FMAX
41 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-2.5 ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+2.5 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
42 L X-157.5 Y-32.5 R0 FMAX M99
43 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-5. ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+5. ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+80. ;KOORD. POVERHNOSTI~
  Q204=+155. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
44 L X-115. Y-50. R0 FMAX M99
45 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-2. ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+2. ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
46 L X-105. Y-7.5 R0 FMAX M99
47 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-4. ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+4. ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+60. ;KOORD. POVERHNOSTI~
  Q204=+175. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
48 L X-60. Y-65. R0 FMAX M99
49 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-5. ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+5. ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+70. ;KOORD. POVERHNOSTI~
  Q204=+165. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
50 L X-85. Y-110. R0 FMAX M99
51 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-5. ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+5. ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+60. ;KOORD. POVERHNOSTI~
  Q204=+175. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
52 L X-35. Y-115. R0 FMAX M99
53 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-3. ;GLUBINA~
  Q206=57. ;PODACHA NA VREZANIE~
  Q202=+3. ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
54 L X-162.5 Y-112.5 R0 FMAX M99
55 L Z0. R0 FMAX M91
56 L Y0. R0 FMAX M91
57 M5 M9
58 ; -----------------------------------------
59 ; END OF OPERATION <1>
60 ; -----------------------------------------
61 ; -----------------------------------------
62 ; START OF OPERATION <2> (DRILLING)
63 ; -----------------------------------------
64 * - OPERATION: DRILLING
65 * - TOOL : DRILL_D8.8
66 TOOL CALL 8 Z S1266
67 M3
68 L X-162.5 Y-112.5 M3 FMAX
69 L Z+235. FMAX
70 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-27.704 ;GLUBINA~
  Q206=152. ;PODACHA NA VREZANIE~
  Q202=+27.704 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
71 L X-162.5 Y-112.5 R0 FMAX M99
72 L Z0. R0 FMAX M91
73 L Y0. R0 FMAX M91
74 M5 M9
75 ; -----------------------------------------
76 ; END OF OPERATION <2>
77 ; -----------------------------------------
78 ; -----------------------------------------
79 ; START OF OPERATION <3> (DRILLING_DEEP)
80 ; -----------------------------------------
81 * - OPERATION: DRILLING_DEEP
82 * - TOOL : DRILL_D11
83 TOOL CALL 9 Z S1013
84 M3
85 L X-115. Y-50. M3 FMAX
86 L Z+235. FMAX
87 CYCL DEF  203 ;UNIVERS. SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-70.408 ;GLUBINA~
  Q206=122. ;PODACHA NA VREZANIE~
  Q202=+17.602 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+80. ;KOORD. POVERHNOSTI~
  Q204=+155. ;2-YE BEZOP. RASSTOJ.~
  Q212=0 ;SJOM MATERIALA~
  Q213=0 ;KOL. OPER. LOMKI STRU.~
  Q205=0 ;MIN. GLUBINA VREZANJA~
  Q208=1000 ;PODACHA VYCHODA~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q211=.001 ;VYDER. VREMENI VNIZU
88 L X-115. Y-50. R0 FMAX M99
89 ; -----------------------------------------
90 ; END OF OPERATION <3>
91 ; -----------------------------------------
92 ; -----------------------------------------
93 ; START OF OPERATION <4> (DRILLING_DEEP_DWELL)
94 ; -----------------------------------------
95 L X-60. Y-65. M3 FMAX
96 CYCL DEF  203 ;UNIVERS. SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-65.805 ;GLUBINA~
  Q206=122. ;PODACHA NA VREZANIE~
  Q202=+16.451 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+60. ;KOORD. POVERHNOSTI~
  Q204=+175. ;2-YE BEZOP. RASSTOJ.~
  Q212=0 ;SJOM MATERIALA~
  Q213=0 ;KOL. OPER. LOMKI STRU.~
  Q205=0 ;MIN. GLUBINA VREZANJA~
  Q208=1000 ;PODACHA VYCHODA~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q211=2. ;VYDER. VREMENI VNIZU
97 L X-60. Y-65. R0 FMAX M99
98 CYCL DEF  203 ;UNIVERS. SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-75.805 ;GLUBINA~
  Q206=122. ;PODACHA NA VREZANIE~
  Q202=+18.951 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+70. ;KOORD. POVERHNOSTI~
  Q204=+165. ;2-YE BEZOP. RASSTOJ.~
  Q212=0 ;SJOM MATERIALA~
  Q213=0 ;KOL. OPER. LOMKI STRU.~
  Q205=0 ;MIN. GLUBINA VREZANJA~
  Q208=1000 ;PODACHA VYCHODA~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q211=2. ;VYDER. VREMENI VNIZU
99 L X-85. Y-110. R0 FMAX M99
100 CYCL DEF  203 ;UNIVERS. SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-65.805 ;GLUBINA~
  Q206=122. ;PODACHA NA VREZANIE~
  Q202=+16.451 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+60. ;KOORD. POVERHNOSTI~
  Q204=+175. ;2-YE BEZOP. RASSTOJ.~
  Q212=0 ;SJOM MATERIALA~
  Q213=0 ;KOL. OPER. LOMKI STRU.~
  Q205=0 ;MIN. GLUBINA VREZANJA~
  Q208=1000 ;PODACHA VYCHODA~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q211=2. ;VYDER. VREMENI VNIZU
101 L X-35. Y-115. R0 FMAX M99
102 L Z0. R0 FMAX M91
103 L Y0. R0 FMAX M91
104 M5 M9
105 ; -----------------------------------------
106 ; END OF OPERATION <4>
107 ; -----------------------------------------
108 ; -----------------------------------------
109 ; START OF OPERATION <5> (DRILLING_DEEP_BREAKCHIP)
110 ; -----------------------------------------
111 * - OPERATION: DRILLING_DEEP_BREAKCHIP
112 * - TOOL : DRILL_D7.5
113 TOOL CALL 10 Z S1485
114 M3
115 L X-157.5 Y-32.5 M3 FMAX
116 L Z+235. FMAX
117 CYCL DEF  205 ;UNIV. GL. SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-59.753 ;GLUBINA~
  Q206=89. ;PODACHA NA VREZANIE~
  Q202=+9.959 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q212=0 ;SJOM MATERIALA~
  Q205=0 ;MIN. GLUBINA VREZANJA~
  Q258=1 ;RASST. BEZ. VVERCHU~
  Q259=1 ;RASST. BEZ. VNIZU~
  Q257=9.959 ;GL. SVERL. PRI LOMANII~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q379=0 ;TOCHKA STARTA~
  Q253=500 ;PODACHA PRED. POZIC.~
  Q208=1000 ;PODACHA VYCHODA~
  Q211=.001 ;VYDER. VREMENI VNIZU
118 L X-157.5 Y-32.5 R0 FMAX M99
119 L Z0. R0 FMAX M91
120 L Y0. R0 FMAX M91
121 M5 M9
122 ; -----------------------------------------
123 ; END OF OPERATION <5>
124 ; -----------------------------------------
125 ; -----------------------------------------
126 ; START OF OPERATION <6> (DRILLING_DEEP_BREAKCHIP_DWELL)
127 ; -----------------------------------------
128 * - OPERATION: DRILLING_DEEP_BREAKCHIP_DWELL
129 * - TOOL : DRILL_D5
130 TOOL CALL 11 Z S2228
131 M3
132 L X-105. Y-7.5 M3 FMAX
133 L Z+235. FMAX
134 CYCL DEF  205 ;UNIV. GL. SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-59.002 ;GLUBINA~
  Q206=134. ;PODACHA NA VREZANIE~
  Q202=+9.834 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q212=0 ;SJOM MATERIALA~
  Q205=0 ;MIN. GLUBINA VREZANJA~
  Q258=1 ;RASST. BEZ. VVERCHU~
  Q259=1 ;RASST. BEZ. VNIZU~
  Q257=9.834 ;GL. SVERL. PRI LOMANII~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q379=0 ;TOCHKA STARTA~
  Q253=500 ;PODACHA PRED. POZIC.~
  Q208=1000 ;PODACHA VYCHODA~
  Q211=2. ;VYDER. VREMENI VNIZU
135 L X-105. Y-7.5 R0 FMAX M99
136 L Z0. R0 FMAX M91
137 L Y0. R0 FMAX M91
138 M5 M9
139 ; -----------------------------------------
140 ; END OF OPERATION <6>
141 ; -----------------------------------------
142 ; -----------------------------------------
143 ; START OF OPERATION <7> (TAPPING_M5X0.75_TAP)
144 ; -----------------------------------------
145 * - OPERATION: TAPPING_M5X0.75_TAP
146 * - TOOL : TAP_M5
147 TOOL CALL 12 Z S955
148 M3
149 L X-105. Y-7.5 M3 FMAX
150 L Z+235. FMAX
151 CYCL DEF  207 ;REZBONAREZANIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-15.75 ;GLUBINA~
  Q239=0.750 ;SCHAG REZBY~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.
152 L X-105. Y-7.5 R0 FMAX M99
153 ; -----------------------------------------
154 ; END OF OPERATION <7>
155 ; -----------------------------------------
156 ; -----------------------------------------
157 ; START OF OPERATION <8> (TAPPING_M5X0.75_TAP_FLOAT)
158 ; -----------------------------------------
159 CYCL DEF  206 ;REZBONAREZANIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-58.25 ;GLUBINA~
  Q206=716. ;PODACHA NA VREZANIE~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
160 L X-105. Y-7.5 R0 FMAX M99
161 ; -----------------------------------------
162 ; END OF OPERATION <8>
163 ; -----------------------------------------
164 ; -----------------------------------------
165 ; START OF OPERATION <9> (TAPPING_M5X0.75_TAP_BREAKCHIP)
166 ; -----------------------------------------
167 CYCL DEF  209 ;REZBONAREZANIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-58.25 ;GLUBINA~
  Q239=0.750 ;SCHAG REZBY~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q257=4.854 ;GL. SVERL. PRI LOMANII~
  Q256=0.2 ;VYCHOD PRI LOMANII~
  Q336=0 ;UGOL SHPINDEL~
  Q403=1.5 ;RPM FACTOR~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.
168 L X-105. Y-7.5 R0 FMAX M99
169 ; -----------------------------------------
170 ; END OF OPERATION <9>
171 ; -----------------------------------------
172 ; -----------------------------------------
173 ; START OF OPERATION <10> (THREADMILL_ID_M5X0.75)
174 ; -----------------------------------------
175 CYCL DEF  262 ;REZBOFREZEROVANIE~
  Q335=10.000 ;NOMINALNYJ DIAMETR~
  Q239=1.250 ;SCHAG REZBY~
  Q201=-58.25 ;GLUBINA~
  Q355=1 ;DOBOVLENJE~
  Q253=500 ;PODACHA PRED. POZIC.~
  Q351=1 ;TIP FREZEROVANIA~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q203=+55. ;KOORD. POVERHNOSTI~
  Q204=+180. ;2-YE BEZOP. RASSTOJ.~
  Q207=700.000 ;PODACHA FREZER~
  Q512=500.000 ;PODACHA PODVODA
176 L X-105. Y-7.5 R0 FMAX M99
177 ; -----------------------------------------
178 ; END OF OPERATION <10>
179 ; -----------------------------------------
180 ; -----------------------------------------
181 ; START OF OPERATION <11> (THREADMILL_OD_M40X2.0)
182 ; -----------------------------------------
183 L X-85. Y-110. M3 FMAX
184 CYCL DEF  267 ;REZBOFREZEROVANIE~
  Q335=0.000 ;NOMINALNYJ DIAMETR~
  Q239=0.000 ;SCHAG REZBY~
  Q201=-15.75 ;GLUBINA~
  Q355=1 ;DOBOVLENJE~
  Q253=500 ;PODACHA PRED. POZIC.~
  Q351=1 ;TIP FREZEROVANIA~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q358=0.000 ;SMESHENIE INSTR. FASKI~
  Q359=0.000 ;SDVIG INSTR GLUB.~
  Q203=+70. ;KOORD. POVERHNOSTI~
  Q204=+165. ;2-YE BEZOP. RASSTOJ.~
  Q254=150.000 ;PODACHA ZENKER..~
  Q207=500.000 ;PODACHA FREZER~
  Q512=50.000 ;PODACHA PODVODA
185 L X-85. Y-110. R0 FMAX M99
186 L Z0. R0 FMAX M91
187 L Y0. R0 FMAX M91
188 M5 M9
189 ; -----------------------------------------
190 ; END OF OPERATION <11>
191 ; -----------------------------------------
192 ; -----------------------------------------
193 ; START OF OPERATION <12> (DRILLING_MULTIPLE)
194 ; -----------------------------------------
195 * - OPERATION: DRILLING_MULTIPLE
196 * - TOOL : DRILL_D8.8
197 TOOL CALL 8 Z S1266
198 M3
199 L X-181.657 Y-8. M3 FMAX
200 L Z+235. FMAX
201 CYCL DEF  200 ;SVERLENIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-17.644 ;GLUBINA~
  Q206=152. ;PODACHA NA VREZANIE~
  Q202=+17.644 ;GLUBINA VREZANJA~
  Q210=0.0 ;VYDER. VREMENI NA VERHU~
  Q203=+70. ;KOORD. POVERHNOSTI~
  Q204=+165. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
202 L X-181.657 Y-8. R0 FMAX M99
203 L Y-73. R0 FMAX M99
204 L Y-138. R0 FMAX M99
205 L X-163.657 R0 FMAX M99
206 L X-145.657 R0 FMAX M99
207 L Z0. R0 FMAX M91
208 L Y0. R0 FMAX M91
209 M5 M9
210 ; -----------------------------------------
211 ; END OF OPERATION <12>
212 ; -----------------------------------------
213 ; -----------------------------------------
214 ; START OF OPERATION <13> (HOLE_MILLING)
215 ; -----------------------------------------
216 * - OPERATION: HOLE_MILLING
217 * - TOOL : EMS_D10_Z2_R1
218 TOOL CALL 13 Z S3183
219 M3
220 L X-115. Y-50. M3 FMAX
221 L Z+235. FMAX
222 L Z+80.5 FMAX
223 CC X-114.5 Y-50.
224 C X-114. Y-50. DR+ F318.
225 CC X-115. Y-50.
226 CP IPA22452.995 IZ-20.5 DR+
227 CC X-114.858 Y-50.152
228 C X-115.926 Y-50.716 DR+
229 CC X-114.814 Y-50.129
230 C X-114.576 Y-51.364 DR+
231 CC X-114.859 Y-49.895
232 C X-113.363 Y-49.926 DR+
233 CC X-115.137 Y-49.889
234 C X-114.728 Y-48.163 DR+
235 CC X-115.172 Y-50.038
236 C X-116.908 Y-49.201 DR+
237 CC X-114.922 Y-50.158
238 C X-116.283 Y-51.892 DR+
239 CC X-114.828 Y-50.038
240 C X-113.335 Y-51.861 DR+
241 CC X-115.004 Y-49.824
242 C X-112.653 Y-48.637 DR+
243 CC X-115.14 Y-49.892
244 C X-115.821 Y-47.191 DR+
245 CC X-115.072 Y-50.161
246 C X-118.133 Y-50.242 DR+
247 CC X-114.92 Y-50.157
248 C X-115.542 Y-53.311 DR+
249 CC X-114.866 Y-49.885
250 C X-111.672 Y-51.297 DR+
251 CC X-115.079 Y-49.791
252 C X-112.949 Y-46.736 DR+
253 CC X-115.129 Y-49.862
254 C X-117.728 Y-47.074 DR+
255 CC X-115. Y-50.
256 C X-117.728 Y-47.074 DR+
257 CC X-116.364 Y-48.537
258 C X-115. Y-50. DR+
259 L Z+235. FMAX
260 L X-85. Y-110. FMAX
261 L Z+70.5 FMAX
262 CC X-84.5 Y-110.
263 C X-84. Y-110. DR+
264 CC X-85. Y-110.
265 CP IPA22452.995 IZ-20.5 DR+
266 CC X-84.858 Y-110.152
267 C X-85.926 Y-110.716 DR+
268 CC X-84.814 Y-110.129
269 C X-84.576 Y-111.364 DR+
270 CC X-84.859 Y-109.895
271 C X-83.363 Y-109.926 DR+
272 CC X-85.137 Y-109.889
273 C X-84.728 Y-108.163 DR+
274 CC X-85.172 Y-110.038
275 C X-86.908 Y-109.201 DR+
276 CC X-84.922 Y-110.158
277 C X-86.283 Y-111.892 DR+
278 CC X-84.828 Y-110.038
279 C X-83.335 Y-111.861 DR+
280 CC X-85.004 Y-109.824
281 C X-82.653 Y-108.637 DR+
282 CC X-85.14 Y-109.892
283 C X-85.821 Y-107.191 DR+
284 CC X-85.072 Y-110.161
285 C X-88.133 Y-110.242 DR+
286 CC X-84.92 Y-110.157
287 C X-85.542 Y-113.311 DR+
288 CC X-84.866 Y-109.885
289 C X-81.672 Y-111.297 DR+
290 CC X-85.079 Y-109.791
291 C X-82.949 Y-106.736 DR+
292 CC X-85.129 Y-109.862
293 C X-87.728 Y-107.074 DR+
294 CC X-85. Y-110.
295 C X-87.728 Y-107.074 DR+
296 CC X-86.364 Y-108.537
297 C X-85. Y-110. DR+
298 L Z+235. FMAX
299 L X-35. Y-115. FMAX
300 L Z+60.5 FMAX
301 CC X-34.5 Y-115.
302 C X-34. Y-115. DR+
303 CC X-35. Y-115.
304 CP IPA22452.995 IZ-20.5 DR+
305 CC X-34.858 Y-115.152
306 C X-35.926 Y-115.716 DR+
307 CC X-34.814 Y-115.129
308 C X-34.576 Y-116.364 DR+
309 CC X-34.859 Y-114.895
310 C X-33.363 Y-114.926 DR+
311 CC X-35.137 Y-114.889
312 C X-34.728 Y-113.163 DR+
313 CC X-35.172 Y-115.038
314 C X-36.908 Y-114.201 DR+
315 CC X-34.922 Y-115.158
316 C X-36.283 Y-116.892 DR+
317 CC X-34.828 Y-115.038
318 C X-33.335 Y-116.861 DR+
319 CC X-35.004 Y-114.824
320 C X-32.653 Y-113.637 DR+
321 CC X-35.14 Y-114.892
322 C X-35.821 Y-112.191 DR+
323 CC X-35.072 Y-115.161
324 C X-38.133 Y-115.242 DR+
325 CC X-34.92 Y-115.157
326 C X-35.542 Y-118.311 DR+
327 CC X-34.866 Y-114.885
328 C X-31.672 Y-116.297 DR+
329 CC X-35.079 Y-114.791
330 C X-32.949 Y-111.736 DR+
331 CC X-35.129 Y-114.862
332 C X-37.728 Y-112.074 DR+
333 CC X-35. Y-115.
334 C X-37.728 Y-112.074 DR+
335 CC X-36.364 Y-113.537
336 C X-35. Y-115. DR+
337 L Z+235. FMAX
338 L Z0. R0 FMAX M91
339 L Y0. R0 FMAX M91
340 M5 M9
341 ; -----------------------------------------
342 ; END OF OPERATION <13>
343 ; -----------------------------------------
344 ; -----------------------------------------
345 ; START OF OPERATION <14> (THREAD_MILLING)
346 ; -----------------------------------------
347 * - OPERATION: THREAD_MILLING
348 * - TOOL : THREAD_MILL_M20X2.5
349 TOOL CALL 14 Z S2210
350 M3
351 L X-115. Y-50. M3 FMAX
352 L Z+235. FMAX
353 L Z+64.706 FMAX
354 L X-113.686 Y-50.823 F707.
355 CC X-113.171 Y-50.
356 CP IPA122.065 IZ.294 DR+
357 CC X-115. Y-50.
358 CP IPA1224. IZ8.5 DR+
359 CC X-116.48 Y-48.925
360 CP IPA122.065 IZ.294 DR+
361 L X-115. Y-50. Z+73.794
362 L Z+235. FMAX
363 ; -----------------------------------------
364 ; END OF OPERATION <14>
365 ; -----------------------------------------
366 ; -----------------------------------------
367 ; START OF OPERATION <15> (BOSS_THREAD_MILLING)
368 ; -----------------------------------------
369 L X-134.587 Y-69.587 M3 FMAX
370 L Z+72.819 FMAX
371 CC X-109.708 Y-50.
372 CP IPA-38.214 IZ-.319 DR- F265.
373 CC X-115. Y-50.
374 CP IPA-1080. IZ-7.5 DR-
375 CC X-109.708 Y-50.
376 CP IPA-38.214 IZ-.319 DR-
377 L Z+235. FMAX
378 L Z0. R0 FMAX M91
379 L Y0. R0 FMAX M91
380 M5 M9
381 ; -----------------------------------------
382 ; END OF OPERATION <15>
383 ; -----------------------------------------
384 ; -----------------------------------------
385 ; START OF OPERATION <16> (DRILLING_REAM)
386 ; -----------------------------------------
387 * - OPERATION: DRILLING_REAM
388 * - TOOL : REAMER_D12
389 TOOL CALL 16 Z S2122
390 M3
391 L X-60. Y-65. M3 FMAX
392 L Z+235. FMAX
393 CYCL DEF  201 ;RAZVIORTIVANIE~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-16. ;GLUBINA~
  Q206=212. ;PODACHA NA VREZANIE~
  Q208=1000 ;PODACHA VYCHODA~
  Q203=+60. ;KOORD. POVERHNOSTI~
  Q204=+175. ;2-YE BEZOP. RASSTOJ.~
  Q211=.001 ;VYDER. VREMENI VNIZU
394 L X-60. Y-65. R0 FMAX M99
395 L Z0. R0 FMAX M91
396 L Y0. R0 FMAX M91
397 M5 M9
398 ; -----------------------------------------
399 ; END OF OPERATION <16>
400 ; -----------------------------------------
401 ; -----------------------------------------
402 ; START OF OPERATION <17> (DRILLING_BORE)
403 ; -----------------------------------------
404 * - OPERATION: DRILLING_BORE
405 * - TOOL : BORE_D12
406 TOOL CALL 15 Z S2122
407 M3
408 L X-145.657 Y-138. M3 FMAX
409 L Z+235. FMAX
410 CYCL DEF  202 ;RASTOCHKA~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-10. ;GLUBINA~
  Q206=212. ;PODACHA NA VREZANIE~
  Q208=1000 ;PODACHA VYCHODA~
  Q203=+70. ;KOORD. POVERHNOSTI~
  Q214=0 ;NAPR.VYHODA IZ MAT.~
  Q336=0 ;UGOL SHPINDEL~
  Q211=.001 ;VYDER. VREMENI VNIZU
411 L X-145.657 Y-138. R0 FMAX M99
412 L X-163.657 R0 FMAX M99
413 CYCL DEF  202 ;RASTOCHKA~
  Q200=+2.5 ;BEZOPASN. RASSTOYANIE~
  Q201=-10. ;GLUBINA~
  Q206=212. ;PODACHA NA VREZANIE~
  Q208=1000 ;PODACHA VYCHODA~
  Q203=+70. ;KOORD. POVERHNOSTI~
  Q204=+165. ;2-YE BEZOP. RASSTOJ.~
  Q214=0 ;NAPR.VYHODA IZ MAT.~
  Q336=0 ;UGOL SHPINDEL~
  Q211=.001 ;VYDER. VREMENI VNIZU
414 L X-181.657 Y-138. R0 FMAX M99
415 L Z0. R0 FMAX M91
416 L Y0. R0 FMAX M91
417 M5 M9
418 ; -----------------------------------------
419 ; END OF OPERATION <17>
420 ; -----------------------------------------
421 END PGM 1000 MM
