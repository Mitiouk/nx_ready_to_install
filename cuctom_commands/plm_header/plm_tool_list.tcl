# =====================================================
# output custom tool list
# 26-12-2018 Mitiouk
# 04-01-2020 Mitiouk
# 15-01-2020 Mitiouk changed variable mom_isv_tool_cutcom_register in Drill tool to mom_isv_tool_adjust_register
proc USER_out_tool_list {} {
# =====================================================
	global mom_isv_tool_count mom_tool_use 
	global mom_isv_tool_name mom_isv_tool_description mom_isv_tool_number 
	global mom_isv_tool_diameter mom_isv_tool_flute_length mom_isv_tool_type
	global mom_isv_tool_corner1_radius mom_isv_tool_adjust_register mom_isv_tool_cutcom_register
	global mom_isv_tool_x_correction mom_isv_tool_type mom_isv_tool_point_angle
	global mom_operation_name_list
    global user_tool_time_array user_tool_cutting_time_array user_tool_no_cutting_time_array  
    global user_tool_path_length_array user_tool_path_cutting_length_array
    global user_tool_counter
    global RAD2DEG PI
    
    #длина строки 40 символов
    #set msg_start_tool_list " -------- СПИСОК ИНСТРУМЕНТОВ ---------"
    set msg_start_tool_list " -------- TOOL LIST -------------------"
    #set msg_end_tool_list   " -------- КОНЕЦ СПИСКА ИНСТРУМЕНТОВ ---"
    set msg_end_tool_list   " -------- END TOOL LIST ---------------"
    
    set user_tool_counter 0
    MOM_output_text "; $msg_start_tool_list"
                        
	for { set i 0 } { $i < $mom_isv_tool_count } { incr i } {
        incr user_tool_counter
        if {[CMD_EXIST TRANSLIT]} {
            set mom_isv_tool_description($i) [TRANSLIT $mom_isv_tool_description($i)]
        }
        if { ![info exists mom_isv_tool_name] } {
            set mom_isv_tool_name($i) "UNUSED"
        }
        if {[info exist mom_isv_tool_type($i)]} {
            
            switch -glob $mom_isv_tool_type($i) {
                "Milling*"   {set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                              "$user_tool_counter\."\
                              "$mom_isv_tool_number($i)"\
                              "$mom_isv_tool_name($i)"\
                              "$mom_isv_tool_cutcom_register($i)"\
                              "$mom_isv_tool_description($i)"\
                              "D=$mom_isv_tool_diameter($i)"\
                              "L=$mom_isv_tool_x_correction($i,0)"\
                              "FL=$mom_isv_tool_flute_length($i)"\
                              "R=$mom_isv_tool_corner1_radius($i)"]
                              MOM_output_text $output
                            }
                "Drilling"  {set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                              "$user_tool_counter\."\
                              "$mom_isv_tool_number($i)"\
                              "$mom_isv_tool_name($i)"\
                              "$mom_isv_tool_adjust_register($i)"\
                              "$mom_isv_tool_description($i)"\
                              "D=$mom_isv_tool_diameter($i)"\
                              "L=$mom_isv_tool_x_correction($i,0)"\
                              "FL=$mom_isv_tool_flute_length($i)"\
                              ""]
                              MOM_output_text $output
                            }
                "Turning*"   {
                                global mom_tool_insert_type
                                global mom_tool_button_diameter
                                global mom_tool_nose_radius
                                global mom_tool_nose_radius
                                global mom_tool_orientation
                                global mom_isv_tool_nose_radius
                                global mom_isv_tool_nose_angle
                                
                                if { ![info exists mom_tool_orientation] } {
                                    set mom_tool_orientation 0
                                    }
                                
                                
                                  if { [info exists mom_tool_insert_type] && $mom_tool_insert_type == 12 } {
                                        set parametr1 " D=[string trimright [format %5.3f $mom_tool_button_diameter] "0"]" ;#BD диаметр
                                    } else {
                                        set parametr1 " r=[string trimright [format %5.3f $mom_tool_nose_radius] "0"]" ;#R радиус при вершине пластины
                                        set parametr2 " Epsi=[string trimright [format %5.3f [expr $mom_tool_nose_radius*$RAD2DEG]] "0"]" ;# угол при вершине пластины
                                        set parametr3 " fi1=[string trimright [format %5.3f [expr  $mom_tool_orientation*$RAD2DEG]] "0"]" ;#OA угол ориентации
                                    }
                                    if { [info exists mom_isv_tool_nose_radius($i)] } {
                                        set _nr $mom_isv_tool_nose_radius($i)
                                    } {
                                        set _nr "n//a"
                                    }
                                    if { [info exists mom_isv_tool_nose_angle($i)] } {
                                        set _na $mom_isv_tool_nose_angle($i)
                                    } {
                                        set _na "n//a"
                                    }
                                set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                "R=$_nr"\
                                "NA=$_na"\
                                "$parametr2"\
                                "$parametr3"]
                                MOM_output_text $output
                }
                "Grooving*" {       
                                    global mom_tool_insert_type
                                    global mom_tool_radius
                                    global mom_tool_side_angle
                                    global mom_tool_tip_angle
                                    global mom_tool_insert_length
                                    global mom_tool_insert_width
                                    global mom_tool_left_angle mom_tool_right_angle
                                    global mom_tool_left_corner_radius
                                    global mom_tool_right_corner_radius
                                    global mom_isv_tool_nose_radius
                                    
                                    if {![info exists mom_tool_insert_type]} { set mom_tool_insert_type 0 }
                                    if {$mom_tool_insert_type==0} {
                                    set _r " r=[string trimright [format %5.3f $mom_tool_radius] "0"]" ;#R радиус при вершине пластины
                                    set _sa " fi1=[string trimright [format %5.3f [expr $mom_tool_side_angle*$RAD2DEG]] "0"]" ;#SA боковой угол
                                    set _ta " fi=[string trimright [format %5.3f [expr (($PI/2)-$mom_tool_tip_angle)*$RAD2DEG]] "0"]" ;#TA угол при вершине
                                    set _il " Lpl=[string trimright [format %5.3f $mom_tool_insert_length] "0"]" ;#IL длинна пластины
                                    set _iw " Spl=[string trimright [format %5.3f $mom_tool_insert_width] "0"]" ;#IW ширина пластины
                                }
                                if {$mom_tool_insert_type==1} {
                                    set _r  "r=polnii_rad " ;#R радиус при вершине пластины
                                    set _sa " fi1=[string trimright [format %5.3f [expr $mom_tool_side_angle*$RAD2DEG]] "0"]" ;#SA боковой угол
                                    set _il " Lpl=[string trimright [format %5.3f $mom_tool_insert_length] "0"]" ;#IL длинна пластины
                                    set _iw " Spl=[string trimright [format %5.3f $mom_tool_insert_width] "0"]" ;#IW ширина пластины
                                    set _ta "radius"
                                }
                                if {$mom_tool_insert_type==2} {
                                    set _la " Al=[string trimright [format %5.3f [expr $mom_tool_left_angle*$RAD2DEG]] "0"]" ;#LA левый угол
                                    set _lr " Ap=[string trimright [format %5.3f [expr $mom_tool_right_angle*$RAD2DEG]] "0"]" ;#LA правый угол
                                    set _il " Lpl=[string trimright [format %5.3f $mom_tool_insert_length] "0"]" ;#IL длинна пластины
                                    set _iw " Spl=[string trimright [format %5.3f $mom_tool_insert_width] "0"]" ;#IW ширина пластины
                                    set _ta "trap"
                                }
                                if {$mom_tool_insert_type==3} {
                                    set _r  " r=[string trimright [format %5.3f $mom_tool_radius] "0"]" ;#R радиус при вершине пластины
                                    set _lr " rl=[string trimright [format %5.3f $mom_tool_left_corner_radius] "0"]" ;#LR левый радиус
                                    set _rr " rp=[string trimright [format %5.3f $mom_tool_right_corner_radius] "0"]" ;#RR правый радиус
                                    set _la " Al=[string trimright [format %5.3f [expr $mom_tool_left_angle*$RAD2DEG]] "0"]" ;#LA левый угол
                                    set _ra " Ap=[string trimright [format %5.3f [expr $mom_tool_right_angle*$RAD2DEG]] "0"]" ;#LA правый угол
                                    set _ta " fi=[string trimright [format %5.3f [expr (($PI/2)-$mom_tool_tip_angle)*$RAD2DEG]] "0"]" ;#TA угол при вершине
                                    set _il " Lpl=[string trimright [format %5.3f $mom_tool_insert_length] "0"]" ;#IL длинна пластины
                                    set _iw " Spl=[string trimright [format %5.3f $mom_tool_insert_width] "0"]" ;#IW ширина пластины
                                    set _ta "user"
                                }
                              set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                "R=$mom_isv_tool_nose_radius($i)"\
                                "$_ta"\
                                "$_il"\
                                "$_iw"]
                              MOM_output_text $output
                             }
                "Threading *" {
                                set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                }
                "Thread *" { 
                                set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                }
                "Form *"    { 
                              set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                            }
                "Centerdrill"   {
                                    set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output}
                "Countersink"  {
                                set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                              }
                "Tap"          {
                                set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                              }
                "Probe"       {
                                 set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                              }
                
                 
                "Solid"     {
                                set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                               MOM_output_text $output
                              }
                default     {
                              set output [format ";%-3s |T%-3s |%-23s |D%-1s |%-22s |%-6s %-6s %-6s %-6s"\
                                "$user_tool_counter\."\
                                "$mom_isv_tool_number($i)"\
                                "$mom_isv_tool_name($i)"\
                                "$mom_isv_tool_cutcom_register($i)"\
                                "$mom_isv_tool_description($i)"\
                                ""\
                                ""\
                                ""\
                                ""]
                              MOM_output_text $output
                            }
            }
        } {      
            MOM_output_literal "; ХЗЧ"
        }
        #MOM_output_to_listing_device "mom_isv_tool_type($i):$mom_isv_tool_type($i)"
        #Время работы инструмента
        #if { [info exists $user_tool_time_array($mom_isv_tool_name($i))]} {
        #;
        #set value [format "%0.2f" $user_tool_time_array($mom_isv_tool_name($i))]
        #global msg_machine_time
        #set value [format "%0.1f" $user_tool_cutting_time_array($mom_isv_tool_name($i))]
        #MOM_output_literal ";Cutting time   :$value  minutes"
        #set value [format "%0.1f" $user_tool_path_length_array($mom_isv_tool_name($i))]
        #MOM_output_literal ";Path Length    : $value  millimeters"
        #set value [format "%0.1f" $user_tool_path_cutting_length_array($mom_isv_tool_name($i))]
        #MOM_output_literal ";Cutting Length : $value millimeters"
        #} 
	};# tool list
	MOM_output_text ";$msg_end_tool_list\n"	
}