#
# rev 1.1 01-02-2020 fix simbol ['] -> [`]
#
#=============================================================
proc TRANSLIT { arg } {
#=============================================================
# Funkciya transliteracii kirillicheskix simvolov
# GOST 7.79-2000
# Тест:
# puts [TRANSLIT "ПРОБА"]
#% PROBA
# puts [TRANSLIT "Хочешь ещё этих мягких булочек?"]
#% Xochesh' eshhyo etix myagkix bulochek?
# puts [TRANSLIT "Объедки"]
#% Ob''edki
# puts [TRANSLIT "Яндекс"]
#% YAndeks
# puts [TRANSLIT "Функция транслитерации кириллических символов"]
#% TransliteraCiya TransLiteraCIya
#=============================================================
    set _val $arg
    set r1 {
            ЦИ CI\
            Ци CI\
            ци Ci\
            ЦЕ CE\
            Це Cе\
            це се\
            ЦЭ CE\
            Це Ce\
            це ce\
            ЦЁ CYO\
            Цё Cyo\
            цё cyo\
            ЦЮ CYU\
            Цю Cyu\
            цю cyu\
            ЦЯ CYA\
            Ця Cya\
            ця cya\
            ЦЙ CJ\
            Цй Cj\
            цй cj\
            А A\
            а a\
            Б B\
            б b\
            В V\
            в v\
            Г G\
            г g\
            Д D\
            д d\
            Е E\
            е e\
            Ё YO\
            ё yo\
            Ж ZH\
            ж zh\
            З Z\
            з z\
            И I\
            и i\
            Й J\
            й j\
            К K\
            к k\
            Л L\
            л l\
            М M\
            м m\
            Н N\
            н n\
            О O\
            о o\
            П P\
            п p\
            Р R\
            р r\
            С S\
            с s\
            Т T\
            т t\
            У U\
            у u\
            Ф F\
            ф f\
            Х X\
            х x\
            Ц CZ\
            ц c\
            Ч CH\
            ч ch\
            Ш SH\
            ш sh\
            Щ SHH\
            щ shh\
            Ъ \`\`\
            ъ \`\`\
            Ь \`\
            ь \`\
            Ы Y`\
            ы y`\
            Э E\
            э e\
            Ю YU\
            ю yu\
            Я YA\
            я ya\
            }              
    foreach {_expr _inst} $r1 {
        regsub -all $_expr $_val $_inst _val
    }
    return $_val
}


#=============================================================
proc to_code {arg} {
#=============================================================

puts "CHAR:$arg = [scan $arg %c] \n"

}

#=============================================================
uplevel #0{
# this variable can control output procedure in transliteration mode
# set user_prohibition_translit 1;# in main setting

global user_prohibition_translit
    if {[info exists user_prohibition_translit] && $user_prohibition_translit == 0} {
        rename MOM_output_literal _MOM_output_literal
        proc MOM_output_literal args {
    
            if { [llength $args] > 1 } {      
                _MOM_output_literal [ TRANSLIT [lindex $args 0] ] [lindex $args 1]
            } {
                _MOM_output_literal [ TRANSLIT [lindex $args 0] ]
            }
        }
    
        rename MOM_output_text _MOM_output_text
        proc MOM_output_text args {
    
            if { [llength $args] > 1 } {      
                _MOM_output_text [ TRANSLIT [lindex $args 0] ] [lindex $args 1]
            } {
                _MOM_output_text [ TRANSLIT [lindex $args 0] ]
            }
        }
    }
};#uplevel
#=============================================================
