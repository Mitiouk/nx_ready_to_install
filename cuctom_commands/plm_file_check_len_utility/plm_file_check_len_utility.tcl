
#=============================================================
proc USER_check_filename_len { args } {
#=============================================================
#=============================================================
# Утилиты для работы с именами файлов
# <27-11-2019> Mitiouk
#=============================================================
# Syntax USER_check_filename_len FILENAME <LENGTH>
#   FILENAME if string of file name
#   LENGTH is limit of length filename string
# Return trimmed string of file name as fileneme~dd
# Default trim length 23 characters
#
# Example 1: USER_check_filename_len C:/test/test_test_test_test.nc 8
#   return: C:/test/test_t~1.nc
# Example 2: USER_check_filename_len test_test_test_test.nc 8
#   return: test_t~2.nc
# Example 3: USER_check_filename_len test_test_test_test.nc.nc.nc 8
#   return: test_t~3.nc
# Example 4: USER_check_filename_len test_test_test_test_test_test_test_test.nc
#   return: test_test_test_test_t~4.nc
# Error variable:
#   user_check_filename_len_error_name  - name of error
#   user_check_filename_len_error       -length original file name
#
#=============================================================
  global user_fn_count
  global user_check_filename_len_error_name user_check_filename_len_error
  catch {unset user_check_filename_len_error_name; unset user_check_filename_len_error}
  
  switch -- [llength $args] {
        1 {set limit 23}
        2 {set limit [lindex $args 1]}
        default { 
            set user_check_filename_len_error_name "Wrong set of arguments proc USER_check_filename_len" 
            return -1
            }
  }
  set fn [file tail [lindex $args 0]]
  set dn [file dirname [lindex $args 0]]
    
  if { ![info exists user_fn_count] } {set user_fn_count 0}
  
    if {[string match "*.*" $fn]} {
        set fe [file extension $fn]
        set fname [file rootname $fn]
        while {[string match "*.*" $fname] == 1} {
            set fname [file rootname $fname] 
        }
    } {set fname $fn}   
    if {[string length $fname] > $limit} {
        set user_check_filename_len_error_name "File name length exceeded"
        set user_check_filename_len_error [string length $fname] 
        incr user_fn_count
        set trim_len [expr $limit - [string length $user_fn_count] - 2]
        set fname [string range $fname 0 $trim_len]
        set fname "$fname~$user_fn_count"
            }
    if {[info exists fe]}  { set fname $fname$fe }
    if {$dn != "." && $dn!= "/"} { set fname $dn/$fname }
    regsub -all "/" $fname "\\" fname   
    return $fname  
}
USER_check_filename_len C:/test/test_test_test_test.nc 8
USER_check_filename_len test_test_test_test.nc 8
USER_check_filename_len test_test_test_test.nc.nc.nc 8
USER_check_filename_len test_test_test_test_test_test_test_test.nc