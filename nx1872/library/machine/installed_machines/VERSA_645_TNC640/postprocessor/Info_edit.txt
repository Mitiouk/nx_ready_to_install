﻿# <Mitiouk> 16-07-2019
Добавлен режим erowa_output_mode
Управляется UDE: WERSA_OUTPUT_MODE
устанавливает значение переменной erowa_output_mode
переменная используется в процедурах проверки:PB_CMD__check_output_erowa_mode и PB_CMD__check_output_default_mode
#<07-07-2019> Mitiouk
	V1.0.2
	M0 output disabled
#<04-08-2019> Mitiouk
	V1.0.3 
	PB_init_new_iks { } & USER_SET_KINEMATIC { }
	global mom_kin_iks_usage
	set mom_kin_iks_usage 0
#<12-08-2109> Mitiouk
	1.1.0 12-08-2019 DEF/EROWA

#<14-08-2109> Mitiouk
	1.1.1 14-08-2019 DEF/EROWA
	Fix proc USER_OUT_WORKPIECE
	1.1.2 14-08-2019 DEF/EROWA
	Fix UDE VERSA DOMASHNYAYA POSICIYA
#<19-08-2019>
    1.2.3 19-08-2019
1)  Remove string: 
	FN 0: ; X HOME POSITION
	FN 0: ; Y HOME POSITION
	FN 0: ; Z HOME POSITION
	
2)	Change operation header:
	Add proc USER_HEADER_OF_OPERATION
	* - OPERATION: FLOOR_WALL_IPW_COPY
	; D10 (T1) DIAM=10.0mm TOOL_LEN=37.0mm CUT_LEN=22.0mm FL_NUM=3
	; ------------------------------
	LBL 10

3)  Add output M140 MB FMAX after each operation 
	
4)	Change proc USER_OUT_WORKPIECE
	If UDE is missing then default output:
	BLK FORM 0.1 Z X-100. Y-100. Z-50.
    BLK FORM 0.2 X+100. Y+100. Z+0.
	
5)	Changed order of start program strings
	
#<19-08-2019>
    1.2.4 19-08-2019
	Change operation header:
	Add proc USER_HEADER_OF_OPERATION
	* - <10> FLOOR_WALL_IPW_COPY
	; D10 (T1) DIAM=10.0mm TOOL_LEN=37.0mm CUT_LEN=22.0mm FL_NUM=3
	; ------------------------------
	LBL 10
	
#<28-08-2019>
    1.2.5 28-08-2019
	Output TOOL CALL each operation in all mode (EROWA/DEFAULT)

#<03-09-2019>
    1.2.6 03-08-2019
	Remove block: TOOL CALL S33423
	in Event Rapid Move before output

#<19-09-2019>
    1.2.7 19-08-2019
    Remove output fifth_axis and fourth_axis after SPATIAL block
    
    

	