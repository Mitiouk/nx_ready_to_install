#=============================================================
proc USER_HEADER_OF_OPERATION {} {
#=============================================================
  global mom_path_name mom_tool_number mom_tool_name
  global mom_tool_type mom_tool_diameter mom_tool_length
  global mom_tool_flutes_number mom_tool_flute_length
  global mom_tool_extension_length
  global erowa_output_mode
  global user_Q1 mom_operation_name
	
	if { [info exists erowa_output_mode] && $erowa_output_mode == ON} {
		return
	}
	
 # Формат вывода:
  #* - <Operation_Number> FLOOR_WALL_IPW_COPY
  #; D10 (T1) D10.0 L37.0 FL22.0 Z3
 	
	if {![info exist user_Q1]} { set user_Q1 10} else {set user_Q1 [expr $user_Q1 +10 ]}
	  
  set user_tool_extension ""
  if {[info exists mom_tool_extension_length]} {
	set user_tool_extension "L${mom_tool_extension_length}"
  }
  
  set user_tool_flutes_number ""
  if {[info exists mom_tool_flutes_number]} {
	set user_tool_flutes_number "Z$mom_tool_flutes_number"
  }
  
  set user_tool_length ""
  if {[info exists mom_tool_flute_length]} {
	set user_tool_length "FL${mom_tool_flute_length}"
  }
  
  set user_tool_diameter ""
  if {[info exists mom_tool_diameter]} {
	set user_tool_diameter "D${mom_tool_diameter}"
  }
  
  if {[info exists mom_tool_type]} {
	switch $mom_tool_type {
	 "Milling" 		{set user_tool_type FREZA}
	 "Turning" 		{set user_tool_type FREZA}
	 "Grooving"		{set user_tool_type FREZA}
	 "Drilling"		{set user_tool_type FREZA}
	 "Threading"	{set user_tool_type FREZA}
	 default {set user_tool_type ""}
	}
  
  } 

  MOM_output_literal "; ------------------------------"
  MOM_output_literal "* - <$user_Q1> $mom_path_name"
  MOM_output_literal "; $mom_tool_name \(T$mom_tool_number\) $user_tool_diameter $user_tool_extension $user_tool_length $user_tool_flutes_number"
  MOM_output_literal "; ------------------------------"
  MOM_output_literal ";"
  MOM_output_literal "LBL $user_Q1"
  MOM_output_literal ";"
}

#=============================================================
proc USER_HEAD_OF_PROGRAM {} {
#



}

#=============================================================
proc USER_SET_KINEMATIC { } {
#=============================================================
uplevel #0 {
						#MOM_output_literal ";>>>Foo USER_SET_KINEMATIC"
						global mom_ude_a_axis_max mom_ude_a_axis_min
						
						global mom_kin_4th_axis_max_limit mom_kin_4th_axis_min_incr
						global mom_kin_4th_axis_min_limit mom_kin_4th_axis_plane
						#global mom_kin_5th_axis_leader mom_kin_5th_axis_limit_action
						#global mom_kin_4th_axis_leader mom_kin_4th_axis_limit_action
						global mom_kin_5th_axis_max_limit mom_kin_5th_axis_min_incr
						global mom_kin_5th_axis_min_limit mom_kin_5th_axis_plane
						DPP_GE_RESTORE_KINEMATICS
                        #set mom_kin_4th_axis_limit_action "Warning"
						#set mom_kin_5th_axis_limit_action "Warning"
                        #if {[info exists mom_ude_b_axis_max]} {
						#	set mom_kin_4th_axis_max_limit $mom_ude_b_axis_max
						#}
						
                        #set mom_kin_4th_axis_min_incr "0.0001"
						#if {[info exists mom_ude_b_axis_min]} {
						#	set mom_kin_4th_axis_min_limit $mom_ude_b_axis_min
						#}
						
                        #set mom_kin_5th_axis_max_limit "360"
                        #set mom_kin_5th_axis_min_incr "0.0001"
                        #set mom_kin_5th_axis_min_limit "-360"
						
						if {![info exists mom_ude_a_axis_min]} {
							set  mom_kin_4th_axis_min_limit -120.0	
						} else {
							set  mom_kin_4th_axis_min_limit $mom_ude_a_axis_min
						}
						if {![info exists mom_ude_a_axis_max]} {
							set  mom_kin_4th_axis_max_limit 120.0
						} else {
							set  mom_kin_4th_axis_max_limit $mom_ude_a_axis_max
						}
						
						global mom_kin_iks_usage
						set mom_kin_iks_usage 0


                        MOM_reload_kinematics
						MOM_update_kinematics
                        
                        #MOM_reload_variable -a mom_pos
                        #MOM_reload_variable -a mom_out_angle_pos
						
            } ;#uplevel

}

#=============================================================
proc USER_RESET_KINEMATIC { } {
#=============================================================
#<Mitiou> 05-04-2019
# Used in PB_CMD__check_block_reset_cycle800
#=============================================================
 global mom_tool_axis mom_out_angle mom_pos
 global mom_prev_pos mom_out_angle_pos mom_prev_out_angle_pos

  set mom_pos(3) 0.0
  set mom_pos(4) 0.0
  set mom_prev_pos(3) 0.0
  set mom_prev_pos(4) 0.0
  set mom_tool_axis(0) 0.0
  set mom_tool_axis(1) 0.0
  set mom_tool_axis(2) 1.0
  set mom_out_angle(0) 0
  set mom_out_angle(1) 0
  set mom_out_angle_pos(0) 0
  set mom_out_angle_pos(1) 0
  
  set mom_prev_out_angle_pos(0) 0
  set mom_prev_out_angle_pos(1) 0
 
 global mom_kin_iks_usage
 set mom_kin_iks_usage 0
  
      
	  MOM_reload_variable -a mom_out_angle_pos
      MOM_reload_variable -a mom_prev_out_angle_pos
      
   
 
 MOM_reload_variable -a mom_out_angle
 MOM_reload_variable -a mom_tool_axis
 MOM_reload_variable -a mom_prev_pos
 MOM_reload_variable -a mom_pos
}
#=============================================================

#=V1.0========================================================
proc USER_CHECK_MODE { } {
#=============================================================
# Контроль режимов резания
# Контроль частоты вращения
# для Heidenhain только частота вращения

	global feed mom_feed_rate mom_user_feed mom_spindle_speed mom_user_feed_value mom_feed_cut_value
	global warning_msg warning_msg_list user_error_list
	global error_operation mom_operation_name
	global mom_ude_min_feed_rate mom_ude_max_feed_rate mom_ude_min_speed mom_ude_max_speed
	global mom_ude_set_limits
	set uf_pm [ASK_FEEDRATE_FPM]
	
	if {[info exists mom_ude_set_limits] && $mom_ude_set_limits == ON} {
		#
		if {[info exists mom_ude_max_feed_rate]} {
			set MAX_FEED_RATE $mom_ude_max_feed_rate
		}
		if {[info exists mom_ude_min_feed_rate]} {
			set MIN_FEED_RATE $mom_ude_min_feed_rate
		}
		if {[info exists mom_ude_min_speed]} {
			set MIN_SPINDLE_SPEED $mom_ude_min_speed
		}
		if {[info exists mom_ude_max_speed]} {
			set MAX_SPINDLE_SPEED $mom_ude_max_speed
		}
		MOM_output_to_listing_device ">>>MAX_FEED_RATE:$MAX_FEED_RATE MIN_FEED_RATE:$MIN_FEED_RATE MAX_SPINDLE_SPEED:$MAX_SPINDLE_SPEED MIN_SPINDLE_SPEED:$MIN_SPINDLE_SPEED"
		#MOM_output_to_listing_device ">>>uf_pm:$uf_pm                                              mom_spindle_speed:$mom_spindle_speed"
		#MOM_output_to_listing_device ">>>feed:$feed"
		#MOM_output_to_listing_device ">>>mom_feed_cut_value:$mom_feed_cut_value"
		
		set control_feed $mom_feed_cut_value
		
		catch {unset mom_ude_set_limits}
	} {
		set MAX_FEED_RATE 30000.
		set MIN_FEED_RATE 0.001
		set MAX_SPINDLE_SPEED 40000.
		set MIN_SPINDLE_SPEED 0.01
	}
	
	#set error_operation 
	#set warning_msg_list 0

	if { [info exists control_feed] } {
		if { $control_feed < 0 || $control_feed == 0 || $control_feed < $MIN_FEED_RATE } {
		
			set warning_msg "OPERATION:<$mom_operation_name> FEEDRATE TOO SLOW Feed=$control_feed FMIN=$MIN_FEED_RATE"
			lappend warning_msg_list $warning_msg
			lappend user_error_list 1
		}
		if { $control_feed > $MAX_FEED_RATE } {
			set warning_msg "OPERATION:<$mom_operation_name> FEEDRATE TOO FAST Feed=$control_feed FMAX=$MAX_FEED_RATE"
			lappend warning_msg_list $warning_msg
			lappend user_error_list 1
			
		}
	}
	
	if { [info exists mom_spindle_speed] } {
		if {$mom_spindle_speed == 0 || $mom_spindle_speed < $MIN_SPINDLE_SPEED} {
			
			set warning_msg "OPERATION:<$mom_operation_name> SPINDLE SPEED TOO SLOW"
			lappend warning_msg_list $warning_msg
			lappend user_error_list 1
		}
		if { $mom_spindle_speed > $MAX_SPINDLE_SPEED} {
			set warning_msg "OPERATION:<$mom_operation_name> SPINDLE SPEED TOO FAST"
			lappend warning_msg_list $warning_msg
			append user_error_list 2			
		}
	}
	
	if { [info exists warning_msg_list] } {
		set i 0
		MOM_output_to_listing_device "\n==============================================================\n"
		foreach a $warning_msg_list {
			
			MOM_output_to_listing_device ">>>WARNING!<$i>  $a\n"
			incr i
		}
		MOM_output_to_listing_device "\n==============================================================\n"
	
		set res [MOM_display_message "Invalid values ​​detected:" "WARNING LIST" "" "Continue" "Abort"]
		if { $res == 2 } {
			MOM_abort "\
			\n\n==========================================\
			\n\n Вывод программы прерван! \
			\n\n==========================================\n\n"
	}
	}
}

#=============================================================
proc USER_DEBUG_VAR { } {
#=============================================================
	global mom_kin_4th_axis_max_limit mom_kin_4th_axis_min_limit
	#MOM_output_literal ";>>> mom_kin_4th_axis_max_limit=$mom_kin_4th_axis_max_limit"
	#MOM_output_literal ";>>> mom_kin_4th_axis_min_limit=$mom_kin_4th_axis_min_limit"
}

#=============================================================
proc USER_output_mode { } {
#=============================================================
#	versa_output_mode ON/OFF Режим вывода для роботизированного комплекса EROWA
#
	global mom_ude_output_mode erowa_output_mode
	
	if {[info exists mom_ude_output_mode] && $mom_ude_output_mode == "EROWA"} {
		set erowa_output_mode ON
	} else {
		catch { unset erowa_output_mode}
		#set erowa_output_mode OFF
	}
}

#=============================================================
proc USER_check_block_plane_spatial { } {
#=============================================================

# This custom command should return
#   1 : Output BLOCK
#   0 : No output
   global seq
   global mom_out_angle_pos
   global dpp_ge

   if {[EQ_is_lt $mom_out_angle_pos(0) 0]} {
      set seq "SEQ-"
   } else {
      set seq "SEQ+"
   }
   
	if { $dpp_ge(toolpath_axis_num)=="5" } { return 0 }
   
   if { $dpp_ge(coord_rot) != "NONE" } {
#<lili 2014-11-24> add following block CREATE to avoid wrong modal rotary axes
      MOM_do_template rapid_rotary CREATE
      MOM_disable_address fourth_axis fifth_axis
      MOM_force Once X Y Z
	  MOM_do_template rapid_move_xy
	  return 1
   } else { return 0 }
}
#=============================================================

proc USER_out_LBL997 {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
	MOM_output_literal ";"
	MOM_output_literal ";======== end of program ========"
	MOM_output_literal ";"
	MOM_output_literal "LBL 997;\tSUBPROG"
	MOM_output_literal "M9"
	MOM_output_literal "LBL 0;\t\tRETURN"
}

#=============================================================
proc USER_out_LBL998 {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
	MOM_output_literal ";"
	MOM_output_literal "LBL 998;\tSUBPROG RESET"
	MOM_output_literal "M9"
	MOM_output_literal "M140 MB MAX"
	MOM_output_literal "M129"
	MOM_do_template plane_reset
	MOM_output_literal "L A0 C0 FMAX"
	MOM_output_literal "LBL 0;\t\tRETURN"
	MOM_force_block once coolant_text
	MOM_force once M_coolant_txt M_coolant
}

#=============================================================
proc USER_out_LBL999 {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
	MOM_output_literal ";"
	MOM_output_literal "LBL 999;\tSUBPROG STOP"
	MOM_output_literal "M0"
	MOM_output_literal "LBL 0;\t\tRETURN"
}

#=============================================================
proc USER_out_LBL {} {
# <Mitiouk> 04-04-2019 
# out microcode for m0/m1 cuctomize operator
# =====================================================
global erowa_output_mode
	if { [info exists erowa_output_mode] && $erowa_output_mode == ON} {
		return
	}
	
	global user_Q1 mom_operation_name
	if {![info exist user_Q1]} { set user_Q1 10} else {set user_Q1 [expr $user_Q1 +10 ]}
	
	#MOM_output_literal ";"
	#MOM_output_literal ";=<$user_Q1> start of operation ===="
	#MOM_output_literal "* - LBL $user_Q1\; $mom_operation_name"
	MOM_output_literal "LBL $user_Q1"
}
#=============================================================

proc USER_preposition_m128 {} {
# <Mitiouk> 17-06-2019 
# вывод блока предпозиционирования перед обработкой 5-осей
# =====================================================
  global user_tcpm_output_mode
	
    if { [PB_CMD__check_block_output_m128] } {
      #поворот ск
	  MOM_force_block Once m140
	  MOM_do_template m140
	  MOM_do_template plane_spatial
      #Конвертирует координаты точки
      global mom_mcs_goto mom_tool_axis mom_pos
      global mom_result


 

      if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
           set i 0
           foreach value $mom_result {
              set mom_pos($i) $value
                #MOM_output_to_listing_device ">>>mom_pos($i)=$mom_pos($i)"
              incr i
           }
        }
      #MOM_output_literal ""
	  
      MOM_do_template initial_move_6
      MOM_do_template initial_move_6_1
      MOM_do_template plane_reset
	  MOM_force once F
      if { [info exists user_tcpm_output_mode] && $user_tcpm_output_mode == "FUNCTION"} {
		MOM_do_template output_function_tcpm
	  } else {
		MOM_do_template output_m128
	  }
      #PB_CMD__check_block_output_m128
    

      global dpp_ge
      global mom_prev_mcs_goto mom_prev_pos mom_arc_center mom_pos_arc_center
      global mom_kin_arc_output_mode mom_kin_helical_arc_output_mode

      if { $dpp_ge(toolpath_axis_num)=="5" } {
      #MOM_force Once fourth_axis fifth_axis
       VMOV 3 mom_mcs_goto mom_pos
       VMOV 3 mom_prev_mcs_goto mom_prev_pos
       VMOV 3 mom_arc_center mom_pos_arc_center
	   
       set mom_kin_arc_output_mode "LINEAR"
       set mom_kin_helical_arc_output_mode "LINEAR"

      MOM_reload_kinematics

      }

      if { [PB_CMD__check_block_rotary_axes] && [PB_CMD__check_block_rapid_rotary] } {
        MOM_do_template rapid_rotary
      }

    }

}

#=============================================================
proc USER_OUT_WORKPIECE { } {
#=============================================================
  global mom_ude_workpiece_set
  global mom_ude_workpiece_fp mom_ude_workpiece_sp mom_ude_workpiece_shape
  global mom_blk_check_out
# По умолчанию выводим:  
  #4.2 Если заготовка не определена, то выводить "Блок 200х200х50"

#	BLK FORM 0.1 Z X-100. Y-100. Z-50.
#	BLK FORM 0.2 X+100. Y+100. Z+0.

  
  if { [info exists mom_blk_check_out] && $mom_blk_check_out != 0 } {
	return 0
  }
  if {![info exists mom_ude_workpiece_set]} {
	set mom_ude_workpiece_set "ACTIVE"
	set mom_ude_workpiece_shape "RECTANGLE"
	set mom_ude_workpiece_fp(0) -100.
	set mom_ude_workpiece_fp(1) -100.
	set mom_ude_workpiece_fp(2) -50.
	set mom_ude_workpiece_sp(0) +100.
	set mom_ude_workpiece_sp(1) +100.
	set mom_ude_workpiece_sp(2) +0.
  }

  #normalize work piece point value
  if {[info exists mom_ude_workpiece_set] && $mom_ude_workpiece_set== "ACTIVE" } {
    if { [EQ_is_gt $mom_ude_workpiece_fp(0) $mom_ude_workpiece_sp(0)] } {
			set temp $mom_ude_workpiece_sp(0);
			set mom_ude_workpiece_sp(0) $mom_ude_workpiece_fp(0);
			set mom_ude_workpiece_fp(0) $temp;
					}
	if { [EQ_is_gt $mom_ude_workpiece_fp(1) $mom_ude_workpiece_sp(1)] } {
			set temp $mom_ude_workpiece_sp(1);
			set mom_ude_workpiece_sp(1) $mom_ude_workpiece_fp(1);
			set mom_ude_workpiece_fp(1) $temp;
					}
	if { [EQ_is_gt $mom_ude_workpiece_fp(2) $mom_ude_workpiece_sp(2)] } {
			set temp $mom_ude_workpiece_sp(2);
			set mom_ude_workpiece_sp(2) $mom_ude_workpiece_fp(2);
			set mom_ude_workpiece_fp(2) $temp;
					}
	
	if {[info exists mom_ude_workpiece_shape]} {
	switch $mom_ude_workpiece_shape {
	  "RECTANGLE" {
					set x_min [format "%+0.3f" $mom_ude_workpiece_fp(0)]
					set y_min [format "%+0.3f" $mom_ude_workpiece_fp(1)]
					set z_min [format "%+0.3f" $mom_ude_workpiece_fp(2)]
					set x_max [format "%+0.3f" $mom_ude_workpiece_sp(0)]
					set y_max [format "%+0.3f" $mom_ude_workpiece_sp(1)]
					set z_max [format "%+0.3f" $mom_ude_workpiece_sp(2)]
					MOM_output_literal "BLK FORM 0.1 Z X$x_min Y$y_min Z$z_min"; #MIN POINT
					MOM_output_literal "BLK FORM 0.2 X$x_max Y$y_max Z$z_max"; #MAX POINT
					}
	  "CYLINDRE" {
					;# EXAMPLE BLK FORM CYLINDER Z R40 L200
					
					set x [expr $mom_ude_workpiece_sp(0) - $mom_ude_workpiece_fp(0)]
					set y [expr $mom_ude_workpiece_sp(1) - $mom_ude_workpiece_fp(1)]
					set r_cyl [expr sqrt(($x * $x) + ($y * $y)) / 2]
					set l_cyl [expr $mom_ude_workpiece_sp(2) - $mom_ude_workpiece_fp(2) ]
					set r_cyl [format "%+.1f" $r_cyl]
					set l_cyl [format "%+.1f" $l_cyl]
					MOM_output_literal "BLK FORM CYLINDER Z R$r_cyl L$l_cyl"
					}
	}
	}
	
	set mom_blk_check_out 1
  }
}
#=============================================================

#=============================================================
proc USER_COOLANT { } {
#=============================================================
}
#=============================================================

#=============================================================
proc USER_coolant_set { } {
#=============================================================

global mom_coolant_mode mom_coolant_status mom_sys_coolant_code
	global mom_ude_coolant_mode
	#<Mitiouk> 09-07-2019
	set mom_sys_coolant_code(AIR)                  "43"
#продувка воздухом
	#ON/FLOOD/OFF/MIST/THRU/TAP  
	
	if { [info exists mom_ude_coolant_mode] } {
		set mom_coolant_status FLOOD
		MOM_force_block Once coolant_text
		switch $mom_ude_coolant_mode {
			"OFF" 	{ set mom_coolant_status OFF 
					      set mom_coolant_mode OFF
					    }
			"AIR" 			{ set mom_coolant_status AIR}
			"FLOOD" 			{ set mom_coolant_status FLOOD}			
			default 				{ set mom_coolant_status FLOOD}
			}
		}
		#catch {unset mom_ude_coolant_mode}
	#MOM_output_to_listing_device ">>> mom_coolant_status: $mom_coolant_status $mom_sys_coolant_code($mom_coolant_status)"
	#MOM_output_literal ";>>> M$mom_sys_coolant_code($mom_coolant_status)"
}

#=============================================================
proc MOM_output_cycle { {a ""} } {
#=============================================================
	# Функция обеспечивает аккумулирование текста и последующий его модальный модальный вывод
	# Вызывается со значимым аргументом для добавления информации
	# Вызывается без аргумента или с пустым аргументом для вывода текста в программу
	# Вызывается с аргументом "-reset" для сброса
	# MOM_output_cycle "текст" /добавление текста в буфер
	# MOM_output_cycle /вывод цикла
	# MOM_output_cycle "-reset" /сброс
	# Применение:
	# Весь текст цикла выводим в функцию MOM_output_cycle "TEXT"
	# когда всё отправлено вызываем MOM_output_cycle без аргументов
	# в событии конец_цикла вызываем MOM_output_cycle -reset для сброса содержимого предварительного вывода 
	
	global cycle_buffer buffer_list
	global prev_cycle_buffer
	if {![info exists cycle_buffer]} 		{ set cycle_buffer "" }
	if {![info exists prev_cycle_buffer]} 	{ set prev_cycle_buffer "" }
	
	if { [string length $a] == 0 } {
		if { [info exists cycle_buffer] && ![string equal $prev_cycle_buffer $cycle_buffer]} {
			#MOM_output_literal $cycle_buffer
			#процедура MOM_output_literal не выводит нормально строки вида "текст \n текст" в режиме работы симуляции станка
			#выводит только до первого символа \n 
			foreach a $buffer_list {
				set out_line ""
				foreach aa $a { set out_line $out_line$aa}
				if {![info exists number_flag] } { MOM_output_literal $out_line; set number_flag 1} else { MOM_output_text $out_line }
			}
			catch { unset number_flag }
			#MOM_output_to_listing_device $cycle_buffer
			set prev_cycle_buffer $cycle_buffer	
			set buffer_list ""
		}
		catch {unset cycle_buffer}
		return 
	}
	if {[string equal $a "-reset"]} {
		catch {unset prev_cycle_buffer}
		catch {unset cycle_buffer}
		return
		}
	
	set cycle_buffer "$cycle_buffer$a"
	lappend buffer_list [list $a]
}

proc USER_cycle_200_output { } {
#=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_motion_event
 set ude_alt_cycle_output ON; #ON/OFF
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
  if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	global dpp_TNC_cycle_feed
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	 	
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q202 $cycle_peck_size
	set q210 $mom_cycle_top_delay
	set q203 $mom_pos(2)
	set q204 $js_return_pos
	set q211 $mom_cycle_delay
	
	#Format
	set q200 [format "%.3f" $q200]
	set q201 [format "%.3f" $q201]
	set q206 [format "%.3f" $q206]
	set q202 [format "%.3f" $q202]
	set q210 [format "%.2f" $q210]
	set q203 [format "%.3f" $q203]
	set q204 [format "%.3f" $q204]
	set q211 [format "%.2f" $q211]
	
	MOM_output_cycle "CYCL DEF 200 SWERLENIJE ~"
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA SVERLENIYA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA VREZANIA ~"
	MOM_output_cycle "\tQ202=$q202\t;GLUBINA VREZANIA ~"
	MOM_output_cycle "\tQ210=$q210\t;VREMYA VIDERJKI VVERHU ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;BEZOP.RASSTOIANIE ~"
	MOM_output_cycle "\tQ211=$q211\t;VREMIA VIDERGKI VNIZU"
	MOM_output_cycle
   
  } else {
  PB_call_macro CYCL_200
  }
}
#=============================================================

proc USER_cycle_201_output { } {
#=============================================================
 #output CYCL DEF 201 alt format
 global ude_alt_cycle_output
 global mom_motion_event mom_ude_return_feed
 global mom_TNC_cycle_return_feed
 global dpp_TNC_cycle_feed mom_feed_cut_value
 global mom_cycle_retract_to
 set ude_alt_cycle_output ON;#ON/OFF
 
 # ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

 #Return_feed
	global mom_TNC_cycle_return_feed dpp_TNC_cycle_feed
	if {![info exists dpp_TNC_cycle_feed]} {
	  set dpp_TNC_cycle_feed $mom_feed_cut_value
	}
   if {![info exists mom_ude_return_feed] || $mom_ude_return_feed==0} {
     set mom_TNC_cycle_return_feed $dpp_TNC_cycle_feed
   } else {
	set mom_TNC_cycle_return_feed $mom_ude_return_feed
   }
  
 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	global mom_retract_feed
	
	#Назначение
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q211 $mom_cycle_delay
	
	if {[info exists mom_retract_feed] && $mom_retract_feed > 0} {
	set q208 $mom_retract_feed } else {
	set q208  $dpp_TNC_cycle_feed }
	
	set q203 $mom_pos(2)
	if {![info exists js_return_pos]} {set js_return_pos $mom_cycle_retract_to}
	set q204 $js_return_pos
	
	if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
	#Форматирование	
	set q200 [format "%+.3f" $q200]
	set q201 [format "%+.3f" $q201]
	set q206 [format "%+.3f" $q206]
	set q211 [format "%+.2f" $q211]
	set q208 [format "%+.3f" $q208]
	set q203 [format "%+.3f" $q203]
	set q204 [format "%+.3f" $q204]
	
	#MOM_output_literal "CYCL DEF 201 REAMING ~ "
	MOM_output_cycle "CYCL DEF 201 RAZWIORTYWANIE ~"
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA NA WREZANJE ~"
	MOM_output_cycle "\tQ211=$q211\t;WYDER.WREMENI WNIZU ~"
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ."
	MOM_output_cycle
	#вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} { MOM_set_seq_on }
  } else { 
	#PB_call_macro CYCL_201
	MOM_output_cycle "CYCL DEF 201\
	Q200=$q200\
	Q201=$q201\
	Q206=$q206\
	Q211=$q211\
	Q208=$q208\
	Q203=$q203\
	MQ204=$q204"
	MOM_output_cycle
	}
}

#=============================================================
proc USER_cycle_202_output { } {
#=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_motion_event
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
 set ude_alt_cycle_output ON

 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	global dpp_TNC_cycle_feed
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	global mom_itnc_bore_q214_mode
	global mom_itnc_bore_q214
	global dpp_TNC_m128_feed_value
	
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q211 $mom_cycle_delay
	set q208 $dpp_TNC_m128_feed_value ;#"FQL2"
	set q203 $mom_pos(2)
	set q204 $js_return_pos
	set mom_itnc_bore_q214 1
	
   
    if { [info exists mom_itnc_bore_q214_mode] } {
		
        if { [string match "0*" $mom_itnc_bore_q214_mode] } {

            set mom_itnc_bore_q214 0
         }
        if { [string match "1*" $mom_itnc_bore_q214_mode] } {

            set mom_itnc_bore_q214 1
         }
        if { [string match "2*" $mom_itnc_bore_q214_mode] } {
            
            set mom_itnc_bore_q214 2
         }
        if { [string match "3*" $mom_itnc_bore_q214_mode] } {
            
            set mom_itnc_bore_q214 3
         }
        if { [string match "4*" $mom_itnc_bore_q214_mode] } {
            set mom_itnc_bore_q214 4
         }
    }
	set q214 $mom_itnc_bore_q214
	if {[info exists mom_cycle_orient]} {
		set q236 $mom_cycle_orient
	} else {set q236 0}
	#format

	set q200 [format "%.3f" $q200]
	set q201 [format "%.3f" $q201]
	set q206 [format "%.3f" $q206]
	set q211 [format "%.2f" $q211]
	set q208 [format "%.2f"	$q208]
	set q203 [format "%.3f" $q203]
	set q204 [format "%.3f" $q204]
	set q214 [format "%.3f" $q214]
	set q236 [format "%.3f" $q214]
	
	if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
	MOM_output_cycle "CYCL DEF 202 RASTOCHKA ~ "
	#прекратить нумеровать
	#set seq_status $mom_sequence_mode
	#MOM_set_seq_off
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA SVERLENIYA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA VREZANIA ~"
	MOM_output_cycle "\tQ211=$q211\t;VREMIA VIDERGKI VNIZU ~"
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;BEZOP.RASSTOIANIE ~"
	MOM_output_cycle "\tQ214=$q214\t;NAPR.WYCHODA IZ MAT ~"
	MOM_output_cycle "\tQ236=$q236\t;UGOL SCHPINDEL"
	MOM_output_cycle
	##вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} {
	#	MOM_set_seq_on}
  } else {
  PB_call_macro CYCL_202
  }
}

#=============================================================
proc USER_cycle_203_output { } {
#=============================================================
# Универсальное сверление
#Mitiouk <04-07-2019>
 #output CYCL DEF 203 alt format
  global ude_alt_cycle_output
  global mom_motion_event mom_ude_return_feed
  global mom_TNC_cycle_return_feed
  global dpp_TNC_cycle_feed mom_feed_cut_value
  global mom_cycle_retract_to
  global mom_cycle_rapid_to
  global mom_cycle_feed_to
  global cycle_peck_size
  global mom_cycle_top_delay
  global mom_pos
  global js_return_pos
  global mom_cycle_delay
  global seq_status mom_sequence_mode
  global mom_feed_departure_value mom_retract_feed
  global mom_TNC_cycle_return_feed dpp_TNC_cycle_feed
  global user_TNC_cycle_step_quantity user_TNC_cycle_step
  global mom_cycle_retract_mode
  
  set ude_alt_cycle_output ON;#ON/OFF
  #
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

# ----Return_feed
	if {![info exists dpp_TNC_cycle_feed]} {
	  set dpp_TNC_cycle_feed $mom_feed_cut_value
	}
   if {![info exists mom_ude_return_feed] || $mom_ude_return_feed==0} {
     set mom_TNC_cycle_return_feed $dpp_TNC_cycle_feed
   } else {
	set mom_TNC_cycle_return_feed $mom_ude_return_feed
   }
   
	if {![info exists js_return_pos]} {set js_return_pos $mom_cycle_retract_to}
 
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	#
	set q202 $cycle_peck_size
	set q210 $mom_cycle_top_delay
	set q203 $mom_pos(2)
	set q204 $js_return_pos

# ---- Декремент приращения шагов по глубине --------
	
	global mom_cycl_203_q212
	set q212 0.0;# default value
	
	if {[info exists mom_cycl_203_q212]} {
		set q212 [format "%+.3f" $mom_cycl_203_q212]
	}
		
# ---- Ломка стружки ----
global mom_cycl_203_q213 mom_cycle_203_option mom_cycle_option

#MOM_output_literal ";Foo mom_cycle_option: $mom_cycle_option"
    
	global mom_motion_event
	#: drill_break_chip_move
	set q213 0
	if {([info exists mom_motion_event] && $mom_motion_event == "drill_break_chip_move") || $mom_cycle_option == "OPTION"} {
			set q213 $user_TNC_cycle_step_quantity
			#set q202 $user_TNC_cycle_step
		}
	
	
# ---- Величина отхода при ломке стружки  ----
global cycl_203_q256
	if {[info exists mom_cycl_203_q256]} {
		set q256 $mom_cycl_203_q256
	} else { 
		set q256 1.0
	}
# ---- Минимальная глубина врезания ----
  global mom_cycl_203_q205
	set q205 0.2
	if {[info exists mom_cycl_203_q205]} {
		set q205 $mom_cycl_203_q205
	}
	set q211 $mom_cycle_delay
	
	if { [info exists mom_retract_feed] && $mom_retract_feed > 0} {
			set q208 $mom_retract_feed
		} else {
			set q208 10000.0
		}

	set q395 0;#Oпорная глубина вершина/цилиндр
	
	#режим вывода
	
# ---- форматирование ---
	set q200 [format "%+.3f" [expr abs($q200)]]
	set q201 [format "%+.3f" $q201]
	set q206 [format "%+.3f" $q206] 
	set q202 [format "%+.3f" $q202]
	set q210 [format "%+.2f" $q210]
	set q203 [format "%+.3f" $q203]
	set q204 [format "%+.3f" $q204]
	set q212 [format "%+.3f" $q212]
	set q213 [format "%+i" $q213]
	set q205 [format "%+.3f" $q205]
	set q211 [format "%+.2f" $q211]
	set q208 [format "%+.3f" $q208]
	set q256 [format "%+.3f" $q256]
	set q395 [format "%+.3f" $q395]
# ---- вывод -----
		set local_cycle_mode 0
		if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} {	
			set local_cycle_mode 1
		}
		
		#global mom_post_in_simulation mom_output_mode_define
		#if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
		#	set local_cycle_mode 1 }
			
	if {[info exists local_cycle_mode] && $local_cycle_mode==1} {
		MOM_output_cycle "CYCL DEF 203 UNIVERS. SWERLENIE ~"
		#set seq_status $mom_sequence_mode
		#MOM_set_seq_off
	
		MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
		MOM_output_cycle "\tQ201=$q201\t;GLUBINA SVERLENIYA ~"
		MOM_output_cycle "\tQ206=$q206\t;PODACHA VREZANIA ~"
		MOM_output_cycle "\tQ202=$q202\t;GLUBINA VREZANIA ~"
		MOM_output_cycle "\tQ210=$q210\t;VREMYA VIDERJKI VVERHU ~"
		MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
		MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
		MOM_output_cycle "\tQ212=$q212\t;SJOM MATERIALA ~"
		MOM_output_cycle "\tQ213=$q213\t;KOL.OPER.LOMKI STRU ~"
		MOM_output_cycle "\tQ205=$q205\t;MIN.GLUBINA WREZANJA ~"
		MOM_output_cycle "\tQ211=$q211\t;VREMIA VIDERGKI VNIZU ~"
		MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"
		MOM_output_cycle "\tQ256=$q256\t;WYCHOD PRI LOMANII ~"
		MOM_output_cycle "\tQ395=$q395\t;KOORD. OTSCHETA GLUB"
		MOM_output_cycle
	} else {
		MOM_output_literal "CYCL DEF 203 \
									Q200=$q200 \
									Q201=$q201 \
									Q206=$q206 \
									Q202=$q202 \
									
									Q210=$q210 \
									Q203=$q203 \
									Q204=$q204 \
									Q212=$q212 \
									Q213=$q213 \
									Q205=$q205 \
									Q211=$q211 \
									Q208=$q208 \
									Q256=$q256 \
									Q395=$q395"		
		}
	#вернуть нумерацию, если надо
	#if {$mom_sequence_mode == ON} {
	#	MOM_set_seq_on
	#}
}

#=============================================================
proc USER_cycle_205_output {} {
#=============================================================
# Универсальное сверление
#Mitiouk <04-07-2019>
#output CYCL DEF 205 alt format
 
 global ude_alt_cycle_output
 global mom_motion_event mom_ude_return_feed
 global mom_TNC_cycle_return_feed
 global dpp_TNC_cycle_feed mom_feed_cut_value
 global mom_cycle_retract_to
 global mom_cycle_rapid_to
 global mom_cycle_feed_to
 global cycle_peck_size
 global mom_cycle_top_delay
 global mom_pos
 global js_return_pos
 global mom_cycle_delay
 global seq_status mom_sequence_mode
 global mom_feed_departure_value mom_retract_feed
 global mom_TNC_cycle_return_feed dpp_TNC_cycle_feed	
 global mom_cycl_205_q257 mom_cycl_205_q256
 
 set ude_alt_cycle_output ON;#ON/OFF
 
# ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

 # ---- Return_feed -----
  if {![info exists dpp_TNC_cycle_feed]} {
	  set dpp_TNC_cycle_feed $mom_feed_cut_value
	}
   if {![info exists mom_ude_return_feed] || $mom_ude_return_feed==0} {
     set mom_TNC_cycle_return_feed $dpp_TNC_cycle_feed
   } else {
	set mom_TNC_cycle_return_feed $mom_ude_return_feed
   }
	if {![info exists js_return_pos]} {set js_return_pos $mom_cycle_retract_to}
# ---- Общие параметры ---	
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q206 $dpp_TNC_cycle_feed
	set q202 $cycle_peck_size
	set q210 $mom_cycle_top_delay
	set q203 $mom_pos(2)
	set q204 $js_return_pos
	
# ---- Специальные параметры --------
# ---- Декремент приращения шагов по глубине --------	
global mom_sjom_materala
	set q212 0.0
	if {[info exists mom_sjom_materala]} {
		set q212 $mom_sjom_materala
	}
	set q205 0.2
	set q258 0.2 ; #RASST.BEZ. WWERCHU
	set q259 0.2 ; #RASST.BEZ. W NIZU
	
# ----WYCHOD PRI LOMANII--------------------------
	set q256 0.2
	if {[info exists mom_cycl_205_q256]} {
		set q256 $mom_cycl_205_q256
	}
	
# ----GL.SWERL.PRI LOMANII------------------------
	set q257 0
	if {[info exists mom_cycl_205_q257]} {
		set q257 $mom_cycl_205_q257
	}
		
	set q211 [format "%+.2f" $mom_cycle_delay]
# -----	TOCHKA STARTA -------------
  global mom_cycl_205_tochka_starta
  set q379 0
  if { [ info exists mom_cycl_205_tochka_starta ] } {
	   set q379 $mom_cycl_205_tochka_starta
    }
# ----- PODACHA PRED.POZIC.
	set q253 750.0

# ----- PODACHA WYCHODA 
	set q208 10000.0
	if { [ info exists mom_retract_feed ] && $mom_retract_feed > 0 } {
			set q208 $mom_retract_feed
		}
		
# ------ q395 опорная глубина /вершина/цилиндр
	set q395 0

# Вывод в программу
  set local_cycle_mode 0
  if { [ info exists ude_alt_cycle_output ] && $ude_alt_cycle_output == ON } {	
			set local_cycle_mode 1
		}
		
		#global mom_post_in_simulation mom_output_mode_define
		#if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
		#	set local_cycle_mode 1 }
			
# Форматирование
	set q200 [format "%+.3f" $q200]
	set q201 [format "%+.3f" $q201]
	set q206 [format "%+.3f" $q206] 
	set q202 [format "%+.3f" $q202]
	set q210 [format "%+.2f" $q210]
	set q203 [format "%+.3f" $q203]
	set q204 [format "%+.3f" $q204]
	set q212 [format "%+.3f" $q212]
	set q205 [format "%+.3f" $q205]
	set q258 [format "%+.3f" $q258]
	set q259 [format "%+.3f" $q259]
	set q257 [format "%+.3f" $q257]
	set q256 [format "%+.3f" $q256]
	set q211 [format "%+.2f" $q211]
	set q253 [format "%+.3f" $q253]
	set q208 [format "%+0.3f" $q208]
	set q379 [format "%+.3f" $q379]
	set q395 [format "%+.3f" $q395]
	
#
			
  if {[info exists local_cycle_mode] && $local_cycle_mode==1} {
      
	MOM_output_cycle "CYCL DEF 205 UNIW. GL. SWERLENIE ~"
#	#прекратить нумеровать
	set seq_status $mom_sequence_mode
	MOM_set_seq_off
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA NA WREZANJE ~"
	MOM_output_cycle "\tQ202=$q202\t;GLUBINA WREZANJA ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
	MOM_output_cycle "\tQ212=$q212\t;SJOM MATERIALA ~"
	MOM_output_cycle "\tQ205=$q205\t;MIN.GLUBINA WREZANJA ~"
	MOM_output_cycle "\tQ258=$q258\t;RASST.BEZ. WWERCHU ~"
	MOM_output_cycle "\tQ259=$q259\t;RASST.BEZ. W NIZU ~"
	MOM_output_cycle "\tQ257=$q257\t;GL.SWERL.PRI LOMANII ~"
	MOM_output_cycle "\tQ256=$q256\t;WYCHOD PRI LOMANII ~"
	MOM_output_cycle "\tQ211=$q211\t;WYDER.WREMENI WNIZU ~"
	MOM_output_cycle "\tQ379=$q379\t;TOCHKA STARTA ~"
	MOM_output_cycle "\tQ253=$q253\t;PODACHA PRED.POZIC. ~"
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"	
	MOM_output_cycle "\tQ395=$q395\t;KOORD. OTSCHETA GLUB"
	MOM_output_cycle
	#if {[info exists mom_sequence_mode] && $mom_sequence_mode=="ON"} {
	#  MOM_set_seq_on
	#  }
		} else {
		
	MOM_output_cycle "CYCL DEF 205 \
		Q200=$q200 \
		Q201=$q201 \
		Q206=$q206 \
		Q202=$q202 \
		Q203=$q203 \
		Q204=$q204 \
		Q212=$q212 \
		Q205=$q205 \
		Q258=$q258 \
		Q259=$q259 \
		Q257=$q257 \
		Q256=$q256 \
		Q211=$q211 \
		Q379=$q379 \
		Q253=$q253 \
		Q208=$q208 \
		Q395=$q395"
	MOM_output_cycle
	}
}

#=============================================================
proc USER_cycle_209_output { } {
 #=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_cycle_rapid_to
 global mom_cycle_feed_to
 global dpp_TNC_cycle_feed
 global cycle_peck_size
 global dpp_TNC_cycle_thread_pitch
 global mom_cycle_top_delay
 global mom_pos
 global js_return_pos
 global mom_cycle_delay
 global seq_status mom_sequence_mode
 global mom_cycle_step1
 global mom_cycle_orient
 global mom_spindle_retract_nominal_value
 global user_cycle_counter
 global mom_motion_event
 
 set ude_alt_cycle_output ON;#ON/OFF
 set local_cycle_mode 0
 
	set q200 $mom_cycle_rapid_to
	set q201 $mom_cycle_feed_to
	set q239 $dpp_TNC_cycle_thread_pitch
	set q203 $mom_pos(2)
	set q204 $js_return_pos
	set q257 $mom_cycle_step1
	set q256 0
	if {[info exists mom_motion_event] && $mom_motion_event != "tap_deep_move"} {
		set q256 1 ;# Коэфициент выхода при ломании
	}
	set q336 $mom_cycle_orient
	 #Q403=1.    ;RPM FACTOR
	if { ![info exists mom_spindle_retract_nominal_value] } {
		set mom_spindle_retract_nominal_value 100; #this is %
	}
	set q403 [expr $mom_spindle_retract_nominal_value* 0.01]
	#FORMAT
	set q200 [format "%+.3f" $q200]
	set q201 [format "%+.3f" $q201]
	set q239 [format "%+.3f" $q239]
	set q203 [format "%+.3f" $q203]
	set q204 [format "%+.3f" $q204]
	set q257 [format "%+.3f" $q257]
	set q256 [format "%+.3f" $q256]
	set q336 [format "%+.3f" $q336]
	set q403 [format "%.2f"  $q403]
	
	# ---- вывод -----
		if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} {	
			set local_cycle_mode 1}
		
		#global mom_post_in_simulation mom_output_mode_define
		#if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
		#	set local_cycle_mode 1 }
			
  if {$local_cycle_mode == 1} { 
    
	MOM_output_cycle "CYCL DEF 209 NAR.WN.REZBY/LOM.ST. ~"
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA REZBY ~"
	MOM_output_cycle "\tQ239=$q239\t;SCHAG REZBY ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
	MOM_output_cycle "\tQ257=$q257\t;GL.SWERL.PRI LOMANII ~"
	MOM_output_cycle "\tQ256=$q256\t;WYCHOD PRI LOMANII ~"
	MOM_output_cycle "\tQ336=$q336\t;UGOL SCHPINDEL ~"
	MOM_output_cycle "\tQ403=$q403\t;RPM FACTOR"
	MOM_output_cycle
	
  } else {
	MOM_output_cycle "CYCL DEF 209 \
							Q200=$q200\
							Q201=$q201\
							Q239=$q239\
							Q203=$q203\
							Q204=$q204\
							Q257=$q257\
							Q256=$q256\
							Q336=$q336\
							Q403=$q403"
	MOM_output_cycle
	#PB_call_macro CYCL_209
  }
}

#=============================================================
proc USER_cycle_207_output { } {
#=============================================================
 #output CYCL DEF 207 alt format
 global ude_alt_cycle_output
 global mom_motion_event
 global mom_cycle_rapid_to
 global mom_cycle_feed_to
 global dpp_TNC_cycle_feed
 global cycle_peck_size
 global mom_cycle_top_delay
 global mom_pos
 global js_return_pos
 global mom_cycle_delay
 global seq_status mom_sequence_mode
 global dpp_TNC_cycle_thread_pitch
 
 set ude_alt_cycle_output ON;# ON
 
 # ---- Проверка для единственного вывода ----
 global user_cycle_counter
 #if {[info exists user_cycle_counter] && $user_cycle_counter > 0 } { return }
# ----  

  set q200 [format "%+.3f" $mom_cycle_rapid_to]
  set q201 [format "%+.3f" $mom_cycle_feed_to]
  set q239 [format "%+.3f" $dpp_TNC_cycle_thread_pitch]
  set q203 [format "%+.3f" $mom_pos(2)]
  set q204 [format "%+.3f" $js_return_pos]
  
  
  # ---- вывод -----
  set local_cycle_mode 1
  if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} {	
	set local_cycle_mode 1
		}
  #global mom_post_in_simulation mom_output_mode_define
  #if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
  # set local_cycle_mode 0 }
 
  if {$local_cycle_mode==1} { 
  	MOM_output_cycle "CYCL DEF 207 NAREZANIE REZBI GS ~"
	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA REZBY ~"
	MOM_output_cycle "\tQ239=$q239\t;SCHAG REZBY ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ."
	MOM_output_cycle
	
  } else {
    MOM_output_cycle "CYCL DEF 207 Q200=$q200 Q201=$q201 Q239=$q239 Q203=$q203 Q204=$q204"
	MOM_output_cycle
	#PB_call_macro CYCL_207
  }
}

#=============================================================
proc USER_cycle_241_output { } {
#=============================================================
 #output CYCL DEF 200 alt format
 global ude_alt_cycle_output
 global mom_motion_event
 global mom_cycle_rapid_to mom_cycle_feed_to dpp_TNC_cycle_feed mom_cycle_delay mom_pos js_return_pos
 global mom_spindle_speed mom_sys_coolant_code mom_coolant_status
 global mom_cycle_241_coolant_on mom_cycle_241_coolant_off
 global mom_cycle_241_dwell_depth
 global mom_retract_spindle_speed mom_direct_spindle_turn mom_spindle_direction
 global mom_retract_feed mom_deeper_feed
 global mom_deeper_starting_point
 global mom_cycle_retract_to
 
 set ude_alt_cycle_output ON;  # ON/OFF Жесткое указание  на режим альтернативного вывода
 
 ### SET PARAMETER
 set q200 [$mom_cycle_rapid_to]
 set q201 [$mom_cycle_feed_to]
 set q206 [$dpp_TNC_cycle_feed]
 set q211 [$mom_cycle_delay]
 set q203 [$mom_pos(2)]
 set q204 [$js_return_pos]
 
 if { [info exists mom_deeper_starting_point] && $mom_deeper_starting_point > 0 } {
   if { [expr abs($mom_deeper_starting_point)] > [expr abs($mom_cycle_feed_to)] } {
     set q379 0
	 #set q379 [expr abs ($mom_cycle_feed_to)]
	 MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q379 in non correct. Value has been changed! New value is $q379!"
	 } else { set q379 $mom_deeper_starting_point}
 } else {
   set q379 0
   MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q379 in non correct. Value has been changed! New value is $q379!"
 }
 
 if { [info exists mom_deeper_feed] && $mom_deeper_feed > 0 } {  
   set q253 $mom_deeper_feed } else { set q253 0 }
   
 if {[EQ_is_gt $q379 0] && [EQ_is_equal $q253 0] } {
   MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q253 in non correct. Value is too slow! Value is $q253!"
 }
  
 
 if { [info exists mom_retract_feed] && $mom_retract_feed > 0} {
  set q208 $mom_retract_feed } else {set q208 0}
 
 if { [info exists mom_direct_spindle_turn] } {
   switch $mom_direct_spindle_turn {
     "M3" {set q426 3}
	 "M4" {set q426 4}
	 "M5" {set q426 5}
     default { 
				if {[info exists mom_spindle_direction] } {
					switch $mom_spindle_direction {
						"CLW" {set q426 3}
						"CCLW" {set q426 4}
						default {set q426 5}
						} 
				} else {set q426 5}
			}
   }
   
   } else { 
   if {[info exists mom_spindle_direction] } {
     switch $mom_spindle_direction {
	  "CLW" {set q426 3}
	  "CCLW" {set q426 4}
	  default {set q426 5}
      } 
    } else {set q426 5}
 }
   
  if { [info exists mom_retract_spindle_speed] } {
    set q427 $mom_retract_spindle_speed
  } else {
    set q427 $mom_spindle_speed
  }
 
  
  set q428 $mom_spindle_speed
 
  if { [info exists mom_cycle_241_coolant_on] } {
   if { [info exists mom_coolant_status] && $mom_cycle_241_coolant_on == "ON" } {
     switch $mom_coolant_status {
			THRU1 { set q429 7}
			THRU  { set q429 26}
			AIR   { set q429 27}
			MIST  { set q429 28}
			FLOOD { set q429 8}
			ON    { set q429 8}
			OFF   { set q429 9}
		default   { set q429 8}
	    }
    } else {
	  if { $mom_cycle_241_coolant_on == "OFF"} {
	    set q429 9
	  } else { set q429 8 }
    }
  }
  
  
  if { [info exists mom_cycle_241_coolant_off] } {
    switch $mom_cycle_241_coolant_off {
	  "ON" 	{
				set q430 $q429
			}
	  "OFF" {set q430 9}
	  default {set q430 9}
	}
   } else { set q430 9 }
   
 
 if {[info exists mom_cycle_241_dwell_depth]} {
  if { $mom_cycle_241_dwell_depth < [expr abs($mom_cycle_feed_to)] && $mom_cycle_241_dwell_depth > 0 } {
   set q435 $mom_cycle_241_dwell_depth
  } else {
    if {$mom_cycle_241_dwell_depth > 0} {
    set q435 [expr abs($mom_cycle_feed_to)] } else {
	set q435 0.0
	}
	MOM_output_to_listing_device ">>> Warning! Cycle parameter: Q435 in non correct. Value has been changed! New value is $q435!"
  }
   
 } else { set q435 0 }; #parameter is disabled
 
#format
	set q200 [format "%+.3f" $q200]
	set q201 [format "%+.3f" $q201]
	set q204 [format "%+.3f" $q204]
	set q206 [format "%+.3f" $q206]
	set q211 [format "%+.2f" $q211]
	set q203 [format "%+.3f" $q203]
	set q379 [format "%+.3f" $q379]
	set q253 [format "%+.3f" $q253]
	set q208 [format "%+.3f" $q208]
	set q426 [format "%+i"   $q426]
	set q427 [format "%+.3f" $q427]
	set q428 [format "%+.3f" $q428]
	set q429 [format "%+i"   $q429 ]
	set q430 [format "%+i"   $q430]
	set q435 [format "%+.3f" $q435]
 
 ### OUTPUT
  if {[info exists ude_alt_cycle_output] && $ude_alt_cycle_output == ON} { 
 
	global mom_cycle_rapid_to
	global mom_cycle_feed_to
	global dpp_TNC_cycle_feed
	global cycle_peck_size
	global mom_cycle_top_delay
	global mom_pos
	global js_return_pos
	global mom_cycle_delay
	global seq_status mom_sequence_mode
	
	global mom_post_in_simulation mom_output_mode_define
	
	if {[info exists mom_post_in_simulation] && $mom_post_in_simulation != 0 } {
	 MOM_output_cycle "CYCL DEF 241 \
								Q200=$q200 \
								Q201=$q201 \
								Q206=$q206 \
								Q211=$q211 \
								Q203=$q203 \
								Q204=$q204 \
								Q379=$q379 \
								Q253=$q253 \
								Q208=$q208 \
								Q426=$q426 \
								Q427=$q427 \
								Q428=$q428 \
								Q429=$q429 \
								Q430=$q430 \
								Q435=$q435"
	 MOM_output_cycle
	
	} else { 
	   
	MOM_output_cycle "CYCL DEF 241 SINGLE-LIP D.H.DRLNG ~"

	MOM_output_cycle "\tQ200=$q200\t;BEZOPASN.RASSTOYANIE ~"
	MOM_output_cycle "\tQ201=$q201\t;GLUBINA ~"
	MOM_output_cycle "\tQ206=$q206\t;PODACHA NA WREZANJE ~"
	MOM_output_cycle "\tQ211=$q211\t;WYDER.WREMENI WNIZU ~"
	MOM_output_cycle "\tQ203=$q203\t;KOORD. POVERHNOSTI ~"
	MOM_output_cycle "\tQ204=$q204\t;2-YE BEZOP.RASSTOJ. ~"
	MOM_output_cycle "\tQ379=$q379\t;TOCHKA STARTA ~"; #!!!
	MOM_output_cycle "\tQ253=$q253\t;PODACHA PRED.POZIC. ~"; #!!!
	MOM_output_cycle "\tQ208=$q208\t;PODACHA WYCHODA ~"; #!!!
	MOM_output_cycle "\tQ426=$q426\t\t;DIR. OF SPINDLE ROT. ~"; #!!!
	MOM_output_cycle "\tQ427=$q427\t;ROT.SPEED INFEED/OUT ~"; #!!!
	MOM_output_cycle "\tQ428=$q428\t;ROT. SPEED DRILLING ~"; #!!!
	MOM_output_cycle "\tQ429=$q429\t\t;COOLANT ON ~";
	MOM_output_cycle "\tQ430=$q430\t\t;COOLANT OFF ~"; 
	MOM_output_cycle "\tQ435=$q435\t;DWELL DEPTH";
	MOM_output_cycle
	}
  } else {
  #PB_call_macro CYCL_200
  MOM_output_cycle "CYCL DEF 241 \
								Q200=$q200 \
								Q201=$q201 \
								Q206=$q206 \
								Q211=$q211 \
								Q203=$q203 \
								Q204=$q204 \
								Q379=$q379 \
								Q273=$q253 \
								Q208=$q208 \
								Q426=$q426 \
								Q427=$q427 \
								Q428=$q428 \
								Q429=$q429 \
								Q430=$q430 \
								Q435=$q435"
 MOM_output_cycle
  
  }
  
}

#=============================================================
proc USER_init_cycle { } {
#=============================================================
# This command initializes some drilling cycle variables.
#
# 06-27-2013 Jason - Initial version
# 03-20-2014 Jason - Refine dpp_TNC_cycle_thread_pitch dpp_TNC_cycle_tap_feed and dpp_TNC_cycle_step_clearance
# 08-21-2015 szl - Enhance the warning message when users set wrong pitch and wrong spindle speed,fix PR7463004

   global cycle_init_flag
   global user_cycle_counter
   global mom_operation_name
   global mom_cycle_delay mom_cycle_top_delay
   global mom_motion_event
   global mom_cycle_step1
   global mom_cycle_step_clearance
   global mom_cycle_retract_mode
   global mom_cycle_feed_to mom_cycle_rapid_to mom_cycle_retract_to
   global mom_tool_pitch
   global mom_cycle_thread_pitch
   global mom_cycle_thread_right_handed
   global mom_cycle_orient
   global mom_spindle_direction mom_spindle_speed
   global js_prev_pos               ;# diy previous Z height
   global js_return_pos             ;# returnZ incremental from top of hole
   global mom_pos
   global mom_prev_pos
   global mom_output_unit
   global feed
   global dpp_ge
   global dpp_TNC_Q203_pos
   global dpp_TNC_cycle_thread_pitch
   global dpp_TNC_cycle_tap_feed
   global dpp_TNC_cycle_step_clearance
   global dpp_TNC_cycle_feed
   global user_TNC_cycle_step_quantity user_TNC_cycle_step; #
   global mom_cycle_feed_rate_mode
   global mom_cycle_feed_rate
   global mom_tool_name
   global mom_feed_cut_unit
   global mom_spindle_rpm
   
   global mom_feed_retract_value mom_retract_feed

  #--------------------Set retract feed__------------------------------------------------------
   if { [info exists mom_feed_retract_value] } {
        set mom_retract_feed $mom_feed_retract_value
    } else {
      catch {unset mom_retract_feed}
    }
   
   #DEBUG OUTPUT
  #MOM_output_literal ";>>>Foo mom_motion_event:$mom_motion_event"
  #MOM_output_literal ";>>>Foo tool_type:$mom_motion_event"
   
   #<Mitiouk>15-05-2019
 global user_coord_rotation_flag
 if {![info exists user_coord_rotation_flag]} {
	MOM_output_to_listing_device ">>> ERROR! No clearance distance"
	MOM_display_message "No clearance for Drill Operation!" "User Abort Post processing" \
	"E" "OK"
	MOM_abort "*** User Abort Post Processing *** "
	}
	
	#<Mitiouk>29-07-219 change depth holle
	global mom_prev_cycle_feed_to
	if {![info exists mom_prev_cycle_feed_to] && [info exists mom_cycle_feed_to]} {
		set mom_prev_cycle_feed_to $mom_cycle_feed_to
	}
	
  if {![info exists user_cycle_counter]} {
        global mom_pos
        #MOM_output_literal ";>>> foo cycle_coord_rotation first X:$mom_pos(0) Y:$mom_pos(1)"
        #PB_CMD_cycle_coord_rotation
        #PB_CMD_coord_rotation_in_operation
        #MOM_output_literal ";>>> foo cycle_coord_rotation first X:$mom_pos(0) Y:$mom_pos(1)"
        set user_cycle_counter 0
    } else {
		#<Mitiouk>29-07-219 change depth holle
		if {[info exists mom_cycle_feed_to]} {
			if {[EQ_is_equal $mom_cycle_feed_to $mom_prev_cycle_feed_to]} {
				incr user_cycle_counter
			} else {
				set user_cycle_counter 0
				set mom_prev_cycle_feed_to $mom_cycle_feed_to
		  } 		
		}
	}
	

  #--------------------Set default cycle orient------------------------------------------------
   if { ![info exists mom_cycle_orient] } {
      set mom_cycle_orient 0
   }
  #--------------------Set default cycle delay-------------------------------------------------
   if { ![info exists mom_cycle_delay] } {
      set mom_cycle_delay 0.0
   }
  #-------------------Set default cycle top delay----------------------------------------------
   if { ![info exists mom_cycle_top_delay]} {
	  set mom_cycle_top_delay 0.0
   }
  #--------------------Set retract distance----------------------------------------------------
   if { [string match "AUTO" $mom_cycle_retract_mode] } {
      set js_return_pos [expr $js_prev_pos - $mom_pos(2)] ;# calc incr retract
      if { [EQ_is_lt $js_return_pos $mom_cycle_retract_to] } {
           set js_return_pos $mom_cycle_retract_to
      }
   } else {
      set js_return_pos $mom_cycle_retract_to
   }
  #--------------------Calculate thread pitch---------------------------------------------------
   if { ![string compare "tap_move" $mom_motion_event] ||\
        ![string compare "tap_deep_move" $mom_motion_event] ||\
        ![string compare "tap_float_move" $mom_motion_event] ||\
        ![string compare "tap_break_chip_move" $mom_motion_event] } {
      if { [info exists mom_tool_pitch] } {
         if { [info exists mom_cycle_thread_pitch] } {
             set dpp_TNC_cycle_thread_pitch $mom_cycle_thread_pitch

      } else {
            set dpp_TNC_cycle_thread_pitch $mom_tool_pitch
         }
         } else {
            #---------Warning---------
            MOM_display_message "$mom_operation_name: No pitch defined on the tool. Please use Tap tool. \n Post Processing will be aborted." "Postprocessor error message" "E"
            MOM_abort_program
         }
      if {![info exists mom_spindle_speed] || [EQ_is_zero $mom_spindle_speed] } {
         MOM_display_message "$mom_operation_name : spindle speed is 0. Post Processing will be aborted." "Postprocessor error message" "E"
         MOM_abort_program
      }
   }
  #--------------------Calculate thread pitch sign-----------------------------------------------
   if {[info exists mom_cycle_thread_right_handed]} {
      if { $mom_cycle_thread_right_handed == "FALSE" } {
          set dpp_TNC_cycle_thread_pitch  [expr $dpp_TNC_cycle_thread_pitch * (-1)]
      }
   } elseif { $mom_spindle_direction == "CCLW" } {
      set dpp_TNC_cycle_thread_pitch  [expr $dpp_TNC_cycle_thread_pitch * (-1)]
   }
  #--------------------Cycle feed rate multiply by 10 in INCH unit-----------------------------
   switch $mom_output_unit {
      IN {
          set dpp_TNC_cycle_feed [expr $feed*10]
      }
      MM {
          set dpp_TNC_cycle_feed $feed
      }
   }
  #--------------------Calculate tap feed------------------------------------------------------
   if { ![string compare "tap_float_move" $mom_motion_event] } {
      set dpp_TNC_cycle_tap_feed [expr abs($dpp_TNC_cycle_thread_pitch) * $mom_spindle_rpm]
   }
  #--------------------Calculate step clearance------------------------------------------------
   if { ![string compare "tap_deep_move" $mom_motion_event] } {
      set dpp_TNC_cycle_step_clearance 0
   }
   if { ![string compare "tap_break_chip_move" $mom_motion_event] } {
      if {[info exists mom_cycle_step_clearance]} {
         if { ![EQ_is_zero $mom_cycle_step_clearance] } {
            set dpp_TNC_cycle_step_clearance $mom_cycle_step_clearance
         } else {
            MOM_output_to_listing_device "$mom_operation_name: Step clearance is 0, please set a value!"
         }
      } else {
         MOM_output_to_listing_device "$mom_operation_name: No step clearance defined."
      }
   }
  #--------------------Calculate peck sizes-----------------------------------------------------
   global mom_tool_diameter
   global cycle_peck_size cycle_type_number
   global mom_operation_name
   set cycle_peck_size [expr ($mom_cycle_feed_to*(-1.0))]     ;# single peck size most cycles

   if { ![string compare "drill_deep_move" $mom_motion_event] ||\
        ![string compare "drill_break_chip_move" $mom_motion_event] } {

      if { ![ info exists mom_cycle_step1 ] || $mom_cycle_step1 == 0 } {
         set cycle_peck_size  [expr $mom_tool_diameter / 2]  ;# default peck  if not set
		 set msg "OPERATION:$mom_operation_name ВНИМАНИЕ! Не указан шаг глубины сверления!\nБудет установлено значение: $cycle_peck_size\n"
		 MOM_output_to_listing_device ">>>> $msg"
		 
		 MOM_display_message $msg  "WARNING Postprocessor message." "W"
      } else {
         set cycle_peck_size  $mom_cycle_step1    ;# real peck
      }
	  
	  set user_TNC_cycle_step_quantity [expr int(abs($mom_cycle_feed_to / $cycle_peck_size))]
	  #set user_TNC_cycle_step_quantity [expr abs($user_TNC_cycle_step_quantity)]
	  if { [expr abs(abs($mom_cycle_feed_to) - abs(($user_TNC_cycle_step_quantity * $mom_cycle_step1)))] > 0.0001} {
		incr user_TNC_cycle_step_quantity
		set user_TNC_cycle_step [expr abs($mom_cycle_feed_to / ($user_TNC_cycle_step_quantity)) ]
	  }
	  #MOM_output_to_listing_device ">>>2 mom_cycle_feed_to:$mom_cycle_feed_to//user_TNC_cycle_step_quantity:$user_TNC_cycle_step_quantity user_TNC_cycle_step:$user_TNC_cycle_step mom_cycle_step1:$mom_cycle_step1"
   }

  # Normally cycle_init_flag is only set if this is a new cycle
  # it is specifically unset in cycle_plane_change event, which
  # happens when a drilling operation goes uphill,
  # (drills a hole at a higher Z than the previous hole)
  # it is _not_ set  when drilling downhill.
  # this next bit of code sets the variable for up or downhill
  # so that the new hole is defined - this is absolutely required
  # to ensure the hole Z height Q203 is set correctly.

   if { $mom_pos(2) != $mom_prev_pos(2) } {
      set cycle_init_flag  "TRUE"
   }
   if { $dpp_ge(cycle_clearance_plane) == "FALSE" } {
      set mom_pos(2) $dpp_TNC_Q203_pos
   }
   set dpp_ge(cycle_clearance_plane) "TRUE"
 
  
 }

#=============================================================
proc USER_END_CYCLE {} {
#=============================================================
   global user_cycle_counter
   global mom_prev_cycle_feed_to
   catch {unset user_cycle_counter; unset mom_prev_cycle_feed_to; MOM_output_cycle "-reset"}
   
}

#=============================================================
proc USER_cycle_coord_rotation { } {
#=============================================================
#=============================================================
# Handle variable-axis drilling cycles using AUTO_3D function
#
#06-18-2012 Jason - Initial version
#10-18-2013 Jason - Behavior change: table axis will not return to zero if the tool vector turn to be along Z axis in cycle plane change.
  #MOM_output_to_listing_device ":>>> Foo USER_cycle_coord_rotation"
  global mom_kin_machine_type
  global save_mom_kin_machine_type
  global mom_tool_axis mom_tool_axis_type
  global mom_5axis_control_pos
  global mom_pos mom_mcs_goto
  global mom_prev_out_angle_pos mom_out_angle_pos
  global mom_prev_tool_axis
  global mom_cycle_rapid_to_pos mom_cycle_retract_to_pos mom_cycle_feed_to_pos
  global mom_cycle_rapid_to mom_cycle_retract_to mom_cycle_feed_to
  global mom_cycle_spindle_axis
  global mom_kin_machine_type
  global mom_cycle_retract_mode
  global js_return_pos js_prev_pos
  global dpp_ge
  global dpp_cycle_plane_real_change
  global seq
  global mom_out_angle_pos
  global dpp_TNC_plane_change_in_rapid
  global coord_rot_angle
  global mom_result
  global mom_kin_machine_type
  global user_coord_rotation_flag; #Mitiouk
  
  if { ![string match "*5_axis*" $mom_kin_machine_type] } { return }

  if {[info exists dpp_ge(ncm_work_plane_change_mode)] && \
      ($dpp_ge(ncm_work_plane_change_mode) != "None" && $dpp_ge(ncm_work_plane_change_mode) != "direct_change")} {
return
  }

  set user_coord_rotation_flag 1; #Mitiouk
  set dpp_ge(coord_rot) [DPP_GE_COOR_ROT "ZYX" coord_rot_angle coord_offset pos]

  if { $dpp_ge(coord_rot) == "NONE" } {
     if {![EQ_is_equal $mom_out_angle_pos(0) $mom_prev_out_angle_pos(0)] || ![EQ_is_equal $mom_out_angle_pos(1) $mom_prev_out_angle_pos(1)]} {
        MOM_output_literal "PLANE RESET STAY"
		#MOM_output_literal ";2"

        # if it's not auto3d condition, restore the kinematics and recalculate mom_pos
        DPP_GE_RESTORE_KINEMATICS
        if {"1" == [MOM_convert_point mom_mcs_goto mom_tool_axis]} {
           set i 0
           foreach value $mom_result {
                 set mom_pos($i) $value
                 incr i
           }
        }

        VMOV 3 mom_pos mom_cycle_feed_to_pos
        MOM_reload_variable -a mom_cycle_feed_to_pos
        MOM_reload_variable -a mom_pos

        MOM_enable_address fourth_axis fifth_axis
        MOM_force Once fourth_axis fifth_axis
        MOM_do_template rapid_rotary
     }
return
  }

  if { $dpp_ge(coord_rot) == "LOCAL_CSYS" } {
     if { ![EQ_is_equal $mom_tool_axis(2) 1.0] } {
        MOM_output_to_listing_device "Warning in $mom_operation_name: Wrong Local MCS, Z axis is not parallel to tool axis vector."
       }
return
  }

  if {![EQ_is_equal $dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)] || ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)] || ![EQ_is_equal $dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)]} {
     set dpp_cycle_plane_real_change 1
     set dpp_ge(coord_rot_angle,0) $coord_rot_angle(2)
     set dpp_ge(coord_rot_angle,1) $coord_rot_angle(1)
     set dpp_ge(coord_rot_angle,2) $coord_rot_angle(0)
     set dpp_ge(prev_coord_rot_angle,0)  $coord_rot_angle(2)
     set dpp_ge(prev_coord_rot_angle,1)  $coord_rot_angle(1)
     set dpp_ge(prev_coord_rot_angle,2)  $coord_rot_angle(0)
     VMOV 3  pos mom_pos
  } else {
     set dpp_cycle_plane_real_change 0
  }

  if {$dpp_cycle_plane_real_change ==1} {

     set mom_cycle_spindle_axis 2

     if {[EQ_is_lt $mom_out_angle_pos(0) 0]} {
        set seq "SEQ-"
     } else {
        set seq "SEQ+"
     }
	
     MOM_do_template plane_spatial;#Mitiouk
     MOM_disable_address fourth_axis fifth_axis
	 MOM_force Once X Y Z
  }

  if { $dpp_cycle_plane_real_change ==1 || $dpp_TNC_plane_change_in_rapid ==1 } {
     if { [string match "AUTO" $mom_cycle_retract_mode] } {
        set js_return_pos [expr $js_prev_pos - $mom_pos(2)] ;# calc incr retract
        if { [EQ_is_lt $js_return_pos $mom_cycle_retract_to] } {
           set js_return_pos $mom_cycle_retract_to
        }
     } else {
       set js_return_pos $mom_cycle_retract_to
     }

     VMOV 3 mom_pos mom_cycle_rapid_to_pos
     VMOV 3 mom_pos mom_cycle_feed_to_pos
     VMOV 3 mom_pos mom_cycle_retract_to_pos
     set mom_cycle_rapid_to_pos(2) [expr $mom_pos(2)+$js_return_pos]
     set mom_cycle_retract_to_pos(2) [expr $mom_pos(2)+$mom_cycle_retract_to]
     set mom_cycle_feed_to_pos(2) [expr $mom_pos(2)+$mom_cycle_feed_to]

     set mom_pos(0) [format %.4f "$mom_pos(0)"]
     set mom_pos(1) [format %.4f "$mom_pos(1)"]
     set mom_cycle_rapid_to_pos(2) [format %.4f "$mom_cycle_rapid_to_pos(2)"]

     MOM_force Once X Y Z
     #MOM_output_literal "L X$mom_pos(0) Y$mom_pos(1) Z$mom_cycle_rapid_to_pos(2) R0 FMAX"
	 #MOM_output_literal "L X$mom_pos(0) Y$mom_pos(1) Z$mom_cycle_rapid_to_pos(2) R0 FMAX"
  }
}

#=============================================================
proc USER_define_fixture_csys { } {
#=============================================================
#<Mitiouk 04-08-2019>
#MOM_output_literal ";Foo >>>USER_define_fixture_csys"
  global mom_ude_datum_option
  global IX IY IZ
  global mom_kin_coordinate_system_type
  global mom_fixture_offset_value
  global saved_mom_fixture_offset_value
  global dpp_TNC_local_offset_flag
  global dpp_TNC_reset_last_local_offset
  global dpp_TNC_fixture_origin ;#Fixture origin is calculated in PB_CMD_set_csys
  global dpp_ge
  #
  global user_csys_flag
  global user_prev_csys
 
 #MOM_output_literal ";>>>mom_fixture_offset_value:$mom_fixture_offset_value"
  if { ![info exists user_csys_flag] } {
    set user_csys_flag 1; set user_prev_csys $mom_fixture_offset_value
   } else {
    if { [EQ_is_equal $mom_fixture_offset_value $user_prev_csys] } {
     return
     }
   }

# 16-06-2019 <Mitiouk>
	#MOM_output_literal ";Foo>>>  mom_kin_coordinate_system_type:$mom_kin_coordinate_system_type"
  if { [PB_CMD__check_block_erowa_output_mode] } {
    if { [info exists mom_fixture_offset_value] } {
      if {$mom_fixture_offset_value !=4 } {
	    set result [MOM_display_message "Внимание! Номер СК не соответствует режиму обработки!" "Postprocessor Message" "W" "Продолжить" "Прервать"]
	    if {$result == 2} { MOM_abort }
	  }
	  if {$mom_kin_coordinate_system_type!="LOCAL"} {
		set result [MOM_display_message "Внимание! Тип СК \($mom_kin_coordinate_system_type \)не соответствует режиму обработки!" "Postprocessor Message" "W" "Продолжить" "Прервать"]
	    if {$result == 2} { MOM_abort }
	  }
    MOM_output_literal "CYCL DEF 247 Q339=+$mom_fixture_offset_value"
    return
  }
}
#

 # if { $dpp_TNC_local_offset_flag == 0 && $dpp_TNC_reset_last_local_offset == 0 } {
  #   if {[info exists saved_mom_fixture_offset_value] && $mom_fixture_offset_value == $saved_mom_fixture_offset_value} {
#return
 #    }
  #}

  #if { $dpp_TNC_local_offset_flag == 1 && $dpp_TNC_reset_last_local_offset == 0 } {
#return
 # }

#Cancel previous datum shift in local csys offset situation
 # if {[info exists saved_mom_fixture_offset_value] && $dpp_TNC_reset_last_local_offset == 1} {
    # MOM_output_literal "CYCL DEF 7.0"
    # MOM_output_literal "CYCL DEF 7.1 X+0.0"
    # MOM_output_literal "CYCL DEF 7.2 Y+0.0"
    # MOM_output_literal "CYCL DEF 7.3 Z+0.0"
 # }

  #set IX X; set IY Y; set IZ Z
  #if {[info exists mom_ude_datum_option]} {
   #  switch $mom_ude_datum_option {
    #    "CYCL 7 #" {
     #       MOM_output_literal "CYCL DEF 7.0"
      #      MOM_output_literal "CYCL DEF 7.1 \#$mom_fixture_offset_value"
       #     set IX IX; set IY IY; set IZ IZ
       # }
        #"CYCL 7 XYZ" {
         #   if {[info exists mom_kin_coordinate_system_type] && $mom_kin_coordinate_system_type != "MAIN"} {
          #MOM_output_literal "CYCL DEF 7.0"
           #MOM_output_literal "CYCL DEF 7.1 X$dpp_TNC_fixture_origin(0)"
            #MOM_output_literal "CYCL DEF 7.2 Y$dpp_TNC_fixture_origin(1)"
            #MOM_output_literal "CYCL DEF 7.3 Z$dpp_TNC_fixture_origin(2)"
            #set IX IX; set IY IY; set IZ IZ
            #}
        #}
       # "CYCL 247" {
            if {[info exists mom_kin_coordinate_system_type] && $mom_kin_coordinate_system_type != "MAIN"} {
            MOM_output_literal "CYCL DEF 247 Q339=+$mom_fixture_offset_value"
             #MOM_output_literal "Q339=+$mom_fixture_offset_value ; DATUM NUMBER"
            #set IX X; set IY Y; set IZ Z
            #MOM_output_literal "CYCL DEF 7.0 ; DATUM SHIFT"
            #MOM_output_literal "CYCL DEF 7.1 X+0.0"
            #MOM_output_literal "CYCL DEF 7.2 Y+0.0"
            #MOM_output_literal "CYCL DEF 7.3 Z+0.0"
            #MOM_output_literal "CYCL DEF 7.1 X$dpp_TNC_fixture_origin(0)"
            #MOM_output_literal "CYCL DEF 7.2 Y$dpp_TNC_fixture_origin(1)"
            #MOM_output_literal "CYCL DEF 7.3 Z$dpp_TNC_fixture_origin(2)"
            #set IX IX; set IY IY; set IZ IZ
          }
     #   }
      #  default {
       #    unset mom_ude_datum_option
    #    }
    # }
 # }

  #set saved_mom_fixture_offset_value $mom_fixture_offset_value



}

#=============================================================
proc MOM_mill_hole_move {} {
#=============================================================
 MOM_output_literal ";Sorry, this is NEW option. Send a support request."
 global cycle_init_flag


   ABORT_EVENT_CHECK

  global mom_user_reaming

   if { ![string compare $cycle_init_flag "TRUE"] } {
      PB_CMD_set_cycle_plane
      PB_CMD_cycle_coord_rotation
      PB_CMD_USER_CYCLE_COORDINATE_ROTATION
      PB_CMD_init_cycle
      #Here insert you cycle output
   }

   if { [llength [info commands PB_CMD_config_cycle_start]] } {
      PB_CMD_config_cycle_start
   }

   MOM_force Once G_motion
   MOM_do_template cycle_parameters

   if { [PB_CMD__check_block_cycle_rapid_to] } {
      MOM_force Once G_motion
      MOM_do_template cycle_parameters_1
   }

   if { [PB_CMD__check_block_tap_cycle] } {
      MOM_force Once M_spindle
      MOM_do_template cycle_parameters_2
   }
   set cycle_init_flag FALSE
}

# Add New cycle
#
#
#=============================================================
proc MOM_mill_hole {} {
#=============================================================
# <Mitiouk> 06-09-2019
# Cycle of mill_hole thread
#
#

 MOM_output_literal "HOLE Milling Cycle nust be here! На хер."
 global cycle_name
 global cycle_init_flag

   set cycle_init_flag TRUE
   set cycle_name HOLE_MILL
   CYCLE_SET

}


#=============================================================
proc MOM_mill_hole_thread {} {
#=============================================================
# <Mitiouk> 06-09-2019
# Cycle of mill_hole thread
#
#
 global cycle_name
 global cycle_init_flag

   set cycle_init_flag TRUE
   set cycle_name HOLE_THREAD_MILL
   CYCLE_SET
}

#=============================================================
proc MOM_mill_hole_thread_move {} {
#=============================================================
  MOM_output_literal ";Sorry, this is NEW option. Send a support request."
  global cycle_init_flag


   ABORT_EVENT_CHECK

  global mom_user_reaming

   if { ![string compare $cycle_init_flag "TRUE"] } {
      PB_CMD_set_cycle_plane
      PB_CMD_cycle_coord_rotation
      PB_CMD_USER_CYCLE_COORDINATE_ROTATION
      PB_CMD_init_cycle
      #Here insert you cycle output
   }

   if { [llength [info commands PB_CMD_config_cycle_start]] } {
      PB_CMD_config_cycle_start
   }

   MOM_force Once G_motion
   MOM_do_template cycle_parameters

   if { [PB_CMD__check_block_cycle_rapid_to] } {
      MOM_force Once G_motion
      MOM_do_template cycle_parameters_1
   }

   if { [PB_CMD__check_block_tap_cycle] } {
      MOM_force Once M_spindle
      MOM_do_template cycle_parameters_2
   }
   set cycle_init_flag FALSE
}


#=============================================================
proc USER_detect_local_offset {} {
#=============================================================

# This command is used to detect offset in local csys.
# Used in initial move and first move
#
# 06-18-2013 Jason - Initial version
# <04-08-2019> Mitiouk
  
  global mom_kin_coordinate_system_type
  global mom_parent_csys_matrix
  global mom_part_unit mom_output_unit
  global mom_fixture_offset_value
  global saved_mom_fixture_offset_value
  global dpp_ge
  global dpp_TNC_local_offset_flag ;#This variable is used to define whether it is needed to define the local csys and record the status of last local csys definition.
  global dpp_TNC_reset_last_local_offset ;#This variable is used to indicate whether it is needed to reset the last local offset
  global IX IY IZ
  #MOM_output_literal ";Foo >>> USER_detect_local_offset"
  set dpp_TNC_reset_last_local_offset 0
  set IX 0.0; set IY 0.0; set IZ 0.0

  if {[info exists mom_kin_coordinate_system_type] && ![string compare "CSYS" $mom_kin_coordinate_system_type]} {
#################################Calculate local offset########################
     if {[array exists mom_parent_csys_matrix]} {
        if {![string compare $mom_part_unit $mom_output_unit]} {
           set unit_conversion 1
        } elseif { ![string compare "IN" $mom_output_unit] } {
           set unit_conversion [expr 1.0/25.4]
        } else {
           set unit_conversion 25.4
        }
        set dpp_ge(coord_offset,0) [expr $unit_conversion*$mom_parent_csys_matrix(9)]
        set dpp_ge(coord_offset,1) [expr $unit_conversion*$mom_parent_csys_matrix(10)]
        set dpp_ge(coord_offset,2) [expr $unit_conversion*$mom_parent_csys_matrix(11)]
      } else {
        set dpp_ge(coord_offset,0) $mom_csys_origin(0)
        set dpp_ge(coord_offset,1) $mom_csys_origin(1)
        set dpp_ge(coord_offset,2) $mom_csys_origin(2)
     }
################################################################################
     if {![EQ_is_equal $dpp_ge(coord_offset,0) 0.0] ||\
         ![EQ_is_equal $dpp_ge(coord_offset,1) 0.0] ||\
         ![EQ_is_equal $dpp_ge(coord_offset,2) 0.0] } {
        if {![info exists saved_mom_fixture_offset_value] || ![EQ_is_equal $mom_fixture_offset_value $saved_mom_fixture_offset_value]} {
           set dpp_TNC_local_offset_flag 1
           set dpp_TNC_reset_last_local_offset 1
           set dpp_ge(prev_coord_offset,0) $dpp_ge(coord_offset,0)
           set dpp_ge(prev_coord_offset,1) $dpp_ge(coord_offset,1)
           set dpp_ge(prev_coord_offset,2) $dpp_ge(coord_offset,2)
		   set IX $dpp_ge(coord_offset,0); set IY $dpp_ge(coord_offset,1); set IZ $dpp_ge(coord_offset,2)
return
        }
        if {[EQ_is_equal $dpp_ge(prev_coord_offset,0) $dpp_ge(coord_offset,0)] &&\
            [EQ_is_equal $dpp_ge(prev_coord_offset,1) $dpp_ge(coord_offset,1)] &&\
            [EQ_is_equal $dpp_ge(prev_coord_offset,2) $dpp_ge(coord_offset,2)]} {
           set dpp_TNC_reset_last_local_offset 0
        } else {
           set dpp_TNC_local_offset_flag 1
           set dpp_TNC_reset_last_local_offset 1
           set dpp_ge(prev_coord_offset,0) $dpp_ge(coord_offset,0)
           set dpp_ge(prev_coord_offset,1) $dpp_ge(coord_offset,1)
           set dpp_ge(prev_coord_offset,2) $dpp_ge(coord_offset,2)
        }
     } else {
        if { $dpp_TNC_local_offset_flag==1 } {
           set dpp_TNC_local_offset_flag 0
           set dpp_TNC_reset_last_local_offset 1
           set dpp_ge(prev_coord_offset,0) $dpp_ge(coord_offset,0)
           set dpp_ge(prev_coord_offset,1) $dpp_ge(coord_offset,1)
           set dpp_ge(prev_coord_offset,2) $dpp_ge(coord_offset,2)
        }
     }
	set IX $dpp_ge(coord_offset,0); set IY $dpp_ge(coord_offset,1); set IZ $dpp_ge(coord_offset,2)
  }

  if {[info exists mom_kin_coordinate_system_type] && ![string compare "LOCAL" $mom_kin_coordinate_system_type]} {
     if { $dpp_TNC_local_offset_flag==1 } {
        set dpp_TNC_local_offset_flag 0
        set dpp_TNC_reset_last_local_offset 1
        set dpp_ge(prev_coord_offset,0) 0
        set dpp_ge(prev_coord_offset,1) 0
        set dpp_ge(prev_coord_offset,2) 0
     }
  }
}
#=============================================================

#****
#uplevel #0 {

  proc USER_HOME_POSITION {} {
#============================================================= 

  global mom_ude_x_home_position mom_ude_y_home_position mom_ude_z_home_position 
  global mom_sys_home_pos mom_ude_user_home_position
 
  if {![info exists mom_ude_user_home_position] || ![string match "ACTIVE" $mom_ude_user_home_position] } {
return 
}
  if {![info exists mom_ude_x_home_position] ||\
      ![info exists mom_ude_y_home_position] ||\
	  ![info exists mom_ude_z_home_position]} {
return
}
  
  set mom_sys_home_pos(0) $mom_ude_x_home_position
  set mom_sys_home_pos(1) $mom_ude_y_home_position
  set mom_sys_home_pos(2) $mom_ude_z_home_position  

  }
#}; #uplevel