lappend lib_config_data  [list "lib_sourcing" "LIB-Sourcing-Version 117"]
set lib_release_version "3.5.3"
#############################################################################################
#
#	Copyright 2014 Siemens Product Lifecycle Management Software Inc.
#				All Rights Reserved.
#
#############################################################################################

set lib_flag(load_ugpost_base)			0
set lib_shell(load_loader) 			0
set lib_shell(load_library) 			0
set lib_ge_tclversion 				[info tclversion]
if {[string match "library.tbc" [file tail [info script]]]} {
	set lib_shell(load_library) 		1
	set lib_ge_debug(library) [info script] ; set lib_config_datas $lib_config_data ; unset lib_config_data
	lappend lib_config_data  [list "library" "Library-Version [format %03d [string trimleft [lindex [lindex [lindex $lib_config_datas end] 1] 1] 0]]"]
}
if {![info exists lib_spf(clock,start)]} {
	if {$lib_ge_tclversion > 8.3} {
		set lib_spf(clock,start) 	[format %.01f [clock clicks -milliseconds]]
	} else {
		set lib_spf(clock,start) 	[format %.01f [clock clicks]]
	}
}

if {![info exists lib_ge_slash] && [string match "*windows*" $tcl_platform(platform)]} {set lib_ge_slash "\\"} else {set lib_ge_slash "/"}
#____________________________________________________________________________________________
# <Documentation>
#
# Handling for the default environment variables
#
# <Example>
#
#____________________________________________________________________________________________
if {[info commands LIB_Shell_environment_handling] != ""} {
	proc LIB_Shell_environment_handling {} {

		global lib_ge_env lib_ge_slash lib_ge_installed_machines

		# Language file and OS specific values
		if {![info exists lib_ge_slash]} {global lib_ge_slash}
		if {[string match "*windows*" $::tcl_platform(platform)]} {set lib_ge_slash "\\"} else {set lib_ge_slash "/"}

		if {![info exists lib_ge_env]} {global lib_ge_env}

		set lib_ge_env(post_pool_dir) 		[MOM_ask_env_var "UGII_POST_POOL_DIR_NG"]
		set lib_ge_env(cam_resource_dir) 	[MOM_ask_env_var "UGII_CAM_RESOURCE_DIR"]
		set lib_ge_env(cam_post_dir) 		[MOM_ask_env_var "UGII_CAM_POST_DIR"]
		set lib_ge_env(tmp_dir)                 [MOM_ask_env_var "UGII_TMP_DIR"]
		if {$lib_ge_env(tmp_dir) == ""} {set lib_ge_env(tmp_dir) $::env(tmp)}

		set lib_ge_installed_machines		[info script]

		regsub -all "/" $lib_ge_env(post_pool_dir) "\\" lib_ge_env(post_pool_dir)
		regsub -all "/" $lib_ge_env(cam_post_dir) "\\" lib_ge_env(cam_post_dir)
		regsub -all "/" [LIB_Shell_format_path_names [file dirname $lib_ge_installed_machines]]$lib_ge_slash "\\" lib_ge_env(installed_machines_dir)

		if {![regexp -nocase -- {\w+} $lib_ge_env(post_pool_dir)]} {set lib_ge_env(post_pool_dir) $lib_ge_env(installed_machines_dir)}

		LIB_Shell_path_init $lib_ge_env(installed_machines_dir)
		LIB_Shell_path_init $lib_ge_env(cam_post_dir)
		if {$lib_ge_env(installed_machines_dir) != $lib_ge_env(post_pool_dir)} {
			LIB_Shell_path_init $lib_ge_env(post_pool_dir)
		}

		set lib_ge_env(version_bit)  		[MOM_ask_env_var "UGII_VERSION_BIT"]
		set lib_ge_env(base_dir)     		[MOM_ask_env_var "UGII_BASE_DIR"]

		set lib_ge_env(major_version)  		[MOM_ask_env_var "UGII_MAJOR_VERSION"]
		set lib_ge_env(minor_version)     	[MOM_ask_env_var "UGII_MINOR_VERSION"]
		set lib_ge_env(subminor_version)     	[MOM_ask_env_var "UGII_SUBMINOR_VERSION"]

		if {![info exists lib_ge_dll_version]} {global lib_ge_dll_version}
		if {$lib_ge_env(minor_version) > 0} {
			set lib_ge_dll_version "NX$lib_ge_env(major_version)$lib_ge_env(minor_version)"
		} else {
			set lib_ge_dll_version "NX$lib_ge_env(major_version)"
		}

		regsub -all "/" $lib_ge_env(base_dir) "\\" lib_ge_env(base_dir)
	}
}

if {[info commands LIB_Shell_main] != ""} {
	proc LIB_Shell_main {} {

		global lib_ge_env lib_ge_slash
		global lib_cycle_path sourcefile

		LIB_Shell_environment_handling

		if {[info commands LIB_GE_source] == ""} {
			set lib_cycle_path 0
			set searchpath "$lib_ge_env(installed_machines_dir,recursively)"
			lappend searchpath $lib_ge_env(post_pool_dir)
			foreach path $searchpath {
				if {$lib_cycle_path} {break}
				foreach name {library lib_sourcing} {
					foreach ext {.tcl .pce .tbc} {
						set sourcefile [LIB_Shell_format_path_names "${path}${lib_ge_slash}$name$ext"]
						if {[LIB_Shell_file_exists $sourcefile]} {
							if {![string match "library.tbc" "$name$ext"] && [string match ".tbc" $ext]} {
								LIB_Shell_abort "ByteCode Loader is not available, cannot process $name$ext"
							}
							uplevel #0 {
								if {[catch {source $sourcefile} err]} {
									if {[info exists ::errorInfo]} {MOM_output_to_listing_device "errorInfo $errorInfo"}
									LIB_Shell_abort "File [file tail $sourcefile] $err not loadable"
								} else {
									set lib_ge_debug(lib_sourcing) "$sourcefile"
									lappend lib_ge_monitored_files "$sourcefile"
									lappend lib_ge_log_message "\n--> $sourcefile loaded"
									set lib_cycle_path 1 ; break
								}
							}
						}
					}
				}
			}
			if {!$lib_cycle_path} {
				LIB_Shell_abort "File lib_sourcing not found"
			}
		}
		LIB_Shell_init
	}
}

#___________________________________________________________________________________________
# <Documentation>
#
# Search directories recursively
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_search_file_recursively {pathname filename {value ""}} {

	global lib_ge_env lib_ge_slash

	upvar $value returnvalue

	set filename [LIB_Shell_format_path_names $pathname$lib_ge_slash$filename]
	if {[LIB_Shell_file_exists $filename 1]} {
		if {![string length $value]} {
			return $filename
		} else {
			set returnvalue $filename
			return 1
		}
	}


	set searchpath "$lib_ge_env(installed_machines_dir,recursively)"
	lappend searchpath $lib_ge_env(post_pool_dir)

	foreach pathnames $searchpath {
		set filenames [LIB_Shell_format_path_names $pathnames$lib_ge_slash[file tail $filename]]
		if {[LIB_Shell_file_exists $filenames 1]} {
			if {![string length $value]} {
				return $filenames
			} else {
				set returnvalue $filenames
				return 1
			}
		}
	}
	return 0
}

#____________________________________________________________________________________________
# <Documentation>
#
# Abort postrun
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_abort {message} {

	MOM_output_to_listing_device $message
	MOM_log_message $message

	MOM_abort $message

}

#____________________________________________________________________________________________
# <Documentation>
#
# Load ByteCodeLoader and Functions
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_load_bytecode {} {

	global lib_ge_env lib_ge_env lib_ge_slash
	global lib_ge_log_message lib_ge_nx_platform
	global lib_ge_monitored_files lib_ge_tclversion
	global lib_ge_ByteCodeLoader_msg

	# if loaded we don't need it again
	if {[regexp -nocase -- {tbcload} [package names]] && [info exists lib_ge_nx_platform]} {return}

	package forget tbcload

	if {$lib_ge_env(version_bit) == "32Bit" || [LIB_Shell_file_exists [file join "$lib_ge_env(base_dir)${lib_ge_slash}ugii${lib_ge_slash}msvcp71.dll"] 1]} {
		set lib_ge_nx_platform 32 ; # NX works with 32Bit
	} elseif {$lib_ge_env(version_bit) == "64Bit" || ![LIB_Shell_file_exists [file join "$lib_ge_env(base_dir)${lib_ge_slash}ugii${lib_ge_slash}msvcp71.dll"] 1]} {
		set lib_ge_nx_platform 64 ; # NX works with 64Bit
	}


	set check 0
	if {$lib_ge_nx_platform == 64} {
		if {$lib_ge_tclversion >= 8.4} {
			if {[LIB_Shell_search_file_recursively $lib_ge_env(post_pool_dir) tbcload17.dll loadfile]} {
				package ifneeded tbcload 1.7 [list load $loadfile] ; set check 1
			}
		} else {
			if {[LIB_Shell_search_file_recursively $lib_ge_env(post_pool_dir) tbcload14.dll loadfile]} {
				package ifneeded tbcload 1.4 [list load $loadfile] ; set check 1
			}
		}
		set lib_ge_ByteCodeLoader_msg "ByteCodeLoader [package versions tbcload] for TCLVersion $lib_ge_tclversion with 64Bit loaded"
	} elseif {$lib_ge_nx_platform == 32} {
		if {[LIB_Shell_search_file_recursively $lib_ge_env(post_pool_dir) tbcload13.dll loadfile]} {
				package ifneeded tbcload 1.3 [list load $loadfile] ; set check 1
		}
		set lib_ge_ByteCodeLoader_msg "ByteCodeLoader [package versions tbcload] for TCLVersion $lib_ge_tclversion with 32Bit loaded"
	}
	if {!$check} {
		set lib_ge_ByteCodeLoader_msg "ByteCodeLoader for TCLVersion $lib_ge_tclversion '${lib_ge_nx_platform}Bit' not found"
	}

}

#____________________________________________________________________________________________
# <Documentation>
#
# Load user function
#
# <Example>
#
# e.g. LIB_Shell_load_user_function "lib_execute_$lib_ge_dll_version" "JEDLL_NG_Execute"
#
#____________________________________________________________________________________________
proc LIB_Shell_load_user_function {{filename ""} {entrypoint ""}} {

	global lib_ge_env lib_ge_slash
	global lib_ge_execute_extension
	global lib_ge_nx_platform
	global lib_ge_user_function
	global lib_ge_monitored_files
	global lib_ge_log_message

	set searchfile	[LIB_Shell_search_file_recursively $lib_ge_env(post_pool_dir) $filename$lib_ge_execute_extension.dll]
	set file 	[LIB_Shell_format_path_names "$lib_ge_env(cam_post_dir)$lib_ge_slash$filename$lib_ge_execute_extension.dll"]

	if {[LIB_Shell_search_file_recursively $lib_ge_env(post_pool_dir) $filename$lib_ge_execute_extension.dll searchfile]} {
		MOM_run_user_function $searchfile "$entrypoint"
		lappend lib_ge_log_message "UserFunction $searchfile loaded"
		lappend lib_ge_user_function $searchfile
		lappend lib_ge_monitored_files $searchfile
	} elseif {[LIB_Shell_file_exists $file]} {
		MOM_run_user_function $file "$entrypoint"
		lappend lib_ge_log_message "UserFunction $file loaded"
		lappend lib_ge_user_function $file
		lappend lib_ge_monitored_files $file
	} else {
		LIB_Shell_abort "Could not find file: $lib_ge_env(post_pool_dir)$lib_ge_slash$filename$lib_ge_execute_extension.dll"
	}

}

#____________________________________________________________________________________________
# <Documentation>
#
# Exchange env variable
#
# <Example>
#
# e.g. set error [JEDLL_SET_VARIABLE UGII_POST_POOL_DIR $lib_ge_env(post_pool_dir)]
#
#____________________________________________________________________________________________
proc LIB_Shell_exchange_env {} {

	global lib_ge_env lib_shell lib_ge_env_init lib_ge_dll_version lib_ge_slash

	set lib_shell(status,controller) 1 ; set lib_shell(controller) [LIB_Shell_format_path_names "$lib_ge_env(installed_machines_dir)${lib_ge_slash}controller"]
	if {![file isdirectory $lib_shell(controller)]} {set lib_shell(status,controller) 0 ; set lib_shell(controller) $lib_ge_env(installed_machines_dir)}

	set lib_shell(status,libraries) 1 ; set lib_shell(libraries) [LIB_Shell_format_path_names "$lib_ge_env(installed_machines_dir)${lib_ge_slash}libraries"]
	if {![file isdirectory $lib_shell(libraries)]} {set lib_shell(status,libraries) 0 ; set lib_shell(libraries) $lib_ge_env(installed_machines_dir)}

	if {$lib_ge_env(major_version) < 9} {
		if {[info commands loader::setvariable] != ""} {
			set error [loader::setvariable UGII_LIB_POST_CURRENT_MASCH_DIR $lib_ge_env(installed_machines_dir)]
			set error [loader::setvariable POST_LIB_CURRENT_MACH_DIR $lib_ge_env(installed_machines_dir)]
			set error [loader::setvariable POST_LIB_CURRENT_CTRL_DIR $lib_shell(controller)]
			set error [loader::setvariable POST_LIB_CURRENT_LIB_DIR $lib_shell(libraries)]
			set error [loader::setvariable POST_SYS_CYCL_VERS "_$lib_ge_dll_version"]
		} elseif {[info commands JEDLL_SET_VARIABLE] != ""} {
			set error [JEDLL_SET_VARIABLE UGII_LIB_POST_CURRENT_MASCH_DIR $lib_ge_env(installed_machines_dir)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_LIB_CURRENT_MACH_DIR $lib_ge_env(installed_machines_dir)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_LIB_CURRENT_CTRL_DIR $lib_shell(controller)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_LIB_CURRENT_LIB_DIR $lib_shell(libraries)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_SYS_CYCL_VERS "_$lib_ge_dll_version"]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
		} else {
			LIB_Shell_abort "Cannot replace environment variable"
		}
	} else {
		if {[info commands loader::setvariable] != ""} {
			set error [loader::setvariable UGII_LIB_POST_CURRENT_MASCH_DIR $lib_ge_env(installed_machines_dir)]
			set error [loader::setvariable POST_LIB_CURRENT_MACH_DIR $lib_ge_env(installed_machines_dir)]
			set error [loader::setvariable POST_LIB_CURRENT_CTRL_DIR $lib_shell(controller)]
			set error [loader::setvariable POST_LIB_CURRENT_LIB_DIR $lib_shell(libraries)]
			set error [loader::setvariable POST_SYS_CYCL_VERS ""]
		} elseif {[info commands JEDLL_SET_VARIABLE] != ""} {
			set error [JEDLL_SET_VARIABLE UGII_LIB_POST_CURRENT_MASCH_DIR $lib_ge_env(installed_machines_dir)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_LIB_CURRENT_MACH_DIR $lib_ge_env(installed_machines_dir)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_LIB_CURRENT_CTRL_DIR $lib_shell(controller)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_LIB_CURRENT_LIB_DIR $lib_shell(libraries)]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			set error [JEDLL_SET_VARIABLE POST_SYS_CYCL_VERS ""]
			if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
		} elseif {[info command MOM_set_env_var] == "MOM_set_env_var"} {
			MOM_set_env_var POST_LIB_CURRENT_MACH_DIR $lib_ge_env(installed_machines_dir)
			MOM_set_env_var POST_LIB_CURRENT_CTRL_DIR $lib_shell(controller)
			MOM_set_env_var POST_LIB_CURRENT_LIB_DIR $lib_shell(libraries)
			MOM_set_env_var POST_SYS_CYCL_VERS ""
		} else {
			LIB_Shell_abort "Cannot replace environment variable"
		}

	}
}

#____________________________________________________________________________________________
# <Documentation>
#
# Load lib_general
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_load_loader {} {

	global lib_ge_env lib_ge_slash sourcefile
	global lib_cycle_loader loaderfile lib_shell

	if {[info commands LIB_GE_source] == "" && ($lib_ge_env(major_version) >= 9 || [string match "85" "$lib_ge_env(major_version)$lib_ge_env(minor_version)"] && $::tcl_version >= 8.4)} {

		if {[llength [info commands ::loader::load]]} {
			lappend lib_ge_log_message "Included LoaderFunction loaded"
			set lib_shell(load_loader) 1
			return
		}

		set searchpath "$lib_ge_env(installed_machines_dir,recursively)"
		lappend searchpath $lib_ge_env(post_pool_dir)

		foreach path $searchpath {

			set sourcefile [LIB_Shell_format_path_names "${path}${lib_ge_slash}loader.dll"]

			if {[LIB_Shell_file_exists $sourcefile]} {
				uplevel #0 {
					catch {
						load $sourcefile ; set loaderfile $sourcefile
						lappend lib_ge_log_message "LoaderFunction INS->$sourcefile<- loaded"
						lappend lib_ge_user_function "$sourcefile"
						lappend lib_ge_monitored_files "$sourcefile"
						set lib_shell(load_loader) 1
					}
				}
			}
		}
	}
}

#____________________________________________________________________________________________
# <Documentation>
#
# Load lib_general
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_load_general {} {

	global lib_ge_env lib_ge_slash lib_ge_debug
	global lib_cycle_path sourcefile errorInfo
	global lib_spf loaderfile

	set errorInfo ""

	if {[info commands LIB_GE_source] == ""} {
		set lib_cycle_path 0
		set searchpath "$lib_ge_env(installed_machines_dir,recursively)"
		lappend searchpath $lib_ge_env(post_pool_dir)

		foreach path $searchpath {
			if {$lib_cycle_path} {break}
			foreach ext {.pce .tbc .tcl} {
				set sourcefile [LIB_Shell_format_path_names "${path}${lib_ge_slash}lib_general$ext"]
				if {[LIB_Shell_file_exists $sourcefile]} {
					if {$ext == ".tbc" && [info exists ::lib_ge_ByteCodeLoader_msg]} {MOM_log_message $::lib_ge_ByteCodeLoader_msg ; unset ::lib_ge_ByteCodeLoader_msg}
					uplevel #0 {
						if {[string match ".pce" [file extension $sourcefile]]} {
							if {![llength [info commands ::loader::load]]} {
								LIB_Shell_abort "Loader is unavailable, check installation to execute encrypted files"
							}
							if {[catch {::loader::load [file nativename [file join $sourcefile]]}]} {
								LIB_Shell_abort "Loader is available, but does not supported this version"
							} ; set status 1
							set lib_ge_debug(lib_general) "$sourcefile"
							lappend lib_ge_monitored_files "$sourcefile"
							lappend lib_ge_log_message "\n--> $sourcefile loaded"
							lappend lib_ge_debug(sourced) $sourcefile
							set lib_cycle_path 1 ; break

						} elseif {![info exists status] && [catch {source $sourcefile} err]} {
							if {[info exists ::errorInfo]} {MOM_output_to_listing_device "errorInfo $errorInfo"}
							LIB_Shell_abort "File lib_general $err not loadable in section"

						} else {

							set lib_ge_debug(lib_general) "$sourcefile"
							lappend lib_ge_monitored_files "$sourcefile"
							lappend lib_ge_log_message "\n--> $sourcefile loaded"
							lappend lib_ge_debug(sourced) $sourcefile
							set lib_cycle_path 1 ; break

						}
					}
				}
			}
		}
		if {!$lib_cycle_path} {
			LIB_Shell_abort "File lib_general not found"
		}
	}

}

#____________________________________________________________________________________________
# <Documentation>
#
# Load lib_xml_handling
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_load_xml_handling {} {

	global lib_ge_env lib_ge_slash lib_ge_debug
	global lib_cycle_path sourcefile errorInfo
	global lib_shell

	set errorInfo ""

	if {!$lib_shell(load_library) && [info commands LIB_XML_xml_to_list] != "LIB_XML_xml_to_list"} {
		set lib_cycle_path 0
		set searchpath "$lib_ge_env(installed_machines_dir,recursively)"
		lappend searchpath $lib_ge_env(post_pool_dir)
		foreach path $searchpath {
			if {$lib_cycle_path} {break}
			foreach ext {.pce .tbc .tcl} {
				set sourcefile [LIB_Shell_format_path_names "${path}${lib_ge_slash}lib_xml_handling$ext"]
				if {[LIB_Shell_file_exists $sourcefile]} {
					if {$ext == ".tbc" && [info exists ::lib_ge_ByteCodeLoader_msg]} {MOM_log_message $::lib_ge_ByteCodeLoader_msg ; unset ::lib_ge_ByteCodeLoader_msg}
					uplevel #0 {

						if {[string match ".pce" [file extension $sourcefile]]} {

							if {[catch {::loader::load [file nativename [file join $sourcefile]]}]} {
								LIB_Shell_abort "Loader is available, but does not supported this version"
							} ; set status 1
							set lib_ge_debug(lib_xml_handling) "$sourcefile"
							lappend lib_ge_monitored_files "$sourcefile"
							lappend lib_ge_log_message "\n--> $sourcefile loaded"
							lappend lib_ge_debug(sourced) $sourcefile
							set lib_cycle_path 1 ; break
						} elseif {![info exists status] && [catch {LIB_GE_source_covered_with_arguments $sourcefile implicitly 0} err]} {
							if {[info exists ::errorInfo]} {MOM_output_to_listing_device "errorInfo $errorInfo"}
							LIB_Shell_abort "File lib_xml_handling '$err' not loadable in section"
						} else {
							set lib_ge_debug(lib_xml_handling) "$sourcefile"
							lappend lib_ge_monitored_files "$sourcefile"
							lappend lib_ge_log_message "\n--> $sourcefile loaded"
							lappend lib_ge_debug(sourced) $sourcefile
							set lib_cycle_path 1 ; break
						}
					}
				}
			}
		}
		if {!$lib_cycle_path} {
			LIB_Shell_abort "File lib_xml_handling not found"
		}
	}
}

#____________________________________________________________________________________________
# <Documentation>
#
# Load ugpost_base
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_load_ugpost_base {} {

	global lib_flag lib_ge_env lib_ge_slash
	global lib_cycle_path sourcefile lib_ge_debug

	if {!$lib_flag(load_ugpost_base)} {return}

	if {[info commands LIB_GE_source] == ""} {
		set lib_cycle_path 0
		set searchpath "$lib_ge_env(installed_machines_dir,recursively)"
		lappend searchpath $lib_ge_env(cam_post_dir)
		lappend searchpath $lib_ge_env(post_pool_dir)
		foreach path $searchpath {
			if {$lib_cycle_path} {break}
			foreach ext {.tbc .tcl} {
				set sourcefile [LIB_Shell_format_path_names "${path}${lib_ge_slash}ugpost_base$ext"]
				if {[LIB_Shell_file_exists $sourcefile]} {
					uplevel #0 {
						if {[catch {source $sourcefile} err]} {
							if {[info exists ::errorInfo]} {MOM_output_to_listing_device "errorInfo $errorInfo"}
							LIB_Shell_abort "File lib_general $err not loadable"
						} else {
							set lib_ge_debug(lib_general) "$sourcefile"
							lappend lib_ge_monitored_files "$sourcefile"
							lappend lib_ge_log_message "\n--> $sourcefile loaded"
							lappend lib_ge_debug(sourced) $sourcefile
							catch {unset mom_sys_leader}
							set lib_cycle_path 1 ; break
						}
					}
				}
			}
		}
		if {!$lib_cycle_path} {
			LIB_Shell_abort "File ugpost_base not found"
		}
	}
}

#____________________________________________________________________________________________
# <Documentation>
#
# Handling for the default environment variables
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_init {} {

	global lib_ge_user_function lib_ge_execute_extension
	global lib_ge_dll_version lib_ge_nx_platform lib_ge_env
	global lib_pp_source_file lib_ge_slash lib_shell
	global lib_load_user_function
	if {$lib_shell(load_library) && [info commands LIB_Shell_init_custom] != ""} {LIB_Shell_init_custom}
	if {$lib_shell(load_library) && [info commands LIB_Shell_load] != ""} {LIB_Shell_load}

	LIB_Shell_load_bytecode

	if {![info exists lib_ge_env(installed_machines_dir,recursively)]} {
		set lib_ge_env(installed_machines_dir,recursively) $lib_ge_env(installed_machines_dir)
	}

	set lib_ge_user_function ""
	set lib_ge_execute_extension "_32bit"
	if {$lib_ge_nx_platform == 64} {set lib_ge_execute_extension "_64bit"}

	# Available as basic functions from NX9, but optionally possible via lib_load_user_function
	if {$lib_ge_env(major_version) < 9 || [info exists lib_load_user_function] && $lib_load_user_function} {
		# Workaround, if inside NX8.5 TCL 8.4 as a preview active
		if {[string match "85" "$lib_ge_env(major_version)$lib_ge_env(minor_version)"] && $::tcl_version >= 8.4} {
			append lib_ge_execute_extension "_84"
		}
		LIB_Shell_load_user_function "lib_execute_$lib_ge_dll_version" "JEDLL_NG_Execute"
	}

	LIB_Shell_load_loader

	LIB_Shell_exchange_env

	LIB_Shell_load_ugpost_base

	LIB_Shell_load_general

	LIB_Shell_load_xml_handling

	LIB_Shell_defined_post_environment

	if {$lib_shell(load_library)} {
		set lib_pp_source_file [LIB_GE_cleanup_list [list "lib_general" "lib_msg" "lib_file_handling" "lib_standard_post_func" "lib_document"] $lib_pp_source_file 1]
	}

	if {[info commands LIB_GE_cleanup_list] != ""} {
		set lib_pp_source_file [LIB_GE_cleanup_list $lib_pp_source_file]
	} else {
		LIB_Shell_abort "Cannot run current post-processor, one or more files could not be loaded"
	}
	LIB_GE_source

}

#____________________________________________________________________________________________
# <Documentation>
#
# Read the sourcingfiles from an XML file
#
# <Example>
#
# <?xml version="1.0" encoding="utf-8"?>
# <Configuration>
#   <Copyright>Copyright © 2012-2015 Siemens Industry Software GmbH & Co. KG</Copyright>
#   <Version>1.0</Version>
#   <Controller>Siemens 840D</Controller>
#   <MachineName>MyMachine</MachineName>
#   <Environment>
#     <Variable Name="UGII_CAM_LIBRARY_INSTALLED_MACHINES_DEF_DIR" Target="D:\Temp\post_mit_kim"></Variable>
#   </Environment>
#   <Sourcing>
#     <Sequence>
#       <SubFolder Folder="Bin" />
#       <SubFolder Folder="Controller" />
#       <SubFolder Folder="Libraries" />
#       <Filename Name="ctrl_s840d_base" Processing="true" />
#       <Filename Name="post_mit_kim_mtb" Processing="true" />
#       <Filename Name="oem_my_oem" Processing="true" />
#       <Filename Name="mach_my_machine" Processing="true" />
#       <Filename Name="post_mit_kim_service" Processing="true" />
#       <Filename Name="post_mit_kim_custom" Processing="true" />
#     </Sequence>
#   </Sourcing>
#   <Metadata>
#     <Comment>
#       <Changes>Uwe Roosz</Changes>
#       <Date>2014/05/15 12:00:00</Date>
#       <Content>Auto-generated by aPB</Content>
#     </Comment>
#     <Customer>
#       <Company></Company>
#       <Address></Address>
#       <Contact></Contact>
#       <Phone></Phone>
#       <Fax></Fax>
#       <Mail></Mail>
#     </Customer>
#   </Metadata>
# </Configuration>
#
#____________________________________________________________________________________________
proc LIB_Shell_external_source {} {

	global lib_ge_installed_machines lib_xml_node_idx
	global lib_pp_source_file lib_pp_source_folder

	set sourcefile [LIB_Shell_format_path_names "[file rootname $lib_ge_installed_machines].psc"]
	if {![LIB_Shell_file_exists $sourcefile]} {return}

	if {[info commands LIB_XML_to_list] != ""} {
		set xmllist [LIB_XML_to_list $sourcefile]
	} else {
		# The current post-processor can not run, because not all files were loaded
		# This is also the case, if utility.pce not available
		return
	}

	# Read out all environment which are to be created
	set node [LIB_XML_get_node $xmllist Environment]

	set lib_xml_node_idx 0
	while	{$lib_xml_node_idx > -1} {
		set namelist [LIB_XML_get_nodes $node Variable]
		set name [lindex [lindex $namelist 1] [expr [lsearch -exact [lindex $namelist 1] "Name"]+1]]
		set target [lindex [lindex $namelist 1] [expr [lsearch -exact [lindex $namelist 1] "Target"]+1]]
		if {[string length $name] > 0} {
			if {[info commands MOM_set_env_var] != ""} {
				set error [MOM_set_env_var $name $target]
			} elseif {[info commands JEDLL_SET_VARIABLE] != ""} {
				set error [JEDLL_SET_VARIABLE $name $target]
				if {$error!=0} {MOM_log_message "ERROR JEDLL_SET_VARIABLE $::errorInfo $::error"}
			} else {
				MOM_log_message "Not possible to set environment variable"
			}
		}
	}

	# Read out all folder which are to be analysed
	set node [LIB_XML_get_node $xmllist Sourcing]
	set node [LIB_XML_get_node $node Sequence]

	set lib_xml_node_idx 0
	while	{$lib_xml_node_idx > -1} {
		set namelist [LIB_XML_get_nodes $node SubFolder]
		set name [lindex [lindex $namelist 1] [expr [lsearch -exact [lindex $namelist 1] "Folder"]+1]]
		if {[string length $name] > 0} {lappend lib_pp_source_folder $name}
	}

	# Read out all filenames which are to be sourced
	set node [LIB_XML_get_node $xmllist Sourcing]
	set node [LIB_XML_get_node $node Sequence]

	set lib_xml_node_idx 0
	while	{$lib_xml_node_idx > -1} {
		set namelist [LIB_XML_get_nodes $node Filename]
		set name [lindex [lindex $namelist 1] [expr [lsearch -exact [lindex $namelist 1] "Name"]+1]]
		set processing [lindex [lindex $namelist 1] [expr [lsearch -exact [lindex $namelist 1] "Processing"]+1]]
		# If processing "Name", the node Processing dosen't exists
		if {[string length $name] > 0 && ([string match "true" $processing] || [string match "Name" $processing])} {lappend lib_pp_source_file $name}
	}

}

set lib_flag(format_path_names,reference) ""
set lib_flag(format_path_names,compare) ""

#____________________________________________________________________________________________
# <Internal Documentation>
#
# This procedure may be used to format pathnames from unix format in windows
# format and delete double backslash
#
# Output:
# D:\Temp\mom\debug.out or \\Server\Temp\mom\debug.out
#
# e.g.
# set error [LIB_Shell_format_path_names "D:/Temp/mom/debug.out"]
#
# <Example>
# set error [LIB_Shell_format_path_names "D:/Temp/mom/debug.out"]
#____________________________________________________________________________________________
proc LIB_Shell_format_path_names {pathname} {

	global lib_flag tcl_platform

	# Store the information into the list to make the procedure a bit faster
	set reference [lsearch -exact $lib_flag(format_path_names,reference) $pathname]
	if {$reference < 0} {
		lappend lib_flag(format_path_names,reference) $pathname
	} else {
		return [lindex $lib_flag(format_path_names,compare) $reference]
	}

	if {[regexp -nocase -- {^[\\\\]|^[\/\/]} $pathname]} {
		regsub -all "/" $pathname "\\" pathname
	}

	if {[string match "*windows*" $tcl_platform(platform)]} {
		regsub -all "/" $pathname "\\" pathname
		if {![regexp -nocase -- {^\\\\} $pathname]} {set unc 0} else {set unc 1}
			 while {[regsub -- "(?q)\\\\" $pathname "\\" pathname]} {}
		if {$unc} {set pathname "\\$pathname"}
	} elseif {![string match "*windows*" $tcl_platform(platform)]} {
		regsub -all "\\\\" $pathname "/" pathname
		if {![regexp -nocase -- {^//} $pathname]} {set unc 0} else {set unc 1}
		while {[regsub -- "(?q)//" $pathname "/" pathname]} {}
		if {$unc} {set pathname "/$pathname"}
	}
	set returnvalue [string trim $pathname]
	if {$reference < 0} {
		lappend lib_flag(format_path_names,compare) $returnvalue
	}
	return $returnvalue

}

#____________________________________________________________________________________________
# <Internal Documentation>
#
# Workaround to handle 'file exists' in a faster way
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_file_exists {check {realtime 0} {reset 0}} {

	global lib_shell

	# TCL has an bug in the commands "file exists"
	# this requires at version 8.4 many times during execution
	if {[info tclversion] != 8.4} {
		return [file exists $check]
	}

	# Replaces the following functionality ; # return [file exists $check]
	set check [LIB_Shell_format_path_names $check]

	set lib_shell(path) 		[file dirname $check]
	set lib_shell(file) 		[file rootname [file tail $check]]
	set lib_shell(extension) 	[file extension $check]

	if {![string length $lib_shell(path)] || ![string length $lib_shell(file)] || ![string length $lib_shell(extension)]} {return 0}
	binary scan $lib_shell(path) H* pathbin

	# Query at loop, updated information is needed
	if {$realtime || [string match $pathbin [lindex [array get lib_shell check] 1]]} {
		return [file exists $check]
	}

	if {$reset && [llength [array get lib_shell $pathbin]]} {unset lib_shell($pathbin)}
	if {![llength [array get lib_shell $pathbin]]} {
		set lib_shell($pathbin) [string tolower [LIB_Shell_file_glob $lib_shell(path)]]
		if {$lib_shell($pathbin) == 0} {
			return 0
		}
	}

	set lib_shell(check) $pathbin

	if {[llength [array get lib_shell $pathbin]]} {
		if {[lsearch -exact [string tolower $lib_shell($pathbin)] [string tolower "$lib_shell(file)$lib_shell(extension)"]] > -1} {
			return 1
		}
	}
	return 0

}

#____________________________________________________________________________________________
# <Internal Documentation>
#
# Workaround to handle 'file isdirectory' in faster way
#
# <Example>
#
#____________________________________________________________________________________________
proc LIB_Shell_directory_exists {check {realtime 0}} {

	global lib_shell

	# Replaces the following functionality ; # return [file isdirectory "$check"]
	if {$realtime == 2} {
		return [file isdirectory $check]
	}

	set check [LIB_Shell_format_path_names $check]
	if {![regexp -nocase -- {^[\\\\]|^[A-Z][:][\\|\/]} $check]} {return 0}
	binary scan $check H* pathbin

	if {!$realtime && [info exists lib_shell($pathbin,unavailable)]} {
		return 0
	} elseif {$realtime || ![info exists lib_shell($pathbin)]} {
		set current_path [pwd]
		if {$realtime && [info exists lib_shell($pathbin)]} {unset lib_shell($pathbin)}
		if {[catch {cd $check}]} {
			if {[string length $pathbin] > 0} {
				lappend lib_shell($pathbin,unavailable) 1
			}
			return 0
		}
		cd $current_path
		lappend lib_shell($pathbin) 1
		return 1
	} elseif {[info exists lib_shell($pathbin)]} {
		return 1
	}
	return 0
}

#____________________________________________________________________________________________
# <Internal Documentation>
#
# Search glob directories
#
# Argument (1):
# path to search
#
# Argument (2):
# f = file
# p = path
# c = all
#
# <Example>
#
# set content [LIB_Shell_file_glob "D:/Temp/Test"]
# returns e.g.: Test Functions Librarys
#____________________________________________________________________________________________
proc LIB_Shell_file_glob {path {argument "file"}} {

	if {[string match "*windows*" $::tcl_platform(platform)]} {
		# Glob leaves TCL crash is some reson, depending on the file name (special characters)
		if {[regexp -nocase -- {^[\\]{2}|^[/]{2}} $path]} {
			# Reaction with unc directories
			switch -- $argument {
				f - file		{return [LIB_Shell_files_from_dir $path Files]}
				p - path		{MOM_output_to_listing_device "\n\nThe feature 'LIB_Shell_file_glob = p - path' is not yet implemented" ; LIB_Shell_abort "Another solution is needed"}
				r - pathrecursively	{MOM_output_to_listing_device "\n\nThe feature 'LIB_Shell_file_glob = r - pathrecursively' is not yet implemented" ; LIB_Shell_abort "Another solution is needed"}
				c - content 		{MOM_output_to_listing_device "\n\nThe feature 'LIB_Shell_file_glob = c - content' is not yet implemented" ; LIB_Shell_abort "Another solution is needed"}
			}
		} else {
			# Reaction with directories
			switch -- $argument {
				f - file		{return [LIB_Shell_files_from_dir $path "/a-d" "*.tcl *.tbc *.pce *.dll *.exe *.psc *.cdl *.def"]}
				p - path		{return result [LIB_Shell_files_from_dir $path "/ad"]}
				r - pathrecursively	{return result [LIB_Shell_files_from_dir $path "/ad" "/s"]}
				c - content 		{return result [LIB_Shell_files_from_dir $path "" "*"]}
			}
		}
	} else {
		if {![catch {
			switch -- $argument {
				f - file	{return [glob *.*]}
				p - path	{
							set all [glob *] ; set dirs ""
						 	foreach e $all {if {[file isdirectory $e]} {lappend dirs $e}}
						 	return $dirs
						}
				c - content 	{return [glob *]}
			}
		}]} else {
			return 0
		}
	}
}

#____________________________________________________________________________________________
# <Internal Documentation>
#
# Delete a file and watch if it's possible
# It's also return true, if the file not exist
#
# <Example>
#
# set content [LIB_Shell_file_delete "D:/Temp/Test.txt"]
# returns e.g.: Test Functions Librarys
#____________________________________________________________________________________________
proc LIB_Shell_file_delete {filename} {

	if {[catch {file delete -force $filename}]} {
		MOM_log_message "Can't delete file '$filename'"
		return 0
	} else {
		return 1
	}

}

#____________________________________________________________________________________________
# <Internal Documentation>
#
# Calling to ask a windows console with an dir command to search directory content
#
# <Internal Example>
# >> This function is a black box procedure <<
#____________________________________________________________________________________________
proc LIB_Shell_files_from_dir {path argument {extensions ""}} {

	global env tcl_platform dir_channel_list lib_ge_slash

	# Glob leaves TCL crash is some reson, depending on the file name (special characters)
	if {[string match "Files" $argument]} {

		set current_path [pwd]
		cd $env(TEMP)

		# Support for UNC
		set temppath [LIB_Shell_format_path_names "$env(TEMP)${lib_ge_slash}PostConfigurator${lib_ge_slash}[pid]"]
		if {![LIB_Shell_directory_exists $temppath]} {
			set error [file mkdir $temppath]
			set error [file attribute $temppath -hidden 1]
		}
		if {[string match "*unix*" $tcl_platform(platform)]} {
			set temppath $env(TMPDIR)
		}

		set transfername [LIB_Shell_format_path_names "$temppath${lib_ge_slash}FilesFromDir.tcl"]
		set checkname [LIB_Shell_format_path_names "$temppath${lib_ge_slash}FilesFromDir.check"]
		set batname [LIB_Shell_format_path_names "$temppath${lib_ge_slash}FilesFromDir.bat"]
		set vbsname [LIB_Shell_format_path_names "$temppath${lib_ge_slash}FilesFromDir.vbs"]

		LIB_Shell_file_delete $vbsname
		LIB_Shell_file_delete $batname
		LIB_Shell_file_delete $checkname
		LIB_Shell_file_delete $transfername

		lappend content "set VBSTEMP=$temppath"
		lappend content "set VBSNAME=$vbsname"
		lappend content "set BATNAME=$batname"
		lappend content "set TRANSFERNAME=$transfername"
		lappend content "del %TRANSFERNAME% > nul 2>&1"
		lappend content ""
		lappend content "@echo set VBSNAME=$vbsname"
		lappend content "@echo set objFSO = CreateObject(\"Scripting.FileSystemObject\")>%VBSNAME%"
		lappend content "@echo objStartFolder = \"[LIB_Shell_format_path_names $path]\">>%VBSNAME%"
		lappend content "@echo Set objFolder = objFSO.GetFolder(objStartFolder)>>%VBSNAME%"
		lappend content "@echo Set colFiles = objFolder.Files>>%VBSNAME%"
		lappend content "@echo For Each objFile in colFiles>>%VBSNAME%"
		lappend content "@echo \tWscript.Echo \"lappend response \{\" ^& objFile.Name ^& \"\}\">>%VBSNAME%"
		lappend content "@echo Next>>%VBSNAME%"
		lappend content "@echo \tWScript.Quit 1>>%VBSNAME%"
		lappend content ""
		lappend content "@echo off & setlocal"
		lappend content ""
		lappend content "cscript //NoLogo %VBSNAME% >\"$transfername\""
		lappend content "echo set fid \[open \{$checkname\} w+\] ; puts \$fid Finish ; flush \$fid ; close \$fid>>\"$transfername\""
		lappend content "del %VBSNAME% > nul 2>&1"
		lappend content ""
		lappend content "exit"

		set error [LIB_Shell_list_to_file $content $batname] ; set content ""

		set batchname "LongFileAnalysis"
		set batchvbs [LIB_Shell_format_path_names "$temppath${lib_ge_slash}$batchname.vbs"]
		set batchresult [LIB_Shell_format_path_names "$temppath${lib_ge_slash}$batchname.[pid]"]

		lappend content "WScript.CreateObject\( \"WScript.Shell\" \).Run \"\"\"$batname\"\"\",0,1"
		lappend content "Set objFSO = CreateObject\(\"Scripting.FileSystemObject\"\)"
		lappend content "Set objFile = objFSO.CreateTextFile\(\"$batchresult\"\)"
		lappend content "WScript.Quit 1"

		set error [LIB_Shell_list_to_file $content $batchvbs]

		set cmd [list exec $::env(COMSPEC) /c start {} $batchvbs &]
		set retval [catch $cmd output]

		LIB_Shell_holding_stack $batchresult
		LIB_Shell_holding_stack $transfername

		if {$retval} {LIB_GE_error_message "Execute silent..." "Unable to execute file 'INS->$batchvbs<-'"}

		LIB_Shell_file_delete $batchresult
		LIB_Shell_file_delete $batchvbs

		source $transfername
		LIB_Shell_holding_stack $checkname
		LIB_Shell_holding_stack $transfername

		LIB_Shell_file_delete $vbsname
		LIB_Shell_file_delete $batname
		LIB_Shell_file_delete $checkname
		LIB_Shell_file_delete $transfername

		cd $current_path

	} else {

		set current_path [pwd]

		if {[catch {cd [LIB_Shell_format_path_names $path]}]} {return 0}

		set channel [open "|cmd /c dir $argument $extensions /on /b"] ; after 10
		lappend dir_channel_list $channel

		while {![eof $channel]} {
			if {[gets $channel readin] != -1} {
				lappend response $readin
			}
		}
		catch {close $channel}
		cd $current_path

	}

	if {[info exists response]} {return $response} else {return ""}

}

#______________________________________________________________________________
#
# Wait until file exists
#
# e.g.
# LIB_Shell_holding_stack "C:\\Temp\\Test.txt"]
#______________________________________________________________________________
proc LIB_Shell_holding_stack {obj {loops 300} {wait 100} {abort 1}} {

	if {[regexp -nocase -- {^[\\]{2}|^[A-Z][:][\\|\/]} $obj]} {set file 1} else {set file 0}
	if {$loops < 0} {set loops [expr abs($loops)] ; set negate 1} else {set negate 0}
	for { set n 1 } { $n <= $loops } { incr n } {
		if {!$negate && !$file && [info exists $obj]} {
			return
		} elseif {!$negate && $file && [file exists $obj]} {
			return
		} elseif {$negate && !$file && ![info exists $obj]} {
			return
		} elseif {$negate && $file && ![file exists $obj]} {
			return
		}
		after $wait
	}

	if {$abort} {
		LIB_Shell_abort "Timeout at LIB_Shell_holding_stack '$obj'"
	}
}

#______________________________________________________________________________
#
# Write a list to a file
#
# e.g.
# set error [LIB_Shell_list_to_file $content "C:\\Temp\\Test.txt"]
#______________________________________________________________________________
proc LIB_Shell_list_to_file {list filename {action w+}} {

	catch {MOM_remove_file $filename}
	set fileid [open $filename $action]
	foreach e $list {
		puts $fileid $e
	}
	flush $fileid
	close $fileid

}

#____________________________________________________________________________________________
# <Internal Documentation>
#
# Search directories recursively
#
# <Example>
#
# set directories [LIB_FH_search_directories_recursively "D:/Temp/Test"]
# returns: D:/Temp/Test D:/Temp/Test/Functions D:/Temp/Test/Librarys D:/Temp/Test/Librarys/Test
#____________________________________________________________________________________________
proc LIB_Shell_path_init {{folder .}} {

	global lib_shell

	set directories [LIB_Shell_format_path_names $folder]

	if {[string match "*windows*" $::tcl_platform(platform)]} {
		set dirs [LIB_Shell_file_glob $directories r]
		foreach name $dirs {
			set name [LIB_Shell_format_path_names $name]
			binary scan $name H* pathbin
			set lib_shell($pathbin) [string tolower $name]
		}
	} else {
		set dirs "" ; lappend dirs $directories ; set initial $dirs ; set initialdir "" ; lappend initialdir [pwd]
		while {[llength $dirs]} {
			set name [lindex $dirs 0]
			if {[info tclversion] > 8.3} {
		    		set dirs [concat [glob -nocomplain -directory [lindex $dirs 0] -type d *] [lrange $dirs 1 end]]
			} else {
				set cd [LIB_Shell_file_glob [lindex $dirs 0] d] ; if {$cd != 0} {lappend directories [lindex $dirs 0] ; set dirs [concat [lrange $dirs 1 end]] ; continue} else {set all [LIB_Shell_file_glob [lindex $dirs 0] c]}
				foreach e $all {if {[file isdirectory [file join [lindex $dirs 0] $e]]} {lappend dirs [file join [lindex $dirs 0] $e]}}
				set dirs [concat [lrange $dirs 1 end]]
			}
			set current_dir [pwd]
			set pathname [LIB_Shell_format_path_names $name]
			binary scan $pathname H* pathbin
			set content [LIB_Shell_file_glob $path]
			if {$content != 0} {
				set lib_shell($pathbin) [string tolower $content]
				cd $current_dir
			}
		}
		cd [lindex $initialdir 0]
	}
}

#____________________________________________________________________________________________
# <Documentation>
#
# This procedure can be called whenever a textoutput need to be checked for special characters.
#
# The options are:
#	0 = direct output (default)
#	1 = replace special characters to international spelling
#	2 = replace special characters to html
#	3 = replace UTF-8 to Unicode
#
# <Example>
# set text [LIB_Shell_replace_special_characters $text]
#____________________________________________________________________________________________
proc LIB_Shell_replace_special_characters {text {special 0}} {

	if {$special == 1} {
		regsub -all {1º} $text "1"    	   text
		regsub -all {2º} $text "2"    	   text
		regsub -all {3º} $text "3"    	   text
		regsub -all {ä}  $text "ae"   	   text
		regsub -all {Ä}  $text "AE"   	   text
		regsub -all {ö}  $text "oe"   	   text
		regsub -all {Ö}  $text "OE"   	   text
		regsub -all {ü}  $text "ue"   	   text
		regsub -all {Ü}  $text "UE"   	   text
		regsub -all {Ø}  $text "D"    	   text
		regsub -all {ø}  $text "D"    	   text
		regsub -all {°}  $text "D"    	   text
		regsub -all {ß}  $text "ss"   	   text
		regsub -all {\t} $text "    " 	   text
		regsub -all {à}  $text "a"    	   text
		regsub -all {À}  $text "A"    	   text
		regsub -all {â}  $text "a"    	   text
		regsub -all {Â}  $text "A"    	   text
		regsub -all {ô}  $text "o"    	   text
		regsub -all {Ô}  $text "O"    	   text
		regsub -all {ç}  $text "c"    	   text
		regsub -all {Ç}  $text "C"    	   text
		regsub -all {é}  $text "e"    	   text
		regsub -all {É}  $text "E"    	   text
		regsub -all {è}  $text "e"    	   text
		regsub -all {È}  $text "E"    	   text
		regsub -all {ê}  $text "e"    	   text
		regsub -all {Ê}  $text "E"    	   text
		regsub -all {ï}  $text "i"    	   text
		regsub -all {Ï}  $text "I"    	   text
		regsub -all {ù}  $text "u"    	   text
		regsub -all {Ù}  $text "U"    	   text
		regsub -all {\"}  $text "Inch"     text
		regsub -all {\$}  $text "Dollar"   text
	} elseif {$special == 2} {
		regsub -all "¡" $text "\\&iexcl;"  text
		regsub -all "¢" $text "\\&cent;"   text
		regsub -all "£" $text "\\&pound;"  text
		regsub -all "¤" $text "\\&curren;" text
		regsub -all "¥" $text "\\&yen;"    text
		regsub -all "¦" $text "\\&brvbar;" text
		regsub -all "§" $text "\\&sect;"   text
		regsub -all "¨" $text "\\&uml;"    text
		regsub -all "©" $text "\\&copy;"   text
		regsub -all "ª" $text "\\&ordf;"   text
		regsub -all "«" $text "\\&laquo;"  text
		regsub -all "¬" $text "\\&not;"    text
		regsub -all "­" $text "\\&shy;"    text
		regsub -all "®" $text "\\&reg;"    text
		regsub -all "¯" $text "\\&macr;"   text
		regsub -all "°" $text "\\&deg;"    text
		regsub -all "±" $text "\\&plusmn;" text
		regsub -all "²" $text "\\&sup2;"   text
		regsub -all "³" $text "\\&sup3;"   text
		regsub -all "´" $text "\\&acute;"  text
		regsub -all "µ" $text "\\&micro;"  text
		regsub -all "¶" $text "\\&para;"   text
		regsub -all "·" $text "\\&middot;" text
		regsub -all "¸" $text "\\&cedil;"  text
		regsub -all "¹" $text "\\&sup1;"   text
		regsub -all "º" $text "\\&ordm;"   text
		regsub -all "»" $text "\\&raquo;"  text
		regsub -all "¼" $text "\\&frac14;" text
		regsub -all "½" $text "\\&frac12;" text
		regsub -all "¾" $text "\\&frac34;" text
		regsub -all "¿" $text "\\&iquest;" text
		regsub -all "À" $text "\\&Agrave;" text
		regsub -all "Á" $text "\\&Aacute;" text
		regsub -all "Â" $text "\\&Acirc;"  text
		regsub -all "Ã" $text "\\&Atilde;" text
		regsub -all "Ä" $text "\\&Auml;"   text
		regsub -all "Å" $text "\\&Aring;"  text
		regsub -all "Æ" $text "\\&AElig;"  text
		regsub -all "Ç" $text "\\&Ccedil;" text
		regsub -all "È" $text "\\&Egrave;" text
		regsub -all "É" $text "\\&Eacute;" text
		regsub -all "Ê" $text "\\&Ecirc;"  text
		regsub -all "Ë" $text "\\&Euml;"   text
		regsub -all "Ì" $text "\\&Igrave;" text
		regsub -all "Í" $text "\\&Iacute;" text
		regsub -all "Î" $text "\\&Icirc;"  text
		regsub -all "Ï" $text "\\&Iuml;"   text
		regsub -all "Ð" $text "\\&ETH;"    text
		regsub -all "Ñ" $text "\\&Ntilde;" text
		regsub -all "Ò" $text "\\&Ograve;" text
		regsub -all "Ó" $text "\\&Oacute;" text
		regsub -all "Ô" $text "\\&Ocirc;"  text
		regsub -all "Õ" $text "\\&Otilde;" text
		regsub -all "Ö" $text "\\&Ouml;"   text
		regsub -all "×" $text "\\&times;"  text
		regsub -all "Ø" $text "\\&Oslash;" text
		regsub -all "Ù" $text "\\&Ugrave;" text
		regsub -all "Ú" $text "\\&Uacute;" text
		regsub -all "Û" $text "\\&Ucirc;"  text
		regsub -all "Ü" $text "\\&Uuml;"   text
		regsub -all "Ý" $text "\\&Yacute;" text
		regsub -all "Þ" $text "\\&THORN;"  text
		regsub -all "ß" $text "\\&szlig;"  text
		regsub -all "à" $text "\\&agrave;" text
		regsub -all "á" $text "\\&aacute;" text
		regsub -all "â" $text "\\&acirc;"  text
		regsub -all "ã" $text "\\&atilde;" text
		regsub -all "ä" $text "\\&auml;"   text
		regsub -all "å" $text "\\&aring;"  text
		regsub -all "æ" $text "\\&aelig;"  text
		regsub -all "ç" $text "\\&ccedil;" text
		regsub -all "è" $text "\\&egrave;" text
		regsub -all "é" $text "\\&eacute;" text
		regsub -all "ê" $text "\\&ecirc;"  text
		regsub -all "ë" $text "\\&euml;"   text
		regsub -all "ì" $text "\\&igrave;" text
		regsub -all "í" $text "\\&iacute;" text
		regsub -all "î" $text "\\&icirc;"  text
		regsub -all "ï" $text "\\&iuml;"   text
		regsub -all "ð" $text "\\&eth;"    text
		regsub -all "ñ" $text "\\&ntilde;" text
		regsub -all "ò" $text "\\&ograve;" text
		regsub -all "ó" $text "\\&oacute;" text
		regsub -all "ô" $text "\\&ocirc;"  text
		regsub -all "õ" $text "\\&otilde;" text
		regsub -all "ö" $text "\\&ouml;"   text
		regsub -all "÷" $text "\\&divide;" text
		regsub -all "ø" $text "\\&oslash;" text
		regsub -all "ù" $text "\\&ugrave;" text
		regsub -all "ú" $text "\\&uacute;" text
		regsub -all "û" $text "\\&ucirc;"  text
		regsub -all "ü" $text "\\&uuml;"   text
		regsub -all "ý" $text "\\&yacute;" text
		regsub -all "þ" $text "\\&thorn;"  text
		regsub -all "ÿ" $text "\\&yuml;"   text
	} elseif {$special == 3} {
		regsub -all "ô" $text "Ã´"    text
		regsub -all "š" $text "Å¡"    text
		regsub -all "¤" $text "Â¤"    text
		regsub -all "ö" $text "Ã¶"    text
		regsub -all "Þ" $text "Å¢"    text
		regsub -all "¦" $text "Â¦"    text
		regsub -all "÷" $text "Ã·"    text
		regsub -all "þ" $text "Å£"    text
		regsub -all "§" $text "Â§"    text
		regsub -all "ú" $text "Ãº"    text
		regsub -all "¨" $text "Â¨"    text
		regsub -all "ü" $text "Ã¼"    text
		regsub -all "©" $text "Â©"    text
		regsub -all "ý" $text "Ã½"    text
		regsub -all "Ù" $text "Å®"    text
		regsub -all "«" $text "Â«"    text
		regsub -all "Ã" $text "Ä‚"    text
		regsub -all "ù" $text "Å¯"    text
		regsub -all "¬" $text "Â¬"    text
		regsub -all "ã" $text "Äƒ"    text
		regsub -all "Û" $text "Å°"    text
		regsub -all "¥" $text "Ä„"    text
		regsub -all "û" $text "Å±"    text
		regsub -all "®" $text "Â®"    text
		regsub -all "¹" $text "Ä..."  text
		regsub -all "°" $text "Â°"    text
		regsub -all "Æ" $text "Ä†"    text
		regsub -all "Ÿ" $text "Åº"    text
		regsub -all "±" $text "Â±"    text
		regsub -all "æ" $text "Ä‡"    text
		regsub -all "¯" $text "Å»"    text
		regsub -all "´" $text "Â´"    text
		regsub -all "È" $text "ÄŒ"    text
		regsub -all "¿" $text "Å¼"    text
		regsub -all "µ" $text "Âµ"    text
		regsub -all "è" $text "Ä?"    text
		regsub -all "Ž" $text "Å½"    text
		regsub -all "¶" $text "Â¶"    text
		regsub -all "Ï" $text "ÄŽ"    text
		regsub -all "ž" $text "Å¾"    text
		regsub -all "·" $text "Â·"    text
		regsub -all "ï" $text "Ä?"    text
		regsub -all "¡" $text "Ë‡"    text
		regsub -all "¸" $text "Â¸"    text
		regsub -all "Ð" $text "Ä?"    text
		regsub -all "¢" $text "Ë~"    text
		regsub -all "»" $text "Â»"    text
		regsub -all "ð" $text "Ä‘"    text
		regsub -all "ÿ" $text "Ë™"    text
		regsub -all "Á" $text "Ã?"    text
		regsub -all "Ê" $text "Ä~"    text
		regsub -all "²" $text "Ë›"    text
		regsub -all "Â" $text "Ã‚"    text
		regsub -all "ê" $text "Ä™"    text
		regsub -all "½" $text "Ë?"    text
		regsub -all "Ä" $text "Ã„"    text
		regsub -all "Ì" $text "Äš"    text
		regsub -all "–" $text "â€“"   text
		regsub -all "Ç" $text "Ã‡"    text
		regsub -all "ì" $text "Ä›"    text
		regsub -all "—" $text "â€”"   text
		regsub -all "É" $text "Ã‰"    text
		regsub -all "Å" $text "Ä¹"    text
		regsub -all "‘" $text "â€~"   text
		regsub -all "Ë" $text "Ã‹"    text
		regsub -all "å" $text "Äº"    text
		regsub -all "’" $text "â€™"   text
		regsub -all "Í" $text "Ã?"    text
		regsub -all "¼" $text "Ä½"    text
		regsub -all "‚" $text "â€š"   text
		regsub -all "Î" $text "ÃŽ"    text
		regsub -all "¾" $text "Ä¾"    text
		regsub -all "“" $text "â€œ"   text
		regsub -all "Ó" $text "Ã“"    text
		regsub -all "£" $text "Å?"    text
		regsub -all "”" $text "â€?"   text
		regsub -all "Ô" $text "Ã”"    text
		regsub -all "³" $text "Å‚"    text
		regsub -all "„" $text "â€ž"   text
		regsub -all "Ö" $text "Ã–"    text
		regsub -all "Ñ" $text "Åƒ"    text
		regsub -all "†" $text "â€"    text
		regsub -all "×" $text "Ã—"    text
		regsub -all "ñ" $text "Å„"    text
		regsub -all "‡" $text "â€¡"   text
		regsub -all "Ú" $text "Ãš"    text
		regsub -all "Ò" $text "Å‡"    text
		regsub -all "•" $text "â€¢"   text
		regsub -all "Ü" $text "Ãœ"    text
		regsub -all "ò" $text "Åˆ"    text
		regsub -all "Ý" $text "Ã?"    text
		regsub -all "Õ" $text "Å?"    text
		regsub -all "‰" $text "â€°"   text
		regsub -all "ß" $text "ÃŸ"    text
		regsub -all "õ" $text "Å‘"    text
		regsub -all "‹" $text "â€¹"   text
		regsub -all "á" $text "Ã¡"    text
		regsub -all "À" $text "Å”"    text
		regsub -all "›" $text "â€º"   text
		regsub -all "â" $text "Ã¢"    text
		regsub -all "à" $text "Å•"    text
		regsub -all "€" $text "â‚¬"   text
		regsub -all "ä" $text "Ã¤"    text
		regsub -all "Ø" $text "Å ~"   text
		regsub -all "™" $text "â„¢"   text
		regsub -all "ç" $text "Ã§"    text
		regsub -all "ø" $text "Å™"    text
		regsub -all "é" $text "Ã©"    text
		regsub -all "Œ" $text "Åš"    text
		regsub -all "ë" $text "Ã«"    text
		regsub -all "œ" $text "Å›"    text
		regsub -all "í" $text "Ã-"    text
		regsub -all "ª" $text "Åž"    text
		regsub -all "î" $text "Ã®"    text
		regsub -all "º" $text "ÅŸ"    text
		regsub -all "ó" $text "Ã³"    text
		regsub -all "Š" $text "Å"     text
	} elseif {$special == 4} {
		regsub -all {ä}  $text "ae"   text
		regsub -all {Ä}  $text "AE"   text
		regsub -all {ö}  $text "oe"   text
		regsub -all {Ö}  $text "OE"   text
		regsub -all {ü}  $text "ue"   text
		regsub -all {Ü}  $text "UE"   text
		regsub -all {ß}  $text "ss"   text
		regsub -all {à}  $text "a"    text
		regsub -all {À}  $text "A"    text
		regsub -all {â}  $text "a"    text
		regsub -all {Â}  $text "A"    text
		regsub -all {ô}  $text "o"    text
		regsub -all {Ô}  $text "O"    text
		regsub -all {ç}  $text "c"    text
		regsub -all {Ç}  $text "C"    text
		regsub -all {é}  $text "e"    text
		regsub -all {É}  $text "E"    text
		regsub -all {è}  $text "e"    text
		regsub -all {È}  $text "E"    text
		regsub -all {ê}  $text "e"    text
		regsub -all {Ê}  $text "E"    text
		regsub -all {ï}  $text "i"    text
		regsub -all {Ï}  $text "I"    text
		regsub -all {ù}  $text "u"    text
		regsub -all {Ù}  $text "U"    text
	}

	return $text

}
