 global mom_pos
 global mom_out_angle_pos mom_prev_out_angle_pos
 global mom_kin_retract_plane
 global mom_post_in_simulation
 global traori_C_1          traori_C_2
 global mom_pos             mom_prev_pos


 set traori_C_1 $mom_out_angle_pos(1)

 if { $traori_C_1 >= 0.0 } { set cond_1 1} else { set cond_1 -1}
 if { $traori_C_2 >= 0.0 } { set cond_2 1} else { set cond_2 -1}

 if { $traori_C_2 != 0.0 } {
     if { $cond_1 != $cond_2 } {
         # ------------------------------------------ If C=180.0 CHECKING ---------------------------------------------
          if { $traori_C_1 < 0.0 } { set L_1 [expr int(360.0 + $traori_C_1)] } else { set L_1 [expr int($traori_C_1)] }
          if { $traori_C_2 < 0.0 } { set L_2 [expr int(360.0 + $traori_C_2)] } else { set L_2 [expr int($traori_C_2)] }

          if { [expr abs(abs($L_1) - abs($L_2))] <= 180.0 } { set invers  1 } else { set invers -1 }

          if { $traori_C_1 > $traori_C_2 } {
              if { $invers == 1 } { set direction  "CW" } else { set direction "CCW" }
          } else {
              if { $invers == 1 } { set direction "CCW" } else { set direction  "CW" }
          }
         # ------------------------------------------------------------------------------------------------------------
         if { $invers == 1 } {
             MOM_output_literal "; == REPOSITION START =="
             #MOM_output_literal "SUPA G0 Z=_Z_HOME"

             if {$cond_1 == 1} {
                 # --- REPOSITIONING CCW ---
                 if { $mom_post_in_simulation == "CSE" } {
                     set DZ_G1 [expr  1*10                    *sin(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     set DY_G1 [expr -1*10                    *cos(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     set DZ_G0 [expr  1*$mom_kin_retract_plane*sin(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     set DY_G0 [expr -1*$mom_kin_retract_plane*cos(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     MOM_output_literal "G1 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G1]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) + $DZ_G1]] "0"] F1000"
                     MOM_output_literal "G0 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G0]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) + $DZ_G0]] "0"]"
                     MOM_do_template trafoof
                     MOM_output_literal "G0 C270."
                     MOM_output_literal "G0 C0."
                     MOM_output_literal "G0 C90."
                 }

                 MOM_output_literal "G0 C[string trimright [format %5.3f $mom_out_angle_pos(1)] "0"]"
                 MOM_do_template traori
                 MOM_output_literal "G0 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G0]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) + $DZ_G0]] "0"]"
                 MOM_output_literal "G1 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G1]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) + $DZ_G1]] "0"] F1000"

             } else {
                 # --- REPOSITIONING  CW ---
                 if { $mom_post_in_simulation == "CSE" } {
                     set DZ_G1 [expr  1*10                    *sin(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     set DY_G1 [expr -1*10                    *cos(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     set DZ_G0 [expr  1*$mom_kin_retract_plane*sin(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     set DY_G0 [expr -1*$mom_kin_retract_plane*cos(3.14159*(90-abs($mom_out_angle_pos(1)))/180)]
                     MOM_output_literal "G1 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G1]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) - $DZ_G1]] "0"] F1000"
                     MOM_output_literal "G0 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G0]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) - $DZ_G0]] "0"]"
                     MOM_do_template trafoof
                     MOM_output_literal "G0 C90."
                     MOM_output_literal "G0 C0."
                     MOM_output_literal "G0 C270."
                 }
                 MOM_output_literal "G0 C[string trimright [format %5.3f $mom_out_angle_pos(1)] "0"]"
                 MOM_do_template traori
                 MOM_output_literal "G0 Y[string trimright [format %5.3f [expr $mom_pos(1) + $DY_G1]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) - $DZ_G1]] "0"]"
                 MOM_output_literal "G1 Y[string trimright [format %5.3f [expr $mom_pos(1) +      0]] "0"] Z[string trimright [format %5.3f [expr $mom_pos(2) -      0]] "0"] F1000"
             }

             MOM_output_literal "; == REPOSITION   END =="
         }
     }
 }
 set traori_C_2 $traori_C_1

 MOM_disable_address G_plane

 # -------------------------------------------------------------------------------