/*
This software and related documentation are proprietary to Siemens Product Lifecycle Management Software Inc.

(c) 2009 Siemens Product Lifecycle Management Software Inc. All Rights Reserved. 

All trademarks belong to their respective holders.
*/
var txt_noBookmarks="No bookmarks available.";
var txt_refresh="Refresh";
var txt_delete="Delete";
var txt_deleteAll="Delete All";
var txt_bookmarks="Bookmarks";
var txt_bookmarkedPage="Bookmarked page";
var txt_pageAlreadyBookmarked="This page is already bookmarked.";
var txt_notValidPage="Not a valid page.";
var txt_cookieNotSupported="Your browser does not appear to have Cookies enabled, please check your browser settings.";
var txt_confirmDeleteAllCookies="Are you sure you want to delete all your bookmarks?";
var txt_noIndexDocument="There are no documents associated with this topic. Please select a subtopic.";
var txt_wasThisUseful="Was this information useful?";
var txt_howYes="Please tell us how this information was useful:";
var txt_howNo="Please tell us how we can make this information more useful:";
var txt_howDontKnow="Please tell us what you were trying to do:";
var txt_yes="Yes";
var txt_no="No";
var txt_dontKnow="I don't know";
var txt_back="Back";
var txt_submit="Submit";
var txt_thanks_p1="Thank you for submitting your feedback to us. Siemens will not contact you directly, but we do use your feedback to improve the information we provide.";
var txt_thanks_p2="Please note, to report any software problems you should contact the Global Technical Access Center (GTAC) following the normal process at your company.";
