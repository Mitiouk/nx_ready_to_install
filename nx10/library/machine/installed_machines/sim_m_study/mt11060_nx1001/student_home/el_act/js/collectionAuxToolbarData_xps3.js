/*
This software and related documentation are proprietary to Siemens Product Lifecycle Management Software Inc.

(c) 2009 Siemens Product Lifecycle Management Software Inc. All Rights Reserved. 

All trademarks belong to their respective holders.
*/
 logo=null;
 collectionAuxToolbar=new Toolbar("collectionAuxToolbar","#f6f6f6","false","top.aux_win.aux_nav",logo,"","");
 collectionAuxToolbar.addItem(expandAll=new Icon("expandAll","Expand All","top.toc.expandAll()","icons/expand_all.gif","","icons/expand_all_ovr.gif","","right","",""));
 collectionAuxToolbar.addItem(collapseAll=new Icon("collapseAll","Collapse All","top.toc.collapseAll()","icons/collapse_all.gif","","icons/collapse_all_ovr.gif","","right","",""));
