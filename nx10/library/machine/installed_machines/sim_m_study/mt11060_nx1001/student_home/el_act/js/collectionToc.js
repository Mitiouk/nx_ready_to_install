/*
This software and related documentation are proprietary to Siemens Product Lifecycle Management Software Inc.

(c) 2009 Siemens Product Lifecycle Management Software Inc. All Rights Reserved.

All trademarks belong to their respective holders.
*/
d9e2=new Toc("Activities and Part Files","#f6f6f6","false");

d9e4=new Topic("Post Building Techniques","topicSet","d9e4","PBT_elect_act/pbt_mt11060_act/index.html","","");
d9e2.addChild(d9e4);

d9e8=new Topic("Student Home","url","d9e8","../","","");
d9e2.addChild(d9e8);

d9e10=new Topic("Read Me","url","d9e10","../../readme_dir/readme.html","","");
d9e2.addChild(d9e10);

toc=d9e2;
