/*
This software and related documentation are proprietary to Siemens Product Lifecycle Management Software Inc.

(c) 2009 Siemens Product Lifecycle Management Software Inc. All Rights Reserved. 

All trademarks belong to their respective holders.
*/
var version="XPS30";
var project="nx7";
var launchFile="index.html";
var topicSetArray=new Array();

var topicSetBookmarkIDArray=new Array("int_elect_act","int_elect_act");
var topicSetBookmarkTitleArray=new Array("Intermediate NX Design and Assemblies","Intermediate NX Design and Assemblies");
var topicSetBookmarkStartFileArray=new Array("index.html","index.html");