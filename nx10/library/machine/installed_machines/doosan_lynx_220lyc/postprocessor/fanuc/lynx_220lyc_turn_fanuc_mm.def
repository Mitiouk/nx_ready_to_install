MACHINE  lathe

INCLUDE {
         $UGII_CAM_USER_DEF_EVENT_DIR/ude.cdl
        }

FORMATTING
{
  WORD_SEPARATOR " "
  END_OF_LINE ""
  SEQUENCE sequence_number 10  10 1 9999

################ FORMAT DECLARATIONS #################
  FORMAT AbsCoord "&__2.3_"
  FORMAT Coordinate "&__5.3_"
  FORMAT Digit_2 "&_02_00"
  FORMAT Digit_4 "&__4_00"
  FORMAT Digit_5 "&__5_00"
  FORMAT Dwell_INVERSE "&__3_00"
  FORMAT Dwell_REVOLUTIONS "&__3_00"
  FORMAT Dwell_SECONDS "&__5.3_"
  FORMAT EventNum "&+03_00"
  FORMAT Feed "&__7.2_"
  FORMAT Feed_FRN "&__5.3_"
  FORMAT Feed_INV "&__5.3_"
  FORMAT Feed_IPM "&__4.1_"
  FORMAT Feed_IPR "&__1.4_"
  FORMAT Feed_MMPM "&__5.0_"
  FORMAT Feed_MMPR "&_01.3_"
  FORMAT Hcode "&_02_00"
  FORMAT Q_cycle_step "&__5_30"
  FORMAT Rev "&__4_00"
  FORMAT String "%s"
  FORMAT Tcode "&_04_00"
  FORMAT Zero_int "&_01_0_"
  FORMAT Zero_real "&_01.10"

################ ADDRESS DECLARATIONS ################
  ADDRESS G_cutcom
  {
      FORMAT      Digit_2
      FORCE       off
      MAX         999999999.9999 Truncate
      MIN         -999999999.9999 Truncate
      LEADER      "G"
  }

  ADDRESS G_feed
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_plane
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_drill_cycles
  {
      FORMAT      String
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_adjust
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_spin
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_return
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_motion
  {
      FORMAT      Digit_2
      FORCE       off
      MAX         9999 Truncate
      MIN         -9999 Truncate
      LEADER      "G"
  }

  ADDRESS G_mode
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G_dwell
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "G"
  }

  ADDRESS G
  {
      FORMAT      Digit_2
      FORCE       off
      MAX         99 Truncate
      MIN         0 Truncate
      LEADER      "G"
  }

  ADDRESS X
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      [$mom_sys_leader(X)]
      ZERO_FORMAT Zero_real
  }

  ADDRESS Y
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      [$mom_sys_leader(Y)]
      ZERO_FORMAT Zero_real
  }

  ADDRESS Z
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      [$mom_sys_leader(Z)]
      ZERO_FORMAT Zero_real
  }

  ADDRESS I
  {
      FORMAT      Coordinate
      FORCE       always
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "I"
      ZERO_FORMAT Zero_real
  }

  ADDRESS J
  {
      FORMAT      Coordinate
      FORCE       always
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "J"
      ZERO_FORMAT Zero_real
  }

  ADDRESS K
  {
      FORMAT      Coordinate
      FORCE       always
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "K"
      ZERO_FORMAT Zero_real
  }

  ADDRESS R
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "R"
      ZERO_FORMAT Zero_real
  }

  ADDRESS cycle_step
  {
      FORMAT      Q_cycle_step
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "Q"
      ZERO_FORMAT Zero_int
  }

  ADDRESS cycle_dwell
  {
      FORMAT      Dwell_SECONDS
      FORCE       off
      MAX         99999.999 Truncate
      MIN         0.001 Truncate
      LEADER      "P"
      ZERO_FORMAT Zero_real
  }

  ADDRESS F
  {
      FORMAT      Feed
      FORCE       off
      MAX         9999.9 Warning
      MIN         .0001 Warning
      LEADER      "F"
      ZERO_FORMAT Zero_real
  }

  ADDRESS S
  {
      FORMAT      Digit_5
      FORCE       off
      MAX         99999 Truncate
      MIN         0 Truncate
      LEADER      "S"
      ZERO_FORMAT Zero_int
  }

  ADDRESS T
  {
      FORMAT      Tcode
      FORCE       off
      MAX         9999 Truncate
      MIN         0 Truncate
      LEADER      "T"
  }

  ADDRESS D
  {
      FORMAT      Digit_2
      FORCE       off
      MAX         99 Truncate
      MIN         0 Truncate
      LEADER      "D"
  }

  ADDRESS H
  {
      FORMAT      AbsCoord
      FORCE       off
      MAX         9999 Truncate
      MIN         -9999 Truncate
      LEADER      "H"
      ZERO_FORMAT Zero_real
  }

  ADDRESS dwell
  {
      FORMAT      Dwell_SECONDS
      FORCE       off
      MAX         99999.999 Truncate
      MIN         0.001 Truncate
      LEADER      "X"
      ZERO_FORMAT Zero_real
  }

  ADDRESS K_cycle
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "K"
      ZERO_FORMAT Zero_real
  }

  ADDRESS cycle_step1
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "I"
      ZERO_FORMAT Zero_real
  }

  ADDRESS Q_cutcom
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         9999.9999 Truncate
      MIN         -9999.9999 Truncate
      LEADER      "Q"
      ZERO_FORMAT Zero_real
  }

  ADDRESS E
  {
      FORMAT      Coordinate
      FORCE       off
      MAX         99999.999 Truncate
      MIN         -99999.999 Truncate
      LEADER      "E"
      ZERO_FORMAT Zero_real
  }

  ADDRESS M_spindle
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "M"
  }

  ADDRESS P_cutcom
  {
      FORMAT      String
      FORCE       always
      MAX         99999.999 Truncate
      MIN         0.001 Truncate
      LEADER      "P"
      ZERO_FORMAT Zero_real
  }

  ADDRESS M_range
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "M"
  }

  ADDRESS M
  {
      FORMAT      Digit_2
      FORCE       off
      LEADER      "M"
  }

  ADDRESS G71_turn_cycle_depth
  {
      FORMAT      Coordinate
      FORCE       always
      LEADER      "U"
      ZERO_FORMAT Zero_real
  }

  ADDRESS G72_turn_cycle_depth
  {
      FORMAT      Coordinate
      FORCE       always
      LEADER      "W"
      ZERO_FORMAT Zero_real
  }

  ADDRESS turn_cycle_retract
  {
      FORMAT      Coordinate
      FORCE       always
      LEADER      "R"
      ZERO_FORMAT Zero_real
  }

  ADDRESS turn_cycle_seqno_begin
  {
      FORMAT      Digit_4
      FORCE       always
      LEADER      "P"
      ZERO_FORMAT Zero_int
  }

  ADDRESS turn_cycle_seqno_end
  {
      FORMAT      Digit_4
      FORCE       always
      LEADER      "Q"
      ZERO_FORMAT Zero_int
  }

  ADDRESS turn_cycle_stock_x
  {
      FORMAT      Coordinate
      FORCE       always
      LEADER      "U"
      ZERO_FORMAT Zero_real
  }

  ADDRESS turn_cycle_stock_z
  {
      FORMAT      Coordinate
      FORCE       always
      LEADER      "W"
      ZERO_FORMAT Zero_real
  }

  ADDRESS M_coolant
  {
      FORMAT      Digit_2
      FORCE       off
      MAX         99 Truncate
      MIN         0 Truncate
      LEADER      "M"
  }

  ADDRESS turn_cycle_feed
  {
      FORMAT      Coordinate
      FORCE       always
      LEADER      "F"
      ZERO_FORMAT Zero_real
  }

  ADDRESS turn_cycle_speed
  {
      FORMAT      Digit_5
      FORCE       always
      LEADER      "S"
      ZERO_FORMAT Zero_int
  }

  ADDRESS turn_cycle_msg
  {
      FORMAT      String
      FORCE       off
      LEADER      "("
      TRAILER     ")"
  }

  ADDRESS N
  {
      FORMAT      Digit_4
      FORCE       off
      MAX         9999 Truncate
      LEADER      [$mom_sys_leader(N)]
      ZERO_FORMAT Zero_int
  }

  ADDRESS Text
  {
      FORMAT      String
      FORCE       always
      LEADER      ""
  }


################ ADDRESS DECLARATIONS ################
  ADDRESS LF_AAXIS
  {
      FORMAT      AbsCoord
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_real
  }

  ADDRESS LF_ENUM
  {
      FORMAT      Digit_5
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_int
  }

  ADDRESS LF_BAXIS
  {
      FORMAT      AbsCoord
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_real
  }

  ADDRESS LF_XABS
  {
      FORMAT      AbsCoord
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_real
  }

  ADDRESS LF_FEED
  {
      FORMAT      Feed
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_real
  }

  ADDRESS LF_YABS
  {
      FORMAT      AbsCoord
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_real
  }

  ADDRESS LF_SPEED
  {
      FORMAT      Rev
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_int
  }

  ADDRESS LF_ZABS
  {
      FORMAT      AbsCoord
      FORCE       always
      LEADER      ""
      ZERO_FORMAT Zero_real
  }


############ BLOCK TEMPLATE DECLARATIONS #############
  BLOCK_TEMPLATE absolute_mode
  {
       G_feed[$mom_sys_feed_rate_mode_code($feed_mode)]\opt
       G_mode[$mom_sys_output_code(ABSOLUTE)]
       G[$mom_sys_unit_code($mom_output_unit)]
  }

  BLOCK_TEMPLATE approach_move
  {
       Text[==>> $mom_coolant_status)]
  }

  BLOCK_TEMPLATE auxfun
  {
       M[$mom_auxfun]
  }

  BLOCK_TEMPLATE bore
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE circular_move
  {
       G_motion[$mom_sys_circle_code($mom_arc_direction)]\opt
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
       R[$mom_arc_radius]
       F[$feed]
  }

  BLOCK_TEMPLATE circular_move_rough_turn_cycle
  {
       G_motion[$mom_sys_circle_code($mom_arc_direction)]\opt
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
       R[$mom_arc_radius]\opt
  }

  BLOCK_TEMPLATE coolant_off
  {
       M_coolant[$mom_sys_coolant_code(OFF)]
  }

  BLOCK_TEMPLATE coolant_on
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE cutcom_off
  {
       G_cutcom[$mom_sys_cutcom_code(OFF)]\opt
  }

  BLOCK_TEMPLATE cutcom_on
  {
       G_cutcom[$mom_sys_cutcom_code($mom_cutcom_status)]\opt
  }

  BLOCK_TEMPLATE cutting_plane
  {
       G_plane[$mom_sys_cutcom_plane_code(ZX)]
  }

  BLOCK_TEMPLATE cycle_bore
  {
       G_motion[$mom_sys_cycle_bore_code]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_retract_to_pos(2)-$mom_cycle_rapid_to_pos(2)]
       F[$feed]
  }

  BLOCK_TEMPLATE cycle_drill
  {
       G_motion[$mom_sys_cycle_drill_deep_code]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_retract_to_pos(2)-$mom_cycle_rapid_to_pos(2)]
       F[$feed]
  }

  BLOCK_TEMPLATE cycle_drill_break_chip
  {
       G_drill_cycles[83]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_retract_to_pos(2)-$mom_cycle_rapid_to_pos(2)]
       cycle_step[$mom_cycle_step1]
       F[$feed]
  }

  BLOCK_TEMPLATE cycle_drill_deep
  {
       G_motion[$mom_sys_cycle_drill_deep_code]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_retract_to_pos(2)-$mom_cycle_rapid_to_pos(2)]
       cycle_step[$mom_cycle_step1]\opt
       cycle_dwell[$mom_cycle_delay]\opt
       F[$feed]
  }

  BLOCK_TEMPLATE cycle_drill_dwell
  {
       G_motion[$mom_sys_cycle_drill_deep_code]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_retract_to_pos(2)-$mom_cycle_rapid_to_pos(2)]
       cycle_dwell[$mom_cycle_delay]\opt
       F[$feed]
  }

  BLOCK_TEMPLATE cycle_off
  {
       G_motion[$mom_sys_cycle_off]
  }

  BLOCK_TEMPLATE cycle_tap
  {
       G_motion[$mom_sys_cycle_tap_code]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_retract_to_pos(2)-$mom_cycle_rapid_to_pos(2)]
       F[$feed]
  }

  BLOCK_TEMPLATE delay
  {
       G_dwell[$mom_sys_delay_code($mom_delay_mode)]
       dwell[$delay_time]
  }

  BLOCK_TEMPLATE drill
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE drill_break_chip
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE drill_deep
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE drill_dwell
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE end_of_path
  {
       M_spindle[$mom_sys_spindle_direction_code(OFF)]
       M_coolant[$mom_sys_coolant_code(OFF)]
  }

  BLOCK_TEMPLATE end_of_program
  {
       M[$mom_sys_end_of_program_code]
  }

  BLOCK_TEMPLATE feed_mode
  {
       G_feed[$mom_sys_feed_rate_mode_code($feed_mode)]\opt
  }

  BLOCK_TEMPLATE feed_mode_MMPM
  {
       G_feed[$mom_sys_feed_rate_mode_code(MMPM)]\opt
  }

  BLOCK_TEMPLATE feed_mode_MMPR
  {
       G_feed[$mom_sys_feed_rate_mode_code(MMPR)]\opt
  }

  BLOCK_TEMPLATE fixture_offset
  {
       G[$mom_fixture_offset_value + 53]
  }

  BLOCK_TEMPLATE inch_metric_mode
  {
       G[$mom_sys_unit_code($mom_output_unit)]
  }

  BLOCK_TEMPLATE incremental_mode
  {
       G_mode[$mom_sys_output_code(INCREMENTAL)]
  }

  BLOCK_TEMPLATE lathe_roughing
  {
       G_motion[$dpp_turn_cycle_g_code]
       turn_cycle_seqno_begin[$dpp_turn_cycle_seqno_begin]
       turn_cycle_seqno_end[$dpp_turn_cycle_seqno_end]
       turn_cycle_stock_x[$dpp_turn_cycle_stock_x]
       turn_cycle_stock_z[$dpp_turn_cycle_stock_z]
       turn_cycle_feed[$dpp_turn_cycle_cut_feed]
  }

  BLOCK_TEMPLATE lathe_roughing_G71
  {
       G_motion[$dpp_turn_cycle_g_code]
       G71_turn_cycle_depth[$mom_turn_cycle_cut_depth]
       turn_cycle_retract[$dpp_turn_cycle_retract]
       turn_cycle_msg[$dpp_turn_cycle_msg]
  }

  BLOCK_TEMPLATE lathe_roughing_G72
  {
       G_motion[$dpp_turn_cycle_g_code]
       G72_turn_cycle_depth[$mom_turn_cycle_cut_depth]
       turn_cycle_retract[$dpp_turn_cycle_retract]
       turn_cycle_msg[$dpp_turn_cycle_msg]
  }

  BLOCK_TEMPLATE lathe_roughing_coolant
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE linear_move
  {
       G_cutcom[$mom_sys_cutcom_code($mom_cutcom_status)]\opt
       G_motion[$mom_sys_linear_code]
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
       F[$feed]
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE linear_move_rough_turn_cycle
  {
       G_motion[$mom_sys_linear_code]
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
  }

  BLOCK_TEMPLATE opstop
  {
       M[$mom_sys_optional_stop_code]
  }

  BLOCK_TEMPLATE output_unit
  {
       G[$mom_sys_unit_code($mom_output_unit)]
  }

  BLOCK_TEMPLATE prefun
  {
       G[$mom_prefun]
  }

  BLOCK_TEMPLATE rapid_move
  {
       G_motion[$mom_sys_rapid_code]
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
  }

  BLOCK_TEMPLATE return_move
  {
       G_motion[$mom_sys_rapid_code]
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
  }

  BLOCK_TEMPLATE rewind_at_start
  {
       M[$mom_sys_rewind_code]
  }

  BLOCK_TEMPLATE rewind_stop_code
  {
       Text[%]
  }

  BLOCK_TEMPLATE sequence_number
  {
       N[$mom_seqnum]
  }

  BLOCK_TEMPLATE spindle_css
  {
       G_spin[$mom_sys_spindle_mode_code(SFM)]
       S[$mom_spindle_speed]
       M_spindle[$mom_sys_spindle_direction_code(CLW)]\opt
       P_cutcom[11]
  }

  BLOCK_TEMPLATE spindle_max_rpm
  {
       G[$mom_sys_spindle_max_rpm_code]
       S[$mom_spindle_maximum_rpm]
  }

  BLOCK_TEMPLATE spindle_off
  {
       M_spindle[$mom_sys_spindle_direction_code(OFF)]
       P_cutcom[11]
  }

  BLOCK_TEMPLATE spindle_range
  {
       M_range[$mom_sys_spindle_range_code($mom_spindle_range)]\opt
  }

  BLOCK_TEMPLATE spindle_rpm
  {
       G_spin[$mom_sys_spindle_mode_code(RPM)]
       S[$mom_spindle_speed]
       M_spindle[$mom_sys_spindle_direction_code(CLW)]\opt
       P_cutcom[11]
  }

  BLOCK_TEMPLATE spindle_rpm_drill
  {
       G_spin[$mom_sys_spindle_mode_code(RPM)]
       S[$mom_spindle_speed]
       M_spindle[$mom_sys_spindle_direction_code(CLW)]
       P_cutcom[11]
  }

  BLOCK_TEMPLATE spindle_rpm_preset
  {
       G_spin[$mom_sys_spindle_mode_code(RPM)]
       S[$mom_spindle_rpm]
       M_spindle[$mom_sys_spindle_direction_code($mom_spindle_direction)]\opt
  }

  BLOCK_TEMPLATE start_of_program
  {
       G_feed[$mom_sys_feed_rate_mode_code(MMPM)]
       G[$mom_sys_unit_code(MM)]
  }

  BLOCK_TEMPLATE stop
  {
       M[$mom_sys_program_stop_code]
  }

  BLOCK_TEMPLATE tap
  {
       M_coolant[$mom_sys_coolant_code(ON)]
  }

  BLOCK_TEMPLATE thread_move
  {
       G_motion[$mom_sys_lathe_thread_advance_type($mom_lathe_thread_advance_type)]
       X[$mom_pos(0)]
       Z[$mom_pos(2)]
       I[$mom_lathe_thread_lead_i]
       K[$mom_lathe_thread_lead_k]
       F[$mom_feed_rate]
  }

  BLOCK_TEMPLATE turn_cycle_end_tag
  {
       Text[(TURN CYCLE END)]
  }

  BLOCK_TEMPLATE turn_cycle_finishing
  {
       G_cutcom[$mom_sys_cutcom_code($mom_cutcom_status)]\opt
       G_motion[$dpp_turn_cycle_g_code]
       turn_cycle_seqno_begin[$dpp_turn_cycle_seqno_begin]
       turn_cycle_seqno_end[$dpp_turn_cycle_seqno_end]
       turn_cycle_feed[$dpp_finish_feed]
  }

  BLOCK_TEMPLATE turn_cycle_start_tag
  {
       Text[(TURN CYCLE START)]
  }

  BLOCK_TEMPLATE post_cycle_set
  {
       G_motion[$mom_sys_cycle_reps_code]
       X[$mom_cycle_feed_to_pos(0)]
       Z[$mom_cycle_feed_to_pos(2)]
       R[$mom_cycle_rapid_to_pos($mom_cycle_spindle_axis)]
       F[$feed]
  }

  BLOCK_TEMPLATE comment_data
  {
       LF_XABS[$mom_pos(0)]
       LF_YABS[$mom_pos(1)]
       LF_ZABS[$mom_pos(2)]
       LF_BAXIS[$mom_pos(4)]
       LF_FEED[$mom_feed_rate]
       LF_SPEED[$mom_spindle_speed]
  }

}
