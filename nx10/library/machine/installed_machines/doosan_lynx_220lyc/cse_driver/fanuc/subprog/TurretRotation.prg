G0 G90 G49

G53 X480.0 
G53 Y0.0 Z628.5 C0

##LANGUAGE AC
  INT nToolID;
  nToolID = getVariable("#4120");
  
  // find the toolpocket related to the given Tool given tool name!
  // extrat the first two digits to get teh pocket number of the choosen tool
  // therefore divide by 100: Assumption T number is always 4 digits
  
  STRING strCarrierTool;

  move (AXIS, "TURRET", (13-nToolID/100)*30, 1.0);
   
  strCarrierTool=getCarrierTool ("TURRET", ITOS(nToolID/100));
  
  // response is there is no tool it is a device
  // now you neet to query the device to get the tool.
  // e.g. getDeviceTool("Devicename","pocket_number of device")
  
  setNextTool (strCarrierTool, "TURRET");
  activateNextTool ("TURRET");

##LANGUAGE NATIVE

G43 H1

M99

