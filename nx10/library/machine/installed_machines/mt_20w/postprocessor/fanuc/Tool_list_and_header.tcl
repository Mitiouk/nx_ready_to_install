proc USER_out_header {} {
# =====================================================
	global mom_part_name mom_toolpath_time mom_parent_group_name
	global mom_date mom_logname mom_group_name mom_path_name mom_toolpath_time
	global mom_oper_program local_mom_group_name mom_toolpath_cutting_time

	if { ![info exists mom_group_name] } { set mom_group_name $mom_path_name } 
	if { [string compare $mom_oper_program $mom_group_name] == 0 } {
		set local_mom_group_name $mom_group_name
	} else {
		set local_mom_group_name [format "%s_%s" $mom_oper_program $mom_path_name]
	}
	MOM_output_literal "(START OF PROGRAM: $local_mom_group_name)"
	set out_part_info 1
	if { $out_part_info == 1 } {
		MOM_output_literal "(PART NAME: [string toupper $mom_part_name])"
		MOM_output_literal "(MACHINE: USER_MACHINE)"
	
		set date_time [clock scan today]
		set date_time [clock format $date_time -format "%x %H:%M"]
		MOM_output_literal "(DATE TIME: $date_time)"
		MOM_output_literal "(PROGRAMMER: [string toupper $mom_logname])"
		MOM_output_literal "(TOTAL MACHINE TIME: [format "%.3f" $mom_toolpath_time] MIN)"
		MOM_output_literal "(CUTTING MACHINE TIME: [format "%.3f" $mom_toolpath_cutting_time] MIN)"
		USER_out_tool_list
	}
}

# =====================================================
proc USER_out_tool_list {} {
# =====================================================
	global mom_isv_tool_count mom_tool_use 
	global mom_isv_tool_name mom_isv_tool_description mom_isv_tool_number 
	global mom_isv_tool_diameter mom_isv_tool_flute_length mom_isv_tool_type
	global mom_isv_tool_corner1_radius mom_isv_tool_adjust_register mom_isv_tool_cutcom_register
	global mom_isv_tool_x_correction mom_isv_tool_type mom_isv_tool_point_angle
	
	MOM_output_literal "(========== TOOL LIST ==========)"	
	for { set i 0 } { $i < $mom_isv_tool_count } { incr i } {
		if { [string match "Milling" $mom_isv_tool_type($i)] } {
			set mom_isv_tool_description1($i) "MILLING TOOL"
			MOM_output_literal "(TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i) D$mom_isv_tool_cutcom_register($i))"
			MOM_output_literal "(D=$mom_isv_tool_diameter($i) L=$mom_isv_tool_x_correction($i,0) FL=$mom_isv_tool_flute_length($i) R=$mom_isv_tool_corner1_radius($i))"
		} elseif { [string match "Drilling" $mom_isv_tool_type($i)] } {
			set mom_isv_tool_description1($i) "DRILLING TOOL"
			MOM_output_literal "(TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i))"
			MOM_output_literal "(D=$mom_isv_tool_diameter($i) L=$mom_isv_tool_x_correction($i,0) FL=$mom_isv_tool_flute_length($i) PA=$mom_isv_tool_point_angle($i))"
		} elseif { [string match "Solid" $mom_isv_tool_type($i)] } {
			set mom_isv_tool_description1($i) "PROBING TOOL"
			MOM_output_literal "(TOOL: $mom_isv_tool_name($i) \[$mom_isv_tool_description1($i)\] T$mom_isv_tool_number($i) H$mom_isv_tool_adjust_register($i))"
		}
	}

	MOM_output_literal "(===============================)"	
	
}