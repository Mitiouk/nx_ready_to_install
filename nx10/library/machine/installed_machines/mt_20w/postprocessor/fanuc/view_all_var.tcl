#=============================================================
proc MOOG_Show_Globals { } {
#=============================================================
# Shows all globals -
# including arrays, and their values
# Just put call to this proc wherever you need it
# You probably want to add some code around the call 
# so you only output data one (or a few) times
#
#  Warning - if added as a custom commend, it takes a while for Postbuilder
#  to check this proc (i.e. Do not click on it to look at the code)
#
# KG Akerboom 15-Nov-2005
#
# Jake Hardwick 29-April-2015 
# Changed to output variables to a file in the directory program is being posted to
# Changed logic check on the array types 
#
    upvar #0 mom_output_file_directory op_dir mom_output_file_basename base_fn 
   #set glob_file ${op_dir}${base_fn}_glbs[string range [clock clicks] 6 e].txt 
    set glob_file "global.txt" 
	set glob_chan [open $glob_file w] ;set global_vars [lsort [info globals]]

    puts $glob_chan "-----------------Global Vars-------------------"
    foreach global_var $global_vars {
        # you can use this to skip a variable type 
        # if {[string match *name* $global_var]} {continue} else { }
        
        # or this to skip all BUT this variable type
        # if {[string match *name* $global_var]} { } else {continue} 
        
        # I put them into one big check.  this is stuff you normally don't care about 
        #if {[string match *mom_tool_use* $global_var]       || \
            [string match *mom_display*   $global_var]      || \
            [string match *platform*      $global_var]      || \
            [string match *mom_attr*      $global_var]      || \
            [string match *pocket*        $global_var]      || \
            [string match *mom_kin*       $global_var]      || \
            [string match *mom_isv*       $global_var]      || \
            [string match *mom_holder*    $global_var]      || \
            [string match *mom_tool_step* $global_var]      || \
            [string match *mom_gohome*    $global_var]      || \
            [string match *mom_from*      $global_var]      || \
            [string match *mcsn*attach*   $global_var]      || \
            [string match *mcs_info*      $global_var]}     {continue} 
        
        upvar #0 $global_var global_val
        if {[array size global_val] > 0} {
                foreach el [lsort [array names global_val]] {
                    puts $glob_chan "$global_var\($el) = $global_val($el)"
                    }
            } else {
                puts $glob_chan "$global_var = $global_val"
            }
        }   

    puts $glob_chan "------------------------------------------------"
    close $glob_chan 
}